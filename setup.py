from setuptools import setup, find_packages

setup(name='mavsim',
      version='1.1',
      url='',
      license='Copyright 2019 PARC, All Rights Reserved.',
      author='G. Michael Youngblood, Jacob Le, and Bob Krivacic',
      author_email='Michael.Youngblood@parc.com',
      description='Micro Air Vehicle Simulator. Library. A no dependency, simplified, and discretized version of ArduPilot.',
      packages=find_packages(exclude=['tests']),
      include_package_data=True,
      long_description=open('README.md').read(),
      zip_safe=False)

#package_data = {'': ['*.tgz', '*.json']},
#include_package_data = True,