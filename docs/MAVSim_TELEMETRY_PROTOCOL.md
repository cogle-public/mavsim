# MAVSim ArduPilot Light Telemetry Protocol
 _Directly from MavProxy except for the messages with the MS prefix, which come from MAVSim_

These are the messages that MAVSim typically receives and handles currently.

Named followed by a tuple formatted as a string sent over multi-cast UDP. 

Format:

SYSTEM_NAME {attribute_1: value_1, ...,  attribute_n: value_n}

Example:

SIMSTATE {roll : 3.13010082209e-06, pitch : 0.246998071671, yaw : -0.122172169387, xacc : 2.39562368393, yacc : -2.97388141917e-05, zacc : -9.50091171265, xgyro : 8.03058863852e-09, ygyro : 2.5897677336e-09, zgyro : 1.92594717952e-09, lat : -353632583, lng : 1491652374}

This dictionary format is easily converted to a regular dictionary by most YAML parsers. See the code in mavsim/memory_buffer.py for how we do this in Python.

### Index

_These are the messages produced by MAVProxy and flow through from ArduPilot following the [MAVLINK Common Message Set](http://mavlink.org/messages/common) for MAVLink Messages_

| MAVLink Common Messages | Description  |
|---|---|
| [HEARTBEAT](http://mavlink.org/messages/common#HEARTBEAT) | The heartbeat message shows that a system is present and responding. This message also indicated mode. |
| [MISSION_CURRENT](http://mavlink.org/messages/common#MISSION_CURRENT) | Message that announces the sequence number of the current active mission item.  |
| [GLOBAL_POSITION_INT](http://mavlink.org/messages/common#GLOBAL_POSITION_INT) | The filtered global position (e.g. fused GPS and accelerometers). This is the one most people use for the actual GPS location of the craft. |
| [ATTITUDE](http://mavlink.org/messages/common#ATTITUDE) | The attitude in the aeronautical frame (right-handed, Z-down, X-front, Y-right). |
| [PARAM_SET](http://mavlink.org/messages/common#PARAM_SET) | Set a parameter value TEMPORARILY to RAM. |
| [HOME_POSITION](http://mavlink.org/messages/common#HOME_POSITION) | The position the system will return to and land on. |
| [PARAM_VALUE](http://mavlink.org/messages/common#PARAM_VALUE) | Emit the value of a onboard parameter. |
| [MISSION_ACK](http://mavlink.org/messages/common#MISSION_ACK) | Ack message during waypoint handling. The type field states if this message is a positive ack (type=0) or if an error happened (type=non-zero). |
| [COMMAND_ACK](http://mavlink.org/messages/common#COMMAND_ACK) | Report status of a command. Includes feedback whether the command was executed. |
| [MISSION_REQUEST](http://mavlink.org/messages/common#MISSION_REQUEST) | Request the information of the mission item with the sequence number seq. |
| [MISSION_ITEM_REACHED](http://mavlink.org/messages/common#MISSION_ITEM_REACHED) | A certain mission item has been reached.  |
| [MISSION_COUNT](http://mavlink.org/messages/common#MISSION_COUNT) | This message is emitted as response to MISSION_REQUEST_LIST by the MAV and to initiate a write transaction. |
| [MISSION_ITEM_INT](http://mavlink.org/messages/common#MISSION_ITEM_INT) | Message encoding a mission item. This message is emitted to announce the presence of a mission item and to set a mission item on the system. |
| [MISSION_ITEM](http://mavlink.org/messages/common#MISSION_ITEM) | Message encoding a mission item. This message is emitted to announce the presence of a mission item and to set a mission item on the system. |


_These messages are unique to MAVSim and are due to the augmentation of MAVProxy_

| MAVSim Subsystem Messages | Description  |
|---|---|
| Payload Subsystem |
| [MS_PAYLOAD_DROP](#MS_PAYLOAD_DROP) | Reports when a payload is dropped |
| [MS_PAYLOAD_HIT](#MS_PAYLOAD_HIT) | Coming soon! |
| [MS_PAYLOAD_ITEM](#MS_PAYLOAD_ITEM) | Coming soon! |
| [MS_PAYLOAD_LOAD](#MS_PAYLOAD_LOAD) | Coming soon! |
| Ownship Subsystem |
| [MS_CRAFT_CRASH](#MS_CRAFT_CRASH) | Report when the craft crashes with a description of what happened |
| Sensing Subsystem |
| [MS_SENSED_OBJECT](#MS_SENSED_OBJECT) | Smart sensor object detection report. Send when something is seen. |
| AIRSPEED | |


_Additional information multicasts shouted into the void for all to hear. They are not recorded in the log database._

| MAVSim Informational Messages | Description  |
|---|---|
| MS_MISSION_START | Appears in the multicast stream when a MAVSim instance starts |
| MS_MISSION_END | Appears in the multicast stream when a MAVSim instance terminates |


_Additional information multicasts shouted into the void for all to hear. They are not recorded in the log database._

| MAVSim Game Informational Messages | Description  |
|---|---|
| YOUR_TURN | |
| CURRENT_GAME_SCORE | |
| GOAL | |
| SCENARIO | |
| INITIAL_STATE| |





----
###MAVSim Messages

<a name="MS_PAYLOAD_DROP"></a>
**MS_PAYLOAD_DROP**

_Reports when a payload is dropped_

Example:

`MS_PAYLOAD_DROP {item : %s, lat : %f, lon : %f, alt : %f, speed : %f}`


| Param | type  | name  | description  |
|  ---  |  ---  |  ---  |     ---      |
|   1   | string| item  | Name of the item dropped |
|   2   | float | lat   | Latitude when dropped |
|   3   | float | lon   | Longitude when dropped |
|   4   | float | alt   | Altitude when dropped |
|   5   | float | speed | Speed of delivery craft when dropped |

<a name="MS_PAYLOAD_HIT"></a>
**MS_PAYLOAD_HIT**

_Coming soon!_

Example:

`MS_PAYLOAD_HIT {item : %s, lat : %f, lon : %f, alt : %f, status : %s}`

| Param | type  | name  | description  |
|  ---  |  ---  |  ---  |     ---      |
|   1   | string| item  | Name of the item dropped |
|   2   | float | lat   | Latitude when hit |
|   3   | float | lon   | Longitude when hit |
|   4   | float | alt   | Altitude when hit |
|   5   | string | status | Description of what happened at the hit |

<a name="MS_PAYLOAD_ITEM"></a>
**MS_PAYLOAD_ITEM**

_Coming soon! Response from a payload query_

Example:

`MS_PAYLOAD_ITEM {item : %s, slot: %d}`

| Param | type  | name  | description  |
|  ---  |  ---  |  ---  |     ---      |
|   1   | string | item | Name of the item |
|   2   | int | slot | Slot item loaded in |

<a name="MS_PAYLOAD_LOAD"></a>
**MS_PAYLOAD_LOAD**

_Coming soon! Response from a payload loading command_

Example:

`MS_PAYLOAD_ITEM {item : %s, slot: %d, status: %s}`

| Param | type  | name  | description  |
|  ---  |  ---  |  ---  |     ---      |
|   1   | string | item | Name of the item |
|   2   | int | slot | Slot item loaded in |
|   3   | string | status | Status of the load operation |

<a name="MS_CRAFT_CRASH"></a>
**MS_CRAFT_CRASH**

_Report when the craft crashes with a description of what happened_

Example:

`MS_CRAFT_CRASH {cause : %s, lat : %f, lon : %f, alt : %f, speed : %f}`

| Param | type  | name  | description  |
|  ---  |  ---  |  ---  |     ---      |
|   1   | string| cause | What caused the craft to crash |
|   2   | float | lat   | Latitude when crashed |
|   3   | float | lon   | Longitude when crashed |
|   4   | float | alt   | Altitude when crashed |
|   5   | float | speed | Speed of craft when crashed |

<a name="MS_SENSED_OBJECT"></a>
**MS_SENSED_OBJECT**

_Smart sensor object detection report. Send when something is seen._

Example:

`MS_SENSED_OBJECT {name : %s, lat : %f, lon : %f, alt : %f, radius : %f}`

| Param | type  | name  | description  |
|  ---  |  ---  |  ---  |     ---      |
|   1   | string| name  | Name of the object using best recognition |
|   2   | float | lat   | Latitude of the object |
|   3   | float | lon   | Longitude of the object |
|   4   | float | alt   | Altitude of the object |
|   5   | float | radius | Radius of a bounding sphere around the object (roughly how big the object is) |

-----

**Telemetry Stream**

1. HEARTBEAT - The heartbeat message shows that a system is present and responding. This message also indicates mode as well as ARM status.
2. GLOBAL_POSITION_INT - The filtered global position (e.g. fused GPS and accelerometers). This is the one most people use for the actual GPS location of the craft.
3. COMMAND_ACK - Report status of a command. Includes feedback whether the command was executed.
4. HOME_POSITION - The position the system will return to and land on.
5. ATTITUDE - The attitude in the aeronautical frame (right-handed, Z-down, X-front, Y-right).
6. MISSION_CURRENT - Message that announces the sequence number of the current active mission item.
7. MISSION_ACK - Ack message during waypoint handling. The type field states if this message is a positive ack (type=0) or if an error happened (type=non-zero).
8. MISSION_REQUEST - Request the information of the mission item with the sequence number seq.
9. MISSION_ITEM_REACHED - A certain mission item has been reached.
10. MISSION_COUNT - This message is emitted as response to MISSION_REQUEST_LIST by the MAV and to initiate a write transaction.
11. MISSION_ITEM_INT - Message encoding a mission item. This message is emitted to announce the presence of a mission item and to set a mission item on the system.
12. MISSION_ITEM - Message encoding a mission item. This message is emitted to announce the presence of a mission item and to set a mission item on the system.
13. PARAM_SET - Set a parameter value TEMPORARILY to RAM.
14. PARAM_VALUE - Emit the value of a onboard parameter.
15. MS_PAYLOAD_DROP - Reports when a payload is dropped
16. MS_PAYLOAD_HIT - Reports the status of a drop
17. MS_PAYLOAD_ITEM - Reports status of a MS_LIST_PAYLOAD 
18. MS_PAYLOAD_LOAD - Reports status of a MS_LOAD_PAYLOAD 
19. MS_CRAFT_CRASH - Report when the craft crashes with a description of what happened
20. MS_SENSED_OBJECT - Smart sensor object detection report. Sent after each move.
21. MISSION_START - Appears in the multicast stream when a MAVSim instance starts
22. MISSION_END - Appears in the multicast stream when a MAVSim instance terminates
23. CURRENT_GAME_SCORE - Returns the current score of the craft after each move.
23. PIC_TAKEN - Description of the tile that the plane took a picture of (Plane's position at the time)
24. CAMERA_ROLL - Totals of all picture categories taken (Hikers, Deer, Bears and Scenery)
25. YOUR_TURN - Signals that the system is waiting on the craft for the next command(s)

_Telemetry starts with a COMMAND_ACK, updates for each position change made during the turn resolution by providing appropriate feedback, and then ends with a YOUR_TURN message. For **Aircraft Mission Setup Commands**, each command is COMMAND_ACKd and the appropriate MISSION_X messages send over telemetry._


