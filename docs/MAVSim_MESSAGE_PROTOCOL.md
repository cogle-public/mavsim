# MAVSim ArduPilot Light Command Message Protocol

Interface with MAVSim by sending commands as a tuple formatted as an octet string and send it over UDP to the server connection. Listen for vehicle activity and state updates on the multicast telemetry stream. Some commands return data, but any *ACK* returned should be considered just a receipt of the command by MAVSIM and not acceptance or completion of the command by ArduPilot, see telemetry for the COMMAND_ACK. Underlying most of these messages is the [MAVLINK Common Message Set](http://mavlink.org/messages/common). Message with the MS prefix are solely handled within MAVSim.

General Format:

('SUBSYSTEM', 'COMMAND', Param 1, 2, 3, 4, 5, 6, 7, 8)

_All strings should be enclosed in single quotes_

### Index


| SIM Command (0) | Description  |
|---|---|
| [OPEN](#SIM-OPEN) 01|Open a scenario (mission save) from the database. |
| [CLOSE](#SIM-CLOSE) 02| Close current vehicle simulation instance. |
| [SAVE](#SIM-SAVE) 03| Save scenario parameters to the database. |
| [NEW](#SIM-NEW) 04| Starts a simulation with all default parameters from the core repository scenarios. |
| [SESSION](#SIM-SESSION) 05| Use to differentiate between different sessions in the database logs. |
| [INSTANCE](#SIM-INSTANCE) 06| Use to differentiate between different instances in the database logs. |
| [PILOT](#SIM-PILOT) 06| Use to differentiate between different pilots in the database logs. |
| [QUIT](#SIM-QUIT) 07| Stops and deletes the current simulation and resets the craft. Any new simulation will have to completely load a new scenario. |
| [RESET](#SIM-RESET) 08| **NOT USED IN APL MAVSim.** Reset the current scenario to the state on last OPEN or NEW. |
| [PAUSE](#SIM-PAUSE) 09| **NOT USED IN APL MAVSim.**  Pause the simulation. |
| [RESUME](#SIM-RESUME) 10| **NOT USED IN APL MAVSim.** Used to start after a PAUSE. |
| [CHECKPOINT](#SIM-CHECKPOINT) 11| Returns a list of the current state variable attributes.|
| [LOAD](#SIM-LOAD) 12| Force the craft to a specific state within the currently loaded scenario. |
| [POSITION_HIKER](#SIM-POSITION_HIKER) 13| Move the hiker to the location specified by x and y. |
| [](#SIM-) 14| |
| [NIXEL_GENERATE] (SIM-NIXEL_GENERATE) 15 | Generate a Nixel Scenario using the provided seed. |


| FLIGHT Command (1)| Description  |
|---|---|
| [SET_MODE](#FLIGHT-SET_MODE) 01 | Set the flight mode |
| [GET_MODE](#FLIGHT-GET_MODE) 02 | Get the current flight mode |
| [ARM](#FLIGHT-ARM) 03 | Arm the craft for operations |
| [DISARM](#FLIGHT-DISARM) 04| Disarm the craft |
| [IS_ARMED](#FLIGHT-IS_ARMED) 05| Check to see if the craft is armed |
| [FLY_TO](#FLIGHT-FLY_TO) 06| Tell craft to go to a specific coordinate |
| [HEAD_TO](#FLIGHT-HEAD_TO) 07| Tell craft to go a specific direction a specific distance |
| [AUTO_TAKEOFF](#FLIGHT-AUTO_TAKEOFF) 08| Tell the craft to takeoff from the current position and ascend to a specific altitude and distance |
| [GET_ATTITUDE](#FLIGHT-GET_ATTITUDE) 09| Get current craft attitude |
| [GET_ALTITUDE](#FLIGHT-GET_ALTITUDE) 10| Get current craft altitude |
| [GET_SPEED](#FLIGHT-GET_SPEED) 11| Get current craft speed |
| [GET_HEADING](#FLIGHT-GET_HEADING) 12| Get the current craft heading |
| [GET_LOCATION](#FLIGHT-GET_LOCATION) 13| Get the current craft location |
| [SET_SPEED](#FLIGHT-SET_SPEED) 18| Set current craft speed in m/s |
| [AUTO_LAND_AT_AIRPORT](#FLIGHT-AUTO_LAND_AT_AIRPORT) 19| Stop current craft activity and fly a mission back to land at the specified airport |
| [AUTO_ABORT](#FLIGHT-AUTO_ABORT) 20| Makes the craft leave an automated sequence such at takeoff or landing.|
| [AUTO_TAXI](#FLIGHT-AUTO_TAXI) 21| Moves the craft from the landing spot to the takeoff spot. |
| _Payload Subsystem_ |
| [MS_LOAD_PAYLOAD](#FLIGHT-MS_LOAD_PAYLOAD) 22| Load the selected slot on the craft with specified payload |
| [MS_DROP_PAYLOAD](#FLIGHT-MS_DROP_PAYLOAD) 23| Drop payload from the selected payload slot |
| [MS_LIST_PAYLOAD](#FLIGHT-MS_LIST_PAYLOAD) 24| Returns a list of current payload on the craft |
| _Mission Support Subsystem_ | **NOT IMPLEMENTED** |
| [CLEAR_MISSION](#FLIGHT-CLEAR_MISSION) 14| **NOT IMPLEMENTED** Clears all current mission waypoints |
| [SET_MISSION_COUNT](#FLIGHT-SET_MISSION_COUNT) 15| **NOT IMPLEMENTED** This tells the system how many items for mission setup |
| [SET_MISSION_ITEM](#FLIGHT-SET_MISSION_ITEM) 16| **NOT IMPLEMENTED** Used to establish each step of a mission |
| [SET_MISSION_CURRENT](#FLIGHT-SET_MISSION_CURRENT) 17| **NOT IMPLEMENTED** Set the mission item on the mission list as the current action step |
| [MS_STORE_MISSION](#FLIGHT-MS_STORE_MISSION) 24| **NOT IMPLEMENTED** Create a mission to store waypoint details |
| [MS_ADD_MISSION_ITEM](#FLIGHT-MS_ADD_MISSION_ITEM) 25| **NOT IMPLEMENTED** Add mission step (item) details |
| [MS_MISSION_LIST](#FLIGHT-MS_MISSION_LIST) 26| **NOT IMPLEMENTED** List of all stored missions by name |
| [MS_LIST_MISSION](#FLIGHT-MS_LIST_MISSION) 27| **NOT IMPLEMENTED** List all items in a specific mission |
| [MS_GET_MISSION_ITEM](#FLIGHT-MS_GET_MISSION_ITEM) 28| **NOT IMPLEMENTED** Retrieve the details of a specific mission item |
| [MS_REPLACE_MISSION_ITEM](#FLIGHT-MS_REPLACE_MISSION_ITEM) 29| **NOT IMPLEMENTED** Replace the specifics of a mission item |
| [MS_MOVE_MISSION_ITEM](#FLIGHT-MS_MOVE_MISSION_ITEM) 30| **NOT IMPLEMENTED** Reorder a mission item in the mission sequence |
| [MS_DELETE_MISSION_ITEM](#FLIGHT-MS_DELETE_MISSION_ITEM) 31| **NOT IMPLEMENTED** Remove a mission item |
| [MS_REPLACE_CURRENT_MISSION](#FLIGHT-MS_REPLACE_CURRENT_MISSION) 32| **NOT IMPLEMENTED** Replace current mission with another and engage |
| [MS_LOAD_MISSION](#FLIGHT-MS_LOAD_MISSION) 33| **NOT IMPLEMENTED** Load the mission into the craft |
| _Fuel Support Subsystem_ |
| [MS_REFUEL](#FLIGHT-MS_REFUEL) 34| Fills the tank to FUEL_MAX |
| [MS_FUEL_STATUS](#FLIGHT-MS_FUEL_STATUS) 35| Provides fuel status |
| _Terrain Support Subsystem_ |
| [MS_QUERY_TERRAIN](#FLIGHT-MS_QUERY_TERRAIN) 36 | No cost command to query what is at a terrain location. |
| [MS_LIST_AIRPORTS](#FLIGHT-MS_LIST_AIRPORTS) 37 | No cost command to get a list of airports and their information. Provided as a list of tuples, where each tuple is an airport with the following information (name, orientation, Landing x position, landing y position, takeoff x position, takeoff y position) |
| _Reconnaissance Support Subsystem_ |
| [MS_TAKE_PIC](#FLIGHT-MS_TAKE_PIC) 38 | No cost command to take a "picture" that describes a space as a hiker, deer, bear or scenery |
| _Flight Platform Parameters_ | | 
| [MS_SET_PARAM](#FLIGHT-MS_SET_PARAM) 38 | No Cost commands. FLIGHT_SAFETY {ON, OFF} : Toggles safety checks for low-level controllers to stay within map boundaries. |
| [MS_QUERY_SLICE](#FLIGHT-MS_QUERY_SLICE) 39 | Return a string representation of an array of size (width x length), each index representing the tiletype of the tiles within the map slice |
| [MS_SET_AOI](#FLIGHT-MS_SET_AOI) 40 | . |


| TELEMETRY Command (2) | Description  |
|---|---|
| [SET_MULTICAST](#TELEMETRY-SET_MULTICAST) 01| Turns on or off multicast broadcasting of telemetry. Defaults to off. |
| [ADD_LISTENER](#TELEMETRY-ADD_LISTENER) 02| Registers a UDP endpoint to receive filtered telemetry commands.  |
| [REMOVE_LISTENER](#TELEMETRY-REMOVE_LISTENER) 03| Removes a UDP endpoint to stop receiving filtered telemetry commands. |


| LOG Command (3)| Description  |
|---|---|
| [CUSTOM_LOG_ENTRY](#LOG-CUSTOM_LOG_ENTRY) 01| Add custom notes to the database |
| [LOG_MISSION_START](#LOG-LOG_MISSION_START) 02| Note when a mission is considered to start in the logs |
| [LOG_MISSION_END](#LOG-LOG_MISSION_END) 03| Note the end time of a mission |


----
### Subsystem: SIM

<a name="SIM-OPEN"></a>
**Command: OPEN**

_Opens a scenario in the simulation with values loaded from the database for a recorded flight to a point. This allows resumption of a flight from the end point. If the user wants to start an original flight, use **NEW**._

**It is VERY IMPORTANT to note that the UUID for the flight is sent by the agent for tracking. This UUID must be universally unique by definition. If non-unique UUIDs are submitted then performance data retrieval and open/save operations will be affected.**

**It is VERY IMPORTANT to note that the EXTENSION UUID for the flight picking up from the save point is sent by the agent for tracking. This UUID must be universally unique by definition. If non-unique UUIDs are submitted then performance data retrieval and open/save operations will be affected.**

`('SIM', 'OPEN', {source uuid}, {extension uuid})`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | source uuid | UUID of the saved instance to load and continue flight operations from |
| 2 | string | extension uuid | The uuid to be used for the continuance of the flight |

<a name="SIM-CLOSE"></a>
**Command: CLOSE**

_Close the simulation and stop all processes related to the ArduPilot Simulation using UNIX commands_

`('SIM', 'CLOSE', {description})`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | optional string | description | Description of the flight instance just flown and closed |

<a name="SIM-SAVE"></a>
**Command: SAVE**

_Save current mission scenario state to the database and continue simulation afterward (essentially a checkpoint stored in the database). A save can only be performed from an OPEN or NEW simulation in progress._

**It is VERY IMPORTANT to note that the EXTENSION UUID for the flight picking up from the save point is sent by the agent for tracking. This UUID must be universally unique by definition. If non-unique UUIDs are submitted then performance data retrieval and open/save operations will be affected.**

`('SIM', 'SAVE', {description}, {extension uuid})`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | description | Description of the checkpoint just saved |
| 2 | string | extension uuid | The uuid to be used for the continuance of the flight |

<a name="SIM-NEW"></a>
**Command: NEW**

_Starts a simulation with all default parameters._

**It is VERY IMPORTANT to note that the UUID for the flight is sent by the agent for tracking. This UUID must be universally unique by definition. If non-unique UUIDs are submitted then performance data retrieval and open/save operations will be affected.**

`('SIM', 'NEW', {scenario}, {uuid})`

| Param | type | name | description |
|---|---|---|---|
| 1 | string | scenario | Specify the scenario base from the repository to use to create this new scenario instance. (NIXEL SPECIFIC: input DNA string to load a specific Nixel scenario) |
| 2 | string | uuid | UUID of this flight |


<a name="SIM-SESSION"></a>
**Command: SESSION**

_Changes the session name.  Use to differentiate between different sessions in the database logs. All database entries are tagged with the session name._

`('SIM', 'SESSION', {session_name})`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | name | Specify the new session name. |

<a name="SIM-INSTANCE"></a>
**Command: INSTANCE**

_Changes the instance name.  Use to differentiate between different instances in the database logs. All database entries are tagged with the instance name._

`('SIM', 'INSTANCE', {instance_name})`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | name | Specify the new instance. |

<a name="SIM-PILOT"></a>
**Command: PILOT**

_Changes the pilot name.  Use to differentiate between different instances in the database logs. All database entries are tagged with the pilot name._

`('SIM', 'PILOT', {pilot_name})`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | pilot_name | Specify the new pilot. |

<a name="SIM-QUIT"></a>
**Command: QUIT**

_Stops and deletes the current simulation and resets the craft. Any new simulation will have to completely load a new scenario._

`('SIM', 'QUIT')`

<a name="SIM-RESET"></a>
**Command: RESET**

__NOT USED IN THE ARDUPILOT LIGHT VARIANT OF MAVSim! Used in full ArduPilot version.__

_Reset the current scenario to the state on last OPEN or NEW._

`('SIM', 'RESET')`

<a name="SIM-PAUSE"></a>
**Command: PAUSE**

__NOT USED IN THE ARDUPILOT LIGHT VARIANT OF MAVSim! Used in full ArduPilot version. APL is turn-based so PAUSE is not necessary.__

_Pause the simulation._

`('SIM', 'PAUSE')`

<a name="SIM-RESUME"></a>
**Command: RESUME**

__NOT USED IN THE ARDUPILOT LIGHT VARIANT OF MAVSim! Used in full ArduPilot version. APL is turn-based so PAUSE is not necessary and so neither is RESUME.__

_Used to start after a PAUSE._

`('SIM', 'RESUME')`

<a name="SIM-CHECKPOINT"></a>
**Command: CHECKPOINT**

_Returns a list of the current state variable attributes._

`('SIM', 'CHECKPOINT')`

Returns a comma separated list of the following attributes:

| Attribute  | type  | name  | description  |
|---|---|---|---|
| 1 | int | x | Craft position x (Range: 0-Max World X) |
| 2 | int | y | Craft position y (Range: 0-Max World Y) |
| 3 | int | alt  | Craft altitude (Range: 0-3) |
| 4 | int | speed | Craft speed (Range: 0-6) |
| 5 | int | yaw | Craft yaw (Range: 1-8, where 1 is North) |
| 6 | int | fuel | Craft fuel level (Range: 0-999999) |
| 7 | bool | armed | Is the craft armed (on)? (Range: "True" or "False") |
| 8 | int (from enum) | craft mode | Craft mode (Range: 0-5 -- MANUAL = 0, GUIDED = 1, AUTO = 2, RTL = 3, LOITER = 4, UNKNOWN = 5)  |
| 9 | list of strings | payload | Craft payload loadout for slots 1, 2, 3, 4 |
| 10 | int (from enum) | game mode | Game mode (Range: 0-4 -- NONE = 0, LOST_HIKER = 1, COUNT_BEARS = 2, COUNT_DEER = 3, COUNT_ANIMALS = 4) |
| 11 | bool | hiker found | Has the craft already found the hiker (i.e., Already picked up in flight by smart sensor)? (Range: "True" or "False") |
| 12 | int | successful package drops | Number of pakage drops that have been already been successfully delivered intact to the hiker. |
| 13 | list of strings | dropped payload | A list of the payload that has been dropped by the craft. |
| 14 | list of tuple positions | craft flight plan | The list of points for the craft to navigate to sequentially. In SIM OPS Mode 0 this is a 3-tuple of craft (x, y, z) and in Modes 1 & 2 this is a 4-tuple of craft (x, y, z, h). "h" is craft yaw for those unfamiliar with this abbreviation. |

<a name="SIM-LOAD"></a>
**Command: LOAD**

_Force the craft to a specific state within the currently loaded scenario._

**WARNING: It is possible to set the craft into a wonky state. Be sure you know what you are doing when setting the craft state. With great power comes great responsibility!**

`('SIM', 'LOAD', x, y, alt, speed, yaw, fuel, armed, craft mode, payload, game mode, hiker found, successful package drops,dropped payload, craft flight plan)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | int | x | Craft position x (Range: 0-Max World X) |
| 2 | int | y | Craft position y (Range: 0-Max World Y) |
| 3 | int | alt  | Craft altitude (Range: 0-3) |
| 4 | int | speed | Craft speed (Range: 0-6) |
| 5 | int | yaw | Craft yaw (Range: 1-8, where 1 is North) |
| 6 | int | fuel | Craft fuel level (Range: 0-999999) |
| 7 | bool | armed | Is the craft armed (on)? (Range: "True" or "False") |
| 8 | int (from enum) | craft mode | Craft mode (Range: 0-5 -- MANUAL = 0, GUIDED = 1, AUTO = 2, RTL = 3, LOITER = 4, UNKNOWN = 5)  |
| 9 | list of strings | payload | Craft payload loadout for slots 1, 2, 3, 4 |
| 10 | int (from enum) | game mode | Game mode (Range: 0-4 -- NONE = 0, LOST_HIKER = 1, COUNT_BEARS = 2, COUNT_DEER = 3, COUNT_ANIMALS = 4) |
| 11 | bool | hiker found | Has the craft already found the hiker (i.e., Already picked up in flight by smart sensor)? (Range: "True" or "False") |
| 12 | int | successful package drops | Number of pakage drops that have been already been successfully delivered intact to the hiker. |
| 13 | list of strings | dropped payload | A list of the payload that has been dropped by the craft. |
| 14 | list of tuple positions | craft flight plan | The list of points for the craft to navigate to sequentially. In SIM OPS Mode 0 this is a 3-tuple of craft (x, y, z) and in Modes 1 & 2 this is a 4-tuple of craft (x, y, z, h). "h" is craft yaw for those unfamiliar with this abbreviation. |

Example:
`('SIM','LOAD', 35, 440, 3, 1, 1, 999477, 'True', 1, ['Food', 'Food', 'Food', 'Food'], 1, 'False', 0, '[]', '[]')`


<a name="SIM-POSITION_HIKER"></a>
**Command: POSITION_HIKER**

_Move the hiker to the location specified by x and y._

`('SIM', 'POSITION_HIKER', x, y)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | int | x | Hiker's new x position |
| 2 | int | y | Hiker's new y position |


<a name="SIM-NIXEL_GENERATE"></a>
`('SIM', 'NIXEL_GENERATE', )`  
**Command: NIXEL_GENERATE**

_Generate a Nixel Scenario._

| Param | type | name | description |  
|---|---|---|---|  
| 1 | string | DNA String | The string that represents the seeds that will be used to generate the Nixel World |  
| 2 | string | name | The name of the world that will be generated.  Default is 'nixel_world' |  
| 3 | list of strings | A list of generator codes that determine what layer generators will be used in generation.  Default is all: ['Terrain', 'Ocean', 'River', 'Biome', 'Tree', 'Airport', 'Building', 'DropPackageMission', 'FindBearsMission'] | 

----
### Subsystem: FLIGHT

_High level flight operation commands_

<a name="FLIGHT-SET_MODE"></a>
**Command: SET_MODE**

_Place the craft in a specific flight mode_

`('FLIGHT', 'SET_MODE', 'MODE')`

| Param  | type  | name  | description  |
|---|---|---|---|
|  1 | string | mode | From {RTL, TRAINING, LAND, AUTOTUNE, STABILIZE, AUTO, GUIDED, LOITER, MANUAL, FBWA, FBWB, CRUISE, CIRCLE, ACRO} |

<a name="FLIGHT-GET_MODE"></a>
**Command: GET_MODE**

_Get the current flight mode of the craft_

`('FLIGHT', 'GET_MODE')`

<a name="FLIGHT-ARM"></a>
**Command: ARM**

_Arm the craft for operations (i.e., turn throttle on)_

`('FLIGHT', 'ARM')`

<a name="FLIGHT-DISARM"></a>
**Command: DISARM**

_Disarm the craft for operations (i.e., turn throttle off)_

`('FLIGHT', 'DISARM')`

<a name="FLIGHT-IS_ARMED"></a>
**Command: IS_ARMED**

_Get the status of craft being armed_

`('FLIGHT', 'IS_ARMED')`

Returns a TRUE or FALSE

<a name="FLIGHT-FLY_TO"></a>
**Command: FLY_TO**

_Tell craft to go to a specific coordinate_

`('FLIGHT', 'FLY_TO', lat, lon, absolute altitude)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | float | lat | Latitude  |
| 2 | float | lon | Longitude  |
| 3 | float | absolute altitude | Altitude from sea level |

<a name="FLIGHT-HEAD_TO"></a>
**Command: HEAD_TO**

_Tell craft to go a specific direction a specific distance at a target altitude_

`('FLIGHT', 'HEAD_TO', heading, distance, absolute altitude)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | float  | heading  | In absolute degrees 0-360, where 0 is North and 180 is South. In sim_op_state=1, heading is an integer in the range [1,8] where North = 1 and North East = 2,... |
| 2 | float  | distance in meters  | How far to travel in the heading direction. In sim_op_state=1, distance 1 moves one square.  |
| 3 | float  | absolute altitude  | Altitude from sea level. In sim_op_state=1, altitude is an integer in the range [0,4] |

<a name="FLIGHT-AUTO_TAKEOFF"></a>
**Command: AUTO_TAKEOFF**

_Tell the craft to takeoff from the current airport and ascend to a specific altitude and distance away from the airport. When it arrives it will shift to LOTER mode around the target point awaiting for further instructions._

`('FLIGHT', 'AUTO_TAKEOFF', {altitude}, {distance})`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | float  | (OPTIONAL) relative altitude  | How high to try to reach to on target waypoint out of the airport. Default is 200m above current ground level. |
| 2 |  integer | (OPTIONAL) relative distance in meters  | How far to set the waypoint away from the airport. Default is 500m from current position. |

<a name="FLIGHT-GET_ATTITUDE"></a>
**Command: GET_ATTITUDE**

_Get current craft attitude_

`('FLIGHT','GET_ATTITUDE')`

_Returns a yaw, roll, pitch as floats (in that order)_

<a name="FLIGHT-GET_ALTITUDE"></a>
**Command: GET_ALTITUDE**

_Get current craft altitude_

`('FLIGHT','GET_ALTITUDE')`

_Returns the current craft altitude_

<a name="FLIGHT-GET_SPEED"></a>
**Command: GET_SPEED**

_Get current craft speed_

`('FLIGHT','GET_SPEED')`

_Returns speed in m/s (float)_

<a name="FLIGHT-GET_HEADING"></a>
**Command: GET_HEADING**

_Get the current craft heading_

`('FLIGHT','GET_HEADING')`

_Returns heading in degrees_

<a name="FLIGHT-GET_LOCATION"></a>
**Command: GET_LOCATION**

_Get the current craft location_

`('FLIGHT','GET_LOCATION')`

_Returns lat, lon, and fix type (see GPS_FIX_TYPE in [MAVLINK Common](http://mavlink.org/messages/common))_

<a name="FLIGHT-CLEAR_MISSION"></a>
**Command: CLEAR_MISSION**

_Clears all current mission waypoints_

`('FLIGHT','CLEAR_MISSION')`

<a name="FLIGHT-SET_MISSION_COUNT"></a>
**Command: SET_MISSION_COUNT**

_For mission setup, do this before setting mission items. This tells the system how many items to expect._

`('FLIGHT','SET_MISSION_COUNT', integer)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1  | integer | item count | The number of steps in the mission being sent |

<a name="FLIGHT-SET_MISSION_ITEM"></a>
**Command: SET_MISSION_ITEM**

_For mission setup, this is used to establish each step of a mission. These are typically sent one at a time after the mission count is sent._

`('FLIGHT','SET_MISSION_ITEM', sequence, current, frame, command, param_1, param_2, param_3, param_4, lat, lon, alt, autocontinue, mission_type)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | integer | sequence | Step number 0-99 |
| 2 | false:0, true:1 | current |   |
| 3 | integer | frame | The coordinate system of the MISSION. <br> FRAME = { MAV_FRAME_GLOBAL = 0, MAV_FRAME_LOCAL = 1, MAV_FRAME_MISSION = 2, MAV_FRAME_GLOBAL_RELATIVE_ALT = 3, MAV_FRAME_LOCAL_ENU = 4} <br> **Most cases will use either absolute (0) or relative (3)** |
| 4 | integer | command ID | See _MAV Commands_ Table below |
| 5 | float | parameter 1 | Depends on command, 0.0 otherwise |
| 6 | float | parameter 2 | Depends on command, 0.0 otherwise |
| 7 | float | parameter 3 | Depends on command, 0.0 otherwise |
| 8 | float | parameter 4 | Depends on command, 0.0 otherwise |
| 9 | float | lat | Latitude or _x_ position |
| 10 | float | lon | Longitude or _y_ position |
| 11 | float | alt | Altitude or _z_ position |
| 12 | false:0, true:1 | autocontinue | autocontinue to next wp? |
| 13 | integer | mission type | See _MAV Mission Types_ Table below. <br> **Most of the time this will be 0** |


MAV Commands:

| Name | ID | Description |
|------|----|-------------|
| MAV_CMD_NAV_WAYPOINT | 16 | Navigate to MISSION.|
| MAV_CMD_NAV_LOITER_UNLIM | 17 | Loiter around this MISSION an unlimited amount of time|
| MAV_CMD_NAV_LOITER_TURNS | 18 | Loiter around this MISSION for X turns|
| MAV_CMD_NAV_LOITER_TIME | 19 | Loiter around this MISSION for X seconds|
| MAV_CMD_NAV_RETURN_TO_LAUNCH | 20 | Return to launch location|
| MAV_CMD_NAV_LAND | 21 | Land at location|
| MAV_CMD_NAV_TAKEOFF | 22 | Takeoff from ground / hand|
| MAV_CMD_NAV_ROI | 80 | Sets the region of interest (ROI) for a sensor set or the vehicle itself.| This can then be used by the vehicles control system to control the vehicle attitude and the attitude of various sensors such as cameras.|
| MAV_CMD_NAV_PATHPLANNING | 81 | Control autonomous path planning on the MAV.|
| MAV_CMD_NAV_LAST | 95 | NOP - This command is only used to mark the upper limit of the NAV/ACTION commands in the enumeration |
| MAV_CMD_CONDITION_DELAY | 112 | Delay mission state machine.|
| MAV_CMD_CONDITION_CHANGE_ALT | 113 | Ascend/descend at rate. Delay mission state machine until desired altitude reached.|
| MAV_CMD_CONDITION_DISTANCE | 114 | Delay mission state machine until within desired distance of next NAV point.|
| MAV_CMD_CONDITION_YAW | 115 | Reach a certain target angle.|
| MAV_CMD_CONDITION_LAST | 159 | NOP - This command is only used to mark the upper limit of the CONDITION commands in the enumeration|
| MAV_CMD_DO_SET_MODE | 176 | Set system mode.|
| MAV_CMD_DO_JUMP | 177 | Jump to the desired command in the mission list. Repeat this action only the specified number of times |
| MAV_CMD_DO_CHANGE_SPEED | 178 | Change speed and/or throttle set points.|
| MAV_CMD_DO_SET_HOME | 179 | Changes the home location either to the current location or a specified location.|
| MAV_CMD_DO_SET_PARAMETER | 180 | Set a system parameter. Caution!  Use of this command requires knowledge of the numeric enumeration value of the parameter.|
| MAV_CMD_DO_SET_RELAY | 181 | Set a relay to a condition.|
| MAV_CMD_DO_REPEAT_RELAY | 182 | Cycle a relay on and off for a desired number of cyles with a desired period.|
| MAV_CMD_DO_SET_SERVO | 183 | Set a servo to a desired PWM value.|
| MAV_CMD_DO_REPEAT_SERVO | 184 | Cycle a between its nominal setting and a desired PWM for a desired number of cycles with a desired period.|
| MAV_CMD_DO_CONTROL_VIDEO | 200 | Control onboard camera system.|
| MAV_CMD_DO_SET_ROI | 201 | Sets the region of interest (ROI) for a sensor set or the vehicle itself.| This can then be used by the vehicles control system to control the vehicle attitude and the attitude of various sensors such as cameras.|
| MAV_CMD_DO_LAST | 240 | NOP - This command is only used to mark the upper limit of the DO commands in the enumeration
| MAV_CMD_PREFLIGHT_CALIBRATION | 241 | Trigger calibration.| This command will be only accepted if in pre-flight mode.|
| MAV_CMD_PREFLIGHT_SET_SENSOR_OFFSETS | 242 | Set sensor offsets.| This command will be only accepted if in pre-flight mode.|
| MAV_CMD_PREFLIGHT_STORAGE | 245 | Request storage of different parameter values and logs.| This command will be only accepted if in pre-flight mode.|
| MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN | 246 | Request the reboot or shutdown of system components.|
| MAV_CMD_OVERRIDE_GOTO | 252 | Hold / continue the current action |
| MAV_CMD_MISSION_START | 300 | start running a mission |
| MAV_CMD_COMPONENT_ARM_DISARM | 400 | Arms / Disarms a component|

See [MAV_CMD on this page for the details and specific command parameters](http://mavlink.org/messages/common)

MAV Mission Types:

| Value | Name | Description |
|-------|------|-------------|
| 0 | MAV_MISSION_TYPE_MISSION | Items are mission commands for main mission.|
| 1 | MAV_MISSION_TYPE_FENCE   |	 Specifies GeoFence area(s). Items are MAV_CMD_FENCE_ GeoFence items.|
|2	|MAV_MISSION_TYPE_RALLY	|Specifies the rally points for the vehicle. Rally points are alternative RTL points. Items are MAV_CMD_RALLY_POINT rally point items.|
|255|	MAV_MISSION_TYPE_ALL|	Only used in MISSION_CLEAR_ALL to clear all mission types.|

<a name="FLIGHT-SET_MISSION_CURRENT"></a>
**Command: SET_MISSION_CURRENT**

_Set the mission item on the mission list as the current action step._

`('FLIGHT','SET_MISSION_CURRENT', item number)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1  | integer | item number | Set the current mission to a specific step. |

<a name="FLIGHT-SET_SPEED"></a>
**Command: SET_SPEED**

_Set current craft speed in m/s_

`('FLIGHT','SET_SPEED', speed)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1  | number | speed  | Set craft speed in meters per second |

<a name="FLIGHT-AUTO_LAND"></a>
**Command: AUTO_LAND**

_Makes the craft stop what it is currently doing and fly a mission back to land at the specified location_

`('FLIGHT', 'AUTO_LAND', lat, lon, altitude, heading)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1  | number | latitude  | Latitude of the landing target |
| 2  | number | longitude | Longitude of the landing target |
| 3  | number | altitude | Absolute altitude of the landing location |
| 4  | number | heading | Landing approach heading |

<a name="FLIGHT-AUTO_LAND_AT_AIRPORT"></a>
**Command: AUTO_LAND_AT_AIRPORT**

_Makes the craft stop what it is currently doing and fly a mission back to land at the specified airport_

Note: The auto landing system does not consider terrain and should be used when close to an airport. If there is a mountain higher than the craft's current altitude between the craft and the desired airport, the auto lander will likely fly right into it on its way. Auto land, not "smart" auto land, actually pretty stupid auto land---you should implement a better one on your autopilot.

`('FLIGHT', 'AUTO_LAND_AT_AIRPORT', 'airport_name')`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1  | string| airport_name  | Name of airport at which to land |

Known airports depends on the scenario.

<a name="FLIGHT-AUTO_ABORT"></a>
**Command: AUTO_ABORT**

_Makes the craft leave an automated sequence such at takeoff or landing. If in the air, the craft will go into LOITER mode._

`('FLIGHT', 'AUTO_ABORT')`


==**MAVSim Payload Sugar**==

_These commands are extensions by MAVSim used for managing simulated payload. Status does not come through telemetry._

<a name="FLIGHT-MS_LOAD_PAYLOAD"></a>
**Command: MS_LOAD_PAYLOAD**

_Load the selected slot on the craft with specified payload. Slots are numbered starting with 0._

`('FLIGHT', 'MS_LOAD_PAYLOAD', integer, 'payload string')`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | payload | You can load anything you like by name, but only if you are sitting still. **No loading allowed in motion.** |

<a name="FLIGHT-MS_DROP_PAYLOAD"></a>
**Command: MS_DROP_PAYLOAD**

_Drop payload from the selected payload slot. Slots are numbered starting with 0._

`('FLIGHT', 'MS_DROP_PAYLOAD', integer)`

* Param 1: slot (integer)

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | integer | slot | Drop item in specified slot. Slot numebr (capacity) depends on aircraft. |

<a name="FLIGHT-MS_LIST_PAYLOAD"></a>
**Command: MS_LIST_PAYLOAD**

_Returns a list of current payload on the craft_

`('FLIGHT', 'MS_LIST_PAYLOAD')`

== **MAVSim Mission Sugar:** ==

_These commands are extensions by MAVSim used for managing missions more efficiently_

<a name="FLIGHT-MS_STORE_MISSION"></a>
**Command: MS_STORE_MISSION**

_Create a mission to store waypoint details_

`('FLIGHT', 'MS_STORE_MISSION', 'mission name', upper_lat, upper_lon, lower_lat, lower_lon, 'note')`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | mission name  | used to identify this mission, should be unique |
| 2 | float  | mission area upper lat  | Upper NW lat of mission area |
| 3 | float  | mission area upper lat  | Upper NW lon of mission area |
| 4 | float  | mission area lower lon  | Lower SE lat of mission area  |
| 5 | float  | mission area lower lon  | Lower SE lon of mission area  |
| 6 | string | note | Note field for storing information  |

<a name="FLIGHT-MS_ADD_MISSION_ITEM"></a>
**Command: MS_ADD_MISSION_ITEM**

_Add mission step (item) details and insert at the beginning, end, or after listed item_

`('FLIGHT', 'MS_ADD_MISSION_ITEM', 'mission name', insert rule, frame, command, param_1, param_2, param_3, param_4, lat, lon, alt, autocontinue)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | mission name | The name of the mission to add this item to |
| 2 | integer | insert rule | 0 = beginning, -1 = end, or number of the mission item to insert this after (numbering starts with 0) |
| 3 | integer | current | See **SET_MISSION_ITEM** above for how to set this |
| 4 | integer | frame | See **SET_MISSION_ITEM** above for how to set this |
| 5 | integer | command | See **SET_MISSION_ITEM** above for how to set this |
| 6 | float | p1 | See **SET_MISSION_ITEM** above for how to set this |
| 7 | float | p2 | See **SET_MISSION_ITEM** above for how to set this |
| 8 | float | p3 | See **SET_MISSION_ITEM** above for how to set this |
| 9 | float | p4 | See **SET_MISSION_ITEM** above for how to set this |
| 10 | float | lat | See **SET_MISSION_ITEM** above for how to set this |
| 11 | float | lon | See **SET_MISSION_ITEM** above for how to set this |
| 12 | float | alt | See **SET_MISSION_ITEM** above for how to set this |
| 13 | integer | autocontinue | See **SET_MISSION_ITEM** above for how to set this |

_Note: For now, we only support a mission_type of 0._

<a name="FLIGHT-MS_MISSION_LIST"></a>
**Command: MS_MISSION_LIST**

_List of all stored missions by name_

`('FLIGHT', 'MS_MISSION_LIST')`

<a name="FLIGHT-MS_LIST_MISSION"></a>
**Command: MS_LIST_MISSION**

_List all items in a specific mission._

`('FLIGHT', 'MS_LIST_MISSION', 'mission name')`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | mission name  | The name of the mission to list item for |


<a name="FLIGHT-MS_GET_MISSION_ITEM"></a>
**Command: MS_GET_MISSION_ITEM**

_Retrieve the details of a specific mission item_

`('FLIGHT', 'MS_GET_MISSION_ITEM', item number)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | mission name | The name of the mission to retrieve item from |
| 2 | integer | item number | List item number |

<a name="FLIGHT-MS_REPLACE_MISSION_ITEM"></a>
**Command: MS_REPLACE_MISSION_ITEM**

_Replace the specifics of a mission item_

`('FLIGHT', 'MS_REPLACE_MISSION_ITEM', 'mission name', item, frame, command, param_1, param_2, param_3, param_4, lat, lon, alt, autocontinue)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | mission name | The name of the mission to replace this item |
| 2 | integer | item | Item number to replace |
| 3 | integer | current | See **SET_MISSION_ITEM** above for how to set this |
| 4 | integer | frame | See **SET_MISSION_ITEM** above for how to set this |
| 5 | integer | command | See **SET_MISSION_ITEM** above for how to set this |
| 6 | float | p1 | See **SET_MISSION_ITEM** above for how to set this |
| 7 | float | p2 | See **SET_MISSION_ITEM** above for how to set this |
| 8 | float | p3 | See **SET_MISSION_ITEM** above for how to set this |
| 9 | float | p4 | See **SET_MISSION_ITEM** above for how to set this |
| 10 | float | lat | See **SET_MISSION_ITEM** above for how to set this |
| 11 | float | lon | See **SET_MISSION_ITEM** above for how to set this |
| 12 | float | alt | See **SET_MISSION_ITEM** above for how to set this |
| 13 | integer | autocontinue | See **SET_MISSION_ITEM** above for how to set this |

<a name="FLIGHT-MS_MOVE_MISSION_ITEM"></a>
**Command: MS_MOVE_MISSION_ITEM**

_Reorder a mission item in the mission sequence_

`('FLIGHT', 'MS_MOVE_MISSION_ITEM', 'mission name', from number, to number)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | mission name | The name of the mission in which to move a mission item |
| 2 | integer | from position | Move from this position, which will copy and then delete it from the list. |
| 3 | integer | to position | Insert in this position, which is in a list without the moved item, so that all item numbers higher that the target item to move have shifted down a position. |

<a name="FLIGHT-MS_DELETE_MISSION_ITEM"></a>
**Command: MS_DELETE_MISSION_ITEM**

_Remove a mission item_

`('FLIGHT', 'MS_DELETE_MISSION_ITEM', item number)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | mission name | The name of the mission in which to delete a mission item |
| 2 | integer | item number | The mission item to delete |

<a name="FLIGHT-MS_REPLACE_CURRENT_MISSION"></a>
**Command: MS_REPLACE_CURRENT_MISSION**

`('FLIGHT', 'MS_REPLACE_CURRENT_MISSION', 'mission name')`

_Take plane into MANUAL. Remove the current mission, load this mission, and set craft into AUTO._

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | mission name | The name of the mission in which to load and execute in a running plane mission. |

<a name="FLIGHT-MS_LOAD_MISSION"></a>
**Command: MS_LOAD_MISSION**

`('FLIGHT', 'MS_LOAD_MISSION', 'mission name')`

_Load the mission into the craft. Replaces current mission. Does not change aircraft MODE._

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | mission name | The name of the mission in which to load into the plane. |

<a name="FLIGHT-MS_REFUEL"></a>
**Command: MS_REFUEL**

`('FLIGHT', 'MS_REFUEL')`

_Fill up with fuel when on the ground at an airport and disarmed in MANUAL mode._


<a name="FLIGHT-MS_FUEL_STATUS"></a>
**Command: MS_FUEL_STATUS**

`('FLIGHT', 'MS_FUEL_STATUS')`

_Get the current fuel status._


<a name="FLIGHT-MS_QUERY_TERRAIN"></a>
**Command: MS_QUERY_TERRAIN**

`('FLIGHT', 'MS_QUERY_TERRAIN', lat, lon)`

_Get information about base map terrain._

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | int | lat | Latitude (y) |
| 2 | int | lon | Longitude (x) |

Returns

`('ACK', 'MS_QUERY_TERRAIN', lat, lon, altitude, name, description)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | int | lat | Latitude position (y) |
| 2 | int | lon | Longitude position(x) |
| 3 | int | altitude | Altitude of this object |
| 4 | string | name | Name of this terrain square |
| 5 | string | description | Description of this terrain square |


<a name="FLIGHT-MS_TAKE_PIC"></a>
**Command: MS_TAKE_PIC**

`('FLIGHT', 'MS_TAKE_PIC')`

_Take a picture of the spot underneath the craft and log the result_

Returns

`('ACK', 'MS_TAKE_PIC', description)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | description | Description of the item that the craft took a picture of. |

<a name="FLIGHT-MS_QUERY_SLICE"></a>
**Command: MS_QUERY_SLICE**

`('FLIGHT', 'MS_QUERY_SLICE', x, y, width, length)`

_ Return a string representation of an array of size (width x length), each index representing the tiletype of the tiles within the map slice_

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | int | x | x position of upper left corner of the slice |
| 2 | int | y | y position of the upper left corner of the slice |
| 3 | int | width | Width of the slice. Default is in settings: aoi_x_extent (20) |
| 4 | int | length | Length of the slice.  Default is in settings: aoi_y_extent (20) |

Returns

`('ACK', 'MS_QUERY_SLICE', slice)`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | slice | String representation of an array of size (width x length), each index representing the tiletype of the tiles within the slice. |


<a name="FLIGHT-MS_SET_AOI"></a>
**Command: MS_QUERY_TERRAIN**

`('FLIGHT', 'MS_SET_AOI', {x}, {y}, {x_extent}, {y_extent})`

----
### Subsystem: TELEMETRY

_Perception and sensing commands to receive all streaming craft information_

<a name="TELEMETRY-SET_MULTICAST"></a>
**Command: SET_MULTICAST**

`('TELEMETRY', 'SET_MULTICAST', state)`

Turns on or off multicast broadcasting of telemetry. Defaults to off.

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | state | 'ON' for on and 'OFF' for off |


<a name="TELEMETRY-ADD_LISTENER"></a>
**Command: ADD_LISTENER**

`('TELEMETRY', 'ADD_LISTENER', ip, port, filter_setting)`

Registers a UDP endpoint to receive filtered telemetry commands. Sending the ip and port of an existing registered endpoint will remove it before establishing a new one with the specified settings. Multiple unique enpoints can be registered and messages will be sent in round-robin fashion.

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | ip | IP number string (e.g., '192.168.0.1') |
| 2 | int | port | Communication port number |
| 3 | int | filter_setting | 0 = all messages, 1 = flight control and sensor messages only, 3 = geospatial update messages only |


<a name="TELEMETRY-REMOVE_LISTENER"></a>
**Command: REMOVE_LISTENER**

`('TELEMETRY', 'REMOVE_LISTENER', ip, port)`

Removes a UDP endpoint to stop receiving filtered telemetry commands. 

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | ip | IP number string (e.g., '192.168.0.1') |
| 2 | int | port | Communication port number |


----
### Subsystem: LOG

_Logging operations placed into the flight recording database_

<a name="LOG-CUSTOM_LOG_ENTRY"></a>
**Command: CUSTOM_LOG_ENTRY**

_Used for adding custom notes to the database_

`('LOG', 'CUSTOM_LOG_ENTRY', 'source string', 'log string', 'details string')`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | source | Who/what is doing this log entry |
| 2 | string | log | Open use organizational string (e.g., "FLIGHT NOTES", "INSTRUCTOR_COMMENTS", "OBSERVATIONS") |
| 3 | string | details | The core message to be logged |

<a name="LOG-MISSION_START"></a>
**Command: LOG_MISSION_START**

_Used for noting when a mission is considered to start in the logs_

`('LOG', 'LOG_MISSION_START', 'Mission Name String', 'Info about this mission string', '2017-08-17 17:44:43.428332')`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | name | Name of the mission |
| 2 | string | info  | Open information about this mission |
| 3 | DateTime string | start  | Date and time mission started |

_This message returns the UUID of this log for use in setting the STOP time._

<a name="LOG-MISSION_END"></a>
**Command: LOG_MISSION_END**

_Used for noting the end time of a mission_

`('LOG', 'LOG_MISSION_END', 'UUID string', '2017-08-18 17:44:43.428332')`

| Param  | type  | name  | description  |
|---|---|---|---|
| 1 | string | UUID | mission to record end time for (returned when start command was issued) |
| 2 | DateTime string | end | Date and time mission ended |



-----

**Aircraft Actions**

_This will cost a turn for each command._

1. SET_MODE 
	* AUTO - Executes the flight plan loaded into the system. After the last command the craft will fly to the HOME location and loiter (circle)
	* GUIDED - Allows flight to a specified point provided one point at a time, the craft will loiter (i.e., circle) around that point when reached.
	* LOITER - Circle around the point of the current location when the command was given.
	* RTL - Flies to the HOME location and loiters (circles)
	* MANUAL - Switches control to the joystick (fly-by-wire) interface. For now it will just fly in the last direction and altitude given until a mode change or constraint limit is reached.
2. AUTO_TAKEOFF - From a takeoff location, flies the plane to a medium altitude at the end of the runway then will fly to the HOME location and loiter (circle)
3. AUTO_LAND_AT_AIRPORT - Will fly the aircraft to the specified airport at the current altitude then land on the runway. 
4. SET_ALTITUDE - Set the aircraft altitude to either GROUND (0m), LOW (30-100m), MEDIUM (500m), or HIGH (1000m) 
5. SET_SPEED - Set aircraft speed to number of squares per turn (0-7) or -1 (stop). If the speed is set to 0 and wind does not provide enough loft, the aircraft will drop 1 altitude level per turn and eventually crash. If the wind conditions do not allow the maximum speed requested, the craft will go as fast as it can (e.g., 7 may be requested, but only 4 is possible, so the craft will move 4 spaces).
6. ARM - Arm the craft for operations (essentially, turn on the engines). Most commands cannot be executed unless the aircraft is armed.
7. DISARM - Disarm the craft
8. FLY_TO - In GUIDED mode, direct craft to go to a specific GPS coordinate
9. HEAD_TO - In GUIDED mode, direct craft to go a specific direction a specific distance
10. SET_MISSION_CURRENT - Set the mission item on the mission list as the current action step
11. AUTO_ABORT - Makes the craft leave an automated sequence such at takeoff or landing. An aborted takeoff will immediately land the plane if it is still over the runway; otherwise, it will deny the command as the craft will fall under normal flight operations. An aborted landing will increase craft airspeed to 1, raise altitude to MEDIUM, and then put the plane in LOITER around HOME.
12. MS_LOAD_PAYLOAD - Load the selected slot on the craft with specified payload
13. MS_DROP_PAYLOAD - Drop payload from the selected payload slot
14. MS_REPLACE_CURRENT_MISSION - Replace current mission with another and engage
15. MS_LOAD_MISSION - Load the mission into the craft
16. MS_REFUEL - If DISARMED and on an airport runway, this will refill the current craft fuel supply 
17. AUTO_TAXI_TO_TAKEOFF - Engages the auto taxi system to move an aircraft from a landing area to a takeoff area on a runway
18. SET_PARAM - Set a simulation or craft parameter
19. MS_NO_ACTION - Sent on a turn when no new commands are intended to be provided. The system will continue with resolving an updated state with no additional action. During the execution of a pre-planned flight this command may be issued on every turn while that flight is active.
20. SET_HOME - Set the HOME location for the aircraft

**Aircraft Mission Setup Commands**

_These commands can be issued within the same turn and feedback is provided with each command from telemetry._ 

_These are all ZERO cost commands and do not forward the turn. Send a MS_NO_ACTION or other command to advance game._

1. CLEAR_MISSION - Clears all current mission waypoints
2. SET_MISSION_COUNT - This tells the system how many items for mission setup
3. SET_MISSION_ITEM - Used to establish each step of a mission
4. MS_STORE_MISSION - Create a mission to store waypoint details
5. MS_ADD_MISSION_ITEM - Add mission step (item) details
6. MS_REPLACE_MISSION_ITEM - Replace the specifics of a mission item
7. MS_MOVE_MISSION_ITEM - Reorder a mission item in the mission sequence
8. MS_DELETE_MISSION_ITEM - Remove a mission item

**Aircraft Sensor Actions**

_Smarter agents will get all of this information from the telemetry stream, but this is provided for those hard-headed folks who just have to poll for state updates. This will cost a turn for each command._

_This will cost a turn for each command._

1. GET_ATTITUDE - Returns the current heading (yaw) (0, 45.0, 90.0, 135.0, 180.0, 225.0, 270.0, 315.0), pitch (0.0 = level, 45.0 = climbing, -45.0 = descending), and roll (-20.0 (slight turn), -40.0 (hard turn) = roll port, 20.0 (slight turn), 40.0 (hard turn) = roll starboard, 0.0 = level) of the craft.
2. GET_ALTITUDE - Returns the current altitude of the craft (GROUND, LOW, MEDIUM, HIGH)
3. GET_SPEED - Returns the current actual and desired speed of the craft (-1, 0-7)
4. GET_HEADING - Returns the current heading (yaw) (0, 45.0, 90.0, 135.0, 180.0, 225.0, 270.0, 315.0)
5. GET_LOCATION - Returns the current GPS coordinates of the craft
6. GET_MODE - Returns the current mode of the craft (AUTO, GUIDED, LOITER, RTL, MANUAL)
7. IS_ARMED - Check to see if the craft is armed
8. MS_LIST_PAYLOAD - Returns a list of current payload on the craft
9. MS_MISSION_LIST - List of all stored missions by name
10. MS_LIST_MISSION - List all items in a specific mission
11. MS_GET_MISSION_ITEM - Retrieve the details of a specific mission item
12. GET_PARAM - Get a simulation or craft parameter
13. GET_HOME - Get the current HOME location for the aircraft


