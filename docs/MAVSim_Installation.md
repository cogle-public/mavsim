# MAVSim and MAVSim Library

Micro-Autonomous Vehicle Simulator, a simulation augmentation and interface based upon ArduPilot's MavProxy to create a richer vehicle simulation.

Written for Python 3

MAVSim as written as a library for inclusion as a micro-air vehicle in other projects; however, a stand alone version is available here in main.py as well.

### Quick and Dirty Installation

1. Download from GitLab at [https://gitlab.com/COGLEProject/mavsim](https://gitlab.com/COGLEProject/mavsim))
2. Change directory into mavsim - `cd mavsim`
3. Edit `db_set.sh` to point to your Postgres Server
4. `/make_lib.sh ; ./install-dev.sh ; ./run.sh`


### Pre-requisites

Setup a [Postgres](https://www.postgresql.org/) Database

`pip3 install` the following Python packages

    DateTime==4.3
    geographiclib==1.49
    geopy==1.18.1
    parse==1.11.1
    psycopg2-binary==2.7.7
    py-dateutil==2.2
    pytz==2018.9
    PyYAML==3.13
    six==1.12.0
    SQLAlchemy==1.2.17
    tmx==1.10
    zope.interface==4.6.0
    pillow==5.3.0
    noise==1.2.2
    numpy==1.15.4
    scipy==1.2.1

A `pip3 install -r requirements.txt` from the `/mavsim` directory also does the trick quite nicely, but you should really set this up in a [virtualenv](https://virtualenv.pypa.io/en/stable/)
 
Install Nixel:
    `pip install git+https://gitlab.com/COGLEProject/parc-nixel`

Be sure to specify the database configuration either in object instantiation OR through the environmental variable LOGGING_DATABASE.

### Installation

`pip3 install git+https://gitlab.com/COGLEProject/mavsim.git`

You will also need to copy the scenarios from a downloaded source directory

`cp ../mavsim/mavsim/apl/scenarios/* ./{VIRTUAL ENVIRONMENT}/lib/python3.*/site-packages/mavsim/apl/scenarios/.`

Note: We call our {VIRTUAL ENVIRONMENT} VENV, so be sure to change that appropriately from the curly-braced holder. Your paths may differ, but the general form is `cp [from mavsim source scenarios] [to mavsim library scenarios folder]`

**OR**

Place the _mavsim_ directory from a downloaded source directory that has had the `./make_lib.sh` command run, which creates the list directory and content, in {VIRTUAL ENVIRONMENT}/lib/python3.7/site-packages OR in your global {Wherever your Python 3 is}/lib/site-packages OR Put your file in any directory you like and add that directory to the PYTHONPATH environment variable

### Usage

Please see `docs/MAVSIM_Instantiation.md` for details 

### Features

* Simple ASCII-based communication protocol (easily parsed) available over UDP and locally as a library
* Simulator Control
  * Start, Stop, Reset, Shutdown
  * Save and Load Simulation
* Simple high and low level aircraft controls
* Smart sensor
  * LIDAR with Automatic Object Recognition simulation
  * Definable accuracy functions
* Relational database logging  
* Turn-based

### Tour of the directory

* docs - Information about MAVSim design and Usage
* tests - Development and regression tests
* tools - Collection point for helper scripts and programs
* mavsim/\*.py code - The implementation of MAVSim
* mavsim/apl - Stored scenario files

Directories such as `dist`, `mavsim.egg.info`, and `VENV` are created when compiling into a library OR instantiating a virtual environment.

**Key Scripts**

* db_set.sh - sets the LOGGING_DATABASE environmental variable, which should be set in order to enable logging
* install-dev.sh - installs a local virtual environment and installs this MAVSim package (local code)
* install-master.sh - installs a local virtual environment and installs the MAVSim package from the master version of the git repository
* make_lib.sh - takes the local code and creates a python library from it
* run.sh - execute a MAVSim instance using main.py





   