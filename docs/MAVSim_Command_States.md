

| Command  | MANUAL | GUIDED | AUTO | RTL | LOITER | IN_AIR_ONLY | ON_GROUND_ONLY |
|    ---   | ---    | ---    | ---  | --- | --- | --- | --- |
| SET_MODE | X | X | X | X | X |   |   |
| AUTO_TAKEOFF | X |   |   |   |   |   | X |
| AUTO_LAND_AT_AIRPORT | X | X | X | X | X | X |   |
| SET_SPEED |    | X |   |   |   | X |  |
| ARM      | X | X | X | X | X |   |   |
| DISARM   | X | X | X | X | X |   |   |
| FLY_TO   |   | X |   |   |   | X |   |
| HEAD_TO  |   | X |   |   |   | X |   |
| SET_MISSION_CURRENT | X | X | X | X | X |   |   |
| AUTO_ABORT |   |   | X  |   |   | X |   |
| MS_LOAD_PAYLOAD | X |   |   |  |   |   | X |
| MS_DROP_PAYLOAD | X | X | X | X | X |   |   |
| MS_REPLACE_CURRENT_MISSION | X | X | X | X | X |   |   |
| MS_LOAD_MISSION | X | X | X | X | X |   |   |
| MS_REFUEL | X |    |   |   |   |   | X |
| AUTO_TAXI_TO_TAKEOFF | X |   |   |   |   |   | X |
| SET_PARAM | X | X | X | X | X |   |   |
| MS_NO_ACTION | X | X | X | X | X |   |   |
| SET_HOME | X | X | X | X | X |   |   |
| TURN_COMPLETE | X | X | X | X | X |   |   |
| CLEAR_MISSION | X | X | X | X | X |   |   |
| SET_MISSION_COUNT | X | X | X | X | X |   |   |
| SET_MISSION_ITEM | X | X | X | X | X |   |   |
| MS_STORE_MISSION | X | X | X | X | X |   |   |
| MS_ADD_MISSION_ITEM | X | X | X | X | X |   |   |
| MS_REPLACE_MISSION_ITEM | X | X | X | X | X |   |   |
| MS_MOVE_MISSION_ITEM | X | X | X | X | X |   |   |
| MS_DELETE_MISSION_ITEM | X | X | X | X | X |   |   |
| GET_ATTITUDE | X | X | X | X | X |   |   |
| GET_ALTITUDE | X | X | X | X | X |   |   |
| GET_SPEED | X | X | X | X | X |   |   |
| GET_HEADING | X | X | X | X | X |   |   |
| GET_LOCATION | X | X | X | X | X |   |   |
| GET_MODE | X | X | X | X | X |   |   |
| IS_ARMED | X | X | X | X | X |   |   |
| MS_LIST_PAYLOAD | X | X | X | X | X |   |   |
| MS_MISSION_LIST | X | X | X | X | X |   |   |
| MS_LIST_MISSION | X | X | X | X | X |   |   |
| MS_GET_MISSION_ITEM | X | X | X | X | X |   |   |
| MS_GET_SMART_SENSOR | X | X | X | X | X |   |   |
| GET_PARAM | X | X | X | X | X |   |   |
| GET_HOME | X | X | X | X | X |   |   |


