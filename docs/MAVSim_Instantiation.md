# Instantiating the MAVSim Library

MAVSim has recently been updated to be a library, but a stand alone version can also be run using main.py or a modification of it. 

This version still uses these old _Kingdom_ maps and another variant called _Pandora_ (You may also hear them called Godiland, which is a really, really old version), which are all [Tiled](https://www.mapeditor.org/)-based. These maps need to be copied over into the library version as described in the top level (install) README because packaging doesn’t like to bring along non-Python code for the libraries (or at least we are not spending additional time to figure that one out). The way forward is with Nixel procedurally generated maps, which are also supported in this version. Soon they will be the only maps supported.


### Example Usage (literally this is just main.py)

    from mavsim import mavsim

	def callback(message):
    	print("CALLBACK MSG >> %s" % message)

	ms = mavsim.MAVSim(verbose=False,
                   quiet=False,
                   nodb=False,
                   server_ip='0.0.0.0',
                   server_port=14555,
                   instance_name='MAVSim',
                   session_name='Jeff',
                   pilot_name='Sally',
                   database_url='postgresql://postgres:123456@localhost:32768/apm_missions',
                   telemetry_cb=callback,
                   nixel_gen=False,
                   sim_op_state = 0)

	# Example command to start a scenario
	# print(ms.command("('SIM','NEW','kingdom-base-A-7','1234')"))

	while 1:
	    pass


Prefer using the named parameters for the function call and not just an implied order as the order may change without warning. Named parameter usage will safeguard against future incompatibilities. None of these parameters is required and all have a default value, but this may lead to an undesired configuration. 

Prefer **not** setting database URL through instantiation, but rather set using the _LOGGING_DATABASE_ environmental variable. This way all stack components will properly attach to the same database.


### Usage

Instantiate a MAVSim object and initialize with the following attributes

    verbose 
        Default = False
        Verbose output (useful for debug)
    
    quiet 
        Default = False
        Mimize printing to the console
    
    nodb 
        Default = False
        Run without database logging
    
    server_ip 
        Default = '0.0.0.0'
        IP for external udp request server
    
    server_port 
        Default = 14555
        Port for external udp request server
                      
    instance_name 
        Default = 'mavsim'
        Name of this mavsim instance for the logged data
    
    session_name 
        Default = 'jeff'
        Name of this mavsim session for the logged data
    
    pilot_name 
        Default = 'Chuck'
        Name of the pilot controlling this craft
    
    database_url 
        Default = None
        The url string for connecting to the database if not using or intentionally overriding the _LOGGING_DATABASE_ environmental variable
    
    telemetry_cb 
        Default = None
        Function to send a message back on for telemetry. This should receive a single String type.

	nixel_gen
        Default = False
        If True, will generate a standard set of Nixel worlds from 'nixel_config.json'. Will not generate any scenarios that are already present in the repository.

	sim_op_state 
		Default = 0
		Sim operation state (simulation rules for navigation, drop, etc.)
		0: Original MAVSim operation state (15 degrees of freedom, drop is dependent on speed and altitude)
		2: Phase 2 COGLE MAVSim operation state (9 degrees of freedom, no 90 degree left or right moves)






       