# Game of Drones Rules

### Game Phases

1. **Environmental Setup**: These steps are all conducted by the _Game Master_. Selection of the base map and selection of all of the static and dynamic initial hidden state and position. A mission is determined with at least one clear goal and a set of end conditions is established. Open scoring for achieving specific milestone configurations in the game are established. 
2. **Mission Assignment**: The mission to be performed by the _Drone Pilot_ is articulated and all available, open information is presented. The "game" starts with the _Drone Pilot_ opening the scenario created by the _Game Master_.

	**The following three phases can be can be mixed and the _Drone Pilot_ can switch between them:**

3. **Mission Planning**: The _Drone Pilot_ may, as an option, pre-plan a set of mission waypoints and actions to enter into the autopilot system. Multiple sets of  named planned flight activities may be established prior to _Mission Performance_. During _Mission Performance_ any of these pre-planned flight may be selected and executed by the autopilot under control of the _Drone Pilot_.
4. **Mission Performance**: The _Drone Pilot_ executes commands to control the drone in order to perform the specified mission. The _Game Master_ updates the stae of the piloted craft in accordance with the environmental rules for executing drone commands, provides sensor reading updates, and updates any changes to available dynamic information streams (e.g., current reported sightings of a hiker, etc.). The _Game Master_ also covertly updates any unseen state changes (e.g., hiker positions). This is conducted in a turn-based manner where the next time-step is ticked and the _Drone Pilot_ may or may not choose an action. IF the _Drone Pilot_ does not chose a new action on their next turn, the game continues with the last action chosen. Then the _Game Master_ updates state and reports observed changes. The process is repeated until an end condition is reached.
5. **Mission Review**: The _Game Master_ evaluates the performance of the _Drone Pilot_ by applying the scoring to performance. Scoring may be performed during the _Mission performance_ if possible; otherwise, all remaining scoring will be done after reaching a game end condition.


All game actions in these phases are recorded in detail. This may involve an external _Scribe_ (a system or person that records the information produced from play).

### Players

1. **Drone Pilot**: Pilots the drone. Given a topological map and an objective (i.e., goal). Participates in game phases 2, 3, and 4.
2. **Game Master (Simulator)**: Lays out the hidden features for the mission on the map (e.g., location of unseen hazards, position of the lost hiker, where the bears are, etc.). Participates in phases 1, 2, 4, and 5. The _Game Master_ in many cases may be a mixed initiative with some parts conducted by a human and other parts by a machine (i.e., a simulator).

### Rules

#### Phase 1 - Environmental Setup
1. _Game Master_ chooses base map for the mission
2. _Game Master_ chooses among possible game goals:
	a. Provision the hiker: Find the lost hiker and try to drop a care package.
	* Count the bears (reconnaissance):  Cover the regions and get an accurate count and last locations of the bears. 
3. _Game Master_ makes choices for specific game goals:
	a. Provision the Hiker: _Game Master_ chooses specific locations for one or more hikers (not revealed)
	* Count the Bears: _Game Master_ places the deer and bears (not revealed)
4.	_Game Master_ establishes a deadline for completing the mission in number of moves, amount of fuel (spatial and activity weighted moves), with refueling limits, or other bounding constraint.
5. _Game Master_ establishes other constraints and environmental features (e.g., wind direction) as appropriate
6. _Game Master_ establishes all valid end conditions
7. _Game Master_ establishes scoring

**Provisioning the Hiker Scoring**

1.	Finding Hiker = 100 pts
2.	Dropping Package to hiker within 2 map tiles = 25 pts

**Counting the Bears**

Over a specified area:

1. Precision = 50 pts * (true positives / true positives + false positives)
2. Recall = 50 pts * (true positives / true positives + false negatives)
3. Perfection Bonus = 25 pts, if 100% precision and recall

**General (All Mission) Scoring**

1.	Landing the aircraft after completing mission = 50 pts
2. Fuel Consumption 
	a. The aircraft consumes 1 fuel unit for each:
		* Move with Sensor reading at high and medium altitudes
		* Change in altitude a single level
	* The aircraft consumes 2 fuel unit for each:
		* Move with Sensor reading at low altitude
	* The aircraft consumes an additional 5 fuel unit for each:
		* Equipment drop
		* Landing 
	* The aircraft consumes an additional 10 fuel unit for each:
		* Takeoff

**Environmental Constants**

1. All grid square are 10m x 10m
2. Low altitude is 10-100m above
3. Medium altitude is 101-500m
4. High altitude is 501-1000m
5. Very High Altitude >1000m

#### Phase 2 - Mission Assignment

1. _Game Master_ provides to the _Drone Pilot_ the environment map with topology information
2. _Game Master_ provides to the _Drone Pilot_ starting location for the mission
3. _Game Master_ provides to the _Drone Pilot_ the game goal
4 _Game Master_ provides to the _Drone Pilot_ information on available airports
5. _Game Master_ provides to the _Drone Pilot_ the starting fuel level, tank capacity, and number of refuels and locations (only at airports)
6. _Game Master_ provides to the _Drone Pilot_ wind and other appropriate environmental information 
7. _Game Master_ provides to the _Drone Pilot_ other constraints as appropriate
8. _Game Master_ provides to the _Drone Pilot_ valid game end conditions
9. _Game Master_ provides to the _Drone Pilot_ scoring rules

#### Phase 3 - Mission Planning

1. The _Drone Pilot_ may establish a set of named, predefined flight routes for the target environment. These flight paths may include:
	a. A home location where the craft will return to if the flight path is completed
	* A sequence of waypoints for the craft to fly to, which may have speed and altitude also defined for them
	* A point to conduct takeoff
	* A point to conduct landing
2. Craft takeoff and landing may only occur at an airport
3. The _Drone Pilot_ may provide annotations on the flight plan. These annotations are preserved in the system.
4. The _Drone Pilot_ may provide annotations on any part of the environment map. These annotations are preserved in the system.

#### Phase 4 - Mission Performance

1. The game is turn-based with the following turn phases:
	a. Given the current state of the environment, the _Drone Pilot_ chooses a single action to take (see Aircraft Actions and Aircraft Sensor Actions below). 

		**Note:** Only one aircraft or aircraft sensor action can be taken each turn, so if the _Drone Pilot_ wants to move then poll sensors, it will cost at least 2 turns. The telemetry stream will update after each move and provide an update to any changes. Anything not changed will not be updated.
	* The _Game Master_ translates the action into a state update for the craft and sensor readings and provides this information to the _Drone Pilot_ through telemetry in preparation for the next turn. Only changes, not complete state, will be provided. If an end condition is reached, this shall be specified and if it is a stopping condition the turns shall end and final scoring shall commence.
	* The _Game Master_ shall update the score of the _Drone Pilot_ as appropriate in accordance with the scoring rules.
	* The _Game Master_ covertly updates the state for other objects in the environment as necessary per their governing rules (e.g., dynamic bears may move position).
2. The _Drone Pilot_ may provide annotations on the flight trail or plan. These annotations are preserved in the system.
3. The _Drone Pilot_ may provide annotations on any part of the environment map. These annotations are preserved in the system.

**Aircraft Rules (Fixed-wing)**

1.	The aircraft takes off from the starting location or from the designated takeoff area of an airport
2.	The aircraft shall land at a landing point or lose the aircraft
3.	Speed and Wind Rules
	a. Aircraft shall move between 1 and 4 squares in a straight path each turn as specified by the _Drone Pilot_. 
	* If the wind is directly behind the aircraft it can move an additional 2 squares (i.e., between 1 and 6 squares) each turn
	* If the wind is directly behind the aircraft in a canyon it shall move between 2 and 7 squares each turn
	* If the wind is directly in front of the aircraft in can move from 0 to 3 squares each turn
	* If the wind is directly in front of the aircraft in a canyon it shall move from 0 to 2 squares each turn
5.	Turning Rules
	* There are 8 directions (N (1, 0.0°), NE (2, 45.0°), E (3, 90.0°), SE (4, 135.0°), S (5, 180.0°), SW (7, 225.0°), W (7, 270.0°), NW (8, 315.0°)) 
	* All turns require at least one square to execute the turn 
	* A turn can be up to 90 degrees from current heading (e.g., heading N, a turn can be W, NW, NE, or E one square).  
	* Turning is accomplished by selecting a FLY_TO or HEAD_TO point in GUIDED mode or through a pre-defined flight path in a waypoint flight plan in AUTO mode. The plane will perform turns following the specified command within the system constraints.
6.	Altitude Rules
	* There are 4 altitude levels:  Ground, Low, Medium, and High
	* The craft may only be on the ground at takeoff and landing at airports
	* At low altitude must avoid most trees and most surface features as obstacles
	* A player can change altitude while moving, but only one altitude level per square.
	* A player may turn and raise a single altitude level in one square

**Aircraft Control**

Please see the [MAVSim ArduPilot Light Command Message Protocol](https://gitlab.com/COGLEProject/mavsim/blob/master/apl/APL_MESSAGE_PROTOCOL.md) document for the command details. This is how you control the craft and sensors via a UDP sent octet string.

**Telemetry Stream**

Please see the [MAVSim ArduPilot Light Telemetry Protocol](https://gitlab.com/COGLEProject/mavsim/blob/master/apl/APL_TELEMETRY_PROTOCOL.md) document for the telemetry details. This is the preferred and no-cost method for getting updates of craft state and sensor readings. This is an octet string sent over UDP to a registered endpoint.

**Wind Rules**

_These are incorporated into the **Aircraft Rules**_

1.	Aircraft move faster when wind behind them
2.	Aircraft move slower when heading into wind
3.	Wind always moves faster down a canyon 

**Sensor Rules**

1.	Sensors and Speed and Turns
	* Sensors only scan at the last square of a move.
2.	Video/Image Smart Sensor (gimbaled, downward facing)
	a. High altitude (Altitude 3):
		1. Acuity: Roughly 70% (1d6, 3-6) that something is in view 
		2. View radius: 2 squares, middle square plus region including 2 squares in all directions
		3. Occlusion: none in this version
	b. Medium altitude (Altitude 2): 
		1. Acuity: 84% (1d6, 2-6).
		2. View radius: 1 square, middle square plus region including 1 square in all directions.
		3. Occlusion: Objects: none in this version
	c. Low altitude (Altitude 1):
		1. Acuity: 100% 
		2. View radius: square below craft
		3. Occlusion: none
	d. Static information should be read from the map.

** Payload Rules **

1. Payloads can only be loaded when on the ground at an airport.
2.	The package is affected by speed and altitude
3.	The higher the altitude and greater the speed the greater the distance it can end up away from the drop point
4.	The package will randomly land in the area determined by altitude and distance with equal probability
5.	The package with an onboard Xerox PARC Smart Sensor will have 0% probability of damage if dropped at altitude 0, 1% at altitude 1 , 40% at altitude 2, and 80% at altitude 3
6.	The package will first report as either OK or DAMAGED
7.	In addition, if the package is dropped above an object it has the following probabilities of being _STUCK: pine trees – 50%, pine tree – 25%, cabin – 50%, flight tower – 15%, firewatch tower – 20%
8.	Also, if dropped into water it has a 50% change of being _SUNK
9.	Finally, in the unfortunate circumstance that the package is dropped onto an active campfire ring, it will have a 100% change of being DAMAGED
10. Valid states for the package are: OK, DAMAGED, OK_STUCK, OK_SUNK, DAMAGED_STUCK, and DAMAGED_SUNK

	

** Mission Behaviors **

1. If the _Drone Pilot_ initiates **AUTO** mode and a plan that contains a takeoff the craft will start at the first step after takeoff.
2. The _default speed_ if not specified is 1.
3. The _default altitude_ is medium.

** Ways to Crash the Aircraft **

1. Altitude of GROUND (0) when not over an airport runway
2. Altitude of LOW (1) and runs into tree or mountain
3. Altitude of MEDIUM (2) and runs into mountain
4. Altitude of HIGH (3) and runs into mountain
5. Running out of fuel or setting speed too low will cause the plane to lose altitude and may result in an altitude of GROUND (0) and a crash when not over an airport runway

**Ways to get the Aircraft into the Air**

1. Automatically by executing an AUTO_TAKEOFF command from the runway takeoff position. If the plane is not at the takeoff position any command will result in an error stating such. To move an aircraft from the landing position to the takeoff position, use the AUTO_TAXI_TO_TAKEOFF command.
2. Fly in AUTO mode with a planned takeoff in the flight plan.

**Ways to get the Aircraft onto the Landing Strip**

1. Automatically by flying the aircraft to the end of the runway appropriate for landing (i.e., into the wind) and then using the AUTO_LAND_AT_AIRPORT command. Executing the command from large distances away risks the craft descending into terrain features since it will descend over the distance to prepare for landing.
2. Gently lower the craft to GROUND altitude and speed 0 over the landing strip through either GUIDED or AUTO commands.


#### Phase 5 - Mission Review

1. The _Game Master_ evaluates the performance of the _Drone Pilot_ by applying the scoring to performance for any unscored points. 
2. A trace of the flight and actions are presented to the user.
3. The accounting of all scoring in space and time are presented to the _Drone Pilot_.
4. The _Drone Pilot_ may provide annotations on any part of a flight trail or plan. These annotations are preserved in the system.
5. The _Drone Pilot_ may provide annotations on any part of the environment map. These annotations are preserved in the system.
6. Historical flights can be reviewed at any time in the review interface.

### Outcomes
1. All flights end in either success or failure. The measure of degrees of success or failure are defined in each mission scoring rules.
2. Outcomes for different simulated instance should be comparable through the scoring and outcome metrics.

