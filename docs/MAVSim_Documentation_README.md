# MAVSim: Documentation

Index:

1. [How to Install MAVSim](./MAVSim_Installation.md) - Getting you setup one step at a time
2. [How to Instantiate MAVSim](./MAVSim_Instantiation.md) - How to setup a library instance
3. [MESSAGE_PROTOCOL](./MAVSim_MESSAGE_PROTOCOL.md) - Command message descriptions for MAVSim
4. [TELEMETRY_PROTOCOL](./MAVSim_TELEMETRY_PROTOCOL.md) - Telemetry (state) message descriptions from MAVSim
5. [SCENARIO](./MAVSim_Game_of_Drones_Rules.md) - The game rules
6. [COMMAND STATES](./MAVSim_Command_States.md) - Availability of commands depending on craft state
 
MAVSim was designed to support intelligent controllers (including machine learning controllers) either externally, by running on the same container/system, or by using MAVSim as a library. All agents communicate to MAVSim to control the craft through messages (UDP or direct) in an octet stream in the format described in [MESSAGE_PROTOCOL](./MAVSim_MESSAGE_PROTOCOL.md). An example of sending these messages can be seen in `/mavsim/tests/cl_autotakeoff.sh` which will attempt to start and make the craft takeoff from a runway. The `/mavsim/tests/cl_udp_client.py` program is an example of how to send an octet stream message to MAVSim externally. See `/mavsim/main.py` for an example of how to instantiate MAVSim locally and interact with the engine.

Please note that some commands to MAVSim return values, while others simply provide an *ACK*nowledgement that the command was received. For commands that respond with just an *ACK* there is no guarantee that the command was actually executed by the craft. The telemetry stream format is described in [TELEMETRY_PROTOCOL](./MAVSim_TELEMETRY_PROTOCOL.md). Messages in telemetry largely are inspired and match ArduPilot and follow the [MAVLINK Protocol Common Message Set](http://mavlink.org/messages/common). Messages with the prefix MS (for __M__AV__S__im, not Microsoft) are unique to MAVSim.

Agent perception comes primarily through *telemetry* by listening over a registered callback or UDP endpoint. Some perception is provided in the response to issued commands, but is almost always visible in the telemetry stream as well. Agent control comes from direct commands to MAVSim. Reasoning is up to the agent. 

Stored data in the log database is only recorded if it is different from the last received update of that message. This is to save storage space due to the large amount of recorded data from craft operations. The same assumptions for streaming data apply.






