import time
import ast
from mavsim.mavsim import MAVSim

# Testing Interface


def callback(message):
    print("CALLBACK MSG >> %s" % message)

ms = MAVSim(verbose=False, quiet=False, nodb=False, server_ip='localhost', server_port=14555, instance_name='MAVSim',
            session_name='Jeff', pilot_name='Sally', nixel_gen=True,
            database_url='postgresql://postgres:apl@localhost:32779/apm_missions', telemetry_cb=callback, sim_op_state=1)

dna = "['COGLE_0:stubland_1:512_2:512_3:256_4:7_5:24|-0.1426885426044464/Terrain_0:0_1:100_2:0.05_3:0.5_4:0.05_5:0.5_6:0.05_7:0.5_8:0.5_9:0.5_10:0.7_11:0.3_12:0.5_13:0.5_14:True/', '0.36023542284965515/Ocean_0:60/', '-0.43587446212768555/River_0:0.01_1:100/', '-0.3501245081424713/Tree_0:500_1:20.0_2:4.0_3:0.01_4:2.0_5:0.1_6:1.9_7:3.0_8:2.2_9:3.5/', '0.6151155829429626/Airport_0:15.0_1:25_2:35_3:1000_4:[]/', '0.34627288579940796/Building_0:150_1:10.0_2:[]_3:1/', '0.31582069396972656/Road_0:3_1:500/', '-0.061891376972198486/DropPackageMission_0:1_3:Find the hiker last located at (88, 186, 41)_4:Provision the hiker with Food_5:Return and report to Southeast International Airport (SEI) airport_6:Southeast Regional Airport_7:Southeast International Airport_8:0_9:20.0_10:20.0_11:40.0/', '-0.25830233097076416/Stub_0:0.8_1:1.0_2:1.0_3:1.0_4:1.0_5:1.0/']"

print(ms.command("('SIM','NEW',\"{}\",'1234')".format(dna)))


# Templates
def FLY_TO(x, y, z, h):
    return "('FLIGHT','FLY_TO', {}, {}, {}, {})".format(y, x, z, h)


def HEAD_TO(h, dist, alt):
    return "('FLIGHT','HEAD_TO', {}, {}, {})".format(h, dist, alt)


def LOAD_PAYLOAD(slot, item):
    return "('FLIGHT', 'MS_LOAD_PAYLOAD', {}, '{}')".format(slot, item)


def DROP_PAYLOAD(slot):
    return "('FLIGHT', 'MS_DROP_PAYLOAD', {})".format(slot)


def LAND(code):
    return "('FLIGHT','AUTO_LAND_AT_AIRPORT', '{}')"

# kingdom-base-A-1: 30, 435, 3
# valley_lakes: 360, 110, 3
# stubland: 350, 270, 3

print(ms.command("('FLIGHT','MS_SET_AOI', {}, {}, {}, {})".format(340, 260, 20, 20)))
print(ms.command("('SIM','VIZ_RECORD', 1, 0)"))
cmds = [LOAD_PAYLOAD(0, "Dummy!"), "('FLIGHT','MS_AUTO_TAKEOFF', 3)", FLY_TO(350, 270, 3, 1), FLY_TO(350, 268, 3, 3)]# DROP_PAYLOAD(0), "('FLIGHT','MS_AUTO_LAND_AT_AIRPORT', 'NEP')"]#  # ["('FLIGHT', 'QUERY_SLICE', 1, 1, 20, 20)"]#
# cmds = ["('FLIGHT', 'MS_QUERY_SLICE', 325, 85)"]

time.sleep(1)

# while True:
#     time.sleep(1)

print(ms.command("('FLIGHT','ARM')"))
# print(ms.craft.a_star_walls(325, 85))

for cmd in cmds:
    print(ms.command(cmd))
    while ms.craft.mode != 4 and ms.craft.mode != 0 and ms.craft.mode != 3:    # While not on the ground and while not loitering
        ms.command("('FLIGHT', 'MS_NO_ACTION')")
        time.sleep(0.3)
    print("-------------------- GOT THERE ------------------")
