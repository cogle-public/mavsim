#!/usr/bin/python

import sqlalchemy
import sys
sys.path.append('..')
from database_interface import *

from sqlalchemy import desc
from sqlalchemy.orm.exc import NoResultFound

database_url = 'postgresql://postgres:123456@docker.for.mac.localhost:33121/apm_missions'
dbase = Database(database_url)


def pull_data():
    if dbase is None:
        return
    else:
        db = None

        try:
            db = dbase.session2.query(StateChange).order_by(StateChange.timestamp).filter_by(simulation_session_uuid=sys.argv[1])
            #print(db)

            for record in db:
                print(record.command)

        except NoResultFound:
            e, v, t = sys.exc_info()
            settings.logger.error("Error (NoResultFound): %s - %s" % (e, str(v)))
            settings.logger.error(type(data))


#print("('SIM','NEW','godiland-base','1234')")
pull_data()