#!/bin/bash
# Information from:
#    https://code.tutsplus.com/tutorials/how-to-write-package-and-distribute-a-library-in-python--cms-28693
#
#
# Make sure you have a stored SSH key for Gitlab prior to executing the next line
pip install --upgrade git+https://gitlab.com/COGLEProject/parc-nixel
#
#
python setup.py sdist
# python setup.py bdist_wheel
