# -----------------------------------------------------------------------------
#  MAVSim:: FLIGHT Command Handler
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from . import settings
import sys
import geopy
import time
import _thread
import numpy as np

from geopy.distance import VincentyDistance
from .waypoint_mission import WaypointMission

def load_mission(craft, mission):
    settings.logger.info("---===|  Loading a new mission into the wp system")
    settings.logger.info("---===|  Clearing mission items.")
    settings.flight_lockout = True
    craft.clearAllMissionItems()
    count = len(mission.waypoint_list)
    settings.logger.info("---===|  Set mission item count to : %d" % count)
    craft.setMissionCount(count)

    item_count = 0
    for item in mission.waypoint_list:
        settings.logger.info("---===|  Setting mission item")
        craft.setMissionItem(item_count,
                             item.current,
                             item.frame,
                             item.command,
                             item.p1,
                             item.p2,
                             item.p3,
                             item.p4,
                             item.lat,
                             item.lon,
                             item.alt,
                             item.autocont, 0)

    settings.logger.info("---===|  Set mission current to 0")
    craft.setMissionCurrent(0)
    settings.logger.info("---===|  New mission loaded!")
    settings.flight_lockout = False


def replace_mission(craft, mission):
    settings.logger.info("---===|  Replacing mission in the wp system")
    settings.logger.info("---===|  Setting MODE to MANUAL for mission load")
    craft.setMode('MANUAL')

    # Load mission
    load_mission(craft, mission)

    # Set mode to AUTO
    settings.logger.info("---===|  Setting MODE to AUTO")
    while craft.getMode() != 'AUTO':
        craft.setMode('AUTO')
        # time.sleep(0.1)

    settings.logger.info("---===|  New mission engaged. You're welcome!")

# -----------------------------------------------------------------------------
#                                                        FLIGHT Command Handler
# -----------------------------------------------------------------------------


class FLIGHTCommandHandler:
    def __init__(self, craft, mem_buffer):
        settings.logger.info("FLIGHT command handler setup...")
        self.craft = craft
        self.mem_buffer = mem_buffer

        # Payload
        self.payload = ["EMPTY", "EMPTY", "EMPTY", "EMPTY"]
        self.payload_limit = 3

        # Mission
        currrent_loaded_mission = None
        mission_list = []

        self.message = None
        self.switch_dict = {'SET_MODE': self.set_mode_command,
                            'GET_MODE': self.get_mode_command,
                            'ARM': self.arm_command,
                            'DISARM': self.disarm_command,
                            'IS_ARMED': self.is_armed_command,
                            'FLY_TO': self.fly_to_command,
                            'HEAD_TO': self.head_to_command,
                            'MS_AUTO_TAKEOFF': self.auto_takeoff_command,
                            'GET_ATTITUDE': self.get_attitude_command,
                            'GET_ALTITUDE': self.get_altitude_command,
                            'GET_HEADING': self.get_heading_command,
                            'GET_SPEED': self.get_speed_command,
                            'GET_LOCATION': self.get_location_command,
                            'CLEAR_MISSION': self.clear_mission_command,
                            'SET_MISSION_COUNT': self.set_mission_count_command,
                            'SET_MISSION_ITEM': self.set_mission_item_command,
                            'SET_MISSION_CURRENT': self.set_mission_current_command,
                            'SET_SPEED': self.set_speed_command,
                            'MS_AUTO_LAND': self.auto_land_command,
                            'MS_AUTO_LAND_AT_AIRPORT': self.auto_land_at_airport_command,
                            'MS_AUTO_ABORT': self.auto_abort_command,
                            'MS_LOAD_PAYLOAD': self.load_payload_command,
                            'MS_DROP_PAYLOAD': self.drop_payload_command,
                            'MS_LIST_PAYLOAD': self.list_payload_command,
                            'MS_STORE_MISSION': self.store_mission_command,
                            'MS_ADD_MISSION_ITEM': self.add_mission_item_command,
                            'MS_MISSION_LIST': self.mission_list_command,
                            'MS_LIST_MISSION': self.list_mission_command,
                            'MS_GET_MISSION_ITEM': self.get_mission_item_command,
                            'MS_REPLACE_MISSION_ITEM': self.replace_mission_item_command,
                            'MS_MOVE_MISSION_ITEM': self.move_mission_item_command,
                            'MS_DELETE_MISSION_ITEM': self.delete_mission_item_command,
                            'MS_REPLACE_CURRENT_MISSION': self.replace_current_mission_command,
                            'MS_LOAD_MISSION': self.load_mission_command,
                            'GET_HOME_POSITION': self.get_home_position_command,
                            'SET_HOME_POSITION': self.set_home_position_command,
                            'MS_NO_ACTION': self.no_action_command,
                            'MS_QUERY_TERRAIN': self.ms_query_terrain,
                            'MS_AUTO_TAXI_TO_TAKEOFF': self.auto_taxi_command,
                            'MS_REFUEL': self.ms_refuel,
                            'MS_LIST_AIRPORTS': self.ms_list_airports,
                            'MS_SET_PARAM': self.ms_set_param,
                            'MS_GET_PARAM': self.ms_get_param,
                            'MS_FUEL_STATUS': self.fuel_status,
                            'MS_TAKE_PIC': self.ms_take_pic,
                            'MS_QUERY_SLICE': self.query_slice,
                            'MS_SET_AOI' : self.set_aoi,
                            'MS_CUSTOM' : self.custom_command
                            }

    def process(self, message):
        self.message = message
        settings.logger.info("...processing command - %s" % message[1])

        if self.mem_buffer.heartbeat_buf is None:
            status = None
        else:
            status = self.mem_buffer.heartbeat_buf['system_status']
        if status is None or (status < 3 or status > 5):
            return "('ERR', 'SYSTEM NOT YET OPERATIONAL')"

        try:
            if settings.craft_state == settings.CraftState.OFF:
                raise Exception("Craft is OFF or DEAD.")

            return self.switch_dict[message[1]]()
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'COMMAND DOES NOT EXIST', %s)" % str.upper(str(v))

    def set_mode_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            self.craft.setMode(self.message[2])
            return "('ACK', 'SET_MODE')"
        except NameError:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'SET_MODE', '%s')" % str.upper(str(v))
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Set Mode Error: %s - %s" % (e, str(v)))
            return "('ERR', 'SET_MODE', '%s')" % str.upper(str(v))

    def get_mode_command(self):
        try:
            mode = self.craft.getMode()
            return "('ACK', 'GET_MODE', '%s')" % mode
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'GET_MODE', '%s')" % str.upper(str(v))

    def arm_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            self.craft.arm()
            return "('ACK', 'ARM')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'ARM', '%s')" % str.upper(str(v))

    def disarm_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            self.craft.disarm()
            return "('ACK', 'DISARM')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'DISARM', '%s')" % str.upper(str(v))

    def is_armed_command(self):
        try:
            value = self.craft.isArmed()
            if value:
                val = "TRUE"
            else:
                val = "FALSE"
            return "('ACK', 'IS_ARMED', '%s')" % val
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'IS_ARMED', '%s')" % str.upper(str(v))

    def fly_to_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            if settings.apl == True:
                # In APL lon, which is x, comes first
                heading = None
                if len(self.message) > 5:
                    heading = self.message[5]

                self.craft.flyTo(self.message[3], self.message[2], self.message[4], h=heading)
            else:
                self.craft.flyTo(self.message[2], self.message[3], self.message[4])
            return "('ACK', 'FLY_TO')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'FLY_TO', '%s')" % str.upper(str(v))

    def head_to_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            heading = self.message[2]
            distance = self.message[3]
            altitude = self.message[4]

            if settings.apl == True:
                self.craft.headTo(heading, distance, altitude)
                return "('ACK', 'HEAD_TO')"

            current_gps = self.craft.getLocation()

            origin = geopy.Point(current_gps['lat'], current_gps['lon'])
            destination = VincentyDistance(kilometers=(distance / 1000.0)).destination(
                origin, heading)

            lat, lon = destination.latitude, destination.longitude

            self.craft.flyTo(lat, lon, self.message[4])
            return "('ACK', 'HEAD_TO')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'HEAD_TO', '%s')" % str.upper(str(v))

    def get_attitude_command(self):
        try:
            value = self.craft.getAttitude()
            return ("('ACK', 'GET_ATTITUDE', %f, %f, %f,)" % (value['yaw'],
                                                              value['roll'],
                                                              value['pitch']))
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'GET_ATTITUDE', '%s')" % str.upper(str(v))

    def get_altitude_command(self):
        try:
            value = self.craft.getAltitude()
            return "('ACK', 'GET_ALTITUDE', %f)" % value
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'GET_ALTITUDE', '%s')" % str.upper(str(v))

    def set_altitude_command(self):
        try:
            self.craft.setAltitude(self.message[2])
            return "('ACK', 'SET_ALTITUDE')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'SET_ALTITUDE', '%s')" % str.upper(str(v))

    def get_home_position_command(self):
        try:
            val = self.craft.getHomePosition()
            return "('ACK', 'GET_HOME_POSITION', %d, %d)" % (val[0], val[1])
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'GET_HOME_POSITION', '%s')" % str.upper(str(v))

    def set_home_position_command(self):
        try:
            self.craft.setHomePosition(self.message[3], self.message[2])
            return "('ACK', 'SET_HOME_POSITION')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'SET_HOME_POSITION', '%s')" % str.upper(str(v))

    def get_heading_command(self):
        try:
            value = self.craft.getHeading()
            return "('ACK', 'GET_HEADING', %f)" % value
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'GET_HEADING', '%s')" % str.upper(str(v))

    def get_speed_command(self):
        try:
            value = self.craft.getSpeed()
            return "('ACK', 'GET_SPEED', %f)" % value
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'GET_SPEED', '%s')" % str.upper(str(v))

    def get_location_command(self):
        try:
            value = self.craft.getLocation()
            return ("('ACK', 'GET_LOCATION', %f, %f, %f)" % (value['lat'],
                                                              value['lon'],
                                                              value['fix_type']))
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'GET_LOCATION', '%s')" % str.upper(str(v))

    def clear_mission_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            self.craft.clearAllMissionItems()
            return "('ACK', 'CLEAR_MISSION')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'CLEAR_MISSION', '%s')" % str.upper(str(v))

    def set_mission_count_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            self.craft.setMissionCount(self.message[2])
            return "('ACK', 'SET_MISSION_COUNT', %d)" % self.message[2]
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'SET_MISSION_COUNT', '%s')" % str.upper(str(v))

    def set_mission_item_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            self.craft.setMissionItem(self.message[2],
                                      self.message[3],
                                      self.message[4],
                                      self.message[5],
                                      self.message[6],
                                      self.message[7],
                                      self.message[8],
                                      self.message[9],
                                      self.message[10],
                                      self.message[11],
                                      self.message[12],
                                      self.message[13],
                                      self.message[14])
            return "('ACK', 'SET_MISSION_ITEM', %d)" % self.message[2]
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'SET_MISSION_ITEM', '%s')" % str.upper(str(v))

    def set_mission_current_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            self.craft.setMissionCurrent(self.message[2])
            return "('ACK', 'SET_MISSION_CURRENT', %d)" % self.message[2]
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'SET_MISSION_COUNT', '%s')" % str.upper(str(v))

    def set_speed_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            self.craft.setSpeed(self.message[2])
            return "('ACK', 'SET_SPEED')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'SET_SPEED', '%s')" % str.upper(str(v))

    def ms_refuel(self):
        try:
            self.craft.refuel()
            return "('ACK', 'MS_REFUEL')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
        return "('ERR', 'MS_REFUEL', '%s')" % str.upper(str(v))

    def ms_query_terrain(self):
        try:
            val = self.craft.query_terrain(self.message[3], self.message[2])
            height = val[2]
            if height < 0:
                height = 0

            return ("('ACK', 'MS_QUERY_TERRAIN', %d, %d, %d, '%s', '%s')" % (self.message[3], self.message[2], height, val[0], val[1].translate({ord('\'') : '', ord('\"') : ''})))
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
        return "('ERR', 'MS_QUERY_TERRAIN', '%s')" % str.upper(str(v))

    def no_action_command(self):
        try:
            self.craft.noop()
            return "('ACK', 'MS_NO_ACTION')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
        return "('ERR', 'MS_NO_ACTION', '%s')" % str.upper(str(v))

    def ms_list_airports(self):
        try:
            val = self.craft.list_airports()
            return ("('ACK', 'MS_LIST_AIRPORTS', '%s')" % str(val))
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
        return "('ERR', 'MS_LIST_AIRPORTS', '%s')" % str.upper(str(v))

    def ms_set_param(self):
        try:
            self.craft.set_param(self.message[2], self.message[3])
            return "('ACK', 'MS_SET_PARAM')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
        return "('ERR', 'MS_SET_PARAM', '%s')" % str.upper(str(v))

    def ms_get_param(self):
        try:
            val = self.craft.get_param(self.message[2])
            return "('ACK', 'MS_GET_PARAM', '%s', '%s')" % (self.message[2], val)
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
        return "('ERR', 'MS_GET_PARAM', '%s')" % str.upper(str(v))

    def fuel_status(self):
        try:
            val = self.craft.fuel_status()
            return ("('ACK', 'MS_FUEL_STATUS', '%d')" % val)
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
        return "('ERR', 'MS_FUEL_STATUS', '%s')" % str.upper(str(v))

    def ms_take_pic(self):
        try:
            val = self.craft.take_pic()
            return "('ACK', 'MS_TAKE_PIC', '%s')" % (val)
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
        return "('ERR', 'MS_TAKE_PIC', '%s')" % str.upper(str(v))

    def query_slice(self):
        try:
            if len(self.message) > 3:
                x = self.message[2]
                y = self.message[3]
                w = settings.aoi_x_extent
                l = settings.aoi_y_extent
            else:
                x = settings.aoi_x
                y = settings.aoi_y
                w = settings.aoi_x_extent
                l = settings.aoi_y_extent

            if len(self.message) > 4:
                w = self.message[4]
            if len(self.message) > 5:
                l = self.message[5]

            val = self.craft.query_slice(x, y, w, l)
            val = val.tolist()
            return "('ACK', 'QUERY_SLICE', '%s')" % (val)
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
        return "('ERR', 'QUERY_SLICE', '%s')" % str.upper(str(v))

    def set_aoi(self):
        try:
            settings.aoi_x = int(self.message[2])
            settings.aoi_y = int(self.message[3])
            settings.aoi_x_extent = int(self.message[4])
            settings.aoi_y_extent = int(self.message[5])

            # store aoi in db
            map_slice = self.craft.query_slice(settings.aoi_x, settings.aoi_y, settings.aoi_x_extent,
                                               settings.aoi_y_extent).tolist()

            self.mem_buffer.store_area_of_interest(self.craft.sim.scenario, str(map_slice))

            return "('ACK', 'MS_SET_AOI')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
        return "('ERR', 'MS_SET_AOI', '%s')" % str.upper(str(v))

    # *************************************************************************
    #                       MAVSim Auto Flight Extensions
    # *************************************************************************

    def auto_takeoff_command(self):
        try:
            altitude = -1   # Signals self.craft.auto_takeoff to use a default based on what type of world is loaded
            distance = 10
            end_heading = None

            if len(self.message) > 2:
                altitude = self.message[2]

            if len(self.message) > 3:
                distance = self.message[3]

            if len(self.message) > 4:
                end_heading = self.message[4]

            self.craft.auto_takeoff(altitude, distance, end_heading)
            return "('ACK', 'AUTO_TAKEOFF')"
        except:
            e, v, t = sys.exc_info()
            return "('ERR', 'AUTO_TAKEOFF', '%s')" % str.upper(str(v))

    def auto_land_command(self):
        return "('ERR', 'THIS FLIGHT COMMAND IS NOT AVAILABLE IN ARDUPILOT LIGHT')"

    def auto_land_at_airport_command(self):
        try:
            self.craft.auto_land_at_airport(self.message[2])
            return "('ACK', 'AUTO_LAND_AT_AIRPORT')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'AUTO_LAND_AT_AIRPORT', '%s')" % str.upper(str(v))

    def auto_abort_command(self):
        try:
            self.craft.auto_abort()
            return "('ACK', 'AUTO_ABORT')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'AUTO_ABORT', '%s')" % str.upper(str(v))

    def auto_taxi_command(self):
        try:
            self.craft.auto_taxi()
            return "('ACK', 'AUTO_TAXI_TO_TAKEOFF')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'AUTO_TAXI_TO_TAKEOFF', '%s')" % str.upper(str(v))


    # *************************************************************************
    #                       MAVSim Payload Extensions
    # *************************************************************************

    def load_payload_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            if settings.apl is True:
                # Cannot load while moving
                if (self.craft.speed > 1 or self.craft.altitude > 0) and (self.craft.sim.nixel_world is not None and self.craft.speed > 1):
                    return "('ERR', 'MS_LOAD_PAYLOAD', 'CANNOT_LOAD_IN_MOTION_OR_IN_THE_AIR')"

                item = self.message[2]

                if item > self.craft.payload_limit or item < 0:
                    return "('ERR', 'MS_LOAD_PAYLOAD', 'SLOT_OUT_OF_RANGE')"

                if self.craft.payload[item] == "EMPTY":
                    self.craft.load_payload(item, self.message[3])

                    payload = ""
                    for item in self.craft.payload:
                        payload = payload + ", '" + item + "'"

                    return ("('ACK', 'MS_LOAD_PAYLOAD'%s)" % (payload))
                else:
                    return "('ERR', 'MS_LOAD_PAYLOAD', 'SLOT_NOT_EMPTY')"

        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_LOAD_PAYLOAD', '%s')" % str.upper(str(v))

    def drop_payload_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            if settings.apl is True:
                item = self.message[2]

                if item > self.craft.payload_limit or item < 0:
                    return "('ERR', 'MS_DROP_PAYLOAD', 'SLOT_OUT_OF_RANGE')"

                dropped = self.craft.resolve_payload_drop(self.message[2])

                if dropped == "EMPTY":
                    return "('ERR', 'MS_DROP_PAYLOAD', 'SLOT_EMPTY')"

                payload = ""
                for item in self.craft.payload:
                    payload = payload + ", '" + item + "'"

                return "('ACK', 'MS_DROP_PAYLOAD', '%s'%s)" % (dropped, payload)
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_DROP_PAYLOAD', '%s')" % str.upper(str(v))

    def list_payload_command(self):
        try:
            if settings.apl is True:
                payload = ""
                for item in self.craft.payload:
                    payload = payload + ", '" + item + "'"

                self.craft.list_payload()

                return "('ACK', 'MS_LIST_PAYLOAD'%s)" % payload
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_LIST_PAYLOAD', '%s')" % str.upper(str(v))

    # *************************************************************************
    #                       MAVSim Mission Extensions
    # *************************************************************************

    # - MS_STORE_MISSION (mission name, other meta data)
    def store_mission_command(self):
        try:
            value = 'Nothing'

            new_mission = WaypointMission(self.message[2],
                                          self.message[3],
                                          self.message[4],
                                          self.message[5],
                                          self.message[6],
                                          self.message[7])

            for item in self.mission_list:
                if item.name == self.message[2]:
                    raise KeyError("Mission name already exists!")

            self.mission_list.append(new_mission)

            return "('ACK', 'MS_STORE_MISSION')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_STORE_MISSION', '%s')" % str.upper(str(v))

    # - MS_ADD_MISSION_ITEM (mission, at_end, at_position #, at_beginning,data)
    def add_mission_item_command(self):
        try:
            value = 'Nothing'

            mission = None
            for item in self.mission_list:
                if self.message[2] == item.name:
                    mission = item

            if mission is None:
                raise KeyError("Mission does not exist!")

            length = len(mission.waypoint_list)
            if message[3] == -1:
                # Insert at the end
                mission.append_waypoint(self.message[4],
                                        self.message[5],
                                        self.message[6],
                                        self.message[7],
                                        self.message[8],
                                        self.message[9],
                                        self.message[10],
                                        self.message[11],
                                        self.message[12],
                                        self.message[13],
                                        self.message[14])
            elif message[3] > 0:
                # Insert at the end
                mission.insert_waypoint(self.message[3],
                                        self.message[4],
                                        self.message[5],
                                        self.message[6],
                                        self.message[7],
                                        self.message[8],
                                        self.message[9],
                                        self.message[10],
                                        self.message[11],
                                        self.message[12],
                                        self.message[13],
                                        self.message[14])
            else:
                raise IndexError("Item insertion problem.")

            return "('ACK', 'MS_ADD_MISSION_ITEM')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_ADD_MISSION_ITEM', '%s')" % str.upper(str(v))

    # - MS_MISSION_LIST
    def mission_list_command(self):
        try:
            output = "["
            count = 0
            length = len(self.mission_list)
            for item in self.missison_list:
                output = output + str(count) + ":" + item
                if count != (length - 1):
                    output = output + ", "
                    count = count + 1
            output = output + "]"

            return "('ACK', 'MS_MISSION_LIST', '%s')" % (output)
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_MISSION_LIST', '%s')" % str.upper(str(v))

    # - MS_LIST_MISSION (mission, all #s)
    def list_mission_command(self):
        try:
            mission = None
            for item in self.mission_list:
                if self.message[2] == item.name:
                    mission = item

            if mission is None:
                raise KeyError("Mission does not exist!")

            value = mission.get_waypoint_list()
            return "('ACK', 'MS_LIST_MISSION', '%s')" % (value)
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_LIST_MISSION', '%s')" % str.upper(str(v))

    # - MS_GET_MISSION_ITEM (mission, #)
    def get_mission_item_command(self):
        try:
            mission = None
            for item in self.mission_list:
                if self.message[2] == item.name:
                    mission = item

            if mission is None:
                raise KeyError("Mission does not exist!")

            value = mission.waypoint_list[self.mesage[3]]
            return "('ACK', 'MS_GET_MISSION_ITEM', '%s')" % str(value)
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_GET_MISSION_ITEM', '%s')" % str.upper(str(v))

    # - MS_REPLACE_MISSION_ITEM (mission, #, new data))
    def replace_mission_item_command(self):
        try:
            mission = None
            for item in self.mission_list:
                if self.message[2] == item.name:
                    mission = item

            if mission is None:
                raise KeyError("Mission does not exist!")

            mission.replace_waypoint(self.message[3],
                                     self.message[4],
                                     self.message[5],
                                     self.message[6],
                                     self.message[7],
                                     self.message[8],
                                     self.message[9],
                                     self.message[10],
                                     self.message[11],
                                     self.message[12],
                                     self.message[13],
                                     self.message[14])

            return "('ACK', 'MS_REPLACE_MISSION_ITEM')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_REPLACE_MISSION_ITEM', '%s')" % str.upper(str(v))

    # - MS_MOVE_MISSION_ITEM (mission, from #, to #)
    def move_mission_item_command(self):
        try:
            mission = None
            for item in self.mission_list:
                if self.message[2] == item.name:
                    mission = item

            if mission is None:
                raise KeyError("Mission does not exist!")

            mission.move_waypoint(self.message[3], self.message[4])
            return "('ACK', 'MS_MOVE_MISSION_ITEM')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_MOVE_MISSION_ITEM', '%s')" % str.upper(str(v))

    # - MS_DELETE_MISSION_ITEM (mission, #)
    def delete_mission_item_command(self):
        try:
            mission = None
            for item in self.mission_list:
                if self.message[2] == item.name:
                    mission = item

            if mission is None:
                raise KeyError("Mission does not exist!")

            del mission.waypoint_list[self.message[3]]
            return "('ACK', 'MS_DELETE_MISSION_ITEM')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_DELETE_MISSION_ITEM', '%s')" % str.upper(str(v))

    # - MS_REPLACE_CURRENT_MISSION(mission)
    def replace_current_mission_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            mission = None
            for item in self.mission_list:
                if self.message[2] == item.name:
                    mission = item

            if mission is None:
                raise KeyError("Mission does not exist!")

            _thread.start_new_thread(replace_mission,
                                    (self.craft, mission))

            return "('ACK', 'MS_REPLACE_CURRENT_MISSION')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_REPLACE_CURRENT_MISSION', '%s')" % str.upper(str(v))

    # - MS_LOAD_MISSION (mission)
    def load_mission_command(self):
        if settings.flight_lockout:
            return "('ERR', 'THIS FLIGHT COMMAND LOCKED OUT DURING AUTO OPERATIONS')"
        try:
            mission = None
            for item in self.mission_list:
                if self.message[2] == item.name:
                    mission = item

            if mission is None:
                raise KeyError("Mission does not exist!")

            _thread.start_new_thread(load_mission,
                                    (self.craft, mission))

            return "('ACK', 'MS_LOAD_MISSION')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))
            return "('ERR', 'MS_LOAD_MISSION', '%s')" % str.upper(str(v))

    def custom_command(self):
        count = self.mem_buffer.mission_count_mcount
        self.craft.getMissionCount()
        while count == self.mem_buffer.mission_count_mcount:
            time.sleep(0.0001)

        return "('ACK', 'CUSTOM', %d)" % (self.mem_buffer.mission_count_buf['count'])

    # def _command(self):
    #     try:
    #         value=self.craft.
    #         return ("('ACK', 'SET_MODE', '%s')" % str.upper(value))
    #     except:
    #         e, v, t=sys.exc_info()
    #         settings.logger.error("Error: %s - %s" % (e, str(v)))
    #         return ("('ERR', 'SET_MODE', '%s')" % str.upper(str(v)))

# fin
