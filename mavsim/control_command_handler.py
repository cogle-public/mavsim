# -----------------------------------------------------------------------------
#  MAVSim::CONTROL Command Handler
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from . import settings

class CONTROLCommandHandler:
    def __init__(self, craft):
        settings.logger.info("CONTROL command handler setup...")
        self.craft = craft

    def process(self, message):
        settings.logger.info("...processing command - %s" % message[1])
        response = 'ACK'
        return response

# fin
