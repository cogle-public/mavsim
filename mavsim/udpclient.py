# -----------------------------------------------------------------------------
#  MAVSim::UDP Client
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from . import settings
import socket

class UDPClient:
    def __init__(self, ip, port, filter):
        settings.logger.info("...udp client initialized")
        self.ip = ip
        self.port = port
        self.filter = filter

        # Create a UDP/IP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # Bind the socket to the port
        self.server_address = (ip, port)
        settings.logger.info('......starting up udp client on %s port %s' % self.server_address)

    def receive(self):
        settings.logger.info('...udp client waiting to receive message')
        data, self.address = self.sock.recvfrom(4096)

        settings.logger.debug('received %s bytes from %s' % (len(data), self.server_address))
        settings.logger.debug(data)

        return data.decode('UTF-8')

    def send(self, message):
            sent = self.sock.sendto(message.encode('UTF-8'), self.server_address)
            settings.logger.debug('sent %s bytes back to %s' % (sent, self.server_address))

    def __del__(self):
        settings.logger.info("Closing udp client")
        self.sock.close()

# fin
