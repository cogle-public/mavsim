# -----------------------------------------------------------------------------
#  MAVSim::ArduPilotLight SIM Command Handler
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from . import settings
import uuid
import sys
from .ardupilotlight_simulation_core_0 import APL_Simulation_Core_0
from .ardupilotlight_simulation_core_1 import APL_Simulation_Core_1
from .ardupilotlight_simulation_core_2 import APL_Simulation_Core_2
from enum import Enum

from .ardupilotlight_0 import CraftMode, GameMode
from .ardupilotlight_1 import CraftMode, GameMode
from .ardupilotlight_2 import CraftMode, GameMode


class SimState(Enum):
    STOPPED = 0
    RUNNING = 1


class APL_SIMCommandHandler:
    def __init__(self, craft, telemetry, buf):
        settings.logger.info("APL SIM command handler setup...")
        self.craft = craft
        self.telemetry = telemetry
        self.buffer = buf
        self.core = None
        self.sim_state = SimState.STOPPED

        self.exigisi_flight_uuid = ''

        self.message = None
        self.switch_dict = {
            'QUIT'      : self.quit,
            'OPEN'      : self.open,
            'CLOSE'     : self.close,
            'RESET'     : self.reset,
            'SAVE'      : self.save,
            'NEW'       : self.new_sim,
            'PAUSE'     : self.pause,
            'RESUME'    : self.resume,
            'SESSION'   : self.session,
            'INSTANCE'  : self.instance,
            'PILOT'     : self.pilot,
            'LOAD'      : self.load,
            'CHECKPOINT': self.checkpoint,
            'POSITION_HIKER': self.position_hiker,
            'NIXEL_GENERATE':  self.nixel_generate,
            'VIZ_RECORD': self.viz_record
        }

    def process(self, message):
        self.message = message
        settings.logger.info("...processing command - %s", message[1])
        return self.switch_dict[message[1]]()

    # Core commands

    def quit(self):
        settings.sim_running = False

        # Shutdown core
        self.core = None

        self.sim_state = SimState.STOPPED

        # Reset state of craft
        self.craft.reset()

        return "('ACK', 'MAVSIM WITH ARDUPILOT LIGHT QUIT')"

    def open(self):
        '''
            message[2] = source uuid  : What to open
            message[3] = extension uuid  : Unique identification for the continued flight
        '''

        if self.sim_state == SimState.RUNNING:
            return "('ERR', 'MAVSIM WITH ARDUPILOT LIGHT CURRENTLY RUNNING, CLOSE TO STOP BEFORE OPENING A SCENARIO')"

        try:

            # Reset state of craft
            settings.logger.info('Getting the craft ready on an open...')
            self.craft.reinit()

            # Find the instance of the saved flight and return a tuple (scenario)
            val = self.buffer.exigisi_extend_flight_apl(self.message[2])
            scenario = val[0]
            ss_uuid = val[1]

            settings.logger.info('Found the base flight for scenario: %s with sim session uuid: %s' % (scenario, ss_uuid))

            if ss_uuid is None or scenario is None:
                # raise Exception('Do not have a simulation session uuid for the base flight???')
                return "('ERR', 'OPEN', 'MAVSIM WITH ARDUPILOT LIGHT CANNOT OPEN THE SPECIFIED SCENARIO')"

            # Extend flight restores craft state to last recorded
            self.exigisi_flight_uuid = self.buffer.exigisi_open_flight_apl(scenario, self.message[3], ss_uuid, None)
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))

            self.close('Potential problematic flight')

            return "('ERR', 'OPEN', '%s')" % str.upper(str(v))

        if (self.core is not None) and (scenario == self.core.scenario):
            settings.logger.info('Resetting simulation core...')
            self.core.reset(self.message[3], self.exigisi_flight_uuid)
        else:
            if 'base' in self.message[3].lower():
                settings.is_nixel_world = False
            else:
                settings.is_nixel_world = True
                # Check if nixel world exists (name must be DNA string or it wont match) continue if it does, generate if not
                settings.nwm.single_generate(scenario)

            settings.logger.info('Starting new simulation core...')
            if settings.sim_op_state == 0:
                self.core = APL_Simulation_Core_0(self.craft, self.telemetry, self.buffer, scenario, self.message[3], False,
                                            self.exigisi_flight_uuid)
            if settings.sim_op_state == 1:
                if 'kingdom' in scenario:
                    return "('ERR', 'CANNOT USE KINGDOM MAPS WITH SIM OP STATE 1 (ELEVATION PHYSICS INCOMPATIBLE)')"
                self.core = APL_Simulation_Core_1(self.craft, self.telemetry, self.buffer, scenario, self.message[3], False,
                                            self.exigisi_flight_uuid)
            if settings.sim_op_state == 2:
                if 'kingdom' in scenario:
                    return "('ERR', 'CANNOT USE KINGDOM MAPS WITH SIM OP STATE 2 (ELEVATION PHYSICS INCOMPATIBLE)')"
                self.core = APL_Simulation_Core_2(self.craft, self.telemetry, self.buffer, scenario, self.message[3], False,
                                            self.exigisi_flight_uuid)

        settings.logger.info('Restoring craft state...')
        self.buffer.restore_craft_state(self.craft, ss_uuid)

        # Restore game moves
        self.core.restore_move_queue(self.craft.plan)
        self.craft.plan = []

        self.sim_state = SimState.RUNNING
        settings.sim_running = True

        return "('ACK', 'MAVSIM WITH ARDUPILOT LIGHT SCENARIO OPENED AND STARTED')"

    def close(self, name = None):
        '''
           message[2] = name  : a description of the flight just flown
        '''
        if self.sim_state == SimState.STOPPED:
            return "('ERR', 'MAVSIM WITH ARDUPILOT LIGHT CURRENTLY STOPPED ALREADY')"

        # Name flight
        if len(self.message) > 2:
            name = self.message[2]

        # Shutdown core
        self.core.release()
        self.core = None

        self.sim_state = SimState.STOPPED

        # Reset state of craft
        self.craft.reset()

        self.buffer.exigisi_close_flight_apl(self.exigisi_flight_uuid, name)

        return "('ACK', 'MAVSIM WITH ARDUPILOT LIGHT SCENARIO STOPPED')"

    def save(self):
        '''
           message[2] = description  : Description of the checkpoint just saved
           message[3] = extension uuid  : Unique identification for the continued flight
        '''
        if self.sim_state == SimState.STOPPED:
            return "('ERR', 'MAVSIM WITH ARDUPILOT LIGHT *NOT* CURRENTLY RUNNING, NEW TO START')"

        if self.core is None:
            return "('ERR', 'CANNOT SAVE DUE TO NO CURRENT SIMULATION LOADED. NEW FIRST THEN SAVE.')"

        try:
            name = self.message[2]
            current_ss_uuid = settings.simulation_session_uuid

            if current_ss_uuid is None:
                raise Exception('Cannot find current simulation sessions UUID???')

            self.buffer.exigisi_close_flight_apl(self.exigisi_flight_uuid, name)

            self.core.session = settings.simulation_session_uuid = str(uuid.uuid4())
            self.core.note = self.message[3]

            self.exigisi_flight_uuid = self.buffer.exigisi_open_flight_apl(self.core.scenario, self.message[3], current_ss_uuid, None)
            self.buffer.exigisi_add_sim_session_uuid_to_flight_apl(self.exigisi_flight_uuid, settings.simulation_session_uuid)
            self.core.flight_uuid = self.exigisi_flight_uuid
            self.core.db_record_uuid = self.buffer.mark_start_of_APL_simulation(self.core.scenario, self.core.session, self.core.note)

            return "('ACK', 'MAVSIM WITH ARDUPILOT LIGHT SCENARIO SAVED', '" + name + "')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))

            self.close('Potential problematic flight')

            return "('ERR', 'SAVE', '%s')" % str.upper(str(v))

    def new_sim(self):
        '''
            message[2] = scenario  : Specify the scenario base from the repository to use to create this new scenario instance
            message[3] = uuid  : Unique identification for the flight
        '''
        if self.sim_state == SimState.RUNNING:
            return "('ERR', 'MAVSIM WITH ARDUPILOT LIGHT CURRENTLY RUNNING, CLOSE TO STOP')"

        try:
            # Reset state of craft
            settings.logger.info('Getting the craft ready...')
            self.craft.reinit()

            self.exigisi_flight_uuid = self.buffer.exigisi_open_flight_apl(self.message[2], self.message[3], None, None)

            #self.core = APL_Simulation_Core(self.craft, self.telemetry, self.buffer, self.message[2], self.message[3], True, self.exigisi_flight_uuid)

            if (self.core is not None) and (self.message[2] == self.core.scenario):
                settings.logger.info('Resetting simulation core...')
                self.core.reset(self.message[3], self.exigisi_flight_uuid)
            else:
                settings.logger.info('Starting new simulation core...')
                if 'base' in self.message[2].lower():
                    settings.is_nixel_world = False
                else:
                    settings.is_nixel_world = True
                    settings.nwm.single_generate(self.message[2])

                if settings.sim_op_state == 0:
                    self.core = APL_Simulation_Core_0(self.craft, self.telemetry, self.buffer, self.message[2], self.message[3], True,
                                                self.exigisi_flight_uuid)
                if settings.sim_op_state == 1:
                    if 'base' in self.message[2].lower():
                        return "('ERR', 'CANNOT USE KINGDOM MAPS WITH SIM OP STATE 1 (ELEVATION PHYSICS INCOMPATIBLE)')"
                    self.core = APL_Simulation_Core_1(self.craft, self.telemetry, self.buffer, self.message[2], self.message[3], True,
                                                self.exigisi_flight_uuid)
                if settings.sim_op_state == 2:
                    if 'base' in self.message[2].lower():
                        return "('ERR', 'CANNOT USE KINGDOM MAPS WITH SIM OP STATE 2 (ELEVATION PHYSICS INCOMPATIBLE)')"
                    self.core = APL_Simulation_Core_2(self.craft, self.telemetry, self.buffer, self.message[2], self.message[3], True,
                                                self.exigisi_flight_uuid)

            self.sim_state = SimState.RUNNING
            settings.sim_running = True

            self.core.move_history.append((self.craft.x - settings.aoi_x, self.craft.y - settings.aoi_y, self.craft.altitude, self.craft.yaw))

            return "('ACK', 'MAVSIM WITH ARDUPILOT LIGHT NEW SCENARIO STARTED')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))

            self.close('Potential problematic flight')

            return "('ERR', 'NEW', '%s')" % str.upper(str(v))

    def nixel_generate(self):
        '''
            message[2]: dna_string = dna string of the scenario to generate
            message[3]: name = name of scenario to generate (optional, default is 'nixel_world')
            message[4]: generators = list of layer generators to use (optional, default is all available layer generators)
        '''
        settings.logger.info("Generating Nixel World")

        if len(message) < 4:
            settings.nwm.single_generate(self.message[2])
        elif len(message) == 4:
            settings.nwm.single_generate(self.message[2], name=self.message[3])
        elif len(message) == 5:
            settings.single_generate(self.message[2], name=self.message[3], generators=self.message[4])
        else:
            return "('ERR', 'INCORRECT NUMBER OF ARGUMENTS ({}) FOR NIXEL GENERATOR')".format(len(self.message))
        
        return "('ACK', 'NIXEL GENERATED {}')".format(self.message[2])

    # Not used functionality

    def pause(self):
            return "('ERR', 'MAVSIM WITH ARDUPILOT LIGHT DOES NOT USE PAUSE')"
    
    def resume(self):
            return "('ERR', 'MAVSIM WITH ARDUPILOT LIGHT DOES NOT USE PAUSE, SO DOES NOT RESUME')"

    def reset(self):
            return "('ERR', 'MAVSIM WITH ARDUPILOT LIGHT DOES NOT USE RESET')"

    # Tagging

    def session(self):
        settings.mavsim_session = self.message[2]
        return "('ACK', 'SESSION NAME CHANGE: " + self.message[2] + "')"

    def instance(self):
        settings.mavsim_instance = self.message[2]
        return "('ACK', 'INSTANCE NAME CHANGE: " + self.message[2] + "')"

    def pilot(self):
        settings.mavsim_pilot = self.message[2]
        return "('ACK', 'PILOT NAME CHANGE: " + self.message[2] + "')"

    def position_hiker(self):
        self.core.hiker_x = int(self.message[2])
        self.core.hiker_y = int(self.message[3])
        return "('ACK', 'POSITION_HIKER')"


    # Extended featyres to support ML

    def load(self):
        '''
            message[2] = Craft X (int)
            message[3] = Craft Y (int)
            message[4] = Craft Altitude (int, 0--3)
            message[5] = Craft speed (int, 0--6)
            message[6] = Craft yaw (int, 1--8)
            message[7] = Craft Fuel (int, 0--999999, -1 for FUEL_MAX)
            message[8] = Craft Armed (boolean as a string, "True" or "False")
            message[9] = Craft Mode (int, MANUAL = 0, GUIDED = 1, AUTO = 2, RTL = 3, LOITER = 4, UNKNOWN = 5)
            message[10] = Craft Payload (list of strings, ["EMPTY", "EMPTY", "EMPTY", "EMPTY"])
            message[11] = Game Mode (int, NONE = 0, LOST_HIKER = 1, COUNT_BEARS = 2, COUNT_DEER = 3, COUNT_ANIMALS = 4)
            message[12] = Found Hiker (boolean as a string, "True" or "False") Has the hiker already been located?

            message[13] = Number of Successful Package Drops (int)
            message[14] = Dropped List (list of strings)
            message[15] = Craft plan (list of coordinate step tuples)
        '''
        try:
            name = 'Saved flight forced by load'
            current_ss_uuid = settings.simulation_session_uuid

            if current_ss_uuid is None:
                raise Exception('Cannot find current simulation sessions UUID???')

            self.buffer.exigisi_close_flight_apl(self.exigisi_flight_uuid, name)

            self.core.session = settings.simulation_session_uuid = str(uuid.uuid4())
            self.core.note = str(settings.simulation_session_uuid)

            self.exigisi_flight_uuid = self.buffer.exigisi_open_flight_apl(self.core.scenario, self.core.note,
                                                                           current_ss_uuid, None)
            self.buffer.exigisi_add_sim_session_uuid_to_flight_apl(self.exigisi_flight_uuid,
                                                                   settings.simulation_session_uuid)
            self.core.flight_uuid = self.exigisi_flight_uuid
            self.core.db_record_uuid = self.buffer.mark_start_of_APL_simulation(self.core.scenario, self.core.session,
                                                                                self.core.note)

            self.core.reset(name, self.exigisi_flight_uuid)

            # Reset state of craft
            settings.logger.info('Getting the craft ready for a new state (load)...')
            self.craft.reinit()

            self.craft.x = self.message[2]
            self.craft.y = self.message[3]
            self.craft.altitude = self.message[4]
            self.craft.speed = self.message[5]
            self.craft.yaw = self.message[6]

            if self.message[7] == -1:
                self.craft.fuel = settings.FUEL_MAX
            else:
                self.craft.fuel = self.message[7]

            self.craft.armed = eval(str(self.message[8])) # Boolean

            # class CraftMode(IntEnum):
            #     MANUAL = 0
            #     GUIDED = 1
            #     AUTO = 2
            #     RTL = 3
            #     LOITER = 4
            #     UNKNOWN = 5
            if self.message[9] == 0:
                self.craft.mode = CraftMode.MANUAL
            elif self.message[9] == 1:
                self.craft.mode = CraftMode.GUIDED
            elif self.message[9] == 2:
                self.craft.mode = CraftMode.AUTO
            elif self.message[9] == 3:
                self.craft.mode = CraftMode.RTL
            elif self.message[9] == 4:
                self.craft.mode = CraftMode.LOITER
            else:
                self.craft.mode = CraftMode.UNKNOWN

            # Payload
            self.craft.payload = eval(str(self.message[10]))  # ["EMPTY", "EMPTY", "EMPTY", "EMPTY"]

            # class GameMode(IntEnum):
            #     NONE = 0
            #     LOST_HIKER = 1
            #     COUNT_BEARS = 2
            #     COUNT_DEER = 3
            #     COUNT_ANIMALS = 4
            # self.game_mode = GameMode.LOST_HIKER
            if self.message[11] == 0:
                self.craft.game_mode = GameMode.NONE
            elif self.message[11] == 1:
                self.craft.game_mode = GameMode.LOST_HIKER
            elif self.message[11] == 2:
                self.craft.game_mode = GameMode.COUNT_BEARS
            elif self.message[11] == 3:
                self.craft.game_mode = GameMode.COUNT_DEER
            elif self.message[11] == 4:
                self.craft.game_mode = GameMode.COUNT_ANIMALS

            self.craft.found_hiker = eval(str(self.message[12])) # Boolean
            self.craft.successful_package_drops = self.message[13]
            self.craft.dropped_list =  eval(str(self.message[14])) # List

            self.craft.plan = eval(str(self.message[15]))

            self.telemetry.telereceiver(
                "INITIAL_STATE {x : %d, y : %d, z : %d, h : %d, p : %d, r : %d, speed : %d, fuel : %d, armed : %d, mode : %d}" %
                (self.craft.x, self.craft.y, self.craft.altitude, self.craft.yaw, self.craft.pitch, self.craft.roll,
                 self.craft.speed, self.craft.fuel, int(self.craft.armed), self.craft.mode))

            self.telemetry.telereceiver("YOUR_TURN")
            self.core.move_history.append((self.craft.x - settings.aoi_x, self.craft.y - settings.aoi_y, self.craft.altitude, self.craft.yaw))

        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Error: %s - %s" % (e, str(v)))

            self.close('Potential problematic flight')

            return "('ERR', 'LOAD', '%s')" % str.upper(str(v))


        return "('ACK', 'MAVSIM WITH ARDUPILOT LIGHT SCENARIO LOADED and RUNNING')"

    def checkpoint(self):
        '''

        '''
        val = "%d, %d, %d, %d, %d, %d, %s, %d, %s, %d, %s, %d, %s, %s" % (self.craft.x,
                                                                         self.craft.y,
                                                                         self.craft.altitude,
                                                                         self.craft.speed,
                                                                         self.craft.yaw,
                                                                         self.craft.fuel,
                                                                         str(self.craft.armed),
                                                                         self.craft.mode,
                                                                         str(self.craft.payload),
                                                                         self.craft.game_mode,
                                                                         str(self.craft.found_hiker),
                                                                         self.craft.successful_package_drops,
                                                                         str(self.craft.dropped_list),
                                                                         str(self.craft.plan))

        return "('ACK', '%s')" % val

    def viz_record(self):
        vr_val = int(self.message[2])
        vr_mode_val = int(self.message[3])

        if 0 <= vr_val <= 1:
            settings.visual_record = vr_val
            settings.visual_record_mode = vr_mode_val
            return "('ACK', 'VIZ_RECORD')"
        else:
            return "('ERR', 'VIZ_RECORD')"

# fin
