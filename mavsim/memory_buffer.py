# -----------------------------------------------------------------------------
#  MAVSim::Memory Buffer
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

import sys
import uuid
import yaml
import threading
import datetime

from .database_interface import *
from dateutil import parser

from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound

# How to add new commands to processing:
#   1. Create a new database inherited class in database_interface.py
#   2. Add a stored message _msg variable in the __init__ constructor
#   3. Add a line for the message switch to the _message function in the
#      self.switchDict in the __init__ constructor
#   4. Add an appropriate line in the class attribute_dict
#   5. Create a _message function to pass the correct parameters and
#      instantiate a database entry object for process_message()
#

global db_critical_section
db_critical_section = False

class MemoryBuffer:
    def __init__(self, comms, telemetry_cb):
        settings.logger.info("Memory Buffer setup...")
        self.start = datetime.datetime.now()
        self.lock = threading.RLock()
        self.telemetry_cb = telemetry_cb

        # External database connector
        if settings.database_url is not None:
            self.dbase = Database(settings.database_url)
        else:
            self.dbase = None
            settings.logger.warning(
                "No database URL set, so cannot log to a database!")

        # Stored messages
        self.simstate_msg = None
        self.sys_status_msg = None
        self.raw_imu_msg = None
        self.scaled_pressure_msg = None
        self.meminfo_msg = None
        self.mission_current_msg = None
        self.gps_raw_int_msg = None
        self.global_position_int_msg = None
        self.servo_output_raw_msg = None
        self.rc_channels_raw_msg = None
        self.attitude_msg = None
        self.ahrs_msg = None
        self.hwstatus_msg = None
        self.wind_msg = None
        self.system_time_msg = None
        self.battery_status_msg = None
        self.vfr_hud_msg = None
        self.param_set_msg = None
        self.heartbeat_msg = None
        self.sensor_offsets_msg = None
        self.scaled_imu2_msg = None
        self.power_status_msg = None
        self.terrain_report_msg = None
        self.local_position_ned_msg = None
        self.rc_channels_msg = None
        self.vibration_msg = None
        self.ekf_status_report_msg = None
        self.ahrs2_msg = None
        self.ahrs3_msg = None
        self.home_position_msg = None
        self.param_value_msg = None
        self.statustext_msg = None
        self.mission_ack_msg = None
        self.command_ack_msg = None
        self.airspeed_autocal_msg = None
        self.nav_controller_output_msg = None
        self.position_target_global_int_msg = None
        self.mission_request_msg = None
        self.mission_item_reached_msg = None
        self.mission_count_msg = None
        self.mission_item_int_msg = None
        self.mission_item_msg = None
        self.autopilot_version_msg = None
        self.fence_point_msg = None
        self.rally_point_msg = None
        self.camera_feedback_msg = None
        self.terrain_request_msg = None
        self.payload_drop_msg = None
        self.payload_load_msg = None
        self.payload_hit_msg = None
        self.craft_crash_msg = None
        self.sensed_object_msg = None
        self.airspeed_msg = None
        self.fuel_msg = None
        self.goal_msg = None
        self.initial_state_msg = None
        self.scenario_msg = None
        self.state_change_msg = None
        self.region_msg = None
        self.game_score_msg = None
        self.camera_roll_msg = None
        self.area_of_interest_msg = None

        # LOCAL
        self.last_update = ""
        self.last_message = None
        self.communication_link = comms
        self.messages_processed = 0
        self.messages_processed_total = 0
        self.messages_stored = 0
        self.messages_ignored = 0

        # Message count for stored messages
        self.simstate_mcount = 0
        self.attitude_mcount = 0
        self.vfr_hud_mcount = 0
        self.gps_raw_int_mcount = 0
        self.global_position_int_mcount = 0
        self.batter_status_mcount = 0
        self.mission_ack_mcount = 0
        self.mission_item_reached_mcount = 0
        self.heartbeat_mcount = 0
        self.param_value_mcount = 0
        self.mission_ack_mcount = 0
        self.command_ack_mcount = 0
        self.mission_count_mcount = 0

        # Stored messages for other modules to use
        self.simstate_buf = None
        self.attitude_buf = None
        self.vfr_hud_buf = None
        self.gps_raw_int_buf = None
        self.global_position_int_buf = None
        self.battery_status_buf = None
        self.mission_ack_buf = None
        self.command_ack_buf = None
        self.mission_item_reached_buf = None
        self.heartbeat_buf = None
        self.param_value_buf = None
        self.mission_count_buf = None

        self.current_apl_mission = ""

        self.received_command = ''
        self.sent_response = ''

        # I originally tried to introspect on type, but the mymavlink library
        # can be flaky now "switching" on first keyword. This reduces
        # dependency on pymavlink and its changes
        #
        self.switchDict = {'SYS_STATUS': self.sys_status_message,
                           'RAW_IMU': self.raw_imu_message,
                           'SCALED_PRESSURE': self.scaled_pressure_message,
                           'MEMINFO': self.meminfo_message,
                           'MISSION_CURRENT': self.mission_current_message,
                           'GPS_RAW_INT': self.gps_raw_int_message,
                           'GLOBAL_POSITION_INT': self.global_position_int_message,
                           'SERVO_OUTPUT_RAW': self.servo_output_raw_message,
                           'RC_CHANNELS_RAW': self.rc_channels_raw_message,
                           'ATTITUDE': self.attitude_message,
                           'AHRS': self.ahrs_message,
                           'HWSTATUS': self.hwstatus_message,
                           'WIND': self.wind_message,
                           'SYSTEM_TIME': self.system_time_message,
                           'BATTERY_STATUS': self.battery_status_message,
                           'VFR_HUD': self.vfr_hud_message,
                           'PARAM_SET': self.param_set_message,
                           'HEARTBEAT': self.heartbeat_message,
                           'SENSOR_OFFSETS': self.sensor_offsets_message,
                           'SCALED_IMU2': self.scaled_imu2_message,
                           'POWER_STATUS': self.power_status_message,
                           'TERRAIN_REPORT': self.terrain_report_message,
                           'LOCAL_POSITION_NED': self.local_position_ned_message,
                           'RC_CHANNELS': self.rc_channels_message,
                           'VIBRATION': self.vibration_message,
                           'EKF_STATUS_REPORT': self.ekf_status_report_message,
                           'AHRS2': self.ahrs2_message,
                           'AHRS3': self.ahrs3_message,
                           'HOME_POSITION': self.home_position_message,
                           'PARAM_VALUE': self.param_value_message,
                           'STATUSTEXT': self.statustext_message,
                           'MISSION_ACK': self.mission_ack_message,
                           'COMMAND_ACK': self.command_ack_message,
                           'AIRSPEED_AUTOCAL': self.airspeed_autocal_message,
                           'NAV_CONTROLLER_OUTPUT': self.nav_controller_output_message,
                           'POSITION_TARGET_GLOBAL_INT': self.position_target_global_int_message,
                           'MISSION_REQUEST': self.mission_request_message,
                           'MISSION_ITEM_REACHED': self.mission_item_reached_message,
                           'MISSION_COUNT': self.mission_count_message,
                           'MISSION_ITEM_INT': self.mission_item_int_message,
                           'MISSION_ITEM': self.mission_item_message,
                           'AUTOPILOT_VERSION': self.autopilot_version_message,
                           'FENCE_POINT': self.fence_point_message,
                           'SIMSTATE': self.simstate_message,
                           'RALLY_POINT': self.rally_point_message,
                           'CAMERA_FEEDBACK': self.camera_feedback_message,
                           'TERRAIN_REQUEST': self.terrain_request_message,
                           'MS_PAYLOAD_DROP': self.payload_drop_message,
                           'MS_PAYLOAD_LOAD': self.payload_load_message,
                           'MS_PAYLOAD_HIT': self.payload_hit_message,
                           'MS_CRAFT_CRASH': self.craft_crash_message,
                           'MS_SENSED_OBJECT': self.sensed_object_message,
                           'MS_MISSION_START': self.store_mission_start_apl,
                           'MS_MISSION_END': self.store_mission_end_apl,
                           'YOUR_TURN' : self.your_turn_message,
                           'AIRSPEED': self.airspeed_message,
                           'FUEL': self.fuel_message,
                           'GOAL': self.goal_message,
                           'INITIAL_STATE': self.initial_state_message,
                           'SCENARIO': self.scenario_message,
                           'STATE_CHANGE': self.state_change_message,
                           'REGION': self.region_message,
                           'GAME_SCORE': self.game_score_message,
                           'PIC_TAKEN' : self.pic_taken_message,
                           'CAMERA_ROLL': self.camera_roll_message
                           }

    def filtered(self, filter, message):
        if filter == 0:
            return False
        elif filter == 1:
            if 'GLOBAL_POSITION_INT' in message:
                return False
            elif 'GPS_RAW_INT' in message:
                return False
            elif 'MS_' in message:
                return False
            elif 'MISSION_' in message:
                return False
            elif 'COMMAND_' in message:
                return False
            elif 'HOME_POSITION' in message:
                return False
            elif 'TERRAIN' in message:
                return False
            elif 'HEARTBEAT' in message:
                return False
            elif 'POSITION_TARGET_GLOBAL_INT' in message:
                return False
            elif 'MISSION_CURRENT' in message:
                return False
            elif 'ATTITUDE' in message:
                return False
            else:
                return True
        else:
            return True

    def targetcast(self, message):
        # Send message to the local callback client if connected
        if self.telemetry_cb is not None:
            settings.logger.debug("MAVSim Telemetry Sending -- %s" % message)
            self.telemetry_cb(message)

        # First check to see if clients to send to
        if len(settings.telemetry_direct_clients) <= 0:
            return

        # Look into the global list of udp clients and send them the message
        for client in settings.telemetry_direct_clients:
            # Filter to allow only certain types of messages if specified
            if not self.filtered(client.filter, message):
                client.send(message)

        settings.logger.debug("Targetcasting from broadcast -- %s" % message)

    def broadcast_telemetry(self):
        # Right now this is just a passthrough, but can be setup to gate
        # broadcasts on time or parametric changes to the stored data values

        settings.logger.debug(
            "From Memory Buffer sending for broadcast -- %s" % self.last_update)
        #self.communication_link.broadcast(self.last_update)
        self.targetcast(self.last_update)

    def broadcast_message_to_telemetry(self, message):
        settings.logger.warning("broadcast_message_to_telemetry scheduled for deprecation")
        settings.logger.debug(
            "Internal sending for broadcast -- %s" % message)
        #self.communication_link.broadcast(message)
        self.targetcast(message)

    def update_telemetry(self, stream_item):
        global db_critical_section
        while db_critical_section:
            pass

        db_critical_section = True

        # Parse telemetry data and update local values
        self.last_update = str(stream_item)
        self.last_message = stream_item

        # Send out to listeners
        self.broadcast_telemetry()

        # Store into database
        if self.dbase is not None:
            self.store_telemetry(stream_item)

        # Update status for every 1,000 messages processed
        self.messages_processed = self.messages_processed + 1
        if self.messages_processed == 1000:
            self.messages_processed_total = self.messages_processed_total + 1

            if self.dbase is not None:
                settings.logger.info("...Processed %dK messages (%d stored, %d ignored)." % (
                    self.messages_processed_total, self.messages_stored, self.messages_ignored))
            else:
                settings.logger.info("...Processed %dK messages." %
                                     self.messages_processed_total)

            self.messages_processed = 0
            self.messages_stored = 0
            self.messages_ignored = 0

        db_critical_section = False

    # DATABASE Storage Functions ---------------------------------------------------------------------
    #
    def store_command_and_response(self, command, response, details=""):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            db = MavSimCommand()

            db.command = command
            self.received_command = command

            db.ack = response
            self.sent_response = response

            db.details = details

            db.uuid = str(uuid.uuid4())
            db.instance = settings.mavsim_instance
            db.session = settings.mavsim_session
            db.timestamp = datetime.datetime.now()
            db.simulation_session_uuid = settings.simulation_session_uuid

            # Put it in the database
            with self.lock:
                self.dbase.session2.add(db)
                self.dbase.session2.commit()

    def store_custom_log(self, source, log, details):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            db = MavSimOpenLog()

            db.source = source
            db.log = log
            db.details = details

            db.uuid = str(uuid.uuid4())
            db.instance = settings.mavsim_instance
            db.session = settings.mavsim_session
            db.simulation_session_uuid = settings.simulation_session_uuid

            # Put it in the database
            with self.lock:
                self.dbase.session2.add(db)
                self.dbase.session2.commit()

    def store_mission_start(self, name, info, start):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            db = MavSimMission()

            db.name = name
            db.info = info
            db.start = parser.parse(start)

            db.uuid = str(uuid.uuid4())
            db.instance = settings.mavsim_instance
            db.session = settings.mavsim_session
            db.timestamp = datetime.datetime.now()
            db.simulation_session_uuid = settings.simulation_session_uuid

            # Put it in the database
            with self.lock:
                self.dbase.session2.add(db)
                self.dbase.session2.commit()

            return str(db.uuid)

    def store_mission_end(self, uuid, end):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            #settings.logger.info("----- Mission End Setting: %s ~ %s " % (uuid, end))
            db = None

            with self.lock:
                try:
                    db = self.dbase.session2.query(
                        MavSimMission).filter_by(uuid=uuid).first()
                    # settings.logger.info(db)
                except MultipleResultsFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error("Store Mission End Error (MultipleResultsFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with it
                except NoResultFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error("Store Mission End Error (NoResultFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with that as well

            db.end = parser.parse(end)

            # Put it in the database
            with self.lock:
                self.dbase.session2.add(db)
                self.dbase.session2.commit()


    def store_telemetry(self, data):
        if settings.craft_state == settings.CraftState.OFF:
            return

        try:
            settings.logger.debug(
                "Telemetry Received Message Type: %s" % (type(data)))
            self.switchDict[str(data).split(None, 1)[0]]()

        except:  # catch all exceptions
            e, v, t = sys.exc_info()
            settings.logger.error("Store Telemetry Error: %s - %s" % (e, str(v)))
            settings.logger.error(type(data))
            settings.logger.error(data)

    def process_message(self, name, object_class, attribute_list, local_copy):
        # Attributes --
        #   name = 'message name as a string'
        #   object_class = MsgDatabaseObjectInstance()
        #   attribute_list = ['list', 'of', 'attributes']
        #   local_copy = self.type_of_msg
        #
        settings.logger.debug("%s received" % name)

        prep = str(self.last_message).split(None, 1)[1]

        if name == 'statustext_message':
            prep = prep.replace('#', 'No.')
            sep = "text :"
            prep2 = prep.split(sep, 1)
            prep = prep2[0] + sep + prep2[1].replace(':', '')

        msg = yaml.load(prep)
        db = object_class
        for key in attribute_list:
            setattr(db, key, msg[key])

        db.uuid = str(uuid.uuid4())
        db.instance = settings.mavsim_instance
        db.session = settings.mavsim_session
        db.timestamp = datetime.datetime.now()
        db.simulation_session_uuid = settings.simulation_session_uuid

        # Special Cases
        if name == 'battery_status_message':
            setattr(db, 'batt_id', msg['id'])

        status = ""

        if getattr(self, local_copy) != db:
            if self.dbase is None or settings.use_db is False:
                return
            else:
                # Put it in the database
                self.dbase.session.add(db)
                self.dbase.session.commit()

            # Store a local copy
            setattr(self, local_copy, db)

            settings.logger.debug("%s storing %s" % (
                object_class.__class__.__name__, db))
            status = "stored it in the database (+)"
            self.messages_stored = self.messages_stored + 1
        else:
            # Do nothing, it all looks the same
            settings.logger.debug("%s looks the same as last." %
                                  object_class.__class__.__name__)
            status = "ignored it as not a significant difference (o)"
            self.messages_ignored = self.messages_ignored + 1

        settings.logger.debug("Processed %s and %s." % (name, status))

    # Class variable containing the attributes for each MAVLink message type we can process
    attribute_dict = {
        'simstate_message': ['roll', 'pitch', 'yaw', 'xacc', 'yacc', 'zacc', 'xgyro', 'ygyro', 'zgyro', 'lat', 'lng'],
        'sys_status_message': ['onboard_control_sensors_present', 'onboard_control_sensors_enabled', 'onboard_control_sensors_health',
                               'load', 'voltage_battery', 'current_battery', 'battery_remaining', 'drop_rate_comm', 'errors_comm', 'errors_count1',
                               'errors_count2', 'errors_count3', 'errors_count4'],
        'raw_imu_message': ['time_usec', 'xacc', 'yacc', 'zacc', 'xgyro', 'ygyro', 'zgyro', 'xmag', 'ymag', 'zmag'],
        'scaled_pressure_message': ['time_boot_ms', 'press_abs', 'press_diff', 'temperature'],
        'mission_current_message': ['seq'],
        'meminfo_message': ['brkval', 'freemem'],
        'gps_raw_int_message': ['time_usec', 'fix_type', 'lat', 'lon', 'alt', 'eph', 'epv', 'vel', 'cog', 'satellites_visible'],
        'global_position_int_message': ['time_boot_ms', 'lat', 'lon', 'alt', 'relative_alt', 'vx', 'vy', 'vz', 'hdg'],
        'servo_output_raw_message': ['time_usec', 'port', 'servo1_raw', 'servo2_raw', 'servo3_raw', 'servo4_raw', 'servo5_raw', 'servo6_raw', 'servo7_raw', 'servo8_raw'],
        'rc_channels_raw_message': ['time_boot_ms', 'port', 'chan1_raw', 'chan2_raw', 'chan3_raw', 'chan4_raw', 'chan5_raw', 'chan6_raw', 'chan7_raw', 'chan8_raw', 'rssi'],
        'attitude_message': ['time_boot_ms', 'roll', 'pitch', 'yaw', 'rollspeed', 'pitchspeed', 'yawspeed'],
        'ahrs_message': ['omegaIx', 'omegaIy', 'omegaIz', 'accel_weight', 'renorm_val', 'error_rp', 'error_yaw'],
        'hwstatus_message': ['Vcc', 'I2Cerr'],
        'wind_message': ['direction', 'speed', 'speed_z'],
        'system_time_message': ['time_unix_usec', 'time_boot_ms'],
        'battery_status_message': ['battery_function', 'type', 'temperature', 'voltages', 'current_battery', 'current_consumed', 'energy_consumed', 'battery_remaining'],
        'vfr_hud_message': ['airspeed', 'groundspeed', 'heading', 'throttle', 'alt', 'climb'],
        'param_set_message': ['target_system', 'target_component', 'param_id', 'param_value'],
        'heartbeat_message': ['type', 'autopilot', 'base_mode', 'custom_mode', 'system_status', 'mavlink_version'],
        'sensor_offsets_message': ['mag_ofs_x', 'mag_ofs_y', 'mag_ofs_z', 'mag_declination',
                                   'raw_press', 'raw_temp', 'gyro_cal_x', 'gyro_cal_y', 'gyro_cal_z', 'accel_cal_x', 'accel_cal_y', 'accel_cal_z'],
        'scaled_imu2_message': ['time_boot_ms', 'xacc', 'yacc', 'zacc', 'xgyro', 'ygyro', 'zgyro', 'xmag', 'ymag', 'zmag'],
        'power_status_message': ['Vcc', 'Vservo', 'flags'],
        'local_position_ned_message': ['time_boot_ms', 'x', 'y', 'z', 'vx', 'vy', 'vz'],
        'terrain_report_message': ['lat', 'lon', 'spacing', 'terrain_height', 'current_height', 'pending', 'loaded'],
        'rc_channels_message': ['time_boot_ms', 'chancount', 'chan1_raw', 'chan2_raw', 'chan3_raw', 'chan4_raw', 'chan5_raw', 'chan6_raw', 'chan7_raw', 'chan8_raw', 'chan9_raw', 'chan10_raw', 'chan11_raw', 'chan12_raw', 'chan13_raw', 'chan14_raw', 'chan15_raw', 'chan16_raw', 'chan17_raw', 'chan18_raw', 'rssi'],
        'vibration_message': ['time_usec', 'vibration_x', 'vibration_y', 'vibration_z', 'clipping_0', 'clipping_1', 'clipping_2'],
        'ekf_status_report_message': ['flags', 'velocity_variance', 'pos_horiz_variance', 'pos_vert_variance', 'compass_variance', 'terrain_alt_variance'],
        'ahrs2_message': ['roll', 'pitch', 'yaw', 'altitude', 'lat', 'lng'],
        'ahrs3_message': ['roll', 'pitch', 'yaw', 'altitude', 'lat', 'lng', 'v1', 'v2', 'v3', 'v4'],
        'home_position_message': ['latitude', 'longitude', 'altitude', 'x', 'y', 'z', 'q', 'approach_x', 'approach_y', 'approach_z'],
        'param_value_message': ['param_id', 'param_value', 'param_type', 'param_count', 'param_index'],
        'statustext_message': ['severity', 'text'],
        'mission_ack_message': ['target_system', 'target_component', 'type'],
        'command_ack_message': ['command', 'result'],
        'airspeed_autocal_message': ['vx', 'vy', 'vz', 'diff_pressure', 'EAS2TAS', 'ratio', 'state_x', 'state_y', 'state_z', 'Pax', 'Pby', 'Pcz'],
        'nav_controller_output_message': ['nav_roll', 'nav_pitch', 'nav_bearing', 'target_bearing', 'wp_dist', 'alt_error', 'aspd_error', 'xtrack_error'],
        'position_target_global_int_message': ['time_boot_ms', 'coordinate_frame', 'type_mask', 'lat_int', 'lon_int', 'alt', 'vx', 'vy', 'vz', 'afx', 'afy', 'afz', 'yaw', 'yaw_rate'],
        'mission_request_message': ['target_system', 'target_component', 'seq'],
        'mission_item_reached_message': ['seq'],
        'mission_count_message': ['target_system', 'target_component', 'count'],
        'mission_item_int_message': ['target_system', 'target_component', 'seq', 'frame', 'command', 'current', 'autocontinue', 'param1', 'param2', 'param3', 'param4', 'x', 'y', 'z'],
        'mission_item_message': ['target_system', 'target_component', 'seq', 'frame', 'command', 'current', 'autocontinue', 'param1', 'param2', 'param3', 'param4', 'x', 'y', 'z'],
        'autopilot_version_message': ['capabilities', 'flight_sw_version', 'middleware_sw_version', 'os_sw_version', 'board_version', 'flight_custom_version',
                                      'middleware_custom_version', 'os_custom_version', 'vendor_id', 'product_id', 'uid'],
        'fence_point_message': ['target_system', 'target_component', 'idx', 'count', 'lat', 'lng'],
        'rally_point_message': ['target_system', 'target_component', 'idx', 'count', 'lat', 'lng', 'alt', 'break_alt', 'land_dir', 'flags'],
        'camera_feedback_message': ['time_usec', 'target_system', 'cam_idx', 'img_idx', 'lat', 'lng', 'alt_msl', 'alt_rel', 'roll', 'pitch', 'yaw', 'foc_len', 'flags'],
        'terrain_request_message': ['lat', 'lon', 'grid_spacing', 'mask'],
        'payload_drop_message': ['slot', 'item', 'lat', 'lon', 'alt', 'speed'],
        'payload_load_message': ['slot', 'item'],
        'payload_hit_message': ['item', 'lat', 'lon', 'alt', 'status'],
        'craft_crash_message': ['cause', 'lat', 'lon', 'alt', 'speed'],
        'sensed_object_message': ['name', 'lat', 'lon', 'alt', 'radius'],
        'airspeed_message': ['time_boot_ms', 'speed'],
        'fuel_message': ['time_boot_ms', 'level'],
        'goal_message': ['order', 'goal'],
        'initial_state_message': ['x','y','z','h','p','r','speed','fuel','armed','mode'],
        'scenario_message': ['map','description','suuid'],
        'state_change_message' : ['command', 'x', 'y', 'altitude', 'yaw', 'pitch', 'roll', 'speed', 'fuel', 'armed', 'mode', 'time_start', 'home_x', 'home_y', 'loiter_x', 'loiter_y', 'wind_direction', 'in_canyon', 'plan', 'rtl_override', 'craft_state', 'payload', 'payload_limit', 'flight_safety'],
        'region_message' : ['region', 'x', 'y'],
        'game_score_message' : ['total_1', 'change_1', 'reason_1','total_2', 'change_2', 'reason_2', 'total_3', 'change_3', 'reason_3'],
        'pic_taken_message' : ['subject', 'lon', 'lat'],
        'camera_roll_message' : ['hikers', 'deer', 'bears', 'scenery'],
        'area_of_interest_message' : ['x', 'y', 'width', 'height', 'map_name', 'map_string']
    }

    def simstate_message(self):
        self.simstate_buf = yaml.load(str(self.last_message).split(None, 1)[1])
        self.process_message('simstate_message', Simstate(),
                             MemoryBuffer.attribute_dict['simstate_message'], 'simstate_msg')

    def sys_status_message(self):
        self.process_message('sys_status_message', SysStatus(),
                             MemoryBuffer.attribute_dict['sys_status_message'], 'sys_status_msg')

    def raw_imu_message(self):
        self.process_message('raw_imu_message', RawIMU(),
                             MemoryBuffer.attribute_dict['raw_imu_message'], 'raw_imu_msg')

    def scaled_pressure_message(self):
        self.process_message('scaled_pressure_message', ScaledPressure(),
                             MemoryBuffer.attribute_dict['scaled_pressure_message'], 'scaled_pressure_msg')

    def meminfo_message(self):
        self.process_message('meminfo_message', MemInfo(),
                             MemoryBuffer.attribute_dict['meminfo_message'], 'meminfo_msg')

    def mission_current_message(self):
        self.process_message('mission_current_message', MissionCurrent(),
                             MemoryBuffer.attribute_dict['mission_current_message'], 'mission_current_msg')

    def gps_raw_int_message(self):
        self.gps_raw_int_buf = yaml.load(str(self.last_message).split(None, 1)[1])
        self.process_message('gps_raw_int_message', GPSRawInt(),
                             MemoryBuffer.attribute_dict['gps_raw_int_message'], 'gps_raw_int_msg')

    def global_position_int_message(self):
        self.global_position_int_buf = yaml.load(str(self.last_message).split(None, 1)[1])
        self.process_message('global_position_int_message', GlobalPositionInt(),
                             MemoryBuffer.attribute_dict['global_position_int_message'],
                             'global_position_int_msg')
        # Trigger sensing
        settings.sensor_loop += 1
        self.global_position_int_mcount += 1

    def servo_output_raw_message(self):
        self.process_message('servo_output_raw_message',
                             ServoOutputRaw(), MemoryBuffer.attribute_dict['servo_output_raw_message'],
                             'servo_output_raw_msg')

    def rc_channels_raw_message(self):
        self.process_message('rc_channels_raw_message',
                             RCChannelsRaw(), MemoryBuffer.attribute_dict['rc_channels_raw_message'], 'rc_channels_raw_msg')

    def attitude_message(self):
        self.attitude_buf = yaml.load(str(self.last_message).split(None, 1)[1])
        self.process_message('attitude_message', Attitude(), MemoryBuffer.attribute_dict['attitude_message'], 'attitude_msg')

    def ahrs_message(self):
        self.process_message('ahrs_message', AHRS(), MemoryBuffer.attribute_dict['ahrs_message'], 'ahrs_msg')

    def hwstatus_message(self):
        self.process_message('hwstatus_message', HWStatus(), MemoryBuffer.attribute_dict['hwstatus_message'], 'hwstatus_msg')

    def wind_message(self):
        self.process_message('wind_message', Wind(), MemoryBuffer.attribute_dict['wind_message'], 'wind_msg')

    def system_time_message(self):
        self.process_message('system_time_message', SystemTime(),
                             MemoryBuffer.attribute_dict['system_time_message'], 'system_time_msg')

    def battery_status_message(self):
        self.battery_status_buf = yaml.load(str(self.last_message).split(None, 1)[1])
        self.process_message('battery_status_message', BatteryStatus(),
                             MemoryBuffer.attribute_dict['battery_status_message'], 'battery_status_msg')

    def vfr_hud_message(self):
        self.vfr_hud_buf = yaml.load(str(self.last_message).split(None, 1)[1])
        self.process_message('vfr_hud_message', VFRHUD(),
                             MemoryBuffer.attribute_dict['vfr_hud_message'], 'vfr_hud_msg')

    def param_set_message(self):
        self.process_message('param_set_message', ParamSet(),
                             MemoryBuffer.attribute_dict['param_set_message'], 'param_set_msg')

    def heartbeat_message(self):
        self.heartbeat_buf = yaml.load(str(self.last_message).split(None, 1)[1])
        self.process_message('heartbeat_message', Heartbeat(),
                             MemoryBuffer.attribute_dict['heartbeat_message'], 'heartbeat_msg')

    def sensor_offsets_message(self):
        self.process_message('sensor_offsets_message', SensorOffsets(),
                             MemoryBuffer.attribute_dict['sensor_offsets_message'], 'sensor_offsets_msg')

    def scaled_imu2_message(self):
        self.process_message('scaled_imu2_message', ScaledIMU2(),
                             MemoryBuffer.attribute_dict['scaled_imu2_message'], 'scaled_imu2_msg')

    def power_status_message(self):
        self.process_message('power_status_message', PowerStatus(),
                             MemoryBuffer.attribute_dict['power_status_message'], 'power_status_msg')

    def local_position_ned_message(self):
        self.process_message('local_position_ned_message', LocalPositionNED(),
                             MemoryBuffer.attribute_dict['local_position_ned_message'], 'local_position_ned_msg')

    def terrain_report_message(self):
        self.process_message('terrain_report_message', TerrainReport(),
                             MemoryBuffer.attribute_dict['terrain_report_message'], 'terrain_report_msg')

    def rc_channels_message(self):
        self.process_message('rc_channels_message', RCChannels(),
                             MemoryBuffer.attribute_dict['rc_channels_message'], 'rc_channels_msg')

    def vibration_message(self):
        self.process_message('vibration_message', Vibration(),
                             MemoryBuffer.attribute_dict['vibration_message'], 'vibration_msg')

    def ekf_status_report_message(self):
        self.process_message('ekf_status_report_message', EKFStatusReport(),
                             MemoryBuffer.attribute_dict['ekf_status_report_message'], 'ekf_status_report_msg')

    def ahrs2_message(self):
        self.process_message('ahrs2_message', AHRS2(),
                             MemoryBuffer.attribute_dict['ahrs2_message'], 'ahrs2_msg')

    def ahrs3_message(self):
        self.process_message('ahrs3_message', AHRS3(),
                             MemoryBuffer.attribute_dict['ahrs3_message'], 'ahrs3_msg')

    def home_position_message(self):
        self.process_message('home_position_message', HomePosition(),
                             MemoryBuffer.attribute_dict['home_position_message'], 'home_position_msg')

    def param_value_message(self):
        msg = str(self.last_message).split(None, 1)[1]
        self.param_value_buf = yaml.load(msg)
        self.process_message('param_value_message', ParamValue(),
                             MemoryBuffer.attribute_dict['param_value_message'], 'param_value_msg')

    def statustext_message(self):
        self.process_message('statustext_message', StatusText(),
                             MemoryBuffer.attribute_dict['statustext_message'], 'statustext_msg')

    def mission_ack_message(self):
        self.mission_ack_buf = yaml.load(str(self.last_message).split(None, 1)[1])
        self.process_message('mission_ack_message', MissionACK(),
                             MemoryBuffer.attribute_dict['mission_ack_message'], 'mission_ack_msg')

    def command_ack_message(self):
        self.command_ack_buf = yaml.load(str(self.last_message).split(None, 1)[1])
        self.process_message('command_ack_message', CommandACK(),
                             MemoryBuffer.attribute_dict['command_ack_message'], 'command_ack_msg')

    def airspeed_autocal_message(self):
        self.process_message('airspeed_autocal_message', AirspeedAutocal(),
                             MemoryBuffer.attribute_dict['airspeed_autocal_message'], 'airspeed_autocal_msg')

    def nav_controller_output_message(self):
        self.process_message('nav_controller_output_message', NavControllerOutput(),
                             MemoryBuffer.attribute_dict['nav_controller_output_message'],
                             'nav_controller_output_msg')

    def position_target_global_int_message(self):
        self.process_message('position_target_global_int_message', PositionTargetGlobalInt(),
                             MemoryBuffer.attribute_dict['position_target_global_int_message'],
                             'position_target_global_int_msg')

    def mission_request_message(self):
        self.process_message('mission_request_message', MissionRequest(),
                            MemoryBuffer.attribute_dict['mission_request_message'], 'mission_request_msg')

    def mission_item_reached_message(self):
        self.mission_item_reached_buf = yaml.load(str(self.last_message).split(None, 1)[1])
        self.process_message('mission_item_reached_message', MissionItemReached(),
                             MemoryBuffer.attribute_dict['mission_item_reached_message'],
                             'mission_item_reached_msg')

    def mission_count_message(self):
        self.mission_count_buf = yaml.load(str(self.last_message).split(None, 1)[1])
        self.process_message('mission_count_message', MissionCount(
        ), MemoryBuffer.attribute_dict['mission_count_message'], 'mission_count_msg')
        self.mission_count_mcount += 1

    def mission_item_int_message(self):
        self.process_message('mission_item_int_message', MissionItemInt(
        ), MemoryBuffer.attribute_dict['mission_item_int_message'], 'mission_item_int_msg')

    def mission_item_message(self):
        self.process_message('mission_item_message', MissionItem(),
                             MemoryBuffer.attribute_dict['mission_item_message'], 'mission_item_msg')

    def autopilot_version_message(self):
        self.process_message('autopilot_version_message', AutopilotVersion(),
                             MemoryBuffer.attribute_dict['autopilot_version_message'],
                             'autopilot_version_msg')

    def fence_point_message(self):
        self.process_message('fence_point_message', FencePoint(),
                             MemoryBuffer.attribute_dict['fence_point_message'], 'fence_point_msg')

    def rally_point_message(self):
        self.process_message('rally_point_message', RallyPoint(),
                             MemoryBuffer.attribute_dict['rally_point_message'], 'rally_point_msg')

    def camera_feedback_message(self):
        self.process_message('camera_feedback_message', CameraFeedback(),
                             MemoryBuffer.attribute_dict['camera_feedback_message'],
                             'camera_feedback_msg')

    def terrain_request_message(self):
        self.process_message('terrain_request_message', TerrainRequest(),
                             MemoryBuffer.attribute_dict['terrain_request_message'],
                             'terrain_request_msg')

    def payload_drop_message(self):
        self.process_message('payload_drop_message', PayloadDrop(),
                         MemoryBuffer.attribute_dict['payload_drop_message'],
                         'payload_drop_msg')

    def payload_load_message(self):
        self.process_message('payload_load_message', PayloadLoad(),
                         MemoryBuffer.attribute_dict['payload_load_message'],
                         'payload_load_msg')

    def payload_hit_message(self):
        self.process_message('payload_hit_message', PayloadHit(),
                         MemoryBuffer.attribute_dict['payload_hit_message'],
                         'payload_hit_msg')

    def craft_crash_message(self):
        self.process_message('craft_crash_message', CraftCrash(),
                         MemoryBuffer.attribute_dict['craft_crash_message'],
                         'craft_crash_msg')

    def sensed_object_message(self):
        self.process_message('sensed_object_message', SensedObject(),
                         MemoryBuffer.attribute_dict['sensed_object_message'],
                         'sensed_object_msg')

    def airspeed_message(self):
        self.process_message('airspeed_message', Airspeed(),
                         MemoryBuffer.attribute_dict['airspeed_message'],
                         'airspeed_msg')

    def fuel_message(self):
        self.process_message('fuel_message', Fuel(),
                         MemoryBuffer.attribute_dict['fuel_message'],
                         'fuel_msg')

    def goal_message(self):
        self.process_message('goal_message', Goal(),
                         MemoryBuffer.attribute_dict['goal_message'],
                         'goal_msg')

    def initial_state_message(self):
        self.process_message('initial_state_message', InitialState(),
                         MemoryBuffer.attribute_dict['initial_state_message'],
                         'initial_state_msg')

    def scenario_message(self):
        self.process_message('scenario_message', Scenario(),
                         MemoryBuffer.attribute_dict['scenario_message'],
                         'scenario_msg')

    def state_change_message(self):
        self.process_message('state_change_message', StateChange(),
                         MemoryBuffer.attribute_dict['state_change_message'],
                         'state_change_msg')

    def region_message(self):
        self.process_message('region_message', Region(),
                         MemoryBuffer.attribute_dict['region_message'],
                         'region_msg')

    def game_score_message(self):
        self.process_message('game_score_message', GameScore(),
                         MemoryBuffer.attribute_dict['game_score_message'],
                         'game_score_msg')
    
    def pic_taken_message(self):
        self.process_message('pic_taken_message', PicTaken(),
                         MemoryBuffer.attribute_dict['pic_taken_message'],
                         'pic_taken_message')

    def camera_roll_message(self):
        self.process_message('camera_roll_message', CameraRoll(),
                         MemoryBuffer.attribute_dict['camera_roll_message'],
                         'camera_roll_msg')

    def area_of_interest_message(self):
        self.process_message('area_of_interest_message', AOI(),
                             MemoryBuffer.attribute_dict['area_of_interest_message'],
                             'area_of_interest_msg')

    #
    # Structures for APL Simulation
    #
    def mark_start_of_APL_simulation(self, scenario, session, note):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            db = SimulationSession()

            db.begin = datetime.datetime.now()
            db.scenario = scenario
            db.session_uuid = session

            db.aoi_x = settings.aoi_x
            db.aoi_y = settings.aoi_y
            db.aoi_w = settings.aoi_x_extent
            db.aoi_h = settings.aoi_y_extent
            db.aoi_map_name = scenario

            db.uuid = str(uuid.uuid4())
            db.instance = settings.mavsim_instance
            db.session = settings.mavsim_session
            db.timestamp = datetime.datetime.now()
            db.simulation_session_uuid = settings.simulation_session_uuid
            db.note = note

            # Put it in the database
            with self.lock:
                self.dbase.session2.add(db)
                self.dbase.session2.commit()

            return str(db.uuid)

    def mark_end_of_APL_simulation(self, uuid):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            db = None

            with self.lock:
                try:
                    db = self.dbase.session2.query(SimulationSession).filter_by(uuid=uuid).first()
                    # settings.logger.info(db)
                except MultipleResultsFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error("Simulation Session End Error (MultipleResultsFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with it
                except NoResultFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error("Simulation Session End Error (NoResultFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with that as well

            db.end = datetime.datetime.now()

            # Put it in the database
            with self.lock:
                self.dbase.session2.add(db)
                self.dbase.session2.commit()

    def store_area_of_interest(self, name, map):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            db = AOI()

            db.x = settings.aoi_x
            db.y = settings.aoi_y
            db.width = settings.aoi_x_extent
            db.height = settings.aoi_y_extent
            db.map_name = name
            db.map_string = str(map)

            db.uuid = uuid.uuid4()
            db.start_timestamp = datetime.datetime.now()
            db.stop_timestamp = datetime.datetime.now()
            db.timestamp = datetime.datetime.now()
            db.simulation_session_uuid = settings.simulation_session_uuid

        with self.lock:
            self.dbase.session2.add(db)
            self.dbase.session2.commit()

    def store_mission_start_apl(self):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            settings.logger.info("----- APL Mission Start")

            db = MavSimMission()

            prep = str(self.last_message).split(None, 1)[1]
            if settings.is_nixel_world:     # Make Nixel DNA string parsable by yaml
                prep = prep.replace("'[", "\"[")
                prep = prep.replace("]'", "]\"")

            # TODO: Make more robust as folks could send wonky stuff in info to break parsing
            msg = yaml.load(prep)

            for key in ['name', 'info']:
                setattr(db, key, msg[key])

            db.start = datetime.datetime.now()

            db.uuid = str(uuid.uuid4())
            db.instance = settings.mavsim_instance
            db.session = settings.mavsim_session
            db.timestamp = datetime.datetime.now()
            db.simulation_session_uuid = settings.simulation_session_uuid

            self.current_apl_mission = str(db.uuid)

            # Put it in the database
            with self.lock:
                self.dbase.session2.add(db)
                self.dbase.session2.commit()

            return str(db.uuid)

    def store_mission_end_apl(self):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            settings.logger.info("----- APL Mission End")
            db = None

            with self.lock:
                try:
                    db = self.dbase.session2.query(
                        MavSimMission).filter_by(uuid=self.current_apl_mission).first()
                    # settings.logger.info(db)
                except MultipleResultsFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error(
                        "Store Mission End Error (MultipleResultsFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with it
                except NoResultFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error("Store Mission End Error (NoResultFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with that as well

            if db is not None:
                db.end = datetime.datetime.now()

            # Put it in the database
            with self.lock:
                self.dbase.session2.add(db)
                self.dbase.session2.commit()

    def your_turn_message(self):
        pass

    def exigisi_open_flight_apl(self, scenario, uuid_val, prior, name):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            settings.logger.info("----- APL Exigisi NEW Flight Open")
            settings.logger.info("---------- (%s, %s, %s, %s" % (scenario, uuid_val, prior, name))
            db = Flights()

            db.uuid = uuid_val
            db.instance = settings.mavsim_instance
            db.session = settings.mavsim_session
            db.display_name = (str(datetime.datetime.now()) + '--' + scenario + '-' + settings.mavsim_instance + '-'+ settings.mavsim_session)
            db.pilot = settings.mavsim_pilot
            db.scenario = scenario

            db.timestamp = datetime.datetime.now()
            db.start_timestamp = datetime.datetime.now()
            db.prior_flight = prior
            db.name = name


            # Put it in the database
            try:
                with self.lock:
                    self.dbase.session2.add(db)
                    self.dbase.session2.commit()
            except:
                self.dbase.session2.rollback()
                db.uuid = str(uuid.uuid4())

                with self.lock:
                    self.dbase.session2.add(db)
                    self.dbase.session2.commit()

        return db.uuid

    def exigisi_extend_flight_apl(self, uuid):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            settings.logger.info("----- APL Exigisi Flight Open for Extension")

            with self.lock:
                try:
                    db = self.dbase.session2.query(Flights).filter_by(uuid=uuid).first()
                    # settings.logger.info(db)
                except MultipleResultsFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error(
                        "APL Exigisi Flight Open for Extension Error (MultipleResultsFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with it
                except NoResultFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error("APL Exigisi Flight Open for Extension Error (NoResultFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with that as well

            if db is None:
                with self.lock:
                    try:
                        db = self.dbase.session2.query(Flights).filter_by(simulation_session_uuid=uuid).first()
                        # settings.logger.info(db)
                    except MultipleResultsFound:
                        e, v, t = sys.exc_info()
                        settings.logger.error(
                            "APL Exigisi Flight Open for Extension Error (MultipleResultsFound): %s - %s" % (e, str(v)))
                        settings.logger.error(type(data))
                        # Deal with it
                    except NoResultFound:
                        e, v, t = sys.exc_info()
                        settings.logger.error(
                            "APL Exigisi Flight Open for Extension Error (NoResultFound): %s - %s" % (e, str(v)))
                        settings.logger.error(type(data))
                        # Deal with that as well

            if db is not None:
                return (db.scenario, db.simulation_session_uuid)
            else:
                raise Exception('Cannot extend flight because provided UUID does not match a flight.')


    def exigisi_add_sim_session_uuid_to_flight_apl(self, flight_uuid, suuid):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            db = None

            with self.lock:
                try:
                    db = self.dbase.session2.query(Flights).filter_by(uuid=flight_uuid).first()
                    # settings.logger.info(db)
                except MultipleResultsFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error(
                        "APL Exigisi Flight Alteration Error (MultipleResultsFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with it
                except NoResultFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error("APL Exigisi Flight Alteration Error (NoResultFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with that as well

            if db is not None:
                db.simulation_session_uuid = suuid

            # Put it in the database
            with self.lock:
                self.dbase.session2.add(db)
                self.dbase.session2.commit()

    def exigisi_close_flight_apl(self, flight_uuid, name):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            settings.logger.info("----- APL Exigisi Flight Close")
            db = None

            with self.lock:
                try:
                    db = self.dbase.session2.query(Flights).filter_by(uuid=flight_uuid).first()
                    # settings.logger.info(db)
                except MultipleResultsFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error(
                        "APL Exigisi Flight Close Error (MultipleResultsFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with it
                except NoResultFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error("APL Exigisi Flight Close Error (NoResultFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with that as well

            if db is not None:
                db.stop_timestamp = datetime.datetime.now()
                if name is not None:
                    db.name = name

            # Put it in the database
            with self.lock:
                self.dbase.session2.add(db)
                self.dbase.session2.commit()

    def record_state_change(self, craft):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            db = StateChange()

            db.command = self.received_command
            db.x = craft.x
            db.y = craft.y
            db.altitude = craft.altitude
            db.yaw = craft.yaw
            db.pitch = craft.pitch
            db.roll = craft.roll
            db.speed = craft.speed
            db.fuel = craft.fuel
            db.armed = craft.armed
            db.mode = int(craft.mode)
            db.time_start = craft.time_start
            db.home_x = craft.home_x
            db.home_y = craft.home_y
            db.loiter_x = craft.loiter_x
            db.loiter_y = craft.loiter_y
            db.wind_direction = craft.wind_direction
            db.in_canyon = craft.in_canyon
            db.plan = str(craft.plan)
            db.rtl_override = craft.rtl_override

            if settings.craft_state == settings.CraftState.OFF:
                db.craft_state = 0
            else:
                db.craft_state = 1

            # db.craft_state = settings.craft_state.value
            db.payload = str(craft.payload)
            db.payload_limit = craft.payload_limit
            db.flight_safety = craft.flight_safety
            db.total_1 = craft.total_1
            db.total_2 = craft.total_2
            db.total_3 = craft.total_3
            db.game_mode = int(craft.game_mode)
            db.found_hiker = craft.found_hiker
            db.successful_package_drops = craft.successful_package_drops
            db.max_package_drops = craft.max_package_drops
            db.landed_after_hiker_found = craft.landed_after_hiker_found
            db.dropped_list = str(craft.dropped_list)

            db.uuid = str(uuid.uuid4())
            db.instance = settings.mavsim_instance
            db.session = settings.mavsim_session
            db.timestamp = datetime.datetime.now()
            db.simulation_session_uuid = settings.simulation_session_uuid


            # Put it in the database
            with self.lock:
                self.dbase.session2.add(db)
                self.dbase.session2.commit()

            return str(db.uuid)


    def restore_craft_state(self, craft, ss_uuid):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            db = None

            with self.lock:
                try:
                    db = self.dbase.session2.query(StateChange).filter_by(simulation_session_uuid=ss_uuid).order_by(StateChange.timestamp.desc()).first()
                    # settings.logger.info(db)
                except MultipleResultsFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error(
                        "APL Exigisi Restore Craft State Error (MultipleResultsFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with it
                except NoResultFound:
                    e, v, t = sys.exc_info()
                    settings.logger.error("APL Exigisi Restore Craft State Error (NoResultFound): %s - %s" % (e, str(v)))
                    settings.logger.error(type(data))
                    # Deal with that as well

            if db is not None:
                craft.x = db.x
                craft.y = db.y
                craft.altitude = db.altitude
                craft.yaw = db.yaw
                craft.pitch = db.pitch
                craft.roll = db.roll
                craft.speed = db.speed
                craft.fuel = db.fuel
                craft.armed = db.armed
                craft.mode = db.mode
                craft.time_start = db.time_start
                craft.home_x = db.home_x
                craft.home_y = db.home_y
                craft.loiter_x = db.loiter_x
                craft.loiter_y = db.loiter_y
                craft.wind_direction = db.wind_direction
                craft.in_canyon = db.in_canyon
                craft.plan = eval(db.plan)
                craft.rtl_override = db.rtl_override
                settings.craft_state = db.craft_state
                craft.payload = eval(db.payload)
                craft.payload_limit = db.payload_limit
                craft.flight_safety = db.flight_safety
                craft.total_1 = db.total_1
                craft.total_2 = db.total_2
                craft.total_3 = db.total_3

                craft.game_mode = db.game_mode
                craft.found_hiker = db.found_hiker
                craft.successful_package_drops = db.successful_package_drops
                craft.max_package_drops = db.max_package_drops
                craft.landed_after_hiker_found = db.landed_after_hiker_found
                craft.dropped_list = eval(db.dropped_list)

    def store_visual_record(self, image):
        if self.dbase is None or settings.use_db is False:
            return
        else:
            db = VisualRecord()

            db.image = image

            db.uuid = str(uuid.uuid4())
            db.instance = settings.mavsim_instance
            db.session = settings.mavsim_session
            db.timestamp = datetime.datetime.now()
            db.simulation_session_uuid = settings.simulation_session_uuid

            # Put it in the database
            with self.lock:
                self.dbase.session2.add(db)
                self.dbase.session2.commit()

            return str(db.uuid)



# fin
