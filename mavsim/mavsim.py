#!./VENV/bin/python3
# -----------------------------------------------------------------------------
#  MAVSim
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator,
#
# usage: mavsim.py [-h] [-v] [-q] [-n] [-sip SERVER_IP] [-sp SERVER_PORT]
#                  [-i INSTANCE_NAME] [-s SESSION_NAME] [-p PILOT_NAME]
#                  [-db DATABASE_URL]
#
# optional arguments:
#   -h, --help            show this help message and exit
#   -v, --verbose         Verbose output (useful for debug)
#   -q, --quiet           Quiet output (speeds up system slightly)
#   -n, --nodb            Turn of database logging (speeds up system)
#   -sip SERVER_IP, --server_ip SERVER_IP
#                         IP for udp request server (default: 0.0.0.0)
#   -sp SERVER_PORT, --server_port SERVER_PORT
#                         Port for request server (default: 14555)
#   -i INSTANCE_NAME, --instance_name INSTANCE_NAME
#                         Name of this mavsim instance for the logged data
#                         (default: "mavsim")
#   -s SESSION_NAME, --session_name SESSION_NAME
#                         Name of this mavsim session for the logged data
#                         (default: "jeff")
#   -p PILOT_NAME, --pilot_name PILOT_NAME
#                         Name of this mavsim pilot for the logged data
#                         (default: "Chuck")
#   -db DATABASE_URL, --database_url DATABASE_URL
#                         The url string for connecting to the database
#                         (default: None)
#
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------
from . import settings
import logging
import ast
import time
import gc
import _thread

from .udpserver import UDPServer
from .memory_buffer import MemoryBuffer

from .flight_command_handler import FLIGHTCommandHandler
from .telemetry_command_handler import TELEMETRYCommandHandler
from .sensor_command_handler import SENSORCommandHandler
from .control_command_handler import CONTROLCommandHandler
from .log_command_handler import LOGCommandHandler

from .ardupilotlight_0 import ArduPilotLight_0
from .ardupilotlight_1 import ArduPilotLight_1
from .ardupilotlight_2 import ArduPilotLight_2
from .nixel_world_manager import NixelWorldManager
from .ardupilotlight_telemetry_receiver import APLTelemetry
from .ardupilotlight_sim_command_handler import APL_SIMCommandHandler


def run(udpserver, mem_buffer, sim_handler, flight_handler, telemetry_handler, sensor_handler, control_handler, log_handler):
    settings.logger.info(
        "~~~~~~~ Threaded main processing loop for UDP requests ~~~~~~")
    while settings.sim_running:

        data = udpserver.receive()
        response = "('ACK')"

        if data and not settings.cmd_lock:
            settings.logger.info("UDP Received: %s" % data)
            msg = ''

            if data[0] == '(' and data[len(data) - 1] == ')':
                mem_buffer.received_command = str(data)

                msg = ast.literal_eval(data)
                if type(msg) is tuple:
                    if msg[0] == 'SIM':
                        settings.logger.info("SIM Command")
                        response = sim_handler.process(msg)
                    elif msg[0] == 'FLIGHT':
                        settings.logger.info("FLIGHT Command")
                        response = flight_handler.process(msg)
                    elif msg[0] == 'TELEMETRY':
                        settings.logger.info("TELEMETRY Command")
                        response = telemetry_handler.process(msg)
                    elif msg[0] == 'SENSOR':
                        settings.logger.info("SENSOR Command")
                        response = sensor_handler.process(msg)
                    elif msg[0] == 'CONTROL':
                        settings.logger.info("CONTROL Command")
                        response = control_handler.process(msg)
                    elif msg[0] == 'LOG':
                        settings.logger.info("LOG Command")
                        response = log_handler.process(msg)
                    else:
                        settings.logger.error(
                            "Unknown command: %s" % msg[0])
                else:
                    settings.logger.error("Unrecognizeable: %s" % msg)
            else:
                settings.logger.error("Unrecognizeable message: %s" % data)

            # return response
            udpserver.send(response)

            # store_command_and_response
            mem_buffer.store_command_and_response(str(data), response)

        time.sleep(0.0001)

# -----------------------------------------------------------------------------
#                                                                          MAIN
# -----------------------------------------------------------------------------


class MAVSim():
    def __init__(self, verbose=False, quiet=False, nodb=False, server_ip='0.0.0.0', server_port=14555, instance_name='mavsim', session_name='jeff', pilot_name='Chuck',
                 database_url=None, telemetry_cb=None, nixel_gen=False, sim_op_state=0):


        # Establish settings located in settings.py, which hosts our global parameters
        settings.init()

        # Set instance name
        if quiet:
            settings.logger.setLevel(logging.ERROR)
            print("MAVSim APL-ND running in quiet mode (sssshhhh)")

        # Set instance name
        if instance_name is not None:
            settings.mavsim_name = instance_name

        # Set session name
        if session_name is not None:
            settings.mavsim_session = session_name

        # Set pilot name
        if pilot_name is not None:
            settings.mavsim_pilot = pilot_name

        # Sim operation state (rules for navigation, drop, etc.)
        if sim_op_state is not None:
            settings.sim_op_state = sim_op_state

        # Setup database connection url
        if database_url is not None:
            settings.database_url = database_url

        # No database logging
        if nodb:
            settings.use_db = False
            settings.database_url = None
            settings.logger.info('NO DATABASE logging. :(')

        # get verbose if necessary
        if verbose:
            settings.logger.info("---!!! Setting maximum verbosity !!!---")
            settings.logger.setLevel(logging.DEBUG)

        settings.logger.info(">>>>> mavsim starting...")
        settings.logger.info(">> Instance = %s" % settings.mavsim_instance)
        settings.logger.info(">> Session = %s" % settings.mavsim_session)
        settings.logger.info(">> Database = %s" % settings.database_url)

        settings.apl = True
        # settings.logger.info("  ________                                _____  ________")
        # settings.logger.info(
        #     " /  _____/_____    _____   ____     _____/ ____\ \______ \_______  ____   ____   ____   ______")
        # settings.logger.info(
        #     "/   \  ___\__  \  /     \_/ __ \   /  _ \   __\   |    |  \_  __ \/  _ \ /    \_/ __ \ /  ___/")
        # settings.logger.info(
        #     "\    \_\  \/ __ \|  Y Y  \  ___/  (  <_> )  |     |    `   \  | \(  <_> )   |  \  ___/ \___ \ ")
        # settings.logger.info(
        #     " \______  (____  /__|_|  /\___  >  \____/|__|    /_______  /__|   \____/|___|  /\___  >____  >")
        # settings.logger.info(
        #     "        \/     \/      \/     \/                         \/                  \/     \/     \/ ")

        settings.logger.info("                         _____      _________   _____________.__          ")
        settings.logger.info("                        /     \    /  _  \   \ /   /   _____/|__| _____   ")
        settings.logger.info("                       /  \ /  \  /  /_\  \   Y   /\_____  \ |  |/     \  ")
        settings.logger.info("                      /    Y    \/    |    \     / /        \|  |  Y Y  \ ")
        settings.logger.info("                      \____|__  /\____|__  /\___/ /_______  /|__|__|_|  / ")
        settings.logger.info("                              \/         \/               \/          \/  ")

        settings.logger.info("")
        settings.logger.info(
            "                                 Micro Air Vehicle Simulator       ")
        settings.logger.info(
            "                                       Version %2.1f       " % settings.software_version)
        settings.logger.info("")
        settings.logger.info(
            "------------------------------    G a m e  O f  D r o n e s   --------------------------------")
        settings.logger.info(
            "-------------------------    A r d u P i l o t   L i g h t  (NO Deps!) -----------------------")

        # Telemetry Multicaster
        self.multicast = None

        # UDP Command Request Server
        self.udpserver = UDPServer(server_ip, server_port)
        settings.logger.info("......established udp server subsystem on %s : %d" % (
            server_ip, server_port))

        # Memory
        self.mem_buffer = MemoryBuffer(self.multicast, telemetry_cb)

        # Nixel World Manager - takes stock of current Nixel worlds in memory and generates them if any are missing (see nixel_config.json)
        settings.nwm = NixelWorldManager()
        if nixel_gen:
            settings.nwm.check_world_library()

        if settings.sim_op_state == 0:
            self.craft = ArduPilotLight_0(self.mem_buffer)
        elif settings.sim_op_state == 1:
            self.craft = ArduPilotLight_1(self.mem_buffer)
        elif settings.sim_op_state == 2:
            self.craft = ArduPilotLight_2(self.mem_buffer)
        else:
            self.craft = ArduPilotLight_0(self.mem_buffer)  # Default

        # ArduPilotLight Telemetry
        self.telemetry = APLTelemetry(self.mem_buffer, self.craft)

        # Setup Message Handlers for incoming commands
        self.sim_handler = APL_SIMCommandHandler(self.craft, self.telemetry, self.mem_buffer)

        self.flight_handler = FLIGHTCommandHandler(self.craft, self.mem_buffer)
        self.telemetry_handler = TELEMETRYCommandHandler(self.craft, self.telemetry)
        self.sensor_handler = SENSORCommandHandler(self.craft, self.mem_buffer, self.sim_handler)
        self.control_handler = CONTROLCommandHandler(self.craft)
        self.log_handler = LOGCommandHandler(self.craft, self.mem_buffer)

        # Craft is now on
        # TODO: move craft control to sim controls
        settings.craft_state = settings.CraftState.ON

        settings.logger.info(
            "~~~~~~~ Setup complete, entering main processing loops ~~~~~~")

        # Kick off the threaded UDP message handler
        self.threaded_listener = _thread.start_new_thread(run,
                                (self.udpserver, self.mem_buffer, self.sim_handler, self.flight_handler,
                                 self.telemetry_handler, self.sensor_handler, self.control_handler, self.log_handler))

    def __del__(self):
        # Termination cleanup
        #
        settings.logger.info("Received simulation termination signal")

        self.threaded_listener.stop()

        # Forced cleanup to clear the network connections
        del self.udpserver
        del self.craft

        gc.collect()
        settings.logger.info("<<<<< mavsim shutting down")

    def command(self, data):
        response = "('ACK')"

        if isinstance(data, bytes):
            data = data.decode('utf-8')

        if not settings.cmd_lock and settings.sim_running:
            settings.logger.info("Command Received: %s" % data)
            msg = ''

            if data[0] == '(' and data[len(data) - 1] == ')':
                self.mem_buffer.received_command = str(data)

                msg = ast.literal_eval(data)
                if type(msg) is tuple:
                    if msg[0] == 'SIM':
                        settings.logger.info("SIM Command")
                        response = self.sim_handler.process(msg)
                    elif msg[0] == 'FLIGHT':
                        settings.logger.info("FLIGHT Command")
                        response = self.flight_handler.process(msg)
                    elif msg[0] == 'TELEMETRY':
                        settings.logger.info("TELEMETRY Command")
                        response = self.telemetry_handler.process(msg)
                    elif msg[0] == 'SENSOR':
                        settings.logger.info("SENSOR Command")
                        response = self.sensor_handler.process(msg)
                    elif msg[0] == 'CONTROL':
                        settings.logger.info("CONTROL Command")
                        response = self.control_handler.process(msg)
                    elif msg[0] == 'LOG':
                        settings.logger.info("LOG Command")
                        response = self.log_handler.process(msg)
                    else:
                        settings.logger.error(
                            "Unknown command: %s" % msg[0])
                        response = "('ERR', 'Unknown command')"
                else:
                    settings.logger.error("Unrecognizeable: %s" % msg)
                    response = "('ERR', 'Unrecognizeable')"
            else:
                settings.logger.error("Unrecognizeable message: %s" % data)
                response = "('ERR', 'Unrecognizeable message')"

            # store_command_and_response
            self.mem_buffer.store_command_and_response(str(data), response)

            return response

# fin
