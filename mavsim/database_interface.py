# -----------------------------------------------------------------------------
#  MAVSim:: Database Interface
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from . import settings
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Float, BigInteger, Boolean, LargeBinary
from sqlalchemy.orm import sessionmaker

# Helper functions
#
def isClose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def not_equal(a, b, attribute_list, float_rel_tol=1e-07, float_abs_tol=1e-07):
    if settings.apl is True:
        return True

    #print("@@@@@ Checking for inequality!!")
    if a is None and b is None:
        return False
    elif a is None:
        return True
    elif b is None:
        return True

    count = 0
    for attribute in attribute_list:
        attr_a = getattr(a, attribute)
        attr_b = getattr(b, attribute)

        if isinstance(attr_a, float):
            #print("%s::%s  --  %.015f =?= %.015f" % (a.__class__.__name__, attribute, attr_a, attr_b))
            if isClose(attr_a, attr_b, float_rel_tol, float_abs_tol):
                count = count + 1
        else:
            #print("%s::%s  --  %s =?= %s" % (a.__class__.__name__, attribute, str(attr_a), str(attr_b)))
            if attr_a == attr_b:
                count = count + 1

    if count == len(attribute_list):
        return False
    else:
        return True


# Database variables
#
Base = declarative_base()
Session = sessionmaker()


class Database:
    def __init__(self, dbinfo):
        self.engine = create_engine(dbinfo, echo=False, pool_pre_ping=False)

        global Base
        Base.metadata.create_all(self.engine)

        global Session
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        self.session.flush()

        global Session2
        Session2 = sessionmaker(bind=self.engine)
        self.session2 = Session2()
        self.session2.flush()


# -------------------------------------------------------------------------------
# ORM Connections for mavsim information
# -------------------------------------------------------------------------------

class MavSimMission(Base):
    __tablename__ = 'mavsim_mission'

    id = Column(Integer, primary_key=True)

    name = Column(String)
    info = Column(String)
    start = Column(DateTime)
    end = Column(DateTime)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        if self.end is None:
            return "<MavSimMission(name='%s', info='%s' start='%s' stop='None')>" % (
                self.name, self.info, self.start.strftime("%Y-%m-%d %H:%M"))
        else:
            return "<MavSimMission(name='%s', info='%s' start='%s' stop='%s')>" % (
                self.name, self.info, self.start.strftime("%Y-%m-%d %H:%M"),
                self.end.strftime("%Y-%m-%d %H:%M"))


class MavSimCommand(Base):
    __tablename__ = 'mavsim_command'

    id = Column(Integer, primary_key=True)

    command = Column(String)
    ack = Column(String)
    details = Column(String)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<MavSimCommand(command='%s', ack='%s', details='%s')>" % (self.command, self.ack, self.details)


class MavSimOpenLog(Base):
    __tablename__ = 'mavsim_openlog'

    id = Column(Integer, primary_key=True)

    source = Column(String)
    log = Column(String)
    details = Column(String)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<MavSimOpenLog(source='%s', log='%s', details='%s')>" % (self.source, self.log, self.details)

# -------------------------------------------------------------------------------
# ORM Connections for MAVLink Commands
# -------------------------------------------------------------------------------


class Simstate(Base):
    # Message format:
    #    SIMSTATE ['roll', 'pitch', 'yaw', 'xacc', 'yacc', 'zacc', 'xgyro', 'ygyro', 'zgyro', 'lat', 'lng']
    #
    # Example:
    #    SIMSTATE {roll : 3.13010082209e-06, pitch : 0.246998071671, yaw : -0.122172169387, xacc : 2.39562368393, yacc : -2.97388141917e-05,
    #       zacc : -9.50091171265, xgyro : 8.03058863852e-09, ygyro : 2.5897677336e-09, zgyro : 1.92594717952e-09, lat : -353632583, lng : 1491652374}
    #
    __tablename__ = 'simstate'

    id = Column(Integer, primary_key=True)

    roll = Column(Float)
    pitch = Column(Float)
    yaw = Column(Float)
    xacc = Column(Float)
    yacc = Column(Float)
    zacc = Column(Float)
    xgyro = Column(Float)
    ygyro = Column(Float)
    zgyro = Column(Float)
    lat = Column(Float)
    lng = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<Simstate(roll='%f' pitch='%f' yaw='%f' xacc='%f', yacc='%f', zacc='%f', lat='%f' lng='%f')>" % (
            self.roll, self.pitch, self.yaw, self.xacc, self.yacc, self.zacc, self.lat, self.lng)

    def __ne__(self, other):
        return not_equal(self, other, ['roll', 'pitch', 'yaw', 'xacc', 'yacc', 'zacc', 'lat', 'lng'], 1e-07, 1e-07)


class SysStatus(Base):
    # Message format:
    #    SYS_STATUS ['onboard_control_sensors_present', 'onboard_control_sensors_enabled', 'onboard_control_sensors_health',
    #                'load', 'voltage_battery', 'current_battery', 'battery_remaining', 'drop_rate_comm', 'errors_comm', 'errors_count1',
    #                'errors_count2', 'errors_count3', 'errors_count4']
    #
    # Example:
    #    SYS_STATUS {onboard_control_sensors_present : 56753183, onboard_control_sensors_enabled : 6389791, onboard_control_sensors_health : 53607455,
    #                load : 0, voltage_battery : 12587, current_battery : 0, battery_remaining : 100, drop_rate_comm : 0, errors_comm : 0, errors_count1 : 0,
    #                errors_count2 : 0, errors_count3 : 0, errors_count4 : 0}
    #
    __tablename__ = 'sysstatus'

    id = Column(Integer, primary_key=True)

    onboard_control_sensors_present = Column(Integer)
    onboard_control_sensors_enabled = Column(Integer)
    onboard_control_sensors_health = Column(Integer)
    load = Column(Integer)
    voltage_battery = Column(Integer)
    current_battery = Column(Integer)
    battery_remaining = Column(Integer)
    drop_rate_comm = Column(Integer)
    errors_comm = Column(Integer)
    errors_count1 = Column(Integer)
    errors_count2 = Column(Integer)
    errors_count3 = Column(Integer)
    errors_count4 = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<SysStatus(onboard_control_sensors_present='%d', onboard_control_sensors_enabled ='%d', onboard_control_sensors_health ='%d', \
                        load='%d', voltage_battery='%d', current_battery='%d', battery_remaining='%d', drop_rate_comm='%d', errors_comm='%d', errors_count1='%d', \
                        errors_count2='%d', errors_count3='%d', errors_count4='%d')>" % (self.onboard_control_sensors_present, self.onboard_control_sensors_enabled, self.onboard_control_sensors_health,
                                                                                         self.load, self.voltage_battery, self.current_battery, self.battery_remaining, self.drop_rate_comm, self.errors_comm,
                                                                                         self.errors_count1, self.errors_count2, self.errors_count3, self.errors_count4)

    def __ne__(self, other):
        return not_equal(self, other, ['onboard_control_sensors_present', 'onboard_control_sensors_enabled', 'onboard_control_sensors_health',
                                       'load', 'voltage_battery', 'current_battery', 'battery_remaining', 'drop_rate_comm', 'errors_comm', 'errors_count1',
                                       'errors_count2', 'errors_count3', 'errors_count4'], 1e-07, 1e-07)


class RawIMU(Base):
    # Message format:
    #    RAW_IMU ['time_usec', 'xacc', 'yacc', 'zacc', 'xgyro', 'ygyro', 'zgyro', 'xmag', 'ymag', 'zmag']
    #
    # Example:
    #    RAW_IMU {time_usec : 1955000, xacc : 244, yacc : 0, zacc : -968, xgyro : 0, ygyro : 0, zgyro : 0, xmag : 341, ymag : 73, zmag : -469}
    #
    __tablename__ = 'raw_imu'

    id = Column(Integer, primary_key=True)

    time_usec = Column(BigInteger)
    xacc = Column(Integer)
    yacc = Column(Integer)
    zacc = Column(Integer)
    xgyro = Column(Integer)
    ygyro = Column(Integer)
    zgyro = Column(Integer)
    xmag = Column(Integer)
    ymag = Column(Integer)
    zmag = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<RawIMU(time_usec='%d', xacc='%d', yacc='%d', zacc='%d', xgyro='%d', ygyro='%d', zgyro='%d', xmag='%d', ymag='%d', zmag='%d')>" % (
            self.time_usec, self.xacc, self.yacc, self.zacc, self.xgyro, self.ygyro, self.zgyro, self.xmag, self.ymag, self.zmag)

    def __ne__(self, other):
        return not_equal(self, other, ['time_usec', 'xacc', 'yacc', 'zacc', 'xmag', 'ymag', 'zmag'], 1e-07, 1e-07)


class ScaledPressure(Base):
    # Message format:
    #    SCALED_PRESSURE ['time_boot_ms', 'press_abs', 'press_diff', 'temperature']
    #
    # Example:
    #    SCALED_PRESSURE {time_boot_ms : 1955, press_abs : 944.994506836, press_diff : -0.00359374983236, temperature : 2561}
    #
    __tablename__ = 'scaled_pressure'

    id = Column(Integer, primary_key=True)

    time_boot_ms = Column(BigInteger)
    press_abs = Column(Float)
    press_diff = Column(Float)
    temperature = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<ScaledPressure(time_boot_ms='%d', press_abs='%f', press_diff='%f', temperature='%d')>" % (self.time_boot_ms,
                                                                                                           self.press_abs, self.press_diff, self.temperature)

    def __ne__(self, other):
        return not_equal(self, other, ['time_boot_ms', 'press_abs', 'press_diff', 'temperature'], 1e-07, 1e-07)


class MemInfo(Base):
    # Message format:
    #    MEMINFO ['brkval', 'freemem']
    #
    # Example:
    #    MEMINFO {brkval : 0, freemem : 0}
    #
    __tablename__ = 'mem_info'

    id = Column(Integer, primary_key=True)

    brkval = Column(Integer)
    freemem = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<MemInfo(brkval='%d', freemem='%d')>" % (self.brkval, self.freemem)

    def __ne__(self, other):
        return not_equal(self, other, ['brkval', 'freemem'], 1e-07, 1e-07)


class MissionCurrent(Base):
    # Message format:
    #    MISSION_CURRENT ['seq']
    #
    # Example:
    #    MISSION_CURRENT {seq : 0}
    #
    __tablename__ = 'mission_current'

    id = Column(Integer, primary_key=True)

    seq = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<MissionCurrent(seq='%d')>" % self.seq

    def __ne__(self, other):
        return not_equal(self, other, ['seq'], 1e-07, 1e-07)


class GPSRawInt(Base):
    # Message format:
    #    GPS_RAW_INT ['time_usec', 'fix_type', 'lat', 'lon', 'alt', 'eph', 'epv', 'vel', 'cog', 'satellites_visible']
    #
    # Example:
    #    GPS_RAW_INT {time_usec : 0, fix_type : 0, lat : 0, lon : 0, alt : 0, eph : 9999, epv : 0, vel : 0, cog : 0, satellites_visible : 0}
    #
    __tablename__ = 'gps_raw_int'

    id = Column(Integer, primary_key=True)

    time_usec = Column(BigInteger)
    fix_type = Column(Integer)
    lat = Column(Integer)
    lon = Column(Integer)
    alt = Column(Integer)
    eph = Column(Integer)
    epv = Column(Integer)
    vel = Column(Integer)
    cog = Column(Integer)
    satellites_visible = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<GPSRawInt(time_usec='%d', fix_type='%d', lat='%d', lon='%d', alt='%d', eph='%d', epv='%d', vel='%d', cog='%d', satellites_visible='%d')>" % (
            self.time_usec, self.fix_type, self.lat, self.lon, self.alt, self.eph, self.epv, self.vel, self.cog, self.satellites_visible)

    def __ne__(self, other):
        return not_equal(self, other, ['time_usec', 'fix_type', 'lat', 'lon', 'alt', 'eph', 'epv', 'vel', 'cog', 'satellites_visible'], 1e-07, 1e-07)


class GlobalPositionInt(Base):
    # Message format:
    #    GLOBAL_POSITION_INT ['time_boot_ms', 'lat', 'lon', 'alt', 'relative_alt', 'vx', 'vy', 'vz', 'hdg']
    #
    # Example:
    #    GLOBAL_POSITION_INT {time_boot_ms : 2955, lat : 2, lon : 0, alt : 170, relative_alt : 172, vx : 0, vy : 0, vz : 0, hdg : 34117}
    #
    __tablename__ = 'global_position_int'

    id = Column(Integer, primary_key=True)

    time_boot_ms = Column(BigInteger)
    lat = Column(Integer)
    lon = Column(Integer)
    alt = Column(Integer)
    relative_alt = Column(Integer)
    vx = Column(Integer)
    vy = Column(Integer)
    vz = Column(Integer)
    hdg = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<GlobalPositionInt(time_boot_ms='%d', lat='%d', lon='%d', alt='%d', relative_alt='%d', vx='%d', vy='%d', vz='%d', hdg='%d')>" % (
            self.time_boot_ms, self.lat, self.lon, self.alt, self.relative_alt, self.vx, self.vy, self.vz, self.hdg)

    def __ne__(self, other):
        return not_equal(self, other, ['time_boot_ms', 'lat', 'lon', 'alt', 'relative_alt', 'vx', 'vy', 'vz', 'hdg'], 1e-07, 1e-07)


class ServoOutputRaw(Base):
    # Message format:
    #    SERVO_OUTPUT_RAW ['time_usec', 'port', 'servo1_raw', 'servo2_raw', 'servo3_raw', 'servo4_raw', 'servo5_raw', 'servo6_raw', 'servo7_raw', 'servo8_raw']
    #
    # Example:
    #    SERVO_OUTPUT_RAW {time_usec : 10635000, port : 0, servo1_raw : 1500, servo2_raw : 1500, servo3_raw : 1000, servo4_raw : 1500, servo5_raw : 1500,
    #                      servo6_raw : 1500, servo7_raw : 1500, servo8_raw : 1500}
    #
    __tablename__ = 'servo_output_raw'

    id = Column(Integer, primary_key=True)

    time_usec = Column(BigInteger)
    port = Column(Integer)
    servo1_raw = Column(Integer)
    servo2_raw = Column(Integer)
    servo3_raw = Column(Integer)
    servo4_raw = Column(Integer)
    servo5_raw = Column(Integer)
    servo6_raw = Column(Integer)
    servo7_raw = Column(Integer)
    servo8_raw = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<ServoOutputRaw(time_usec='%d', port='%d', servo1_raw='%d', servo2_raw='%d', servo3_raw='%d', servo4_raw='%d', servo5_raw='%d', servo6_raw='%d', servo7_raw='%d', servo8_raw='%d')>" % (
            self.time_usec, self.port, self.servo1_raw, self.servo2_raw, self.servo3_raw, self.servo4_raw, self.servo5_raw, self.servo6_raw, self.servo7_raw, self.servo8_raw)

    def __ne__(self, other):
        return not_equal(self, other, ['time_usec', 'port', 'servo1_raw', 'servo2_raw', 'servo3_raw', 'servo4_raw', 'servo5_raw', 'servo6_raw', 'servo7_raw', 'servo8_raw'], 1e-07, 1e-07)


class RCChannels(Base):
    # Message format:
    #    RC_CHANNELS ['time_boot_ms', 'chancount', 'chan1_raw', 'chan2_raw', 'chan3_raw',
    #                 'chan4_raw', 'chan5_raw', 'chan6_raw', 'chan7_raw', 'chan8_raw',
    #                 'chan9_raw', 'chan10_raw', 'chan11_raw', 'chan12_raw', 'chan13_raw', 'chan14_raw',
    #                 'chan15_raw', 'chan16_raw', 'chan17_raw', 'chan18_raw', 'rssi']
    #
    # Example:
    #    RC_CHANNELS {time_boot_ms : 10635, chancount : 16, chan1_raw : 1500, chan2_raw : 1500, chan3_raw : 1000,
    #                 chan4_raw : 1500, chan5_raw : 1800, chan6_raw : 1000, chan7_raw : 1000, chan8_raw : 1800,
    #                 chan9_raw : 0, chan10_raw : 0, chan11_raw : 0, chan12_raw : 0, chan13_raw : 0, chan14_raw : 0,
    #                 chan15_raw : 0, chan16_raw : 0, chan17_raw : 0, chan18_raw : 0, rssi : 0}
    #
    __tablename__ = 'rc_channels'

    id = Column(Integer, primary_key=True)

    time_boot_ms = Column(BigInteger)
    chancount = Column(Integer)
    chan1_raw = Column(Integer)
    chan2_raw = Column(Integer)
    chan3_raw = Column(Integer)
    chan4_raw = Column(Integer)
    chan5_raw = Column(Integer)
    chan6_raw = Column(Integer)
    chan7_raw = Column(Integer)
    chan8_raw = Column(Integer)
    chan9_raw = Column(Integer)
    chan10_raw = Column(Integer)
    chan11_raw = Column(Integer)
    chan12_raw = Column(Integer)
    chan13_raw = Column(Integer)
    chan14_raw = Column(Integer)
    chan15_raw = Column(Integer)
    chan16_raw = Column(Integer)
    chan17_raw = Column(Integer)
    chan18_raw = Column(Integer)
    rssi = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<RCChannels(time_boot_ms='%d', chancount='%d', chan1_raw='%d', chan2_raw='%d', chan3_raw='%d', \
                         chan4_raw='%d', chan5_raw='%d', chan6_raw='%d', chan7_raw='%d', chan8_raw='%d', \
                         chan9_raw='%d', chan10_raw='%d', chan11_raw='%d', chan12_raw='%d', chan13_raw='%d', chan14_raw='%d', \
                         chan15_raw='%d', chan16_raw='%d', chan17_raw='%d', chan18_raw='%d', rssi='%d')>" % (self.time_boot_ms,
                                                                                                             self.chancount, self.chan1_raw, self.chan2_raw, self.chan3_raw,
                                                                                                             self.chan4_raw, self.chan5_raw, self.chan6_raw, self.chan7_raw, self.chan8_raw,
                                                                                                             self.chan9_raw, self.chan10_raw, self.chan11_raw, self.chan12_raw, self.chan13_raw, self.chan14_raw,
                                                                                                             self.chan15_raw, self.chan16_raw, self.chan17_raw, self.chan18_raw, self.rssi)

    def __ne__(self, other):
        return not_equal(self, other, ['time_boot_ms', 'chancount', 'chan1_raw', 'chan2_raw', 'chan3_raw',
                                       'chan4_raw', 'chan5_raw', 'chan6_raw', 'chan7_raw', 'chan8_raw',
                                       'chan9_raw', 'chan10_raw', 'chan11_raw', 'chan12_raw', 'chan13_raw', 'chan14_raw',
                                       'chan15_raw', 'chan16_raw', 'chan17_raw', 'chan18_raw', 'rssi'], 1e-07, 1e-07)


class RCChannelsRaw(Base):
    # Message format:
    #    RC_CHANNELS_RAW ['time_boot_ms', 'port', 'chan1_raw', 'chan2_raw', 'chan3_raw',
    #                     'chan4_raw', 'chan5_raw', 'chan6_raw', 'chan7_raw', 'chan8_raw', 'rssi']
    #
    # Example:
    #    RC_CHANNELS_RAW {time_boot_ms : 10875, port : 0, chan1_raw : 1500, chan2_raw : 1500, chan3_raw : 1000,
    #                     chan4_raw : 1500, chan5_raw : 1800, chan6_raw : 1000, chan7_raw : 1000, chan8_raw : 1800, rssi : 0}
    #
    __tablename__ = 'rc_channels_raw'

    id = Column(Integer, primary_key=True)

    time_boot_ms = Column(BigInteger)
    port = Column(Integer)
    chan1_raw = Column(Integer)
    chan2_raw = Column(Integer)
    chan3_raw = Column(Integer)
    chan4_raw = Column(Integer)
    chan5_raw = Column(Integer)
    chan6_raw = Column(Integer)
    chan7_raw = Column(Integer)
    chan8_raw = Column(Integer)
    rssi = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<RCChannelsRaw(time_boot_ms='%d', port='%d', chan1_raw='%d', chan2_raw='%d', chan3_raw='%d', \
                             chan4_raw='%d', chan5_raw='%d', chan6_raw='%d', chan7_raw='%d', chan8_raw='%d', rssi='%d')>" % (self.time_boot_ms,
                                                                                                                             self.port, self.chan1_raw, self.chan2_raw, self.chan3_raw,
                                                                                                                             self.chan4_raw, self.chan5_raw, self.chan6_raw, self.chan7_raw, self.chan8_raw, self.rssi)

    def __ne__(self, other):
        return not_equal(self, other, ['time_boot_ms', 'port', 'chan1_raw', 'chan2_raw', 'chan3_raw',
                                       'chan4_raw', 'chan5_raw', 'chan6_raw', 'chan7_raw', 'chan8_raw', 'rssi'], 1e-07, 1e-07)


class Attitude(Base):
    # Message format:
    #    ATTITUDE ['time_boot_ms', 'roll', 'pitch', 'yaw', 'rollspeed', 'pitchspeed', 'yawspeed']
    #
    # Example:
    #    ATTITUDE {time_boot_ms : 22395, roll : 0.000390322355088, pitch : 0.230382025242, yaw : -0.119957029819,
    #              rollspeed : -1.92406005226e-05, pitchspeed : 0.000272802251857, yawspeed : 0.00140124955215}
    #
    __tablename__ = 'attitude'

    id = Column(Integer, primary_key=True)

    time_boot_ms = Column(BigInteger)
    roll = Column(Float)
    pitch = Column(Float)
    yaw = Column(Float)
    rollspeed = Column(Float)
    pitchspeed = Column(Float)
    yawspeed = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<Attitude(time_boot_ms='%d', roll='%f', pitch='%f', yaw='%f', rollspeed='%f', pitchspeed='%f', yawspeed='%f'>" % (self.time_boot_ms,
                                                                                                                                  self.roll, self.pitch, self.yaw, self.rollspeed, self.pitchspeed, self.yawspeed)

    def __ne__(self, other):
        return not_equal(self, other, ['time_boot_ms', 'roll', 'pitch', 'yaw', 'rollspeed', 'pitchspeed', 'yawspeed'], 1e-07, 1e-07)


class AHRS(Base):
    # Message format:
    #    AHRS ['omegaIx', 'omegaIy', 'omegaIz', 'accel_weight', 'renorm_val', 'error_rp', 'error_yaw']
    #
    # Example:
    #    AHRS {omegaIx : -0.000277602841379, omegaIy : 5.19787136e-06, omegaIz : 0.000979423290119, accel_weight : 0.0,
    #          renorm_val : 0.0, error_rp : 0.000513987615705, error_yaw : 0.00409703096375}
    #
    __tablename__ = 'ahrs'

    id = Column(Integer, primary_key=True)

    omegaIx = Column(Float)
    omegaIy = Column(Float)
    omegaIz = Column(Float)
    accel_weight = Column(Float)
    renorm_val = Column(Float)
    error_rp = Column(Float)
    error_yaw = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<AHRS(omegaIx='%f', omegaIy='%f', omegaIz='%f', accel_weight='%f', renorm_val='%f', error_rp='%f', error_yaw='%f')>" % (self.omegaIx,
                                                                                                                                        self.omegaIy, self.omegaIz, self.accel_weight, self.renorm_val, self.error_rp, self.error_yaw)

    def __ne__(self, other):
        return not_equal(self, other, ['omegaIx', 'omegaIy', 'omegaIz', 'accel_weight', 'renorm_val', 'error_rp', 'error_yaw'], 1e-07, 1e-07)


class AHRS2(Base):
    # Message format:
    #    AHRS2 ['roll', 'pitch', 'yaw', 'altitude', 'lat', 'lng']
    #
    # Example:
    #    AHRS2 {roll : 0.000618040445261, pitch : 0.247469633818, yaw : -0.106240436435, altitude : 0.0, lat : 0, lng : 0}
    #
    __tablename__ = 'ahrs2'

    id = Column(Integer, primary_key=True)

    roll = Column(Float)
    pitch = Column(Float)
    yaw = Column(Float)
    altitude = Column(Float)
    lat = Column(Integer)
    lng = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<AHRS2(roll='%f', pitch='%f', yaw='%f', altitude='%f', lat='%d', lng='%d')>" % (self.roll,
                                                                                                self.pitch, self.yaw, self.altitude, self.lat, self.lng)

    def __ne__(self, other):
        return not_equal(self, other, ['roll', 'pitch', 'yaw', 'altitude', 'lat', 'lng'], 1e-07, 1e-07)


class AHRS3(Base):
    # Message format:
    #    AHRS3 ['roll', 'pitch', 'yaw', 'altitude', 'lat', 'lng', 'v1', 'v2', 'v3', 'v4'}
    #
    # Example:
    #    AHRS3 {roll : 0.000618040445261, pitch : 0.247469633818, yaw : -0.106240436435, altitude : 584.179992676,
    #           lat : -353632583, lng : 1491652374, v1 : 0.0, v2 : 0.0, v3 : 0.0, v4 : 0.0}
    #
    __tablename__ = 'ahrs3'

    id = Column(Integer, primary_key=True)

    roll = Column(Float)
    pitch = Column(Float)
    yaw = Column(Float)
    altitude = Column(Float)
    lat = Column(Integer)
    lng = Column(Integer)
    v1 = Column(Float)
    v2 = Column(Float)
    v3 = Column(Float)
    v4 = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<AHRS3(roll='%f', pitch='%f', yaw='%f', altitude='%f', lat='%d', lng='%d', v1='%f', v2='%f', v3='%f', v4='%f')>" % (self.roll,
                                                                                                                                    self.pitch, self.yaw, self.altitude, self.lat, self.lng, self.v1, self.v2, self.v3, self.v4)

    def __ne__(self, other):
        return not_equal(self, other, ['roll', 'pitch', 'yaw', 'altitude', 'lat', 'lng', 'v1', 'v2', 'v3', 'v4'], 1e-07, 1e-07)


class HWStatus(Base):
    # Message format:
    #    HWSTATUS ['Vcc', 'I2Cerr']
    #
    # Example:
    #    HWSTATUS {Vcc : 5000, I2Cerr : 0}
    #
    __tablename__ = 'hw_status'

    id = Column(Integer, primary_key=True)

    Vcc = Column(Integer)
    I2Cerr = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<HWStatus(Vcc='%d', I2Cerr='%d')>" % (self.Vcc, self.I2Cerr)

    def __ne__(self, other):
        return not_equal(self, other, ['Vcc', 'I2Cerr'], 1e-07, 1e-07)


class Wind(Base):
    # Message format:
    #    WIND ['direction', 'speed', 'speed_z']
    #
    # Example:
    #    WIND {direction : -6.94911766052, speed : 0.023286793381, speed_z : 0.00570494262502}
    #
    __tablename__ = 'wind'

    id = Column(Integer, primary_key=True)

    direction = Column(Float)
    speed = Column(Float)
    speed_z = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<Wind(direction='%f', speed='%f', speed_z='%f')>" % (self.direction, self.speed, self.speed_z)

    def __ne__(self, other):
        return not_equal(self, other, ['direction', 'speed', 'speed_z'], 1e-07, 1e-07)


class SystemTime(Base):
    # Message format:
    #    SYSTEM_TIME ['time_unix_usec', 'time_boot_ms']
    #
    # Example:
    #    SYSTEM_TIME {time_unix_usec : 1501023424980000, time_boot_ms : 22395}
    #
    __tablename__ = 'system_time'

    id = Column(Integer, primary_key=True)

    time_unix_usec = Column(BigInteger)
    time_boot_ms = Column(BigInteger)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<SystemTime(time_unix_usec='%d', time_boot_ms='%d')>" % (self.time_unix_usec, self.time_boot_ms)

    def __ne__(self, other):
        return not_equal(self, other, ['time_unix_usec', 'time_boot_ms'], 1e-07, 1e-07)


class BatteryStatus(Base):
    # Message format:
    #    BATTERY_STATUS ['id', 'battery_function', 'type', 'temperature',
    #       'voltages', 'current_battery', 'current_consumed', 'energy_consumed', 'battery_remaining']
    #
    # Example:
    #    BATTERY_STATUS {id : 0, battery_function : 0, type : 0, temperature : 32767,
    #       voltages : [65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535],
    #       current_battery : 0, current_consumed : 0, energy_consumed : -1, battery_remaining : 100}
    #
    __tablename__ = 'battery_status'

    id = Column(Integer, primary_key=True)

    batt_id = Column(Integer)
    battery_function = Column(Integer)
    type = Column(Integer)
    temperature = Column(Integer)
    voltages = Column(String)
    current_battery = Column(Integer)
    current_consumed = Column(Integer)
    energy_consumed = Column(Integer)
    battery_remaining = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<BatteryStatus(id='%d', battery_function='%d', type='%d', temperature='%d', \
              voltages='%s', current_battery='%d', current_consumed='%d', energy_consumed='%d', battery_remaining='%d')>" % (self.batt_id,
                                                                                                                             self.battery_function, self.type, self.temperature, self.voltages, self.current_battery, self.current_consumed,
                                                                                                                             self.energy_consumed, self.battery_remaining)

    def __ne__(self, other):
        return not_equal(self, other, ['batt_id', 'battery_function', 'type', 'temperature',
                                       'current_battery', 'current_consumed', 'energy_consumed', 'battery_remaining'], 1e-07, 1e-07)


class VFRHUD(Base):
    # Message format:
    #    VFR_HUD ['airspeed', 'groundspeed', 'heading', 'throttle', 'alt', 'climb']
    #
    # Example:
    #    VFR_HUD {airspeed : 0.022061701864, groundspeed : 4.55038243672e-05, heading : 353, throttle : 0,
    #             alt : 584.309997559, climb : -0.163024142385}
    #
    __tablename__ = 'vfr_hud'

    id = Column(Integer, primary_key=True)

    airspeed = Column(Float)
    groundspeed = Column(Float)
    heading = Column(Integer)
    throttle = Column(Integer)
    alt = Column(Float)
    climb = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<VFRHUD(airspeed='%f', groundspeed='%f', heading='%d', throttle='%d', alt='%f', climb='%f')>" % (self.airspeed,
                                                                                                                 self.groundspeed, self.heading, self.throttle, self.alt, self.climb)

    def __ne__(self, other):
        return not_equal(self, other, ['airspeed', 'groundspeed', 'heading', 'throttle', 'alt', 'climb'], 1e-07, 1e-07)


class ParamSet(Base):
    # Message format:
    #    PARAM_SET ['target_system', 'target_component', 'param_id', 'param_value']
    #
    # Example:
    #    No example ???
    #
    __tablename__ = 'param_set'

    id = Column(Integer, primary_key=True)

    target_system = Column(Integer)
    target_component = Column(Integer)
    param_id = Column(Integer)
    param_value = Column(String)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<ParamSet(target_system='%d', target_component='%d', param_id='%d', param_value='%s')>" % (self.target_system,
                                                                                                           self.target_component, self.param_id, self.param_value)

    def __ne__(self, other):
        return not_equal(self, other, ['target_system', 'target_component', 'param_id', 'param_value'], 1e-07, 1e-07)


class ScaledIMU2(Base):
    # Message format:
    #    SCALED_IMU2 ['time_boot_ms', 'xacc', 'yacc', 'zacc', 'xgyro', 'ygyro', 'zgyro', 'xmag', 'ymag', 'zmag']
    #
    # Example:
    #    SCALED_IMU2 {time_boot_ms : 1715, xacc : 244, yacc : 0, zacc : -968, xgyro : 0, ygyro : 0, zgyro : 0,
    #                 xmag : 341, ymag : 73, zmag : -469}
    #
    __tablename__ = 'scaled_imu2'

    id = Column(Integer, primary_key=True)

    time_boot_ms = Column(BigInteger)
    xacc = Column(Integer)
    yacc = Column(Integer)
    zacc = Column(Integer)
    xgyro = Column(Integer)
    ygyro = Column(Integer)
    zgyro = Column(Integer)
    xmag = Column(Integer)
    ymag = Column(Integer)
    zmag = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<ScaledIMU2(time_boot_ms='%d', xacc='%d', yacc='%d', zacc='%d', xgyro='%d', ygyro='%d', zgyro='%d', xmag='%d', ymag='%d', zmag='%d')>" % (self.time_boot_ms,
                                                                                                                                                          self.xacc, self.yacc, self.zacc, self.xgyro, self.ygyro, self.zgyro, self.xmag, self.ymag, self.zmag)

    def __ne__(self, other):
        return not_equal(self, other, ['time_boot_ms', 'xacc', 'yacc', 'zacc', 'xmag', 'ymag', 'zmag'], 1e-07, 1e-07)


class Heartbeat(Base):
    # Message format:
    #    HEARTBEAT ['type', 'autopilot', 'base_mode', 'custom_mode', 'system_status', 'mavlink_version']
    #
    # Example:
    #    HEARTBEAT {type : 1, autopilot : 3, base_mode : 81, custom_mode : 0, system_status : 3, mavlink_version : 3}
    #
    __tablename__ = 'heartbeat'

    id = Column(Integer, primary_key=True)

    type = Column(Integer)
    autopilot = Column(Integer)
    base_mode = Column(Integer)
    custom_mode = Column(Integer)
    system_status = Column(Integer)
    mavlink_version = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<Heartbeat(type='%d', autopilot='%d', base_mode='%d', custom_mode='%d', system_status='%d', mavlink_version='%d')>" % (self.type,
                                                                                                                                       self.autopilot, self.base_mode, self.custom_mode, self.system_status, self.mavlink_version)

    def __ne__(self, other):
        return not_equal(self, other, ['type', 'autopilot', 'base_mode', 'custom_mode', 'system_status', 'mavlink_version'], 1e-07, 1e-07)


class SensorOffsets(Base):
    # Message format:
    #    SENSOR_OFFSETS ['mag_ofs_x', 'mag_ofs_y', 'mag_ofs_z', 'mag_declination',
    #                    'raw_press', 'raw_temp', 'gyro_cal_x', 'gyro_cal_y',
    #                    'gyro_cal_z', 'accel_cal_x', 'accel_cal_y', 'accel_cal_z']
    #
    # Example:
    #    SENSOR_OFFSETS {mag_ofs_x : 5, mag_ofs_y : 13, mag_ofs_z : -17, mag_declination : 0.0,
    #                    raw_press : 94497, raw_temp : 2647, gyro_cal_x : 0.0, gyro_cal_y : 0.0,
    #                    gyro_cal_z : 0.0, accel_cal_x : 0.0010000000475,
    #                    accel_cal_y : 0.0010000000475, accel_cal_z : 0.0010000000475}
    #
    __tablename__ = 'sensor_offsets'

    id = Column(Integer, primary_key=True)

    mag_ofs_x = Column(Integer)
    mag_ofs_y = Column(Integer)
    mag_ofs_z = Column(Integer)
    mag_declination = Column(Float)
    raw_press = Column(Integer)
    raw_temp = Column(Integer)
    gyro_cal_x = Column(Float)
    gyro_cal_y = Column(Float)
    gyro_cal_z = Column(Float)
    accel_cal_x = Column(Float)
    accel_cal_y = Column(Float)
    accel_cal_z = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<SensorOffsets(mag_ofs_x='%d', mag_ofs_y='%d', mag_ofs_z='%d', mag_declination='%f', \
                            raw_press='%d', raw_temp='%d', gyro_cal_x='%f', gyro_cal_y='%f', \
                            gyro_cal_z='%f', accel_cal_x='%f', accel_cal_y='%f', accel_cal_z='%f')>" % (self.mag_ofs_x,
                                                                                                        self.mag_ofs_y, self.mag_ofs_z, self.mag_declination,
                                                                                                        self.raw_press, self.raw_temp, self.gyro_cal_x, self.gyro_cal_y,
                                                                                                        self.gyro_cal_z, self.accel_cal_x, self.accel_cal_y, self.accel_cal_z)

    def __ne__(self, other):
        return not_equal(self, other, ['mag_ofs_x', 'mag_ofs_y', 'mag_ofs_z', 'mag_declination',
                                       'raw_press', 'raw_temp', 'gyro_cal_x', 'gyro_cal_y',
                                       'gyro_cal_z', 'accel_cal_x', 'accel_cal_y', 'accel_cal_z'], 1e-07, 1e-07)


class PowerStatus(Base):
    # Message format:
    #    POWER_STATUS ['Vcc', 'Vservo', 'flags']
    #
    # Example:
    #    POWER_STATUS {Vcc : 5000, Vservo : 0, flags : 0}
    #
    __tablename__ = 'power_status'

    id = Column(Integer, primary_key=True)

    Vcc = Column(Integer)
    Vservo = Column(Integer)
    flags = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<PowerStatus(Vcc='%d', Vservo='%d', flags='%d')>" % (self.Vcc, self.Vservo, self.flags)

    def __ne__(self, other):
        return not_equal(self, other, ['Vcc', 'Vservo', 'flags'], 1e-07, 1e-07)


class TerrainReport(Base):
    # Message format:
    #    TERRAIN_REPORT ['lat', 'lon', 'spacing', 'terrain_height', 'current_height', 'pending', 'loaded']
    #
    # Example:
    #    TERRAIN_REPORT {lat : -353632583, lon : 1491652374, spacing : 100, terrain_height : 583.884094238,
    #                    current_height : -583.664123535, pending : 280, loaded : 224}
    #
    __tablename__ = 'terrain_reports'

    id = Column(Integer, primary_key=True)

    lat = Column(Integer)
    lon = Column(Integer)
    spacing = Column(Integer)
    terrain_height = Column(Float)
    current_height = Column(Float)
    pending = Column(Integer)
    loaded = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<TerrainReport(lat='%d', lon='%d', spacing='%d', terrain_height='%f', current_height='%f', pending='%d', loaded='%d')>" % (self.lat,
                                                                                                                                           self.lon, self.spacing, self.terrain_height, self.current_height, self.pending, self.loaded)

    def __ne__(self, other):
        return not_equal(self, other, ['lat', 'lon', 'spacing', 'terrain_height', 'current_height', 'pending', 'loaded'], 1e-07, 1e-07)


class LocalPositionNED(Base):
    # Message format:
    #    LOCAL_POSITION_NED ['time_boot_ms', 'x', 'y', 'z', 'vx', 'vy', 'vz']
    #
    # Example:
    #    LOCAL_POSITION_NED {time_boot_ms : 37275, x : 0.000534089980647, y : -0.000761588453315, z : 0.299379944801,
    #                        vx : -9.73779533524e-05, vy : 0.000160355906701, vz : 0.00397930899635}
    #
    __tablename__ = 'local_position_ned'

    id = Column(Integer, primary_key=True)

    time_boot_ms = Column(BigInteger)
    x = Column(Float)
    y = Column(Float)
    z = Column(Float)
    vx = Column(Float)
    vy = Column(Float)
    vz = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<LocalPositionNED(time_boot_ms='%d', x='%f', y='%f', z='%f', vx='%f', vy='%f', vz='%f')>" % (self.time_boot_ms,
                                                                                                             self.x, self.y, self.z, self.vx, self.vy, self.vz)

    def __ne__(self, other):
        return not_equal(self, other, ['time_boot_ms', 'x', 'y', 'z', 'vx', 'vy', 'vz'], 1e-07, 1e-07)


class Vibration(Base):
    # Message format:
    #    VIBRATION ['time_usec', 'vibration_x', 'vibration_y', 'vibration_z', 'clipping_0', 'clipping_1', 'clipping_2']
    #
    # Example:
    #    VIBRATION {time_usec : 37275000, vibration_x : 0.00613814592361, vibration_y : 0.00559959700331,
    #               vibration_z : 0.00544817512855, clipping_0 : 0, clipping_1 : 0, clipping_2 : 0}
    #
    __tablename__ = 'vibration'

    id = Column(Integer, primary_key=True)

    time_usec = Column(BigInteger)
    vibration_x = Column(Float)
    vibration_y = Column(Float)
    vibration_z = Column(Float)
    clipping_0 = Column(Integer)
    clipping_1 = Column(Integer)
    clipping_2 = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<Vibration(time_usec='%d', vibration_x='%f', vibration_y='%f', vibration_z='%f', clipping_0='%d', clipping_1='%d', clipping_2='%d')>" % (self.time_usec,
                                                                                                                                                         self.vibration_x, self.vibration_y, self.vibration_z, self.clipping_0, self.clipping_1, self.clipping_2)

    def __ne__(self, other):
        return not_equal(self, other, ['time_usec', 'vibration_x', 'vibration_y', 'vibration_z', 'clipping_0', 'clipping_1', 'clipping_2'], 1e-07, 1e-07)


class EKFStatusReport(Base):
    # Message format:
    #    EKF_STATUS_REPORT ['flags', 'velocity_variance', 'pos_horiz_variance', 'pos_vert_variance', 'compass_variance', 'terrain_alt_variance']
    #
    # Example:
    #    EKF_STATUS_REPORT {flags : 895, velocity_variance : 0.000628539535683, pos_horiz_variance : 2.75208058156e-05,
    #                       pos_vert_variance : 0.00307484879158, compass_variance : 0.0123343411833, terrain_alt_variance : 0.0176199749112}
    #
    __tablename__ = 'ekf_status_report'

    id = Column(Integer, primary_key=True)

    flags = Column(Integer)
    velocity_variance = Column(Float)
    pos_horiz_variance = Column(Float)
    pos_vert_variance = Column(Float)
    compass_variance = Column(Float)
    terrain_alt_variance = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<EKFStatusReport(flags='%d', velocity_variance='%f', pos_horiz_variance='%f', pos_vert_variance='%f', compass_variance='%f', terrain_alt_variance='%f')>" % (self.flags,
                                                                                                                                                                             self.velocity_variance, self.pos_horiz_variance, self.pos_vert_variance, self.compass_variance, self.terrain_alt_variance)

    def __ne__(self, other):
        return not_equal(self, other, ['flags', 'velocity_variance', 'pos_horiz_variance', 'pos_vert_variance', 'compass_variance', 'terrain_alt_variance'], 1e-07, 1e-07)


class HomePosition(Base):
    # Message format:
    #    HOME_POSITION ['latitude', 'longitude', 'altitude', 'x', 'y', 'z', 'q', 'approach_x', 'approach_y', 'approach_z']
    #
    # Example:
    #    HOME_POSITION {latitude : -353632583, longitude : 1491652375, altitude : 584190, x : 0.0, y : 0.0, z : 0.0,
    #                   q : [1.0, 0.0, 0.0, 0.0], approach_x : 0.0, approach_y : 0.0, approach_z : 0.0}
    #
    __tablename__ = 'home_position'

    id = Column(Integer, primary_key=True)

    latitude = Column(Integer)
    longitude = Column(Integer)
    altitude = Column(Integer)
    x = Column(Float)
    y = Column(Float)
    z = Column(Float)
    q = Column(String)
    approach_x = Column(Float)
    approach_y = Column(Float)
    approach_z = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<HomePosition(atitude='%d', longitude='%d', altitude='%d', x='%f', y='%f', z='%f', q='%s', approach_x='%f', approach_y='%f', approach_z='%f')>" % (self.latitude,
                                                                                                                                                                   self.longitude, self.altitude, self.x, self.y, self.z, self.q, self.approach_x, self.approach_y, self.approach_z)

    def __ne__(self, other):
        return not_equal(self, other, ['latitude', 'longitude', 'altitude', 'x', 'y', 'z', 'approach_x', 'approach_y', 'approach_z'], 1e-07, 1e-07)


class ParamValue(Base):
    # Message format:
    #    PARAM_VALUE ['param_id', 'param_value', 'param_type', 'param_count', 'param_index']
    #
    # Example:
    #    PARAM_VALUE {param_id : COMPASS_OFS_X, param_value : 5.69455575943, param_type : 9, param_count : 898, param_index : 65535}
    #
    __tablename__ = 'param_value'

    id = Column(Integer, primary_key=True)

    param_id = Column(String)
    param_value = Column(Float)
    param_type = Column(Integer)
    param_count = Column(Integer)
    param_index = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<ParamValue(param_id='%s', param_value='%f', param_type='%d', param_count='%d', param_index='%d')>" % (self.param_id,
                                                                                                                       self.param_value, self.param_type, self.param_count, self.param_index)

    def __ne__(self, other):
        return not_equal(self, other, ['param_id', 'param_value', 'param_type', 'param_count', 'param_index'], 1e-07, 1e-07)


class StatusText(Base):
    # Message format:
    #    STATUSTEXT ['severity', 'text']
    #
    # Example:
    #    STATUSTEXT {severity : 6, text : ArduPlane V3.8.0beta5 (379005eb)}
    #
    __tablename__ = 'status_text'

    id = Column(Integer, primary_key=True)

    severity = Column(Integer)
    text = Column(String)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<StatusText(severity='%d', text='%s')>" % (self.severity, self.text)

    def __ne__(self, other):
        return not_equal(self, other, ['severity', 'text'], 1e-07, 1e-07)


class MissionACK(Base):
    # Message format:
    #    MISSION_ACK ['target_system', 'target_component', 'type']
    #
    # Example:
    #    MISSION_ACK {target_system : 255, target_component : 0, type : 0}
    #
    __tablename__ = 'mission_ack'

    id = Column(Integer, primary_key=True)

    target_system = Column(Integer)
    target_component = Column(Integer)
    type = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<MissionACK(target_system='%d', target_component='%d', type='%d')>" % (self.target_system,
                                                                                       self.target_component, self.type)

    def __ne__(self, other):
        return not_equal(self, other, ['target_system', 'target_component', 'type'], 1e-07, 1e-07)


class CommandACK(Base):
    # Message format:
    #    COMMAND_ACK ['command', 'result']
    #
    # Example:
    #    COMMAND_ACK {command : 400, result : 0}
    #
    __tablename__ = 'command_ack'

    id = Column(Integer, primary_key=True)

    command = Column(Integer)
    result = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<CommandACK(command='%d', result='%d')>" % (self.command, self.result)

    def __ne__(self, other):
        return not_equal(self, other, ['command', 'result'], 1e-07, 1e-07)


class AirspeedAutocal(Base):
    # Message format:
    #    AIRSPEED_AUTOCAL ['vx', 'vy', 'vz', 'diff_pressure', 'EAS2TAS', 'ratio', 'state_x', 'state_y', 'state_z', 'Pax', 'Pby', 'Pcz']
    #
    # Example:
    #    AIRSPEED_AUTOCAL {vx : 10.186000824, vy : -0.448000013828, vz : -1.12300002575, diff_pressure : 68.0500488281, EAS2TAS : 1.07119417191,
    #         ratio : 1.99360001087, state_x : 0.0, state_y : 0.0, state_z : 0.708240866661, Pax : 100.0, Pby : 100.0, Pcz : 9.99999997475e-07}
    #
    __tablename__ = 'airspeed_autocal'

    id = Column(Integer, primary_key=True)

    vx = Column(Float)
    vy = Column(Float)
    vz = Column(Float)
    diff_pressure = Column(Float)
    EAS2TAS = Column(Float)
    ratio = Column(Float)
    state_x = Column(Float)
    state_y = Column(Float)
    state_z = Column(Float)
    Pax = Column(Float)
    Pby = Column(Float)
    Pcz = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<AirspeedAutocal(vx='%f', vy='%f', vz='%f', diff_pressure='%f', EAS2TAS='%f', ratio='%f', state_x='%f', state_y='%f', state_z='%f', Pax='%f', Pby='%f', Pcz='%f')>" % (self.vx,
                                                                                                                                                                                       self.vy, self.vz, self.diff_pressure, self.EAS2TAS, self.ratio, self.state_x, self.state_y, self.state_z, self.Pax, self.Pby, self.Pcz)

    def __ne__(self, other):
        return not_equal(self, other, ['vx', 'vy', 'vz', 'diff_pressure', 'EAS2TAS', 'ratio', 'state_x', 'state_y', 'state_z', 'Pax', 'Pby', 'Pcz'], 1e-07, 1e-07)


class NavControllerOutput(Base):
    # Message format:
    #    NAV_CONTROLLER_OUTPUT ['nav_roll', 'nav_pitch', 'nav_bearing', 'target_bearing', 'wp_dist', 'alt_error', 'aspd_error', 'xtrack_error']
    #
    # Example:
    #    NAV_CONTROLLER_OUTPUT {nav_roll : 0.0, nav_pitch : 17.0399990082, nav_bearing : -3, target_bearing : -3, wp_dist : 14,
    #                           alt_error : 27.5799999237, aspd_error : 1186.52441406, xtrack_error : 0.0}
    #
    __tablename__ = 'nav_controller_output'

    id = Column(Integer, primary_key=True)

    nav_roll = Column(Float)
    nav_pitch = Column(Float)
    nav_bearing = Column(Integer)
    target_bearing = Column(Integer)
    wp_dist = Column(Integer)
    alt_error = Column(Float)
    aspd_error = Column(Float)
    xtrack_error = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<NavControllerOutput(nav_roll='%f', nav_pitch='%f', nav_bearing='%d', target_bearing='%d', wp_dist='%d', alt_error='%f', aspd_error='%f', xtrack_error='%f')>" % (self.nav_roll,
                                                                                                                                                                                  self.nav_pitch, self.nav_bearing, self.target_bearing, self.wp_dist, self.alt_error, self.aspd_error, self.xtrack_error)

    def __ne__(self, other):
        return not_equal(self, other, ['nav_roll', 'nav_pitch', 'nav_bearing', 'target_bearing', 'wp_dist', 'alt_error', 'aspd_error', 'xtrack_error'], 1e-07, 1e-07)


class PositionTargetGlobalInt(Base):
    # Message format:
    #    POSITION_TARGET_GLOBAL_INT ['time_boot_ms', 'coordinate_frame', 'type_mask', 'lat_int', 'lon_int', 'alt', 'vx', 'vy', 'vz', 'afx', 'afy', 'afz', 'yaw', 'yaw_rate']
    #
    # Example:
    #    POSITION_TARGET_GLOBAL_INT {time_boot_ms : 246735, coordinate_frame : 5, type_mask : 65528, lat_int : -353632578,
    #                                lon_int : 1491652394, alt : 612.320007324, vx : 0.0, vy : 0.0, vz : 0.0, afx : 0.0, afy : 0.0,
    #                                afz : 0.0, yaw : 0.0, yaw_rate : 0.0}
    #
    __tablename__ = 'position_target_global_int'

    id = Column(Integer, primary_key=True)

    time_boot_ms = Column(BigInteger)
    coordinate_frame = Column(Integer)
    type_mask = Column(Integer)
    lat_int = Column(Integer)
    lon_int = Column(Integer)
    alt = Column(Float)
    vx = Column(Float)
    vy = Column(Float)
    vz = Column(Float)
    afx = Column(Float)
    afy = Column(Float)
    afz = Column(Float)
    yaw = Column(Float)
    yaw_rate = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<PositionTargetGlobalInt(time_boot_ms='%d', coordinate_frame='%d', type_mask='%d', lat_int='%d', lon_int='%d', alt='%f', vx='%f', vy='%f', vz='%f', afx='%f', afy='%f', afz='%f', yaw='%f', yaw_rate='%f')>" % (self.time_boot_ms,
                                                                                                                                                                                                                                self.coordinate_frame, self.type_mask, self.lat_int, self.lon_int, self.alt, self.vx, self.vy, self.vz, self.afx, self.afy, self.afz, self.yaw, self.yaw_rate)

    def __ne__(self, other):
        return not_equal(self, other, ['time_boot_ms', 'coordinate_frame', 'type_mask', 'lat_int', 'lon_int', 'alt', 'vx', 'vy', 'vz', 'afx', 'afy', 'afz', 'yaw', 'yaw_rate'], 1e-07, 1e-07)


class MissionRequest(Base):
    # Message format:
    #    MISSION_REQUEST ['target_system', 'target_component', 'seq']
    #
    # Example:
    #    MISSION_REQUEST {target_system : 255, target_component : 0, seq : 1}
    #
    __tablename__ = 'mission_request'

    id = Column(Integer, primary_key=True)

    target_system = Column(Integer)
    target_component = Column(Integer)
    seq = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<MissionRequest(target_system='%d', target_component='%d', seq='%d')>" % (self.target_system, self.target_component, self.seq)

    def __ne__(self, other):
        return not_equal(self, other, ['target_system', 'target_component', 'seq'], 1e-07, 1e-07)


class MissionItemReached(Base):
    # Message format:
    #    MISSION_ITEM_REACHED ['seq']
    #
    # Example:
    #    MISSION_ITEM_REACHED {seq : 1}
    #
    __tablename__ = 'mission_item_reached'

    id = Column(Integer, primary_key=True)

    seq = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<MissionItemReached(seq='%d')>" % self.seq

    def __ne__(self, other):
        return not_equal(self, other, ['seq'], 1e-07, 1e-07)


class MissionCount(Base):
    # Message format:
    #    MISSION_COUNT ['target_system', 'target_component', 'count']
    #
    # Example:
    #    MISSION_COUNT {target_system : 255, target_component : 0, count : 3}
    #
    __tablename__ = 'mission_count'

    id = Column(Integer, primary_key=True)

    target_system = Column(Integer)
    target_component = Column(Integer)
    count = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<MissionCount(target_system='%d', target_component='%d', count='%d')>" % (self.target_system, self.target_component, self.count)

    def __ne__(self, other):
        return not_equal(self, other, ['target_system', 'target_component', 'count'], 1e-07, 1e-07)


class MissionItemInt(Base):
    # Message format:
    #    MISSION_ITEM_INT ['target_system', 'target_component', 'seq', 'frame', 'command', 'current',
    #                      'autocontinue', 'param1', 'param2', 'param3', 'param4', 'x', 'y', 'z']
    #
    # Example:
    #    MISSION_ITEM_INT {target_system : 255, target_component : 0, seq : 2, frame : 3, command : 16, current : 0,
    #                      autocontinue : 1, param1 : 0.0, param2 : 0.0, param3 : 0.0, param4 : 0.0, x : -353594656,
    #                      y : 1491617024, z : 99.8000030518}
    #
    __tablename__ = 'mission_item_int'

    id = Column(Integer, primary_key=True)

    target_system = Column(Integer)
    target_component = Column(Integer)
    seq = Column(Integer)
    frame = Column(Integer)
    command = Column(Integer)
    current = Column(Integer)
    autocontinue = Column(Integer)
    param1 = Column(Float)
    param2 = Column(Float)
    param3 = Column(Float)
    param4 = Column(Float)
    x = Column(Float)
    y = Column(Float)
    z = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<MissionItemInt(target_system='%d', target_component='%d', seq='%d', frame='%d', command='%d', current='%d', \
                              autocontinue='%d', param1='%f', param2='%f', param3='%f', param4='%f', x='%f', y='%f', z='%f')>" % (self.target_system,
                                                                                                                                  self.target_component, self.seq, self.frame, self.command, self.current,
                                                                                                                                  self.autocontinue, self.param1, self.param2, self.param3, self.param4, self.x, self.y, self.z)

    def __ne__(self, other):
        return not_equal(self, other, ['target_system', 'target_component', 'seq', 'frame', 'command', 'current',
                                       'autocontinue', 'param1', 'param2', 'param3', 'param4', 'x', 'y', 'z'], 1e-07, 1e-07)


class MissionItem(Base):
    # Message format:
    #    MISSION_ITEM ['target_system', 'target_component', 'seq', 'frame', 'command', 'current',
    #                      'autocontinue', 'param1', 'param2', 'param3', 'param4', 'x', 'y', 'z']
    #
    # Example:
    #    MISSION_ITEM {target_system : 255, target_component : 0, seq : 2, frame : 3, command : 16, current : 0,
    #                      autocontinue : 1, param1 : 0.0, param2 : 0.0, param3 : 0.0, param4 : 0.0, x : -353594656,
    #                      y : 1491617024, z : 99.8000030518}
    #
    __tablename__ = 'mission_item'

    id = Column(Integer, primary_key=True)

    target_system = Column(Integer)
    target_component = Column(Integer)
    seq = Column(Integer)
    frame = Column(Integer)
    command = Column(Integer)
    current = Column(Integer)
    autocontinue = Column(Integer)
    param1 = Column(Float)
    param2 = Column(Float)
    param3 = Column(Float)
    param4 = Column(Float)
    x = Column(Float)
    y = Column(Float)
    z = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<MissionItem(target_system='%d', target_component='%d', seq='%d', frame='%d', command='%d', current='%d', \
                              autocontinue='%d', param1='%f', param2='%f', param3='%f', param4='%f', x='%f', y='%f', z='%f')>" % (self.target_system,
                                                                                                                                  self.target_component, self.seq, self.frame, self.command, self.current,
                                                                                                                                  self.autocontinue, self.param1, self.param2, self.param3, self.param4, self.x, self.y, self.z)

    def __ne__(self, other):
        return not_equal(self, other, ['target_system', 'target_component', 'seq', 'frame', 'command', 'current',
                                       'autocontinue', 'param1', 'param2', 'param3', 'param4', 'x', 'y', 'z'], 1e-07, 1e-07)

class AutopilotVersion(Base):
    # Message format:
    #    AUTOPILOT_VERSION ['capabilities', 'flight_sw_version', 'middleware_sw_version',
    #                       'os_sw_version', 'board_version', 'flight_custom_version',
    #                       'middleware_custom_version', 'os_custom_version',
    #                       'vendor_id', 'product_id', 'uid']
    #
    # Example:
    #    AUTOPILOT_VERSION {capabilities : 4943, flight_sw_version : 50856068, middleware_sw_version : 0,
    #                       os_sw_version : 0, board_version : 0, flight_custom_version : [51, 55, 57, 48, 48, 53, 101, 98],
    #                       middleware_custom_version : [0, 0, 0, 0, 0, 0, 0, 0], os_custom_version : [0, 0, 0, 0, 0, 0, 0, 0],
    #                       vendor_id : 0, product_id : 0, uid : 0}
    #
    __tablename__ = 'autopilot_version'

    id = Column(Integer, primary_key=True)

    capabilities = Column(Integer)
    flight_sw_version = Column(Integer)
    middleware_sw_version = Column(Integer)
    os_sw_version = Column(Integer)
    board_version = Column(Integer)
    flight_custom_version = Column(String)
    middleware_custom_version = Column(String)
    os_custom_version = Column(String)
    vendor_id = Column(Integer)
    product_id = Column(Integer)
    uid = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<AutopilotVersion(capabilities='%d', flight_sw_version='%d', middleware_sw_version='%d', \
                              os_sw_version='%d', board_version='%d', flight_custom_version='%s', \
                              middleware_custom_version='%s', os_custom_version='%s', \
                              vendor_id='%d', product_id='%d', uid='%d')>" % (self.capabilities, self.flight_sw_version, self.middleware_sw_version,
                                                                              self.os_sw_version, self.board_version, self.flight_custom_version,
                                                                              self.middleware_custom_version, self.os_custom_version,
                                                                              self.vendor_id, self.product_id, self.uid)

    def __ne__(self, other):
        return not_equal(self, other, ['capabilities', 'flight_sw_version', 'middleware_sw_version',
                                       'os_sw_version', 'board_version', 'vendor_id', 'product_id', 'uid'], 1e-07, 1e-07)


class FencePoint(Base):
    # Message format:
    #    FENCE_POINT ['target_system', 'target_component', 'idx', 'count', 'lat', 'lng']
    #
    # Example:
    #    FENCE_POINT {target_system : 255, target_component : 0, idx : 0, count : 5, lat : -35.3650169373, lng : 149.165802002}
    #
    __tablename__ = 'fence_point'

    id = Column(Integer, primary_key=True)

    target_system = Column(Integer)
    target_component = Column(Integer)
    idx = Column(Integer)
    count = Column(Integer)
    lat = Column(Float)
    lng = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<FencePoint(target_system='%d', target_component='%d', idx='%d', count='%d', lat='%f', lng='%f')>" % (self.target_system,
                                                                                                                      self.target_component, self.idx, self.count, self.lat, self.lng)

    def __ne__(self, other):
        return not_equal(self, other, ['target_system', 'target_component', 'idx', 'count', 'lat', 'lng'], 1e-07, 1e-07)


class RallyPoint(Base):
    # Message format:
    #    RALLY_POINT ['target_system', 'target_component', 'idx', 'count', 'lat', 'lng', 'alt', 'break_alt', 'land_dir', 'flags']
    #
    # Example:
    #    RALLY_POINT {target_system : 255, target_component : 0, idx : 0, count : 1, lat : -353650507, lng : 1491631530,
    #                 alt : 50, break_alt : 0, land_dir : 0, flags : 0}
    #
    __tablename__ = 'rally_point'

    id = Column(Integer, primary_key=True)

    target_system = Column(Integer)
    target_component = Column(Integer)
    idx = Column(Integer)
    count = Column(Integer)
    lat = Column(Float)
    lng = Column(Float)
    alt = Column(Integer)
    break_alt = Column(Integer)
    land_dir = Column(Integer)
    flags = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<RallyPoint(target_system='%d', target_component='%d', idx='%d', count='%d', lat='%f', lng='%f', alt='%d', break_alt='%d', land_dir='%d', flags='%d')>" % (self.target_system,
                                                                                                                                                                           self.target_component, self.idx, self.count, self.lat, self.lng, self.alt, self.break_alt, self.land_dir, self.flags)

    def __ne__(self, other):
        return not_equal(self, other, ['target_system', 'target_component', 'idx', 'count', 'lat', 'lng', 'alt', 'break_alt', 'land_dir', 'flags'], 1e-07, 1e-07)


class CameraFeedback(Base):
    # Message format:
    #    CAMERA_FEEDBACK ['time_usec', 'target_system', 'cam_idx', 'img_idx', 'lat','lng', 'alt_msl', 'alt_rel', 'roll', 'pitch', 'yaw', 'foc_len', 'flags']
    #
    # Example:
    #    CAMERA_FEEDBACK {time_usec : 1502241101680000, target_system : 0, cam_idx : 0, img_idx : 1, lat : -353630284,
    #                     lng : 1491456951, alt_msl : 636.900024414, alt_rel : 52.7099990845, roll : -49.0200004578,
    #                     pitch : -7.17000007629, yaw : 254.619995117, foc_len : 0.0, flags : 0}
    #
    __tablename__ = 'camera_feedback'

    id = Column(Integer, primary_key=True)

    time_usec = Column(BigInteger)
    target_system = Column(Integer)
    cam_idx = Column(Integer)
    img_idx = Column(Integer)
    lat = Column(Integer)
    lng = Column(Integer)
    alt_msl = Column(Float)
    alt_rel = Column(Float)
    roll = Column(Float)
    pitch = Column(Float)
    yaw = Column(Float)
    foc_len = Column(Float)
    flags = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<CameraFeedback(time_usec='%d', target_system='%d', cam_idx='%d', img_idx='%d', lat='%d',lng='%d', alt_msl='%f', alt_rel='%f', roll='%f', pitch='%f', yaw='%f', foc_len='%f', flags='%d')>" % (self.time_usec,
                                                                                                                                                                                                               self.target_system, self.cam_idx, self.img_idx, self.lat, self.lng, self.alt_msl, self.alt_rel, self.roll, self.pitch, self.yaw, self.foc_len, self.flags)

    def __ne__(self, other):
        return not_equal(self, other, ['time_usec', 'target_system', 'cam_idx', 'img_idx', 'lat', 'lng', 'alt_msl', 'alt_rel', 'roll', 'pitch', 'yaw', 'foc_len', 'flags'], 1e-07, 1e-07)


class TerrainRequest(Base):
    # Message format:
    #    TERRAIN_REQUEST ['lat', 'lon', 'grid_spacing', 'mask']
    #
    # Example:
    #  TERRAIN_REQUEST {lat : -353963287, lon : 1491243631, grid_spacing : 100, mask : 72057594037927935}
    #
    __tablename__ = 'terrain_request'

    id = Column(Integer, primary_key=True)

    lat = Column(Integer)
    lon = Column(Integer)
    grid_spacing = Column(Integer)
    mask = Column(BigInteger)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<TerrainRequest(lat='%d', lon='%d', grid_spacing='%d', mask='%d')>" % (self.lat, self.lon, self.grid_spacing, self.mask)

    def __ne__(self, other):
        return not_equal(self, other, ['lat', 'lon', 'grid_spacing'], 1e-07, 1e-07)


class PayloadDrop(Base):
    # Message format:
    #    MS_PAYLOAD_DROP {item : %s, lat : %f, lon : %f, alt : %f, speed : %f}
    #
    # Example:
    #    MS_PAYLOAD_DROP {item : %s, lat : %f, lon : %f, alt : %f, speed : %f}
    #
    __tablename__ = 'payload_drop'

    id = Column(Integer, primary_key=True)

    slot = Column(Integer)
    item = Column(String)
    lat = Column(Float)
    lon = Column(Float)
    alt = Column(Float)
    speed = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<PayloadDrop(slot= %d, item='%s', lat='%f', lon='%f', alt='%f', speed='%f')>" % (self.slot, self.item, self.lat, self.lon, self.alt, self.speed)

    def __ne__(self, other):
        return not_equal(self, other, ['slot', 'item', 'lat', 'lon', 'alt', 'speed'], 1e-07, 1e-07)

class PayloadLoad(Base):
    # Message format:
    #    MS_PAYLOAD_LOAD {slot: %d, item: %d}
    #
    __tablename__ = 'payload_load'

    id = Column(Integer, primary_key=True)

    slot = Column(Integer)
    item = Column(String)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<PayloadLoad(slot=%d, item='%s')>" % (self.slot, self.item)

    def __ne__(self, other):
        return not_equal(self, other, ['slot', 'item'], 1e-07, 1e-07)

class PayloadHit(Base):
    # Message format:
    #    MS_PAYLOAD_HIT {item: %s, lat: %f, lon: %f, alt: %f, status: %s}
    #
    __tablename__ = 'payload_hit'

    id = Column(Integer, primary_key=True)

    item = Column(String)
    lat = Column(Float)
    lon = Column(Float)
    alt = Column(Float)
    status = Column(String)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<PayloadHit(item='%s', lat='%f', lon='%f', alt='%f', status='%s')>" % (self.item, self.lat, self.lon, self.alt, self.status)

    def __ne__(self, other):
        return not_equal(self, other, ['item', 'lat', 'lon', 'alt', 'status'], 1e-07, 1e-07)


class CraftCrash(Base):
    # Message format:
    #    MS_CRAFT_CRASH {cause : %s, lat : %f, lon : %f, alt : %f, speed : %f}
    #
    # Example:
    #    MS_CRAFT_CRASH {cause : %s, lat : %f, lon : %f, alt : %f, speed : %f}
    #
    __tablename__ = 'craft_crash'

    id = Column(Integer, primary_key=True)

    cause = Column(String)
    lat = Column(Float)
    lon = Column(Float)
    alt = Column(Float)
    speed = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<CraftCrash(cause='%s', lat='%f', lon='%f', alt='%f', speed='%f')>" % (
        self.cause, self.lat, self.lon, self.alt, self.speed)

    def __ne__(self, other):
        return not_equal(self, other, ['timestamp', 'cause', 'lat', 'lon', 'alt', 'speed'], 1e-07, 1e-07)

class SensedObject(Base):
    # Message format:
    #    MS_SENSED_OBJECT {name : %s, lat : %f, lon : %f, alt : %f, radius : %f}
    #
    # Example:
    #    MS_SENSED_OBJECT {name : %s, lat : %f, lon : %f, alt : %f, radius : %f}
    #
    __tablename__ = 'sensed_object'

    id = Column(Integer, primary_key=True)

    name = Column(String)
    lat = Column(Float)
    lon = Column(Float)
    alt = Column(Float)
    radius = Column(Float)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<SensedObject(name='%s', lat='%f', lon='%f', alt='%f', radius='%f')>" % (
            self.name, self.lat, self.lon, self.alt, self.radius)

    def __ne__(self, other):
        return not_equal(self, other, ['name', 'lat', 'lon', 'alt', 'radius'], 1e-07, 1e-07)


#
# Added for ArduPilot Light Simulation
#
class SimulationSession(Base):
    __tablename__ = 'simulation_session'

    id = Column(Integer, primary_key=True)

    begin = Column(DateTime)
    end   = Column(DateTime)
    scenario = Column(String)
    session_uuid = Column(String)
    note = Column(String)

    # Add AOI x,y,w,h,name here?
    aoi_x = Column(Integer)
    aoi_y = Column(Integer)
    aoi_w = Column(Integer)
    aoi_h = Column(Integer)
    aoi_map_name = Column(String)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<SimulationSession(begin='%s', end='%s', scenario='%s', session='%s')>" % (
            self.begin, self.end, self.scenario, self.session)

    def __ne__(self, other):
        return not_equal(self, other, ['begin', 'end', 'scenario', 'session'], 1e-07, 1e-07)

#
# Added for ArduPilot Light Simulation
#
class Airspeed(Base):
    __tablename__ = 'apl_airspeed'

    id = Column(Integer, primary_key=True)

    time_boot_ms = Column(Integer)
    speed = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<Airspeed(time_boot_ms=%d, speed=%d)>" % (self.time_boot_ms, self.speed)

    def __ne__(self, other):
        return not_equal(self, other, ['time_boot_ms', 'speed'], 1e-07, 1e-07)

#
# Added for ArduPilot Light Simulation
#
class Fuel(Base):
    __tablename__ = 'apl_fuel'

    id = Column(Integer, primary_key=True)

    time_boot_ms = Column(Integer)
    level = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<Fuel(time_boot_ms=%d, level=%d)>" % (self.time_boot_ms, self.level)

    def __ne__(self, other):
        return not_equal(self, other, ['time_boot_ms', 'level'], 1e-07, 1e-07)


#
# Added for ArduPilot Light Simulation
#
class Goal(Base):
    __tablename__ = 'apl_goal'

    id = Column(Integer, primary_key=True)

    order = Column(Integer)
    goal = Column(String)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<Goal(order=%d, goal='%s')>" % (self.order, self.goal)

    def __ne__(self, other):
        return not_equal(self, other, ['order', 'goal'], 1e-07, 1e-07)

#
# Added for ArduPilot Light Simulation
#
class InitialState(Base):
    __tablename__ = 'apl_initial_state'

    id = Column(Integer, primary_key=True)

    # x: 12, y: 485, z: 0, h: 3, p: 0, r: 0, speed: 0, fuel: 99999, armed: False, mode: 0

    x = Column(Integer)
    y = Column(Integer)
    z = Column(Integer)
    h = Column(Integer)
    p = Column(Integer)
    r = Column(Integer)
    speed = Column(Integer)
    fuel = Column(Integer)
    armed = Column(Integer)
    mode = Column(Integer)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<InitialState(x=%d, y=%d, z=%d, h=%d, p=%d, r=%d, speed=%d, fuel=%d, armed=%d, mode=%d)>" % (
                    self.x, self.y, self.z, self.h, self.p, self.r, self.speed, self.fuel, self.armed, self.mode)

    def __ne__(self, other):
        return not_equal(self, other, ['x','y','z','h','p','r','speed','fuel','armed','mode'], 1e-07, 1e-07)

#
# Added for ArduPilot Light Simulation
#
class Scenario(Base):
    __tablename__ = 'apl_scenario'

    id = Column(Integer, primary_key=True)

    map = Column(String)
    description = Column(String)
    suuid = Column(String)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<Scenario(map='%s', description='%s', suuid='%s')>" % (self.map, self.description, self.suuid)

    def __ne__(self, other):
        return not_equal(self, other, ['map','description','suuid'], 1e-07, 1e-07)

class VisualRecord(Base):
    __tablename__ = 'apl_viz_record'

    id = Column(Integer, primary_key=True)

    image = Column(String)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<VisualRecord(image='%s')>" % (self.image)

    def __ne__(self, other):
        return not_equal(self, other, ['image'], 1e-07, 1e-07)

# Track state changes from each command issued in APL
#
class StateChange(Base):
    __tablename__ = 'apl_state_change'

    id = Column(Integer, primary_key=True)

    command = Column(String)
    x = Column(Integer)
    y = Column(Integer)
    altitude = Column(Integer)
    yaw = Column(Integer)
    pitch = Column(Integer)
    roll = Column(Integer)
    speed = Column(Integer)
    fuel = Column(Integer)
    armed = Column(Boolean)
    mode = Column(Integer)
    time_start = Column(DateTime)
    home_x = Column(Integer)
    home_y = Column(Integer)
    loiter_x = Column(Integer)
    loiter_y = Column(Integer)
    wind_direction = Column(Integer)
    in_canyon = Column(Boolean)
    plan = Column(String)
    rtl_override = Column(Boolean)
    craft_state = Column(Integer)
    payload = Column(String)
    payload_limit = Column(Integer)
    flight_safety = Column(Boolean)
    total_1 = Column(Integer)
    total_2 = Column(Integer)
    total_3 = Column(Integer)
    game_mode = Column(Integer)
    found_hiker = Column(Boolean)
    successful_package_drops = Column(Integer)
    max_package_drops = Column(Integer)
    landed_after_hiker_found = Column(Boolean)
    dropped_list = Column(String)

    uuid = Column(String)
    instance = Column(String)
    session = Column(String)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<StateChange(x='%d', y='%d', z='%d', h='%d', p='%d', r='%d', speed='%d', fuel='%d', armed='%r', mode='%d')>" % (
                    self.x, self.y, self.z, self.h, self.p, self.r, self.speed, self.fuel, self.armed, self.mode)

    def __ne__(self, other):
        return True


class Region(Base):
    __tablename__ = 'apl_region'

    id = Column(Integer, primary_key=True)

    region = Column(String)
    x = Column(Integer)
    y = Column(Integer)

    uuid = Column(String)
    start_timestamp = Column(DateTime)
    stop_timestamp = Column(DateTime)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return "<Region(region='%s', x='%d', y='%d')>" % (self.region, self.x, self.y)

    def __ne__(self, other):
        return not_equal(self, other, ['region', 'x', 'y'], 1e-07, 1e-07)


class GameScore(Base):
    __tablename__ = 'apl_game_score'

    id = Column(Integer, primary_key=True)

    total_1 = Column(Integer)
    change_1 = Column(Integer)
    reason_1 = Column(String)

    total_2 = Column(Integer)
    change_2 = Column(Integer)
    reason_2 = Column(String)

    total_3 = Column(Integer)
    change_3 = Column(Integer)
    reason_3 = Column(String)

    uuid = Column(String)
    start_timestamp = Column(DateTime)
    stop_timestamp = Column(DateTime)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return ("<GameScore(total_1='%d', change_1='%d', reason_1='%s', total_2='%d', change_2='%d', reason_2='%s', total_3='%d', change_3='%d', reason_3='%s')>" %
                (self.total_1, self.change_1, self.reason_1, self.total_2, self.change_2, self.reason_2, self.total_3, self.change_3, self.reason_3))

    def __ne__(self, other):
        return not_equal(self, other, ['total_1', 'change_1', 'reason_1','total_2', 'change_2', 'reason_2', 'total_3', 'change_3', 'reason_3'], 1e-07,
                         1e-07)

class PicTaken(Base):
    __tablename__ = 'apl_pic_taken'

    id = Column(Integer, primary_key=True)
    
    subject = Column(String)
    lat = Column(Integer)
    lon = Column(Integer)

    uuid = Column(String)
    start_timestamp = Column(DateTime)
    stop_timestamp = Column(DateTime)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return("<PicTaken(subject='%s', lon='%d', lat='%d')>" % (self.subject, self.lon, self.lat))

    def __ne__(self, other):
        return not_equal(self, other, ['subject', 'lon', 'lat'], 1e-07, 1e-07)


class CameraRoll(Base):
    __tablename__ = 'apl_camera_roll'

    id = Column(Integer, primary_key=True)
    
    hikers = Column(Integer)
    deer = Column(Integer)
    bears = Column(Integer)
    scenery = Column(Integer)

    uuid = Column(String)
    start_timestamp = Column(DateTime)
    stop_timestamp = Column(DateTime)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return("<CameraRoll(hikers='%d', deer='%d', bears='%d', scenery='%d')>" % (self.hikers, self.deer, self.bears, self.scenery))

    def __ne__(self, other):
        return not_equal(self, other, ['hikers', 'deer', 'bears', 'scenery'], 1e-07, 1e-07)


class AOI(Base):
    __tablename__ = 'apl_area_of_interest'

    id = Column(Integer, primary_key=True)

    x = Column(Integer)
    y = Column(Integer)
    width = Column(Integer)
    height = Column(Integer)
    map_name = Column(String)
    map_string = Column(String)

    uuid = Column(String)
    start_timestamp = Column(DateTime)
    stop_timestamp = Column(DateTime)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return ("<AOI(x='%d', y='%d', width='%d', height='%d', map_name='%s', map_string='%s')>" % 
                (self.x, self.y, self.width, self.height, self.map_name, self.map_string))


#
# Added for ArduPilot Light Simulation
# This is a Exigisi controlled data structure
#
class Flights(Base):
    __tablename__ = 'flights'

    id = Column(Integer, primary_key=True)

    uuid = Column(String)
    display_name = Column(String)
    session = Column(String)
    instance = Column(String)
    pilot = Column(String)
    scenario = Column(String)
    prior_flight = Column(String)
    name = Column(String)

    start_timestamp = Column(DateTime)
    stop_timestamp = Column(DateTime)
    timestamp = Column(DateTime)
    simulation_session_uuid = Column(String)

    def __repr__(self):
        return ("<Flights(uuid='%s', display_name='%s', session='%s', instance='%s', pilot='%s', scenario='%s', prior_flight='%s', name='%s')>" %
                 (self.uuid, self.display_name, self.session, self.instance, self.pilot, self.scenario, self.prior_flight). self.name)

    def __ne__(self, other):
        return not_equal(self, other, ['uuid', 'display_name', 'session', 'instance', 'pilot', 'scenario', 'prior_flight', 'name'], 1e-07, 1e-07)

# fin
