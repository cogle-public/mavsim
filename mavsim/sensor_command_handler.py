# -----------------------------------------------------------------------------
#  MAVSim::SENSOR Command Handler
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from. import settings
import sys

class StaticObject:
    def __init__(self, name, lat, lon, alt, radius, detect_100, detect_75, detect_50, detect_25):
        self.name = name
        self.lat = lat
        self.lon = lon
        self.alt = alt
        self.radius = radius
        self.detect_100 = detect_100
        self.detect_75 = detect_75
        self.detect_50 = detect_50
        self.detect_25 = detect_25


class DynamicObject:
    def __init__(self, name, lat, lon, alt, radius, detect_100, detect_75, detect_50, detect_25, movement_alg):
        self.name = name
        self.lat = lat
        self.lon = lon
        self.alt = alt
        self.radius = radius
        self.detect_100 = detect_100
        self.detect_75 = detect_75
        self.detect_50 = detect_50
        self.detect_25 = detect_25
        self.movement_alg = movement_alg


def sensor_thread(sensor):
    settings.logger.info("Sensing loop setup...")
    loop = 0
    while settings.sim_running:
        if settings.sensor_loop != loop:
            if settings.craft_state == settings.CraftState.ON:
                try:
                    loop = settings.sensor_loop
                    sensor.get_current_craft_sensing()
                except:
                    e, v, t = sys.exc_info()
                    settings.logger.error("Sensor Thread Error: %s - %s" % (e, str(v)))


class SENSORCommandHandler:
    def __init__(self, craft, mem_buffer, sim_command):
        settings.logger.info("SENSOR command handler setup...")
        self.craft = craft
        self.mem_buffer = mem_buffer
        self.sim_command = sim_command

        # 1/2 Field of View for the primary sensor
        self.sensing_theta = 25.0
        # Bounding sphere radius for craft
        self.craft_radius = 4.2
        # Minimum relative altitude
        self.min_relative_alt = -5.5

        self.static_objects_list = []
        self.dynamic_objects_list = []

        self.switch_dict = {'MS_ADD_STATIC_OBJECT': self.add_static_object_command,
                            'MS_ADD_DYNAMIC_OBJECT': self.add_dynamic_object_command}

        # Craft State
        self.lat = None
        self.lon = None
        self.rel_alt = None
        self.alt = None
        self.last_gps_count = -1


    def __del__(self):
        del self.mem_buffer
        del self.craft

    def process(self, message):
        self.message = message
        settings.logger.info("...processing command - %s" % message[1])

        try:
            if settings.craft_state == settings.CraftState.OFF:
                raise Exception("Craft is OFF or DEAD.")

            return self.switch_dict[message[1]]()
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Process Error: %s - %s" % (e, str(v)))
            return "('ERR', 'COMMAND DOES NOT EXIST', %s)" % str.upper(str(v))

    # def get_current_craft_sensing(self):
    #     # 1. Get current gps lat, lon, and relative_alt
    #     if self.mem_buffer.global_position_int_buf is not None:
    #         self.lat = self.mem_buffer.global_position_int_buf['lat']
    #         self.lon = self.mem_buffer.global_position_int_buf['lon']
    #         self.alt = self.mem_buffer.global_position_int_buf['alt']
    #         self.rel_alt = 1e3 * (self.alt*1e-3 - self.srtm.GetElevation(1e-7*self.lat, 1e-7*self.lon))
    #         # print("Craft (%f, %f) - %f [%f]" % (self.lat*1e-07, self.lon*1e-07, self.alt*1e-03, self.rel_alt*1e-03))
    #     else:
    #         return False
    #
    #     # 2. Determine if in collision with the ground,
    #     #    if so set DEAD send out through telemetry
    #     if (self.rel_alt*1e-03) < self.min_relative_alt:
    #         self.mem_buffer.update_telemetry(
    #             ("MS_CRAFT_CRASH {cause : %s, lat : %f, lon : %f, alt : %f, speed : %f}" %
    #             ("Collided with ground", self.lat, self.lon, self.alt, self.craft.getSpeed())))
    #         settings.logger.info("!!!!! CRASH -- Collided with ground !!!!!")
    #
    #         # TODO: Turn off aircraft simulation
    #         self.craft.disarm()
    #         #self.sim_command.controls.pause()
    #         settings.craft_state = settings.CraftState.OFF
    #         return True
    #
    #     # 3. Determine if in collision with any entity on lists
    #     #    if so set DEAD send out through telemetry
    #     # 4. Determine conic section of sensing cone
    #     # 5. Determine collision of objects with sensing cone
    #     # 6. Report objects in collision as appropriate
    #     for obj in self.static_objects_list:
    #         detected = False
    #         # print "Checking for Object: %s" % obj.name
    #
    #         # Within proper altitude range?
    #         if 0 > obj.alt > ((self.rel_alt*1e-03) + self.craft_radius + obj.radius):
    #             continue
    #
    #         # print "    Within altitude range"
    #
    #         # Within altitude adjusted radius
    #         detect_radius = math.fabs((self.rel_alt*1e-03) - obj.alt) * math.tan(math.radians(self.sensing_theta))
    #         # print "    Detection radius = %f" % detect_radius
    #         craft_gps = (self.lat*1e-07, self.lon*1e-07)
    #         # print "    Craft: %f, %f - %f" % (self.lat*1e-07, self.lon*1e-07, self.rel_alt*1e-03)
    #         obj_gps = (obj.lat, obj.lon)
    #         # print "    Object: %f, %f - %f" % (obj.lat, obj.lon, obj.alt)
    #         distance = vincenty(craft_gps, obj_gps).meters
    #         # print "    Distance = %f" % distance
    #         if distance <= detect_radius:
    #             detected = True
    #             # print "    Within detection radius !"
    #
    #         # In collision with craft?
    #         alt_delta = math.fabs((self.rel_alt*1e-03) - obj.alt)
    #         proximity_radius = self.craft_radius + obj.radius
    #         if alt_delta < proximity_radius and distance < proximity_radius:
    #             description = "Collided with object %s" % obj.name
    #             self.mem_buffer.update_telemetry(
    #                 ("MS_CRAFT_CRASH {cause : %s, lat : %f, lon : %f, alt : %f, speed : %f}" %
    #                 (description, self.lat, self.lon, self.alt, self.craft.getSpeed())))
    #             settings.logger.info("!!!!! CRASH -- %s !!!!!" % description)
    #
    #             # TODO: Turn off aircraft simulation
    #             self.craft.disarm()
    #             #self.sim_command.controls.pause()
    #             settings.craft_state = settings.CraftState.OFF
    #             return True
    #
    #         # print "    Not in collision !"
    #
    #         # Inside cone, so how much is detected?
    #         if detected:
    #             d = distance
    #             R = detect_radius
    #             r = obj.radius
    #             overlap = 0.0
    #
    #             if d <= math.fabs(R - r):
    #                 # One circle is entirely enclosed in the other.
    #                 overlap = math.pi * min(R, r) ** 2
    #                 # print "      Complete circle overlap = %f" % overlap
    #             elif d >= r + R:
    #                 # The circles don't overlap at all.
    #                 overlap = 0.0
    #                 # print "      No circle overlap = %f" % overlap
    #             else:
    #                 r2, R2, d2 = r ** 2, R ** 2, d ** 2
    #                 alpha = math.acos((d2 + r2 - R2) / (2 * d * r))
    #                 beta = math.acos((d2 + R2 - r2) / (2 * d * R))
    #                 overlap = (r2 * alpha + R2 * beta - 0.5 * (r2 * math.sin(2 * alpha) + R2 * math.sin(2 * beta)))
    #                 # print "      Some circle overlap = %f" % overlap
    #
    #             area = overlap / (math.pi * detect_radius ** 2)
    #
    #             if area > 0.0:
    #                 # print ">>>-----> Detected %s with %f overlap" % (obj.name, area*1e04)
    #
    #                 # 20% noise magic number
    #                 #   Should this be specified per object?
    #                 # TODO: Address magic number issue
    #                 error = (0.2 * area) * (random.randint(1,100)/100.0)
    #                 if random.randint(1,100) > 50:
    #                     error = -error
    #                 object_recognition_probability = ((area * 1e04) + error)
    #
    #                 object_recognized_as = "unknown"
    #
    #                 if object_recognition_probability > 0.99:
    #                     object_recognized_as = obj.detect_100
    #                 elif object_recognition_probability >= 0.75:
    #                     object_recognized_as = obj.detect_75
    #                 elif object_recognition_probability >= 0.5:
    #                     object_recognized_as = obj.detect_50
    #                 elif object_recognition_probability >= 0.25:
    #                     object_recognized_as = obj.detect_25
    #
    #                 # print ">>>----->Object is %s" % (object_recognized_as)
    #
    #                 # Send out through telemetry
    #                 settings.logger.info("Sensing object: %s (%.9f, %.9f, %.9f) with radius %.9f at a distance of %.9f" %
    #                                      (object_recognized_as, obj.lat, obj.lon, obj.alt, obj.radius, distance))
    #                 self.mem_buffer.update_telemetry(
    #                     ("MS_SENSED_OBJECT {name : %s, lat : %.9f, lon : %.9f, alt : %.9f, radius : %.9f}" %
    #                      (object_recognized_as, obj.lat, obj.lon, obj.alt, obj.radius)))
    #
    #         # print "-----------------------------------------------------------------"
    #
    #     return False

    def add_static_object_command(self):
        try:
            value = 'SUCCESS'

            obj = StaticObject(self.message[2],
                               self.message[3],
                               self.message[4],
                               self.message[5],
                               self.message[6],
                               self.message[7],
                               self.message[8],
                               self.message[9],
                               self.message[10])

            self.static_objects_list.append(obj)

            return "('ACK', 'ADD_STATIC_OBJECT', '%s')" % str.upper(value)
        except:
            e, v, t=sys.exc_info()
            settings.logger.error("Add Static Object Error: %s - %s" % (e, str(v)))
            return "('ERR', 'ADD_STATIC_OBJECT', '%s')" % str.upper(str(v))

    def add_dynamic_object_command(self):
        try:
            value='NOT IMPLEMENTED YET'
            return "('ACK', 'ADD_DYNAMIC_OBJECT', '%s')" % str.upper(value)
        except:
            e, v, t=sys.exc_info()
            settings.logger.error("Add Dynamic Object Error: %s - %s" % (e, str(v)))
            return "('ERR', 'ADD_DYNAMIC_OBJECT', '%s')" % str.upper(str(v))

    # def _command(self):
    #     try:
    # 		value = 'AWESOME'
    #         return ("('ACK', '', '%s')" % str.upper(value))
    #     except:
    #         e, v, t=sys.exc_info()
    #         settings.logger.error("Error: %s - %s" % (e, str(v)))
    #         return ("('ERR', 'SET_MODE', '%s')" % str.upper(str(v)))

# fin
