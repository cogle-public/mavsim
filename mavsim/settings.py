# -----------------------------------------------------------------------------
#  MAVSim::Settings - Where GLOBAL variables and components are defined
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

import sys
import logging
import os


def enum(**enums):
    return type('Enum', (), enums)


def init():
    global software_version
    software_version = 1.1

    # Get an instance of a logger and configure
    global logger
    logger = logging.getLogger(__name__)
    hdlr = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s %(levelname)s MAVSIM %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)

    # set the log level
    # PRODUCTION: set this to ERROR otherwise INFO
    logger.setLevel(logging.INFO)

    # Simulation state
    #
    global sim_running
    sim_running = True

    global telemetry_engaged
    telemetry_engaged = True

    global flight_lockout
    flight_lockout = False

    global flight_auto_abort
    flight_auto_abort = False

    # Nixel support
    #
    global nwm  # Nixel world manager, used for generating/managing local world repository
    nwm = None

    global is_nixel_world
    is_nixel_world = False 

    # Connection Ports
    #
    global mav_ip
    mav_ip = None
    global telemetry_ip
    telemetry_ip = None

    # User commands lock
    #
    global cmd_lock
    cmd_lock = False

    # Sensor semaphore
    global sensor_loop
    sensor_loop = 0

    # Simulator Instance and Session
    #
    global mavsim_instance
    mavsim_instance = "mavsim"
    try:
        mavsim_instance = str(os.environ['INSTANCE'])
    except:
        pass

    global mavsim_session
    mavsim_session = "jeff"
    # Jeff Sessions ! (LMAO!!)
    try:
        mavsim_session = str(os.environ['SESSION'])
    except:
        pass

    global mavsim_pilot
    mavsim_pilot = "Chuck"
    # In homage to Chuck Yeager
    try:
        mavsim_pilot = str(os.environ['PILOT'])
    except:
        pass

    global use_db
    use_db = True

    global database_url
    database_url = os.environ.get('LOGGING_DATABASE', None)
    if database_url is None:
        use_db = False
        logger.warning('NO DATABASE logging. :(')
    else:
        logger.info('Connecting to database with the following specification %s' % database_url)

    # database_url = dbinfo  = 'postgresql://postgres:123456@localhost:32768/apm_missions'
    # Example AWS URL
    # database_url = dbinfo = 'postgresql://cogle:ardupilot@cogle-mission-recorder.cobddoocz4f4.us-east-1.rds.amazonaws.com:5432/apm_missions'
    # If not going to use a database, then set to None
    # database_url = None

    # Sim operation state (rules for navigation, drop, etc.)
    # 0: original mavsim operation state (15 degrees of freedom, drop is dependent on speed and altitude)
    # 2: Phase 2 COGLE operation state (9 degrees of freedom)
    global sim_op_state
    sim_op_state = 0

    global CraftState
    CraftState = enum(OFF=0, ON=1)

    global craft_state
    craft_state = CraftState.OFF

    global telemetry_direct_clients
    telemetry_direct_clients = []

    global multicast_sending
    multicast_sending = False

    global apl
    apl = False

    global FUEL_MAX
    FUEL_MAX = 999999

    global simulation_session_uuid
    simulation_session_uuid = ''

    global aoi_x
    aoi_x = -1

    global aoi_y
    aoi_y = -1

    global aoi_x_extent
    aoi_x_extent = 20

    global aoi_y_extent
    aoi_y_extent = 20

    global visual_record
    visual_record = 0

    global visual_record_mode
    visual_record_mode = 1

class StandardError(Exception):
    def __init__(self, message):
        super().__init__(message)


# fin
