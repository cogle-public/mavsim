# -----------------------------------------------------------------------------
#  MAVSim::ArduPilotLight
# -----------------------------------------------------------------------------
# Micro-Air Vghicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from . import settings
from .settings import StandardError
import datetime
import math
import operator
import heapq
import numpy as np

from itertools import product
from random import randint, random
from math import sin, cos
from ast import literal_eval

from .ardupilotlight_simulation_core_0 import Game_Move
from enum import IntEnum


class CraftMode(IntEnum):
    MANUAL = 0
    GUIDED = 1
    AUTO = 2
    RTL = 3
    LOITER = 4
    UNKNOWN = 5


class GameMode(IntEnum):
    NONE = 0
    LOST_HIKER = 1
    COUNT_BEARS = 2
    COUNT_DEER = 3
    COUNT_ANIMALS = 4


class CraftAltitude(IntEnum):
    GROUND = 0
    LOW = 1
    MEDIUM = 2
    HIGH = 3


class CraftUpdate(IntEnum):
    MODE_CHANGE = 0
    ARM_CHANGE = 1
    GET_DATA = 2
    SPEED_CHANGE = 3
    HOME_CHANGE = 4
    ALTITUDE_CHANGE = 5
    NEW_PLAN = 6
    LOAD_PAYLOAD = 7
    DROP_PAYLOAD = 8


class ArduPilotLight_0:
    def __init__(self, buf):
        settings.logger.info("ArduPilot Light initialization...")
        self.buf = buf # Memory Buffer, receive all messages through here
        self.x = 0
        self.y = 0
        self.altitude = 0
        self.speed = 0
        self.yaw = 0
        self.pitch = 0
        self.roll = 0
        self.fuel = settings.FUEL_MAX
        self.armed = False
        self.mode = CraftMode.MANUAL
        self.telemetry = None
        self.time_start = datetime.datetime.now()
        self.home_x = 0
        self.home_y = 0
        self.home_z = 0
        self.loiter_x = 0
        self.loiter_y = 0
        self.loiter_z = 0

        self.wind_direction = 7
        self.in_canyon = False

        self.sim = None

        self.plan = []

        self.rtl_override = False
        settings.craft_state = settings.CraftState.ON

        # Payload
        self.payload = ["EMPTY", "EMPTY", "EMPTY", "EMPTY"]
        self.payload_limit = 3

        # Settable parameters
        self.flight_safety = True

        # Game Scoring
        self.total_1 = 0
        self.total_2 = 0
        self.total_3 = 0

        # Pictures taken
        self.pics_hikers = 0
        self.pics_deer = 0
        self.pics_bears = 0
        self.pics_scenery = 0

        self.game_mode = GameMode.LOST_HIKER
        self.found_hiker = False
        self.successful_package_drops = 0
        self.max_package_drops = 2
        self.landed_after_hiker_found = False
        self.dropped_list = []

    def reinit(self):
        settings.logger.info("ArduPilot Light re-initialization...")
        self.x = 0
        self.y = 0
        self.altitude = 0
        self.speed = 0
        self.yaw = 0
        self.pitch = 0
        self.roll = 0
        self.fuel = settings.FUEL_MAX
        self.armed = False
        self.mode = CraftMode.MANUAL
        self.time_start = datetime.datetime.now()
        self.home_x = 0
        self.home_y = 0
        self.home_z = 0
        self.loiter_x = 0
        self.loiter_y = 0
        self.loiter_z = 0

        self.wind_direction = 7
        self.in_canyon = False

        self.plan = []

        self.rtl_override = False
        settings.craft_state = settings.CraftState.ON

        # Payload
        self.payload = ["EMPTY", "EMPTY", "EMPTY", "EMPTY"]
        self.payload_limit = 3
        self.payload_drop = False
        self.payload_load = False

        # Settable parameters
        self.flight_safety = True

        # Game Scoring
        self.total_1 = 0
        self.total_2 = 0
        self.total_3 = 0

        self.game_mode = GameMode.LOST_HIKER
        self.found_hiker = False
        self.successful_package_drops = 0
        self.max_package_drops = 2
        self.landed_after_hiker_found = False
        self.dropped_list = []

    def __del__(self):
        settings.logger.info("...ArduPilot Light shutting down")

    '''
        ----------------------------------------------------------------------------------------------------------------
                                                    Basic Instrumentation
        ----------------------------------------------------------------------------------------------------------------
    '''

    def isArmed(self):
        self.telemetry.telereceiver(self.output_command_ack(33330105, 0))
        self.state_update(CraftUpdate.GET_DATA)

        return self.armed

    def arm(self):
        if self.fuel > 0:
            self.armed = True
            self.telemetry.telereceiver(self.output_command_ack(33330103, 0))
        else:
            self.armed = False
            self.telemetry.telereceiver(self.output_command_ack(33330103, 1))
            raise StandardError("Can not arm when out of fuel.")

        self.state_update(CraftUpdate.ARM_CHANGE)

    def disarm(self):
        self.armed = False

        self.telemetry.telereceiver(self.output_command_ack(33330104, 0))
        self.state_update(CraftUpdate.ARM_CHANGE)

    def getMode(self):
        self.telemetry.telereceiver(self.output_command_ack(33330102, 0))
        self.state_update(CraftUpdate.GET_DATA)

        mode_str = ''

        if self.mode == CraftMode.AUTO:
            mode_str = 'AUTO'
        elif self.mode == CraftMode.RTL:
            mode_str = 'RTL'
        elif self.mode == CraftMode.LOITER:
            mode_str = 'LOITER'
        elif self.mode == CraftMode.GUIDED:
            mode_str = 'GUIDED'
        elif self.mode == CraftMode.MANUAL:
            mode_str = 'MANUAL'
        else:
            mode_str = 'UNKNOWN'

        return mode_str

    def setMode(self, mode_in):
        status = 0

        if mode_in == 'MANUAL':
            mode = CraftMode.MANUAL
        elif mode_in == 'AUTO':
            mode = CraftMode.AUTO
        elif mode_in == 'GUIDED':
            mode = CraftMode.GUIDED
        elif mode_in == 'LOITER':
            mode = CraftMode.LOITER
        elif mode_in == 'RTL':
            mode = CraftMode.RTL
        else:
            status = 1
            mode = CraftMode.UNKNOWN
            settings.logger.error("--- Craft::setMode trying to set unknown mode \'%s\'" % mode_in)

        if self.altitude == 0 and not (mode == CraftMode.MANUAL or mode == CraftMode.AUTO):
            status = 1
            settings.logger.error("--- Craft::setMode trying to set craft in incorrect mode %s while on the ground" % mode_in)
            raise StandardError("Can only set to MANUAL or AUTO modes when on the ground.")
        else:
            self.mode = mode

        self.telemetry.telereceiver(self.output_command_ack(33330101, status))

        if status == 0:
            self.state_update(CraftUpdate.MODE_CHANGE)

    def getSpeed(self):
        self.telemetry.telereceiver(self.output_command_ack(33330111, 0))
        self.state_update(CraftUpdate.GET_DATA)

        return self.speed

    def setSpeed(self, speed):
        if self.armed is False and self.altitude > 0:
            raise StandardError("Must be ARMED and in the air.")

        self.mode = CraftMode.GUIDED

        into_wind = False
        with_wind = False

        if self.yaw == self.wind_direction:
            into_wind = True

        if abs(self.yaw - self.wind_direction) == 4:
            with_wind = True

        if self.altitude == 0:
            self.speed = 0
            self.telemetry.telereceiver(self.output_command_ack(33330118, 0))
            self.state_update(CraftUpdate.SPEED_CHANGE)
            return

        if self.in_canyon:

            if into_wind:
                if speed < 0:
                    speed = 0
                elif speed > 2:
                    speed = 2

                self.speed = speed
                self.telemetry.telereceiver(self.output_command_ack(33330118, 0))
                self.state_update(CraftUpdate.SPEED_CHANGE)
                return

            if with_wind:
                if speed < 2:
                    speed = 2
                elif speed > 7:
                    speed = 7

                self.speed = speed
                self.telemetry.telereceiver(self.output_command_ack(33330118, 0))
                self.state_update(CraftUpdate.SPEED_CHANGE)
                return

            if speed < 1:
                speed = 1
            elif speed > 4:
                speed = 4

            self.speed = speed
            self.telemetry.telereceiver(self.output_command_ack(33330118, 0))
            self.state_update(CraftUpdate.SPEED_CHANGE)
            return

        else:
            if into_wind:
                if speed < 0:
                    speed = 0
                elif speed > 3:
                    speed = 3

                self.speed = speed
                self.telemetry.telereceiver(self.output_command_ack(33330118, 0))
                self.state_update(CraftUpdate.SPEED_CHANGE)
                return

            if with_wind:
                if speed < 1:
                    speed = 1
                elif speed > 6:
                    speed = 6

                self.speed = speed
                self.telemetry.telereceiver(self.output_command_ack(33330118, 0))
                self.state_update(CraftUpdate.SPEED_CHANGE)
                return

            if speed < 1:
                speed = 1
            elif speed > 4:
                speed = 4

            self.speed = speed
            self.telemetry.telereceiver(self.output_command_ack(33330118, 0))
            self.state_update(CraftUpdate.SPEED_CHANGE)
            return

    def getHomePosition(self):
        self.telemetry.telereceiver(self.output_command_ack(33330199, 0))
        self.state_update(CraftUpdate.GET_DATA)

        return (self.home_y, self.home_x)

    def setHomePosition(self, x, y, z=0):

        if x >= self.sim.map_width:
            x = self.sim.map_width - 1

        if x < 0:
            x = 0

        if y >= self.sim.map_length:
            y = self.sim.map_length - 1

        if y < 0:
            y = 0

        if z >= self.sim.map_height:
            z = self.sim.map_height - 1

        if z < 0:
            z = 0

        self.home_x = x
        self.home_y = y
        self.home_z = z

        self.telemetry.telereceiver(self.output_command_ack(33330199, 0))
        self.state_update(CraftUpdate.HOME_CHANGE)

    def getAltitude(self):

        self.telemetry.telereceiver(self.output_command_ack(33330110, 0))
        self.state_update(CraftUpdate.GET_DATA)

        return self.altitude

    def setAltitude(self, alt):
        if abs(alt-self.altitude) > 1:
            raise StandardError("Can only set altitude 1 step up or down at a time.")
        elif abs(alt - self.altitude) > 0:
            if (alt - self.altitude) > 0:
                val = 1
                # Fuel cost
                self.fuel -= 1
            else:
                val = -1
        else:
            val = 0

        self.altitude += val

        if self.altitude > self.sim.map_height:
            self.altitude = self.sim.map_height - 1

        if self.altitude < 0:
            self.altitude = 0

        self.telemetry.telereceiver(self.output_command_ack(33330199, 0))
        self.state_update(CraftUpdate.ALTITUDE_CHANGE)

    def getAttitude(self):
        self.telemetry.telereceiver(self.output_command_ack(33330109, 0))
        self.state_update(CraftUpdate.GET_DATA)

        val = {}
        val['yaw'] = self.yaw
        val['pitch'] = self.pitch
        val['roll'] = self.roll

        return val

    def getHeading(self):
        self.telemetry.telereceiver(self.output_command_ack(33330112, 0))
        self.state_update(CraftUpdate.GET_DATA)

        return self.yaw

    def getLocation(self):
        self.telemetry.telereceiver(self.output_command_ack(33330113, 0))
        self.state_update(CraftUpdate.GET_DATA)

        val = {}
        val['lon'] = self.x
        val['lat'] = self.y
        val['fix_type'] = 0

        return val

    def query_terrain(self, x, y):
        q = self.sim.about_this_coordinate(x, y)

        if q is not None and q.name != 'M-T':
            val = (q.name, q.description, q.altitude)
            return val
        else:
            raise StandardError('This section of terrain does not appear to exist in this scenario.')

    def list_airports(self):
        airport_list = []

        for o in self.sim.object_layer_list:
            if o.type == 1:
                airport = (o.code, o.orientation, o.land[0], o.land[1], o.takeoff[0], o.takeoff[1])
                airport_list.append(airport)

        # self.telemetry.telereceiver(self.output_command_ack(33330137, 0))
        return airport_list

    def refuel(self):
        if self.armed is False and self.altitude == 0 and self.speed == 0 and self.sim.simple_at_airport():
            self.fuel = settings.FUEL_MAX
            settings.logger.info('>>>>>>>>>>>> REFUELING')
            self.telemetry.telereceiver(self.output_command_ack(33330134, 0))
        else:
            settings.logger.error('<<<<<<<<<<<< FAILED REFUELING')
            self.telemetry.telereceiver(self.output_command_ack(33330134, 1))

        self.state_update(CraftUpdate.GET_DATA)

    def fuel_status(self):
        self.telemetry.telereceiver(self.output_command_ack(33330135, 0))
        self.state_update(CraftUpdate.GET_DATA)

        return self.fuel

    def noop(self):
        self.telemetry.telereceiver(self.output_command_ack(33330199, 0))
        self.state_update(CraftUpdate.GET_DATA)

    def take_pic(self):
        pic = self.query_terrain(self.x, self.y)[0]
        
        if pic == 'hiker':
            self.pics_hikers += 1
        if pic == 'deer':
            self.pics_deer += 1
        if pic == 'bear':
            self.pics_bears += 1
        else:
            pic = 'scenery' # Comment this out to get more precise subject info
            self.pics_scenery += 1

        self.telemetry.telereceiver("PIC_TAKEN {subject : '%s', lon : %d, lat : %d}" % (pic, self.x, self.y))
        self.telemetry.telereceiver("CAMERA_ROLL {hikers : %d, deer : %d, bears : %d, scenery : %d}" %
                    (self.pics_hikers, self.pics_deer, self.pics_bears, self.pics_scenery))

        return pic

    def query_slice(self, x, y, w, l):
        codex = {
            "pine tree": 1.0,
            "grass": 2.0,
            "pine trees": 3.0,
            "trail": 5.0,
            "unstriped road": 9.0,
            "striped road": 10.0,
            "runway": 12.0,
            "shore": 15.0,
            "water": 15.0,
            "shore bank": 15.0
        }

        np_map = [[2.0 for _ in range(l)] for _ in range(w)]

        for b in range(l):
            ypos = y+b
            for a in range(w):
                xpos = x+w
                if xpos >= 0 and xpos < self.sim.map_width and ypos >= 0 and ypos < self.sim.map_length:
                    terrain = self.query_terrain(xpos, ypos)
                    if terrain[0] in codex:
                        np_map[b][a] = v
                    elif 'box canyon' in terrain[0] or 'hill' in terrain[0] or terrain[0] == 'mountain ridge':
                        if terrain[2] == 1:
                            np_map[b][a] = 24.0
                        if terrain[2] == 2:
                            np_map[b][a] = 25.0
                        if terrain[2] == 3:
                            np_map[b][a] = 26.0
                        if terrain[2] == 4:
                            np_map[b][a] = 31.0
                    elif 'shore' in terrain[0]:
                        np_map[b][a] = 15.0

        return np.array(np_map)

    def set_param(self, param, set):
        if param == 'FLIGHT_SAFETY':
            if set == 'OFF':
                self.flight_safety = False
                settings.logger.error('!!!!!!!!!!!! Turning FLIGHT SAFETY OFF !!!!!!!!!!!!!!')
            else:
                self.flight_safety = True
                settings.logger.error('~~~~~~~~~~~~ Turning FLIGHT SAFETY ON  ~~~~~~~~~~~~~~')

    def get_param(self, param):
        if param == 'FLIGHT_SAFETY':
            if self.flight_safety == False:
                return 'OFF'
            elif self.flight_safety == True:
                return 'ON'
            else:
                return 'UNKNOWN'

    '''
        ----------------------------------------------------------------------------------------------------------------
                                                       Guided Commands
        ----------------------------------------------------------------------------------------------------------------
    '''

    def flyTo(self, x, y, alt, h=None):
        if self.armed is False and self.altitude > 0:
            raise StandardError("Must be ARMED and in the air.")

        self.mode = CraftMode.GUIDED

        # World boundary checks for flight safety
        #
        if self.flight_safety:
            if x >= self.sim.map_width:
                x = self.sim.map_width - 1

            if x < 0:
                x = 0

            if y >= self.sim.map_length:
                y = self.sim.map_length - 1

            if y < 0:
                y = 0

        if alt < 0:
            alt = 0

        if alt > self.sim.map_height:
            alt = self.sim.map_height - 1

        settings.logger.info('Planning a path from (%d, %d, %d) to (%d, %d, %d)' % (self.x, self.y, self.altitude, x, y, alt))

        self.sim.move_queue = []

        self.fly_to_core(self.x, self.y, self.yaw, self.altitude, x, y, alt, h, 1, CraftMode.GUIDED, True)

        settings.logger.info('FLY TO Plan to fly to from (%d, %d, %d) heading %d to (%d, %d, %d)' % (self.x, self.y,
                                                                                    self.altitude, self.yaw, x, y, alt))
        # for n in self.sim.move_queue:
        #     settings.logger.info(n)
        # settings.logger.info("Move queue length: {}".format(len(self.sim.move_queue)))

        self.telemetry.telereceiver(self.output_command_ack(33330106, 0))
        self.state_update(CraftUpdate.NEW_PLAN)

    def flyToPlan(self, x, y, alt, h):

        # World boundary checks for flight safety
        #
        if self.flight_safety:
            if x >= self.sim.map_width:
                x = self.sim.map_width - 1

            if x < 0:
                x = 0

            if y >= self.sim.map_length:
                y = self.sim.map_length - 1

            if y < 0:
                y = 0

        if alt < 0:
            alt = 0

        if alt > self.sim.map_height:
            alt = self.sim.map_height - 1

        settings.logger.info('Only Planning a path from (%d, %d, %d) to (%d, %d, %d)' % (self.x, self.y, self.altitude, x, y, alt))

        self.sim.move_queue = []

        self.fly_to_core(self.x, self.y, self.yaw, self.altitude, x, y, alt, h, 1, CraftMode.GUIDED, True)

        settings.logger.info('FLY TO PLAN - Plan to fly to from (%d, %d, %d) heading %d to (%d, %d, %d)' % (self.x, self.y,
                                                                                    self.altitude, self.yaw, x, y, alt))
        # for n in self.sim.move_queue:
        #     settings.logger.info(n)

    def headTo(self, heading, distance, alt):
        if self.armed is False and self.altitude > 0:
            raise StandardError("Must be ARMED and in the air.")

        self.mode = CraftMode.GUIDED

        settings.logger.info('HEAD TO Plan to fly to from (%d, %d, %d) heading %d to heading %d a distance of %d at altitude %d' %
                                                           (self.x, self.y, self.altitude, self.yaw, heading, distance, alt))

        if heading == 1:
            head = 90
            theta = math.radians(head)
            x = int(math.ceil(self.x + distance * cos(theta)))
            y = int(math.floor(self.y - distance * sin(theta)))
        elif heading == 2:
            head = 45
            theta = math.radians(head)
            x = int(math.ceil(self.x + distance * cos(theta)))
            y = int(math.floor(self.y - distance * sin(theta)))
        elif heading == 3:
            head = 0
            theta = math.radians(head)
            x = int(math.ceil(self.x + distance * cos(theta)))
            y = int(math.floor(self.y - distance * sin(theta)))
        elif heading == 4:
            head = 315
            theta = math.radians(head)
            x = int(math.ceil(self.x + distance * cos(theta)))
            y = int(math.ceil(self.y - distance * sin(theta)))
        elif heading == 5:
            head = 270
            theta = math.radians(head)
            x = int(math.ceil(self.x + distance * cos(theta)))
            y = int(math.ceil(self.y - distance * sin(theta)))
        elif heading == 6:
            head = 225
            theta = math.radians(head)
            x = int(math.floor(self.x + distance * cos(theta)))
            y = int(math.ceil(self.y - distance * sin(theta)))
        elif heading == 7:
            head = 180
            theta = math.radians(head)
            x = int(math.ceil(self.x + distance * cos(theta)))
            y = int(math.floor(self.y - distance * sin(theta)))
        elif heading == 8:
            head = 135
            theta = math.radians(head)
            x = int(math.floor(self.x + distance * cos(theta)))
            y = int(math.floor(self.y - distance * sin(theta)))

        # World boundary checks for flight safety
        #
        if self.flight_safety:
            if x >= self.sim.map_width:
                x = self.sim.map_width - 1
            elif x < 0:
                x = 0

            if y >= self.sim.map_length:
                y = self.sim.map_length - 1
            elif y < 0:
                y = 0

            if alt >= self.sim.map_height:
                alt = self.sim.map_height - 1
            elif alt < 0:
                alt = 0

        # Convert to flyTo command
        self.flyTo(x, y, alt, heading)

        self.telemetry.telereceiver(self.output_command_ack(33330107, 0))

    def fly_to_core(self, x, y, h, current_alt, gx, gy, altitude, gh, speed, mode, armed, cruising=50, alt_min=0):
        self.plan_path(x, y, current_alt, h, gx, gy, altitude, gh, cruising, alt_min)
        if len(self.plan) > 1:
            self.plan.pop(0)

        alt_curr = current_alt
        h_curr = self.yaw

        if settings.is_nixel_world:

            for step in self.plan:
                p = 0
                if alt_curr > step[2]:
                    p = -45
                elif alt_curr < step[2]:
                    p = 45

                delta = 0
                if 0 < abs(step[3] - h_curr) <= 2:
                    delta = step[3] - h_curr
                elif abs(step[3] - h_curr) == 6:
                    if step[3] < h_curr:
                        delta = -2
                    else:
                        delta = 2
                elif abs(step[3] - h_curr) == 7:
                    if step[3] < h_curr:
                        delta = -1
                    else:
                        delta = 1

                if delta == 0:
                    r = 0
                elif delta == 1:
                    r = 20
                elif delta == 2:
                    r = 40
                elif delta == -1:
                    r = -20
                elif delta == -2:
                    r = -40
                else:
                    r = 0
                
                self.sim.move_queue.append(Game_Move(step[0], step[1], step[2], step[3], p, r, speed, 1, mode, armed))

                alt_curr = step[2]
                h_curr = step[3]
        else:
            dive = False

            if altitude < current_alt:
                # print('Is %d < %d ??' % (altitude, self.altitude))
                dive = True
            alt_changes = 0

            if abs(altitude - current_alt) > 0:
                alt_changes = abs(altitude - current_alt)

            steps = len(self.plan)
            alt_steps = 0
            if alt_changes > 0:
                alt_steps = steps // (alt_changes + 1)

            if alt_steps == 0 and steps < 2:
                alt_steps = 1

            count = 0
            a_count = 1
            alt_curr = current_alt
            h_curr = self.yaw

            for step in self.plan:
                p = 0

                # Determine Altitude and Pitch Change
                if a_count == alt_steps and alt_steps != 0:
                    if alt_curr == altitude:
                        # print('level')
                        pass
                    elif dive:
                        # print('Falling')
                        alt_curr -= 1
                        p = -45
                    else:
                        # print('Climbing')
                        alt_curr += 1
                        p = 45

                    a_count = 0

                # Determine Roll
                #  8 1 = 7 ~ 1
                #  8 2 = 6 ~ 2
                #  7 1 = 6 ~ 2
                #  1 8 = -7 ~ -1
                #  1 7 = -6 ~ -2
                #  2 8 = -6 ~ -2

                delta = 0
                if 0 < abs(step[2] - h_curr) <= 2:
                    delta = step[2] - h_curr
                elif abs(step[2] - h_curr) == 6:
                    if step[2] < h_curr:
                        delta = -2
                    else:
                        delta = 2
                elif abs(step[2] - h_curr) == 7:
                    if step[2] < h_curr:
                        delta = -1
                    else:
                        delta = 1

                if delta == 0:
                    r = 0
                elif delta == 1:
                    r = 20
                elif delta == 2:
                    r = 40
                elif delta == -1:
                    r = -20
                elif delta == -2:
                    r = -40
                else:
                    r = 0

                self.sim.move_queue.append(Game_Move(step[0], step[1], alt_curr, step[2], p, r, speed, 1, mode, armed))

                h_curr = step[2]
                count += 1
                a_count += 1

        for i in self.sim.move_queue:
            settings.logger.info(i)

    '''
        ----------------------------------------------------------------------------------------------------------------
                                                       Auto Commands
        ----------------------------------------------------------------------------------------------------------------
    '''

    def auto_takeoff(self, altitude, distance, end_heading):

        # Clear any current plans
        # Calculate a set of moves 10 spaces in front of current position
        # Keep speed at 1 for each for duration
        # Rise to altitude of 1

        blc = self.sim.about_this_coordinate(self.x, self.y)

        if self.armed is False or (self.mode != CraftMode.MANUAL):
            raise StandardError("Must be ARMED and in MANUAL mode")

        if blc.name != 'runway':
            raise StandardError('Can only takeoff only from an airport.')

        if distance >= self.sim.map_width//2:
            distance = self.sim.map_width//2

        if distance < 0:
            if settings.is_nixel_world:
                distance = 5
            else:
                distance = 1

        if altitude < 0:
            if settings.is_nixel_world:
                altitude = blc.altitude + 2
            else:
                altitude = blc.altitude + 1

        if altitude > self.sim.map_height:
            altitude = self.sim.map_height

        settings.logger.info('Flying to an alt of %d at distance of %d' % (altitude, distance))

        self.sim.move_queue = []

        if end_heading is None:
            end_heading = self.yaw

        if settings.is_nixel_world:
            r_dist = abs(distance - 5)  # NIXEL CHANGE: Move to end of runway first, then get wheels off of the ground

            plan_begin = []
            plan_end = []
            if self.yaw == 3:
                self.fly_to_core(self.x, self.y, self.yaw, self.altitude, self.x + 5, self.y, self.altitude, self.yaw, 1, CraftMode.AUTO, True)
                plan_begin = self.sim.move_queue
                i_step = plan_begin[-1]
                self.sim.move_queue = []
                self.fly_to_core(i_step.x, i_step.y, i_step.yaw, i_step.altitude, i_step.x + r_dist, i_step.y, altitude, end_heading, 1, CraftMode.AUTO, True)
            elif self.yaw == 1:
                self.fly_to_core(self.x, self.y, self.yaw, self.altitude, self.x, self.y - 5, self.altitude, self.yaw, 1, CraftMode.AUTO, True)
                plan_begin = self.sim.move_queue
                i_step = plan_begin[-1]
                self.sim.move_queue = []
                self.fly_to_core(i_step.x, i_step.y, i_step.yaw, i_step.altitude, i_step.x, i_step.y - r_dist, altitude, end_heading, 1, CraftMode.AUTO, True)
            elif self.yaw == 5:
                self.fly_to_core(self.x, self.y, self.yaw, self.altitude, self.x, self.y + 5, self.altitude, self.yaw, 1, CraftMode.AUTO, True)
                plan_begin = self.sim.move_queue
                i_step = plan_begin[-1]
                self.sim.move_queue = []
                self.fly_to_core(i_step.x, i_step.y, i_step.yaw, i_step.altitude, i_step.x, i_step.y + r_dist, altitude, end_heading, 1, CraftMode.AUTO, True)
            elif self.yaw == 7:
                self.fly_to_core(self.x, self.y, self.yaw, self.altitude, self.x - 5, self.y, self.altitude, self.yaw, 1, CraftMode.AUTO, True)
                plan_begin = self.sim.move_queue
                i_step = plan_begin[-1]
                self.sim.move_queue = []
                self.fly_to_core(i_step.x, i_step.y, i_step.yaw, i_step.altitude, i_step.x - r_dist, i_step.y, altitude, end_heading, 1, CraftMode.AUTO, True)
            else:
                raise StandardError('Cannot figure out how to takeoff from this airport!')
            
            plan_end = self.sim.move_queue
            self.sim.move_queue = plan_begin + plan_end

        else:
            if self.yaw == 3:
                self.fly_to_core(self.x, self.y, self.yaw, self.altitude, self.x + distance, self.y, altitude, end_heading, 1, CraftMode.AUTO, True)
            elif self.yaw == 1:
                self.fly_to_core(self.x, self.y, self.yaw, self.altitude, self.x, self.y - distance, altitude, end_heading, 1, CraftMode.AUTO, True)
            elif self.yaw == 5:
                self.fly_to_core(self.x, self.y, self.yaw, self.altitude, self.x, self.y + distance, altitude, end_heading, 1, CraftMode.AUTO, True)
            elif self.yaw == 7:
                self.fly_to_core(self.x, self.y, self.yaw, self.altitude, self.x - distance, self.y, altitude, end_heading, 1, CraftMode.AUTO, True)
            else:
                raise StandardError('Cannot figure out how to takeoff from this airport!')

        settings.logger.info('Auto takeoff plan')
        # for n in self.sim.move_queue:
        #     settings.logger.info(n)

        self.armed = True
        self.mode = CraftMode.AUTO
        self.speed = 1

        # Fuel cost
        self.fuel -= 10

        self.telemetry.telereceiver(self.output_command_ack(33330108, 0))
        self.state_update(CraftUpdate.NEW_PLAN)

    def auto_land_at_airport(self, airport_code):

        blc = self.sim.about_this_coordinate(self.x, self.y)

        if self.altitude <= blc.altitude and self.speed == 0:
            raise StandardError("Must be flying in order to auto land")

        if self.armed is False:
            raise StandardError("Must be armed in order to auto land")

        self.sim.move_queue = []

        found = False
        east_west_oriented = False
        west_approach = False
        north_south_oriented = False
        north_approach = False
        target_x = 0
        target_y = 0
        target_z = 0
        airport_x = 0
        airport_y = 0
        airport_z = 0

        for o in self.sim.object_layer_list:
            if o.code == airport_code:
                airport_x = o.land[0]
                airport_y = o.land[1]
                airport_z = o.altitude
                target_z = o.altitude + 2

                # Find orientation of the airport and best approach point
                if settings.is_nixel_world:    # NIXEL CHANGE: Nixel airports are set up slightly differently
                    if o.width > o.length:
                        east_west_oriented = True
                        target_x = o.land[0] + 7
                        target_y = o.land[1]
                    else:
                        north_south_oriented = True
                        target_x = o.land[0]
                        target_y = o.land[1] + 7
                else:
                    if abs(o.land[0] - o.takeoff[0]) > 1:
                        east_west_oriented = True
                        if o.takeoff[0] < o.land[0]:
                            west_approach = True
                    elif abs(o.land[1] - o.takeoff[1]) > 1:
                        north_south_oriented = True
                        if o.takeoff[1] > o.land[1]:
                            north_approach = True
                    else:
                        raise StandardError("Cannot determine airport orientation and approach information? Is data correct?")

                    if east_west_oriented:
                        if west_approach:
                            target_x = o.takeoff[0] - 2
                            target_y = o.land[1]
                        else:
                            target_x = o.takeoff[0] + 2
                            target_y = o.land[1]
                    elif north_south_oriented:
                        if north_approach:
                            target_x = o.land[0]
                            target_y = o.takeoff[1] + 2
                        else:
                            target_x = o.land[0]
                            target_y = o.takeoff[1] - 2
                    else:
                        raise StandardError("Confused on which way to land?")

                found = True

        if not found:
            raise StandardError("Airport '%s' not found in this scenario map." % (airport_code))

        settings.logger.info('Planning a route back to %s from (%d, %d) heading %d to (%d, %d).' % (airport_code,
                                                                        self.x, self.y, self.yaw, airport_x, airport_y))

        # Plan a course to the airport
        if north_south_oriented:
            if north_approach:
                self.fly_to_core(self.x, self.y, self.yaw, self.altitude, target_x, target_y, target_z, 5, 1, CraftMode.AUTO, True, alt_min=1)
            else:
                self.fly_to_core(self.x, self.y, self.yaw, self.altitude, target_x, target_y, target_z, 1, 1, CraftMode.AUTO, True, alt_min=1)
        elif east_west_oriented:
            if west_approach:
                self.fly_to_core(self.x, self.y, self.yaw, self.altitude, target_x, target_y, target_z, 3, 1, CraftMode.AUTO, True, alt_min=1)
            else:
                self.fly_to_core(self.x, self.y, self.yaw, self.altitude, target_x, target_y, target_z, 7, 1, CraftMode.AUTO, True, alt_min=1)

        plan_begin = self.sim.move_queue

        # settings.logger.info('Plan from current position to before airport')
        # for n in plan_begin:
        #     settings.logger.info(n)

        end = plan_begin[-1]
        self.sim.move_queue = []

        # Plan a landing
        if settings.is_nixel_world:    # NIXEL CHANGE: FlyTo command with heading in mind
            if north_south_oriented:
                if north_approach:
                    self.fly_to_core(end.x, end.y, end.yaw, end.altitude, airport_x, airport_y, airport_z+1, 5, 0, CraftMode.AUTO, True, alt_min=0)
                else:
                    self.fly_to_core(end.x, end.y, end.yaw, end.altitude, airport_x, airport_y, airport_z+1, 1, 0, CraftMode.AUTO, True, alt_min=0)
            elif east_west_oriented:
                if west_approach:
                    self.fly_to_core(end.x, end.y, end.yaw, end.altitude, airport_x, airport_y, airport_z+1, 3, 0, CraftMode.AUTO, True, alt_min=0)
                else:
                    self.fly_to_core(end.x, end.y, end.yaw, end.altitude, airport_x, airport_y, airport_z+1, 7, 0, CraftMode.AUTO, True, alt_min=0)
        else:
            self.fly_to_core(end.x, end.y, end.yaw, 1, airport_x, airport_y, 0, None, 1, CraftMode.AUTO, True)
        plan_end = self.sim.move_queue

        # settings.logger.info('Plan from before airport to landing spot')
        # for n in plan_end:
        #     settings.logger.info(n)

        # Set final speed to ZERO
        end = plan_end[-1]
        end.speed = 0
        end.mode = CraftMode.MANUAL
        end.armed = False

        self.sim.move_queue = []
        self.sim.move_queue = plan_begin + plan_end

        settings.logger.info('Complete auto landing plan from current to airport to landing spot')
        # for n in self.sim.move_queue:
        #     settings.logger.info(n)

        self.mode = CraftMode.AUTO
        self.speed = 1

        # Fuel cost
        self.fuel -= 5

        self.telemetry.telereceiver(self.output_command_ack(33330119, 0))
        self.state_update(CraftUpdate.NEW_PLAN)

    def auto_abort(self):
        if self.armed is False or (self.mode != CraftMode.AUTO):
            raise StandardError("Must be ARMED and in AUTO mode")

        settings.logger.info('Auto aborting!!!')

        # Clear any current plans
        # If alt = 0, stop plane on ground
        # Else fly straight, low, and slow
        if self.altitude == self.sim.about_this_coordinate(self.x, self.y).altitude:
            self.speed = 0
        else:
            self.sim.move_queue = []
            self.speed = 1

        self.mode = CraftMode.MANUAL

        self.telemetry.telereceiver(self.output_command_ack(33330120, 0))
        self.state_update(CraftUpdate.NEW_PLAN)

    def auto_taxi(self):
        settings.logger.info("Craft at {}, taxi to takeoff".format((self.x, self.y, self.altitude)))
        for o in self.sim.object_layer_list:
            # settings.logger.info("{} at {}".format(o.name, o.takeoff))
            if self.euclidean_distance(o.land[0], o.land[1], self.x, self.y) < 10.0:
                airport_x = o.takeoff[0]
                airport_y = o.takeoff[1]
                airport_z = o.altitude
                if settings.is_nixel_world:    # NIXEL CHANGE: auto taxi needs to move to correct altitude
                    airport_z = airport_z + 1
                airport_heading = o.orientation

                self.sim.move_queue = []
                self.sim.move_queue.append(Game_Move(airport_x, airport_y, airport_z, airport_heading, 0, 0, 0, 1, CraftMode.MANUAL, False))

                self.armed = True
                self.mode = CraftMode.AUTO
                self.speed = 1

                self.telemetry.telereceiver(self.output_command_ack(33330121, 0))
                self.state_update(CraftUpdate.NEW_PLAN)
                return True

        self.telemetry.telereceiver(self.output_command_ack(33330121, 1))
        raise StandardError("Cannot determine airport at which I am located!")

    def abortLanding(self):
        raise StandardError('APL does not use abortLanding, use auto_abort instead.')

    def takeoff(self):
        raise StandardError('APL does not use takeoff, use auto_takeoff instead.')

    def land(self):
        raise StandardError('APL does not use land, use auto_land_at_airport instead.')

    '''
        ----------------------------------------------------------------------------------------------------------------
                                                       Payload Commands
        ----------------------------------------------------------------------------------------------------------------
    '''
    def resolve_payload_drop(self, slot):
        settings.logger.info('Resolving payload drop...')

        name = self.payload[slot]

        # MS_PAYLOAD_DROP {slot: %s, item: %s, lat: %f, lon: %f, alt: %f, speed: %f}
        msg = "MS_PAYLOAD_DROP {slot : %d, item : '%s', lat : %f, lon : %f, alt : %f, speed : %f}" % (
                int(slot), name, self.y, self.x, self.altitude, self.speed)

        if name != 'EMPTY':
            self.telemetry.telereceiver(msg)
            self.payload[slot] = 'EMPTY'
            self.telemetry.telereceiver(self.output_command_ack(33330123, 0))
        else:
            self.telemetry.telereceiver(self.output_command_ack(33330123, 1))
            return 'EMPTY'

        # Determine randomly where the payload will hit as it gets blown by wind, aerodynamics, and tumbles on the ground
        distance = self.altitude + self.speed - 1
        lucky = int((2 * distance + 1)**2)

        x = 0
        y = 0

        if randint(1, lucky) >= lucky:
            x = self.x
            y = self.y
        else:
            heading = randint(1, 8)

            if heading == 1:
                theta = math.radians(90)
                x = int(math.ceil(self.x + distance * cos(theta)))
                y = int(math.floor(self.y - distance * sin(theta)))
            elif heading == 2:
                theta = math.radians(45)
                x = int(math.ceil(self.x + distance * cos(theta)))
                y = int(math.floor(self.y - distance * sin(theta)))
            elif heading == 3:
                theta = math.radians(0)
                x = int(math.ceil(self.x + distance * cos(theta)))
                y = int(math.floor(self.y - distance * sin(theta)))
            elif heading == 4:
                theta = math.radians(315)
                x = int(math.ceil(self.x + distance * cos(theta)))
                y = int(math.ceil(self.y - distance * sin(theta)))
            elif heading == 5:
                theta = math.radians(270)
                x = int(math.ceil(self.x + distance * cos(theta)))
                y = int(math.ceil(self.y - distance * sin(theta)))
            elif heading == 6:
                theta = math.radians(225)
                x = int(math.floor(self.x + distance * cos(theta)))
                y = int(math.ceil(self.y - distance * sin(theta)))
            elif heading == 7:
                theta = math.radians(180)
                x = int(math.ceil(self.x + distance * cos(theta)))
                y = int(math.floor(self.y - distance * sin(theta)))
            elif heading == 8:
                theta = math.radians(135)
                x = int(math.floor(self.x + distance * cos(theta)))
                y = int(math.floor(self.y - distance * sin(theta)))
            else:
                raise StandardError('Rolled a direction not in 1 through 8? in resolve payload drop!?! ')

        # What is the terrain we landed on?
        blc = self.sim.about_this_coordinate(x, y)

        # MS_PAYLOAD_HIT {item: %s, lat: %f, lon: %f, alt: %f, status: %s}
        #
        # Is the package damaged due to altitude?
        damaged = False
        stuck = False
        sunk = False
        alt = -1

        # Determine variability
        if settings.is_nixel_world:    # NIXEL CHANGE: TODO: need to replace this with a different physics model
            craft_h = self.altitude
            land_h = blc.altitude
            relative_e = craft_h - land_h

            norm_e = relative_e / self.sim.map_height
            chance = random() * self.speed

            if val > 0.9:
                damaged = True
            elif val > 0.8 and chance < 0.8:
                damaged = True
            elif val > 0.7 and chance < 0.7:
                damaged = True
            elif val > 0.6 and chance < 0.6:
                damaged = True
            elif val > 0.5 and chance < 0.5:
                damaged = True
            elif val > 0.4 and chance < 0.4:
                damaged = True
            elif val > 0.3 and chance < 0.3:
                damaged = True
            elif val > 0.2 and chance < 0.2:
                damaged = True
            
        else:
            val = randint(1, 100) + distance

            if self.altitude == 0:
                self.state_update(CraftUpdate.DROP_PAYLOAD)
                return name
            elif self.altitude == 1:
                if val > 99:
                    damaged = True
            elif self.altitude == 2:
                if val > 60:
                    damaged = True
            else:
                if val > 20:
                    damaged = True

        if settings.is_nixel_world:
            if 'trees' in blc.name:
                if randint(1, 100) > 75:
                    stuck = True
                else:
                    alt = blc.altitude - 1
            elif blc.name == 'Firewatch Tower':
                if randint(1, 100) > 20:
                    stuck = True
                else:
                    alt = blc.altitude - 1
            elif blc.name == 'Flight Tower':
                if randint(1, 100) > 85:
                    stuck = True
                else:
                    alt = blc.altitude - 1
            else:
                stuck = False

            if 2 <= blc.type <= 5:  # Sunk in water
                if randint(1, 100) > 50 and not stuck:
                    sunk = True

            # Package altitude?
            if alt == -1:
                alt = blc.altitude

        else:
            # Impact of the terrain on the package
            # Stuck in a tree?
            if blc.name == 'pine trees':
                if randint(1, 100) > 50:
                    stuck = True
                else:
                    alt = blc.altitude - 1
            elif blc.name == 'pine tree':
                if randint(1, 100) > 75:
                    stuck = True
                else:
                    alt = blc.altitude - 1
            elif blc.name == 'cabin':
                if randint(1, 100) > 50:
                    stuck = True
                else:
                    alt = blc.altitude - 1
            elif blc.name == 'flight tower':
                if randint(1, 100) > 85:
                    stuck = True
                else:
                    alt = blc.altitude - 1
            elif blc.name == 'firewatch tower':
                if randint(1, 100) > 20:
                    stuck = True
                else:
                    alt = blc.altitude - 1
            else:
                stuck = False

            # Drop into the fire then package will be damaged
            if blc.name == 'active campfire ring':
                damaged = True

            # Sunk in a river?
            if blc.tiletype == 2:
                if randint(1, 100) > 50 and not stuck:
                    sunk = True

            # Package altitude?
            if alt == -1:
                alt = blc.altitude

        # Formulate HIT message
        status = 'OK'
        if damaged:
            status = 'DAMAGED'

        if stuck:
            status += '_STUCK'

        if sunk:
            status += '_SUNK'

        msg = "MS_PAYLOAD_HIT {item : '%s', lat : %f, lon : %f, alt : %f, status : '%s'}" % (
                name, y, x, alt, status)
        self.telemetry.telereceiver(msg)
        settings.logger.info(msg)

        # Fuel cost
        self.fuel -= 5

        self.state_update(CraftUpdate.DROP_PAYLOAD)

        if self.game_mode == GameMode.LOST_HIKER and status == 'OK' and self.found_hiker is True and self.euclidean_distance(x, y, self.sim.hiker_x, self.sim.hiker_y) < 2.9 and self.successful_package_drops < self.max_package_drops:
            if name in self.dropped_list:
                pass
            else:
                self.dropped_list.append(name)
                self.successful_package_drops += 1
                self.total_1 += 25

                self.telemetry.telereceiver(
                    "GAME_SCORE {total_1 : %d, change_1 : %d, reason_1 : '%s', total_2 : %d, change_2 : %d, reason_2 : '%s', total_3 : %d, change_3 : %d, reason_3 : '%s'}" %
                    (self.total_1, 25, 'Provisioned hiker', self.total_2, 0, 'MT', self.total_3,
                     0, 'MT',))

        return name

    def load_payload(self, slot, item):
        settings.logger.info('Payload load... %d:\'%s\'' % (slot, item))
        self.payload[slot] = item
        self.telemetry.telereceiver(self.output_command_ack(33330122, 0))
        self.telemetry.telereceiver("MS_PAYLOAD_LOAD {slot : %d, item : '%s'}" % (slot, item))
        self.state_update(CraftUpdate.LOAD_PAYLOAD)

    def list_payload(self):
        '''
            Purely for penalizing polling. Agents should just keep track of this.
        '''
        settings.logger.info('Payload list... incurring a move penalty.')
        self.telemetry.telereceiver(self.output_command_ack(33330124, 0))
        self.state_update(CraftUpdate.GET_DATA)

    '''
        ----------------------------------------------------------------------------------------------------------------
                                                       Mission Commands
        ----------------------------------------------------------------------------------------------------------------
    '''

    def clearAllMissionItems(self):
        pass

    def setMissionCount(self, items):
        pass

    def setMissionItem(self, seq, current, frame, command, p1, p2, p3, p4, x, y, z, autocontinue, mission_type=0):
        pass

    def setMissionCurrent(self, current):
        pass

    def getMissionCount(self):
        pass

    '''
        ----------------------------------------------------------------------------------------------------------------
                                      Process the command to update the craft state
        ----------------------------------------------------------------------------------------------------------------
    '''
    def state_update(self, change):
        settings.logger.info("--- Craft state update")

        if change == CraftUpdate.MODE_CHANGE:

            # If not in AUTO then clear plan
            if self.mode != CraftMode.AUTO:
                self.sim.move_queue = []

            # If now in RTL, plan a path to HOME
            if self.mode == CraftMode.RTL:
                self.flyToPlan(self.home_x, self.home_y, 1, None)

            # If now in LOITER mode, loiter here
            if self.mode == CraftMode.LOITER:
                self.loiter_x = self.x
                self.loiter_y = self.y
                self.loiter_z = self.altitude

        elif change == CraftUpdate.ARM_CHANGE:
            pass
        elif change == CraftUpdate.GET_DATA:
            pass
        elif change == CraftUpdate.SPEED_CHANGE:
            pass
        elif change == CraftUpdate.HOME_CHANGE:
            self.telemetry.telereceiver(self.output_home_position())
        elif change == CraftUpdate.ALTITUDE_CHANGE:
            pass
        elif change == CraftUpdate.NEW_PLAN:
            pass
        elif change == CraftUpdate.LOAD_PAYLOAD:
            pass
        elif change == CraftUpdate.DROP_PAYLOAD:
            pass
        else:
            pass

        step = 1

        if self.speed > 0:
            step = self.speed

        self.sim.forward_simulation(step)

    '''
        ----------------------------------------------------------------------------------------------------------------
                                                    Telemetry Messages 
        ----------------------------------------------------------------------------------------------------------------
        
        Telemetry Stream
            HEARTBEAT
            GLOBAL_POSITION_INT
            COMMAND_ACK
            HOME_POSITION
            ATTITUDE
            
            MISSION_CURRENT 
            MISSION_ACK
            MISSION_ITEM_REACHED
            MISSION_COUNT
            MISSION_REQUEST_LIST
            MISSION_ITEM_INT
            MISSION_ITEM
            
            PARAM_SET
            PARAM_VALUE
    '''

    def set_telemetry(self, telemetry):
        self.telemetry = telemetry

        # Update all messages
        self.telemetry.telereceiver(self.output_heartbeat())
        self.telemetry.telereceiver(self.output_global_position_int())
        self.telemetry.telereceiver(self.output_home_position())
        self.telemetry.telereceiver(self.output_attitude())
        self.telemetry.telereceiver(self.output_airspeed())

    def output_heartbeat(self):
        if self.mode == CraftMode.MANUAL:
            base = 81
            custom = 0
        elif self.mode == CraftMode.GUIDED:
            base = 89
            custom = 15
        elif self.mode == CraftMode.AUTO:
            base = 89
            custom = 10
        elif self.mode == CraftMode.LOITER:
            base = 89
            custom = 12
        elif self.mode == CraftMode.RTL:
            base = 89
            custom = 11
        else:
            pass

        if self.altitude == 0:
            status = 3
        else:
            status = 4

        if self.armed:
            base += 128

        return "HEARTBEAT {type : 1, autopilot : 3333, base_mode : %d, custom_mode : %d, system_status : %d, mavlink_version : 3}" % (base, custom, status)

    def output_global_position_int(self):
        ms = datetime.datetime.now() - self.time_start
        # blc = self.sim.about_this_coordinate(self.x, self.y)  # Causes an error on startup because sim isn't initialized yet?

        # if settings.is_nixel_world:    # NIXEL CHANGES: Updated relative altitude
        #     alt = self.altitude * 10
        # else:
        if self.altitude == 0:
            alt = 0
        elif self.altitude == 1:
            alt = 35
        elif self.altitude == 2:
            alt = 400
        else:
            alt = 999

        lat = (30.0 + ((500 - self.y) / 1000.0)) * 1e07
        lon = (-(110.0 + ((500 - self.x) / 1000.0))) * 1e07

        return ("GLOBAL_POSITION_INT {time_boot_ms : %d, lat : %d, lon : %d, alt : %d, relative_alt : %d, vx : %d, vy : %d, vz : %d, hdg : %d}" %
                (ms.microseconds, lat, lon,  alt, alt - 0, self.x, self.y, self.altitude, self.yaw))

    def output_command_ack(self, command, result):
        return ("COMMAND_ACK {command : %d, result : %d}" % (command, result))

    def output_home_position(self):
        lat = (30.0 + ((500 - self.home_y) / 1000.0)) * 1e07
        lon = (-(110.0 + ((500 - self.home_x) / 1000.0))) * 1e07

        return ("HOME_POSITION {latitude : %d, longitude : %d, altitude : %d, x : %f, y : %f, z : %f, q : [0.0, 0.0, 0.0, 0.0], approach_x : 0.0, approach_y : 0.0, approach_z : 0.0}" %
                                (lat, lon, self.altitude, self.home_x, self.home_y, self.home_z))

    def output_attitude(self):
        ms = datetime.datetime.now() - self.time_start

        return ("ATTITUDE {time_boot_ms : %d, roll : %f, pitch : %f, yaw : %f, rollspeed : 0.0, pitchspeed : 0.0, yawspeed : 0.0}" %
                  (ms.microseconds, self.roll, self.pitch, self.yaw))

    def output_airspeed(self):
        ms = datetime.datetime.now() - self.time_start

        return ("AIRSPEED {time_boot_ms : %d, speed : %d}" % (ms.microseconds, self.speed))

    def output_fuel(self):
        ms = datetime.datetime.now() - self.time_start

        return ("FUEL {time_boot_ms : %d, level : %d}" % (ms.microseconds, self.fuel))

    '''
        Unused commands in the version
    '''

    def reset(self):
        pass

    def openCommunications(self, ip, port):
        pass

    def closeCommunications(self):
        pass

    def dictCopy(self, src, keys):
        pass

    def setDataStreams(self, which=None, freq=4, enable=True):
        pass

    '''
        ----------------------------------------------------------------------------------------------------------------
                                                       Path Planner
        ----------------------------------------------------------------------------------------------------------------
    '''

    # NIXEL CHANGE: All plan paths have from_z/to_z required arguments
    def plan_path(self, from_x, from_y, from_z, from_yaw, to_x, to_y, to_z, to_yaw, cruising, alt_min):   # TODO expose this to flyTo cmd!
        self.plan = []
        if settings.is_nixel_world:
            self.my_search3d(from_x, from_y, from_z, from_yaw, to_x, to_y, to_z, to_yaw, cruising, alt_min) # NIXEL CHANGE: Include z axis
        else:
            self.my_search(from_x, from_y, from_yaw, to_x, to_y)

    def euclidean_distance(self, x1, y1, x2, y2):
        return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

    def euclidean_distance3d(self, x1, y1, z1, x2, y2, z2):
        return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2 + (z2 - z1) ** 2)

    # Returns an angle towards (x2, y2) relative to (x1, y1)
    def relative_angle(self, x1, y1, x2, y2, h):
        if x1 == x2 and y1 == y2:
            return h    # Same space, same heading!
        angle = math.degrees(math.atan2(y2 - y1, x2 - x1))
        # settings.logger.info("Calculated relative angle between {} from {} -> {}".format(angle, (x1, y1), (x2, y2)))
        angle = angle % 360
        # settings.logger.info("Checking in at {}".format(angle))
        if (0 <= angle and angle < 22.5) or (337.5 <= angle and angle <= 360):
            return 5
        elif 22.5 <= angle and angle < 67.5:
            return 4
        elif 67.5 <= angle and angle < 112.5:
            return 3
        elif 112.5 <= angle and angle < 157.5:
            return 2
        elif 157.5 <= angle and angle < 202.5:
            return 1
        elif 202.5 <= angle and angle < 247.5:
            return 8
        elif 247.5 <= angle and angle < 292.5:
            return 7
        elif 292.5 <= angle and angle < 337.5:
            return 6

    def my_search(self, x, y, h, gx, gy):

        # print("Conducting my search...")
        if (x == gx and y == gy):
            return

        if h < 1 or h > 8:
            raise StandardError('Received heading out of range - %s' % h)

        # World boundary checks for flight safety
        #
        if self.flight_safety:
            if gx < 0:
                gx = 0

            if gx > self.sim.map_width:
                gx = self.sim.map_width - 1

            if gy < 0:
                gy = 0

            if gy > self.sim.map_length:
                gy = self.sim.map_length - 1

        if self.euclidean_distance(x, y, gx, gy) <= 1:
            # print('Very close!')
            loc = 0
            delta_x = gx - x
            delta_y = gy - y
            if delta_x == 0 and delta_y == -1:
                loc = 1
            elif delta_x == 1 and delta_y == -1:
                loc = 2
            elif delta_x == 1 and delta_y == 0:
                loc = 3
            elif delta_x == 1 and delta_y == 1:
                loc = 4
            elif delta_x == 0 and delta_y == 1:
                loc = 5
            elif delta_x == -1 and delta_y == 1:
                loc = 6
            elif delta_x == -1 and delta_y == 0:
                loc = 7
            elif delta_x == -1 and delta_y == -1:
                loc = 8

            # print('Target at location %d' % loc)

            if h == 1 and loc in [4, 5, 6]:
                x += 0
                y += -1
                self.plan.append((x, y, h))
                # print("Moving to %d, %d, %d" % (x, y, h))
            elif h == 2 and loc in [5, 6, 7]:
                x += 1
                y += -1
                self.plan.append((x, y, h))
                # print("Moving to %d, %d, %d" % (x, y, h))
            elif h == 3 and loc in [6, 7, 8]:
                x += 1
                y += 0
                self.plan.append((x, y, h))
                # print("Moving to %d, %d, %d" % (x, y, h))
            elif h == 4 and loc in [7, 8, 1]:
                x += 1
                y += 1
                self.plan.append((x, y, h))
                # print("Moving to %d, %d, %d" % (x, y, h))
            elif h == 5 and loc in [8, 1, 2]:
                x += 0
                y += 1
                self.plan.append((x, y, h))
                # print("Moving to %d, %d, %d" % (x, y, h))
            elif h == 6 and loc in [1, 2, 3]:
                x += -1
                y += 1
                self.plan.append((x, y, h))
                # print("Moving to %d, %d, %d" % (x, y, h))
            elif h == 7 and loc in [2, 3, 4]:
                x += -1
                y += 0
                self.plan.append((x, y, h))
                # print("Moving to %d, %d, %d" % (x, y, h))
            elif h == 8 and loc in [3, 4, 5]:
                x += -1
                y += -1
                self.plan.append((x, y, h))
                # print("Moving to %d, %d, %d" % (x, y, h))

        while not (x == gx and y == gy):

            # explore neighbors clockwise starting by the one on the right
            #
            dist = {}

            if h == 1:
                dist['1'] = self.euclidean_distance(gx, gy, (x - 1), y)         # 7
                dist['2'] = self.euclidean_distance(gx, gy, (x - 1), (y - 1))   # 8
                dist['3'] = self.euclidean_distance(gx, gy, x, (y - 1))         # 1
                dist['4'] = self.euclidean_distance(gx, gy, (x + 1), (y - 1))   # 2
                dist['5'] = self.euclidean_distance(gx, gy, (x + 1), y)         # 3

                sorted_dict = sorted(dist.items(), key=operator.itemgetter(1))

                key = (sorted_dict[0])[0]

                if key == '1':
                    s_tuple = (x - 1, y, 7, gx, gy)
                elif key == '2':
                    s_tuple = (x - 1, y - 1, 8, gx, gy)
                elif key == '3':
                    s_tuple = (x, y - 1, 1, gx, gy)
                elif key == '4':
                    s_tuple = (x + 1, y - 1, 2, gx, gy)
                elif key == '5':
                    s_tuple = (x + 1, y, 3, gx, gy)

            elif h == 2:
                dist['1'] = self.euclidean_distance(gx, gy, (x - 1), (y - 1))   # 8
                dist['2'] = self.euclidean_distance(gx, gy, x, (y - 1))         # 1
                dist['3'] = self.euclidean_distance(gx, gy, (x + 1), (y - 1))   # 2
                dist['4'] = self.euclidean_distance(gx, gy, (x + 1), y)         # 3
                dist['5'] = self.euclidean_distance(gx, gy, (x + 1), (y + 1))   # 4

                sorted_dict = sorted(dist.items(), key=operator.itemgetter(1))

                key = (sorted_dict[0])[0]

                if key == '1':
                    s_tuple = (x - 1, y - 1, 8, gx, gy)
                elif key == '2':
                    s_tuple = (x, y - 1, 1, gx, gy)
                elif key == '3':
                    s_tuple = (x + 1, y - 1, 2, gx, gy)
                elif key == '4':
                    s_tuple = (x + 1, y, 3, gx, gy)
                elif key == '5':
                    s_tuple = (x + 1, y + 1, 4, gx, gy)

            elif h == 3:
                dist['1'] = self.euclidean_distance(gx, gy, x, (y - 1))         # 1
                dist['2'] = self.euclidean_distance(gx, gy, (x + 1), (y - 1))   # 2
                dist['3'] = self.euclidean_distance(gx, gy, (x + 1), y)         # 3
                dist['4'] = self.euclidean_distance(gx, gy, (x + 1), (y + 1))   # 4
                dist['5'] = self.euclidean_distance(gx, gy, x, (y + 1))         # 5

                sorted_dict = sorted(dist.items(), key=operator.itemgetter(1))

                key = (sorted_dict[0])[0]

                if key == '1':
                    s_tuple = (x, y - 1, 1, gx, gy)
                elif key == '2':
                    s_tuple = (x + 1, y - 1, 2, gx, gy)
                elif key == '3':
                    s_tuple = (x + 1, y, 3, gx, gy)
                elif key == '4':
                    s_tuple = (x + 1, y + 1, 4, gx, gy)
                elif key == '5':
                    s_tuple = (x, y + 1, 5, gx, gy)

            elif h == 4:
                dist['1'] = self.euclidean_distance(gx, gy, (x + 1), (y - 1))   # 2
                dist['2'] = self.euclidean_distance(gx, gy, (x + 1), y)         # 3
                dist['3'] = self.euclidean_distance(gx, gy, (x + 1), (y + 1))   # 4
                dist['4'] = self.euclidean_distance(gx, gy, x, (y + 1))         # 5
                dist['5'] = self.euclidean_distance(gx, gy, (x - 1), (y + 1))   # 6

                sorted_dict = sorted(dist.items(), key=operator.itemgetter(1))

                key = (sorted_dict[0])[0]

                if key == '1':
                    s_tuple = (x + 1, y - 1, 2, gx, gy)
                elif key == '2':
                    s_tuple = (x + 1, y, 3, gx, gy)
                elif key == '3':
                    s_tuple = (x + 1, y + 1, 4, gx, gy)
                elif key == '4':
                    s_tuple = (x, y + 1, 5, gx, gy)
                elif key == '5':
                    s_tuple = (x - 1, y + 1, 6, gx, gy)

            elif h == 5:
                dist['1'] = self.euclidean_distance(gx, gy, (x + 1), y)         # 3
                dist['2'] = self.euclidean_distance(gx, gy, (x + 1), (y + 1))   # 4
                dist['3'] = self.euclidean_distance(gx, gy, x, (y + 1))         # 5
                dist['4'] = self.euclidean_distance(gx, gy, (x - 1), (y + 1))   # 6
                dist['5'] = self.euclidean_distance(gx, gy, (x - 1), y)         # 7

                sorted_dict = sorted(dist.items(), key=operator.itemgetter(1))

                key = (sorted_dict[0])[0]

                if key == '1':
                    s_tuple = (x + 1, y, 3, gx, gy)
                elif key == '2':
                    s_tuple = (x + 1, y + 1, 4, gx, gy)
                elif key == '3':
                    s_tuple = (x, y + 1, 5, gx, gy)
                elif key == '4':
                    s_tuple = (x - 1, y + 1, 6, gx, gy)
                elif key == '5':
                    s_tuple = (x - 1, y, 7, gx, gy)

            elif h == 6:
                dist['1'] = self.euclidean_distance(gx, gy, (x + 1), (y + 1))   # 4
                dist['2'] = self.euclidean_distance(gx, gy, x, (y + 1))         # 5
                dist['3'] = self.euclidean_distance(gx, gy, (x - 1), (y + 1))   # 6
                dist['4'] = self.euclidean_distance(gx, gy, (x - 1), y)         # 7
                dist['5'] = self.euclidean_distance(gx, gy, (x - 1), (y - 1))   # 8

                sorted_dict = sorted(dist.items(), key=operator.itemgetter(1))

                key = (sorted_dict[0])[0]

                if key == '1':
                    s_tuple = (x + 1, y + 1, 4, gx, gy)
                elif key == '2':
                    s_tuple = (x, y + 1, 5, gx, gy)
                elif key == '3':
                    s_tuple = (x - 1, y + 1, 6, gx, gy)
                elif key == '4':
                    s_tuple = (x - 1, y, 7, gx, gy)
                elif key == '5':
                    s_tuple = (x - 1, y - 1, 8, gx, gy)

            elif h == 7:
                dist['1'] = self.euclidean_distance(gx, gy, x, (y + 1))         # 5
                dist['2'] = self.euclidean_distance(gx, gy, (x - 1), (y + 1))   # 6
                dist['3'] = self.euclidean_distance(gx, gy, (x - 1), y)         # 7
                dist['4'] = self.euclidean_distance(gx, gy, (x - 1), (y - 1))   # 8
                dist['5'] = self.euclidean_distance(gx, gy, x, (y - 1))         # 1

                sorted_dict = sorted(dist.items(), key=operator.itemgetter(1))

                key = (sorted_dict[0])[0]

                if key == '1':
                    s_tuple = (x, y + 1, 5, gx, gy)
                elif key == '2':
                    s_tuple = (x - 1, y + 1, 6, gx, gy)
                elif key == '3':
                    s_tuple = (x - 1, y, 7, gx, gy)
                elif key == '4':
                    s_tuple = (x - 1, y - 1, 8, gx, gy)
                elif key == '5':
                    s_tuple = (x, y - 1, 1, gx, gy)

            elif h == 8:
                dist['1'] = self.euclidean_distance(gx, gy, (x - 1), (y + 1))   # 6
                dist['2'] = self.euclidean_distance(gx, gy, (x - 1), y)         # 7
                dist['3'] = self.euclidean_distance(gx, gy, (x - 1), (y - 1))   # 8
                dist['4'] = self.euclidean_distance(gx, gy, x, (y - 1))         # 1
                dist['5'] = self.euclidean_distance(gx, gy, (x + 1), (y - 1))   # 2

                sorted_dict = sorted(dist.items(), key=operator.itemgetter(1))

                key = (sorted_dict[0])[0]

                if key == '1':
                    s_tuple = (x - 1, y + 1, 6, gx, gy)
                elif key == '2':
                    s_tuple = (x - 1, y, 7, gx, gy)
                elif key == '3':
                    s_tuple = (x - 1, y - 1, 8, gx, gy)
                elif key == '4':
                    s_tuple = (x, y - 1, 1, gx, gy)
                elif key == '5':
                    s_tuple = (x + 1, y - 1, 2, gx, gy)

            else:
                raise StandardError('Planning received a heading out of range??')

            self.plan.append((x, y, h))
            # print("Going to %d, %d, %d" % (x, y, h))

            x = s_tuple[0]
            y = s_tuple[1]
            h = s_tuple[2]

        self.plan.append((x, y, h))
        # print("Ending at %d, %d, %d" % (x, y, h))

    def my_search3d(self, sx, sy, sz, h, gx, gy, gz, gh, cruise_height, alt_min):
        max_search_dist = 115
        
        if h < 1 or h > 8:
            raise StandardError('Received heading out of range - %s' % h)

        # World boundary checks for flight safety
        #
        if self.flight_safety:
            if gx < 0:
                gx = 0
            elif gx > self.sim.map_width:
                gx = self.sim.map_width - 1

            if gy < 0:
                gy = 0
            elif gy > self.sim.map_length:
                gy = self.sim.map_length - 1

            if gz < 0:
                gz = 0
            elif gz > self.sim.map_height:
                gz = self.sim.map_height - 1

        # Find the closest height to our cruise height (cruise_height is assumed to be relative)
        cruise = min(self.sim.nixel_world.paths.keys(), key=lambda x: abs(x - cruise_height))

        # Get the closest node in edges to start
        edges_to_start = sorted(self.sim.nixel_world.edges.keys(), key=lambda x: self.euclidean_distance(sx, sy, x[0], x[1]))[:3]
        start = min(edges_to_start, key=lambda x: self.euclidean_distance(gx, gy, x[0], x[1]))     # Of the 3 closest, get to the one closest to the end
        dist_from_start_to_cur = self.euclidean_distance(sx, sy, start[0], start[1])
        dist_from_cur_to_end = self.euclidean_distance(sx, sy, gx, gy)

        # Find node that is closest to end
        edges_to_end = sorted(self.sim.nixel_world.edges.keys(), key=lambda x: self.euclidean_distance(gx, gy, x[0], x[1]))
        end_point = edges_to_end[0]
        dist_from_edge_to_end = self.euclidean_distance(gx, gy, end_point[0], end_point[1])
        
        # If there is an edge in between the current position and end, use navmesh
        if dist_from_cur_to_end > dist_from_start_to_cur and dist_from_cur_to_end > dist_from_edge_to_end:
            settings.logger.info("Using waypoint search...")
            visited = set()
            # Get start point coordinates
            for conn, raw_path in self.sim.nixel_world.paths[cruise].items():
                # settings.logger.info("Searching for start edge pos")
                if str(start) in conn.split(' -> ')[0]:
                    path = literal_eval(raw_path)
                    # settings.logger.info("Found start edge pos")
                    snx, sny, snz, snh = path[0]
                    break
            
            settings.logger.info("Moving to starting waypoint located at ({}, {}, {})".format(snx, sny, snz))

            to_start = self.greedy_search(sx, sy, sz, h, snx, sny, snz, snh, alt_min)
            self.plan.extend(to_start)
            
            settings.logger.info("Ending waypoint located at {}".format(end_point))

            current = to_start[-1]

            # Graph traverse
            while True:
                cx, cy, cz, ch = current

                settings.logger.info("Currently at: {}".format(current))

                # Break out of waypoint serach if we have gotten close enough
                if current[:2] == end_point or self.euclidean_distance3d(gx, gy, gz, cx, cy, cz) <= max_search_dist:
                    break

                dist = self.euclidean_distance(cx, cy, gx, gy)

                # If we aren't on a vertex and other vertices are closer to end, get to the closest one
                if current[:2] not in self.sim.nixel_world.edges.keys() and dist > dist_from_edge_to_end:
                    closest_to_current = sorted(self.sim.nixel_world.edges.keys(), key=lambda x: self.euclidean_distance(cx, cy, x[0], x[1]))[:3] # Get the 3 closest edges
                    rp = min(closest_to_current, key=lambda x: self.euclidean_distance(gx, gy, x[0], x[1]))     # Of the 3 closest, get to the one closest to the end
                    # Find the missing z value: starts of paths tend to have the same z value
                    for conn, raw_path in self.sim.nixel_world.paths[cruise].items():
                        if str(rp) in conn.split(' -> ')[0]:
                            path = literal_eval(raw_path)
                            settings.logger.info("Repositioning to waypoint {} from {}".format(path[0], current))
                            self.plan.extend(self.greedy_search(cx, cy, cz, ch, rp[0], rp[1], path[0][2], path[0][3], alt_min))
                            current = path[0]
                            cx, cy, cz, ch = current
                            break
                    settings.logger.info("Current position after first repositioning: {}".format(current))

                # Break out of waypoint serach if we have gotten close enough
                if current[:2] == end_point or self.euclidean_distance3d(gx, gy, gz, cx, cy, cz) <= max_search_dist:
                    break

                # Plan towards the neighboring waypoint that is closest to end
                neighbors = self.sim.nixel_world.edges[current[:2]]['neighbors']
                next_p = sorted(neighbors, key=lambda x: self.euclidean_distance(x[0], x[1], gx, gy))[0]
                settings.logger.info("Neighboring waypoint closest to end is {}".format(next_p))
                
                # Append path to next waypoint to the plan
                for connection, raw_path in self.sim.nixel_world.paths[cruise].items():
                    conn = connection.split(" -> ")
                    if str(current[:2]) in conn[0] and str(next_p) in conn[1]:
                        path = literal_eval(raw_path)       # TODO: Come up with a way to store translated paths after acessing them
                        settings.logger.info("Path connection is: {}".format(connection))

                        if current != path[0]:     # ensure that craft starts from beginning of path
                            settings.logger.info("Realigning to {}".format(path[0]))
                            self.plan.extend(self.greedy_search(cx, cy, cz, ch, path[0][0], path[0][1], path[0][2], path[0][3], alt_min))
                            current = path[0]

                        self.plan.extend(path)
                        current = path[-1]
                        settings.logger.info("Moved to {}".format(current))
                        break

            # Plan a path to the true end
            settings.logger.info("Exiting waypoint search, pathfinding to destination ({}, {}, {})".format(gx, gy, gz))
            self.plan.extend(self.greedy_search(current[0], current[1], current[2], current[3], gx, gy, gz, gh, alt_min))
            settings.logger.info("Arrived at {} with heading {}".format(self.plan[-1][:3], self.plan[-1][3]))
        else:   # Otherwise if the craft is close enough, we can just plan a path there
            settings.logger.info("Pathfinding to destination ({}, {}, {})".format(gx, gy, gz))
            self.plan.extend(self.greedy_search(sx, sy, sz, h, gx, gy, gz, gh, alt_min))
            settings.logger.info("Arrived at {} with heading {}".format(self.plan[-1][:3], self.plan[-1][3]))

    def greedy_search(self, sx, sy, sz, heading, gx, gy, gz, gh, min_alt):
        MEM_SIZE = 25

        # queue = []
        came_from = {}
        cost_so_far = {}
        memory = []

        # set up start position w/ elevation
        start = (sx, sy, sz)
        # set up end position w/ elevation
        end = (gx, gy, gz)
        
        startp = (sx, sy, sz, heading)
        endp = (gx, gy, gz, gh)

        # settings.logger.info("Searching from {} to {} (end heading: {})".format(start, end, gh))
        current = startp
        cost_so_far[startp] = 0

        moves = [[],
                 [(0, -1, -1), (0, -1, 0), (0, -1, 1)],         # 1
                 [(1, -1, -1), (1, -1, 0), (1, -1, 1)],        # 2
                 [(1, 0, -1), (1, 0, 0), (1, 0, 1)],            # 3
                 [(1, 1, -1), (1, 1, 0), (1, 1, 1)],          # 4
                 [(0, 1, -1), (0, 1, 0), (0, 1, 1)],            # 5
                 [(-1, 1, -1), (-1, 1, 0), (-1, 1, 1)],         # 6
                 [(-1, 0, -1), (-1, 0, 0), (-1, 0, 1)],         # 7
                 [(-1, -1, -1), (-1, -1, 0), (-1, -1, 1)]]      # 8
        
        if gh is not None:
            # List of "entry way" positions right before the end position
            project_out = []
            entry = (0, 0, 0)
            bentry = (0, 0, 0)
            if gh == 5:
                entry = (gx, gy-1, gz, gh)
                project_out.extend([(gx-1, gy-2, gz), (gx, gy-2, gz), (gx+1, gy-2, gz)])
            elif gh == 4:
                entry = (gx-1, gy-1, gz, gh)
                project_out.extend([(gx-2, gy, gz), (gx-2, gy-1, gz), (gx-2, gy-2, gz), (gx-1, gy-2, gz), (gx, gy-2, gz)])
            elif gh == 3:
                entry = (gx-1, gy, gz, gh)
                project_out.extend([(gx-2, gy+1, gz), (gx-2, gy, gz), (gx-2, gy-1, gz)])
            if gh == 2:
                entry = (gx-1, gy+1, gz, gh)
                project_out.extend([(gx-2, gy, gz), (gx-2, gy+1, gz), (gx-2, gy+2, gz), (gx-1, gy+2, gz), (gx, gy+2, gz)])
            elif gh == 1:
                entry = (gx, gy+1, gz, gh)
                project_out.extend([(gx-1, gy+2, gz), (gx, gy+2, gz), (gx+1, gy+2, gz)])
            elif gh == 8:
                entry = (gx+1, gy+1, gz, gh)
                project_out.extend([(gx, gy+2, gz), (gx+1, gy+2, gz), (gx+2, gy+2, gz), (gx+2, gy+1, gz), (gx+2, gy, gz)])
            if gh == 7:
                entry = (gx+1, gy, gz, gh)
                project_out.extend([(gx+2, gy+1, gz), (gx+2, gy, gz), (gx+2, gy-1, gz)])
            elif gh == 6:
                entry = (gx+1, gy-1, gz, gh)
                project_out.extend([(gx, gy-2, gz), (gx+1, gy-2, gz), (gx+2, gy-2, gz), (gx+2, gy-1, gz), (gx+2, gy, gz)])

            bentry = min(project_out, key=lambda x: self.euclidean_distance3d(sx, sy, sz, x[0], x[1], x[2]))
            entry_block = self.sim.about_this_coordinate(bentry[0], bentry[1])

            if entry_block is None or entry_block.altitude >= bentry[2]:
                settings.logger.info("Original entry point obstructed, choosing another from: {}".format(project_out))
                original = bentry
                for e in project_out:
                    block = self.sim.about_this_coordinate(e[0], e[1])
                    if block is None:
                        continue
                    if block.altitude < e[2]:
                        bentry = e
                        settings.logger.info("New entry point is {}".format(bentry))
                        break
                if original == bentry:
                    raise StandardError("Pathfinder could not find a solution!")

        settings.logger.info("Starting search at {}".format(startp))

        while True:     # TODO: Figure out a solid end condition
            cx, cy, cz, ch = current

            close = gx-2 <= cx and cx <= gx+2 and gy-2 <= cy and cy <= gy+2 and gz-2 <= cz and cz <= gz+2

            # Early exit
            if gh is None and current[:3] == end:
                break
            # Fill in the blanks, go to entry point then end

            # if self.euclidean_distance3d(cx, cy, cz, end[0], end[1], end[2]) <= 2:
            #     settings.logger.info("start: {} current: {} end: {} close: {}".format(startp, current, endp, close))

            # Different possible directions to go based on current heading
            m_range = []
            if ch == 1:
                m_range = [7, 8, 1, 2, 3]
            elif ch == 2:
                m_range = [8, 1, 2, 3, 4]
            elif ch == 3:
                m_range = [1, 2, 3, 4, 5]
            elif ch == 4:
                m_range = [2, 3, 4, 5, 6]
            elif ch == 5:
                m_range = [3, 4, 5, 6, 7]
            elif ch == 6:
                m_range = [4, 5, 6, 7, 8]
            elif ch == 7:
                m_range = [5, 6, 7, 8, 1]
            elif ch == 8:
                m_range = [6, 7, 8, 1, 2]

            neighbors = []

            for mh in m_range:  # Iterate through different headings available
                for m in moves[mh]: # Grab positions from list - should be 3 new entries for increase, hold, or decrease elevation
                    n = (cx + m[0], cy + m[1], cz + m[2])
                    nxt = n + (mh,)
                    if not (0 <= n[0] and n[0] < self.sim.map_width and 0 <= n[1] and n[1] < self.sim.map_length and 0 <= n[2] and n[2] < self.sim.map_height):
                        continue
                    block = self.sim.about_this_coordinate(n[0], n[1])
                    if block.altitude + min_alt > n[2]:
                        continue

                    # Close to goal, start planning in
                    if gh is not None and close:
                        ndist = self.euclidean_distance3d(n[0], n[1], n[2], bentry[0], bentry[1], bentry[2])   # Calculate distance to end
                        ncost = ndist + cost_so_far[current]    # Heuristic
                        if nxt not in cost_so_far or ncost < cost_so_far[nxt]:
                            cost_so_far[nxt] = ncost         # Record distance travelled so far
                            neighbors.append((ndist, nxt))
                    else:
                        ncost = self.euclidean_distance3d(n[0], n[1], n[2], end[0], end[1], end[2]) + cost_so_far[current]
                        if nxt not in cost_so_far or ncost < cost_so_far[nxt]:
                            cost_so_far[nxt] = ncost
                            neighbors.append((ncost, nxt))

            if len(neighbors) == 0:
                if len(memory) > 0:
                    neighbors = memory.pop()
                else:
                    raise StandardError("Pathfinder could not find a solution (out of memory)!")

            if gh is not None and close: # 2 spaces away
                if current[:3] != bentry:   # Focus on getting to a position behind the entry point
                    sneighbors = sorted(neighbors, key=lambda x: x[0])
                    npos = sneighbors.pop(0)
                    if len(sneighbors) > 0:
                        memory.append(sneighbors)
                    came_from[npos[1]] = current
                    current = npos[1]
                elif current[:3] == bentry and gh not in m_range:   # We're before the entry point but not in a position to get in
                    settings.logger.info("Current heading is {}, kicking craft out of close range to realign".format(current[3]))
                    sneighbors = sorted(neighbors, key=lambda x: x[0], reverse=True)
                    npos = sneighbors.pop(0)
                    if len(sneighbors) > 0:
                        memory.append(sneighbors)
                    came_from[npos[1]] = current
                    current = npos[1]
                elif current[:3] == bentry:     # Ready to guide the craft in
                    break
            else:
                sneighbors = sorted(neighbors, key=lambda x: x[0])
                npos = sneighbors.pop(0)
                if len(sneighbors) > 0:
                    memory.append(sneighbors)
                came_from[npos[1]] = current
                current = npos[1]
            if len(memory) > MEM_SIZE:
                # settings.logger.info("Trimming memory...")
                memory.pop(0)   # Trim memory size

        # Unravel path
        path = []
        try:    # Check if path was completed - if not, return None
            came_from[current]
        except KeyError:
            settings.logger.info("Warning: could be either already be at entry point (safe) or an incomplete path (unsafe)!")

        c = current
        # Should be the end, not sure what ending heading is if not specified so use last known pos

        # To avoid interfering with the came_from dict, append end of path here
        if gh is not None:
            path.append(endp)
            path.append(entry)

        while c != startp:
            # settings.logger.info("Next pos is {}".format(c))
            path.append(c)
            if c in came_from:
                c = came_from[c]
            else:
                break   # Should only happen when we are already at the desired point (nothing has been added to came_from)

        path.append(start + (heading,))
        path.reverse()

        # Uncomment this to output a test csv file with the generated path!
        # with open("pathfind.txt", "w+") as f:
        #     for p in path:
        #         f.write("{},{},{}\r\n".format(p[0], p[1], p[2]))

        return path

    # Convenience rename function for three_dir_search
    def get_path(self, from_x, from_y, from_z, from_h, to_x, to_y, to_z, to_h=None, min_alt=0):
        return self.my_search3d(from_x, from_y, from_z, from_h, to_x, to_y, to_z, to_h, min_alt)

    # fin
