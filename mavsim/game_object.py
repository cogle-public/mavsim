

'''
    --------------------------------------------------------------------------------------------------------------------
                                              Map Object Support Classes
    --------------------------------------------------------------------------------------------------------------------
'''


class Obj_Layer_Object:
    def __init__(self, oname, x, y, width, length, name, type, description, code="", orientation=-1, takeoff=(-1,-1), land=(-1,-1), altitude=0,):
        self.obj_name = oname
        self.x = x
        self.y = y
        self.altitude = altitude
        self.width = width
        self.length = length

        # Properties
        self.name = name
        self.type = type
        self.description = description

        # Only for Airports (Type = 1)
        self.code = code
        self.orientation = orientation
        self.takeoff = takeoff
        self.land = land

    def __str__(self):
        return "Object Layer Object: oname = %s, x = %f, y = %f, width = %f, length = %f, name = %s, type = %d, description = %s , code = %s, orientation = %d, takeoff = %s, land = %s" % (self.obj_name, self.x, self.y,
                                                                                             self.width, self.length,
                                                                                             self.name, self.type,
                                                                                             self.description, self.code,
                                                                                             self.orientation, self.takeoff,
                                                                                             self.land)


class Hidden_Layer_Object:
    def __init__(self, name, description, type, gp_uuid, agent_visible, screen_visible, position):
        self.name = name
        self.description = description
        self.type = type
        self.gp_uuid = gp_uuid
        self.agent_visible = agent_visible
        self.screen_visible = screen_visible
        self.x = position[0]
        self.y = position[1]
        self.altitude = 0

        if len(position) > 2:
            self.z = position[2]
            self.altitude = self.z

    def __str__(self):
        return "Hidden Layer Object: name = %s, x = %d, y = %d, description = %s , type = %d" % (self.name, self.x, self.y,
                                                                                             self.description, self.type)


class Base_Layer_Object:
    def __init__(self, name, description, altitude, tiletype, x, y):
        self.name = name
        self.description = description
        self.tiletype = tiletype
        self.altitude = altitude
        self.x = x
        self.y = y

    def __str__(self):
        return "Base Layer Object: name = %s, x = %d, y = %d, description = %s , type = %d, altitude = %d" % (
                                              self.name, self.x, self.y, self.description, self.tiletype, self.altitude)
