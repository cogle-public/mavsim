import os
import uuid
import tarfile
import json
import pathlib
from itertools import product
from ast import literal_eval

from PIL import Image

from . import settings
from .game_object import Obj_Layer_Object, Hidden_Layer_Object, Base_Layer_Object

script_dir = pathlib.Path(__file__).parent.resolve()


class NixelLoader:
    def __init__(self):
        # Components of a Nixel world - eg, Water.png, Objects.png, etc...
        self.components = []

        self.scenario_parameters = None # parameters for generation
        self.scenario_metadata = None   # generated information
        self.data = {}  # Holds scenario information

        self.layers = {
            "water": None,
            "objects": None,
            "assemblies": None,
            "terrain": None
        }
        
        # Need features that simulation requires
        self.map_width = 0
        self.map_length = 0
        self.map_height = 0
        self.tile_map = []

        self.regions = []
        self.edges = {}
        self.paths = {}

        self.hidden_layer_objects = []
        self.obj_layer_objects = []
        self.base_layer_objects = []

    def load_nixel_world(self, filename):
        # Loading new Nixel world, clear away old components
        self.components.clear()
        self.edges = {}
        self.paths = {}
        self.hidden_layer_objects.clear()
        self.obj_layer_objects.clear()
        self.base_layer_objects.clear()

        if tarfile.is_tarfile(filename):
            with tarfile.open(filename) as scenario:
                names = scenario.getnames()
                fpath = str(script_dir) + "/apl/nixel_scenarios/"
                # settings.logger.info("Scenario file contains: {}".format(names))

                # if 'water.png' in names and 'objects.png' in names and 'assemblies.png' in names and 'terrain' in names:
                self.components.extend(names)
                scenario.extractall(fpath)

                # Gather values that MAVSim needs
                for comp in self.components:
                    comp_path = fpath + comp
                    name = os.path.splitext(comp_path)
                    if '.png' == name[-1]: # its a heightmap style of component
                        settings.logger.debug("Loading .png layer: {}".format(comp_path))
                        try:
                            img = Image.open(comp_path)
                        except IOError:
                            settings.logger.info('ERROR: Could not find image {}!'.format(comp_path))
                            continue
                        
                        if 'water' in name[0]:
                            self.layers['water'] = img
                        elif 'objects' in name[0]:
                            self.layers['objects'] = img
                        elif 'assemblies' in name[0]:
                            self.layers['assemblies'] = img
                        elif 'terrain' in name[0]:
                            self.layers['terrain'] = img
                        if settings.sim_op_state == 1 and 'flattened' in name[0]:
                            settings.logger.info("Loading flattened map augment file: {}".format(comp_path))
                            self.layers['flattened'] = img
                    elif '.json' == name[-1]:
                        if '_parameters' in name[0]:
                            settings.logger.debug("Loading scenario generation parameters: {}".format(comp_path))
                            with open(comp_path) as params:
                                self.scenario_parameters = json.load(params)
                            self.map_width = self.scenario_parameters['width']
                            self.map_length = self.scenario_parameters['length']
                            self.map_height = self.scenario_parameters['height']
                            self.tile_map = [[None for _ in range(self.map_width)] for _ in range(self.map_length)]
                        elif '_metadata' in name[0]:
                            settings.logger.debug("Loading scenario generated data: {}".format(comp_path))
                            with open(comp_path) as metadata:
                                self.scenario_metadata = json.load(metadata)

                            # Load airports
                            for airport in self.scenario_metadata['Airport']:
                                if airport['type'] == 'Airport':
                                    x = airport['origin_x']
                                    y = airport['origin_y']
                                    z = airport['origin_z']

                                    self.obj_layer_objects.append(Obj_Layer_Object(airport['airport_code'],
                                                                  x,
                                                                  y,
                                                                  airport['width'],
                                                                  airport['length'],
                                                                  airport['name'],
                                                                  1,
                                                                  'A remote airport',
                                                                  altitude=z,
                                                                  code=airport['airport_code'],
                                                                  orientation=airport['orientation'],
                                                                  takeoff=literal_eval(airport['takeoff']),
                                                                  land=literal_eval(airport['land'])))
                        elif 'tesselation_navmesh' in name[0]:
                            settings.logger.debug("Loading navmesh file: {}".format(comp_path))
                            with open(comp_path) as navmesh_file:
                                f = json.load(navmesh_file)
                                for np in f['edge_vertices'].keys():
                                    npos = literal_eval(np)
                                    self.edges[npos] = f['edge_vertices'][np]
                                    self.edges[npos]['neighbors'] = literal_eval(f['edge_vertices'][np]['neighbors'])
                        elif 'tesselation_paths' in name[0]:
                            settings.logger.debug("Loading paths file: {}".format(comp_path))
                            with open(comp_path) as paths_file:
                                f = json.load(paths_file)
                                # settings.logger.info("Finished loading")
                                for e in f.keys():
                                    elevation = literal_eval(e)
                                    self.paths[elevation] = f[e]
                                    # self.paths[elevation] = {path_key: literal_eval(path) for path_key, path in f['paths'][e].items()}
                                    # self.paths[elevation] = f['paths'][e]

        else:
            settings.logger.info('Unable to load {}: not a .tar file!'.format(filename))

        average = 0
        # Map out tile_map
        for x, y in product(range(self.map_width), range(self.map_length)):
            layer_items = []

            for layer, img in self.layers.items():
                if 'flattened' == layer:    # Add stubby mountains later?
                    continue
                layer_items.append(img.getpixel((x, y)))

            blc = sorted(layer_items, key=lambda x: x[0])[-1]
            self.tile_map[x][y] = blc
            average += blc[0]
        
        try:
            ocean_level = 0
            for g in self.scenario_parameters['generators']:
                if 'ocean' in g['name']:
                    ocean_level = g['ocean_level']
                    break

            average = average/len(self.tile_map) # - ocean_level
        except KeyError:
            average = average/len(self.tile_map)

        if settings.sim_op_state == 1:  # Scale the elevations
            # settings.logger.info(self.elevations)

            e_one = [16, 17, 18, 19]    # Trees
            e_two = [21, 22]    # Towers

            bx, by = (0, 0)

            # settings.logger.info("Loading {}x{}x{} map".format(self.map_width, self.map_length, self.map_height))
            for x, y in product(range(self.map_width), range(self.map_length)):
                blc = self.tile_map[x][y]
                tt = blc[1]

                # modify red/z value
                if tt in e_one:
                    self.tile_map[x][y] = (1, tt, blc[2])
                elif tt in e_two:
                    self.tile_map[x][y] = (2, tt, blc[2])
                elif tt == 31:
                    self.tile_map[x][y] = (4, tt, blc[2])
                else:
                    self.tile_map[x][y] = (0, tt, blc[2])

            if 'flattened' in self.layers:
                stubs = self.layers['flattened']
                avoid = [2, 3, 4, 16, 20, 21, 22]
                # for x, y in product(range(self.map_width), range(self.map_length)):
                for x in range(self.map_width):
                    for y in range(self.map_length):
                        blc = self.tile_map[x][y]
                        stub_z = stubs.getpixel((x, y))[0]
                        nz = blc[0] + stub_z
                        if stub_z == 1:
                            self.tile_map[x][y] = (nz, 24.0, blc[2])
                        elif stub_z == 2:
                            self.tile_map[x][y] = (nz, 25.0, blc[2])
                        elif stub_z == 3:
                            self.tile_map[x][y] = (nz, 26.0, blc[2])
                        elif stub_z == 4:
                            self.tile_map[x][y] = (nz, 31.0, blc[2])

            for a in self.obj_layer_objects:
                if 'airport' in a.name.lower():
                    a.altitude = 0

    def about_this_coordinate(self, x, y):
        f_layer = ''
        value = 0

        # required fields for objects
        name = 'M-T'
        description = ''
        altitude = -1
        ttype = -1

        # settings.logger.info("Getting {} from ({},{})".format(self.tile_map[x][y], x, y))

        altitude, ttype, value = self.tile_map[x][y]

        if ttype == 0: # Air (shouldn't ever be on top...)
            name = 'Air'
            description = 'Nothing but air'
        elif ttype == 1: # Terrain (shouldn't ever be on top...)
            name = 'Terrain'
            description = 'Dirt'
        elif ttype == 2: # Ocean
            name = 'Ocean'
            description = 'Deep blue sea'
        elif ttype == 3: # River
            # Grab metadata
            name = 'River'
            description = 'A river that flows downhill'
        elif ttype == 4: # Lake
            name = 'Lake'
            description = 'A freshwater lake'
        elif ttype == 5: # Riverbed
            name = 'Riverbed'
            description = 'The mud underneath a body of fresh water'
        elif ttype == 6: # Forest
            name = 'Forest'
            description = 'A temperate forest floor'
        elif ttype == 7: # Rainforest
            name = 'Rain Forest'
            description = 'A tropical, moist forest floor'
        elif ttype == 8: # Grassland
            name = 'Grassland'
            description = 'Grassy fields'
        elif ttype == 9: # Savanna
            name = 'Savanna'
            description = 'Arid grassy fields'
        elif ttype == 10: # Desert
            name = 'Desert'
            description = 'Sandy desert'
        elif ttype == 11: # Boreal Forest
            name = 'Boreal Forest'
            description = 'Colder, less moist forest floor'
        elif ttype == 12: # Tundra
            name = 'Tundra'
            description = 'Cold, dry earth with little vegetation'
        elif ttype == 13: # Snow
            name = 'Snow'
            description = 'Bright white snow'
        elif ttype == 23: # Hiker
            name = 'Terrain'
            description = 'Dirt'
            # name = self.data['hiker']['name']     # Currently not using hiker
            # description = self.data['hiker']['description']
            # agent_visible = self.data['hiker']['agent_visible']
            # screen_visible = self.data['hiker']['screen_visible']
            # position = literal_eval(self.data['hiker']['location'])
            # return Hidden_Layer_Object(name, description, ttype, str(uuid.uuid4()), agent_visible, screen_visible, position)
        elif ttype == 14: # Road
            # Extract metadata
            name = 'Road'
            description = 'Road for driving with lanes marked'
        elif ttype == 15: # Path
            name = 'Path'
            description = 'A worn dirt path'
        elif ttype == 16: # Banana Trees
            name = 'Banana Trees'
            description = 'A group of trees native to rainforests'
        elif ttype == 17: # Oak Trees
            name = 'Oak Trees'
            description = 'A group of trees native to forests'
        elif ttype == 18: # Acacia Trees
            name = 'Acacia Trees'
            description = 'A group of trees native to savannas'
        elif ttype == 19: # Pine Trees
            name = 'Pine Trees'
            description = 'A group of trees native to boreal forests'
        elif ttype == 20: # Tarmac -> runway
            name = 'runway'
            description = 'Aircraft runway for takeoff and landing'
            # Extract metadata to create object if it has not already been created
        elif ttype == 21: # Airtower -> Flight Tower
            name = 'Flight Tower'
            description = 'Air control tower for an airport'
        elif ttype == 22: # Firewatch tower
            name = 'Firewatch Tower'
            description = 'U.S. Forest Service Firewatch Tower (Say "Hi" to Delilah for me)'

        if settings.sim_op_state == 1:
            if ttype == 24.0:
                name = 'mountain ridge 1'
                description = 'A low mountain ridge'
            elif ttype == 25.0:
                name = 'mountain ridge 2'
                description = 'An average mountain ridge'
            elif ttype == 26.0:
                name = 'mountain ridge 3'
                description = 'A tall mountain ridge'
            elif ttype == 31.0:
                name = 'mountain ridge 4'
                description = 'A towering mountain ridge'

        return Base_Layer_Object(name, description, altitude, ttype, x, y)
