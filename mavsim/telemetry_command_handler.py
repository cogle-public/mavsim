# -----------------------------------------------------------------------------
#  MAVSim::TELEMETRY Command Handler
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from . import settings
import time
import sys

from .udpclient import UDPClient

class TELEMETRYCommandHandler:
    def __init__(self, craft, telemetry):
        settings.logger.info("TELEMETRY command handler setup...")
        self.craft = craft
        self.telemetry = telemetry

        self.switch_dict = {'RESET_COMMS': self.reset_comms_command,
                            'SET_MULTICAST': self.set_multicast,
                            'ADD_LISTENER': self.add_listener,
                            'REMOVE_LISTENER': self.remove_listener
                            }

    def process(self, message):
        self.message = message
        settings.logger.info("...processing command - %s" % message[1])

        return self.switch_dict[message[1]]()

    def reset_comms_command(self):
        try:
            self.telemetry.close()
            self.craft.closeCommunications()
            settings.logger.info("---Closing telemetry and craft communications")

            time.sleep(1.0)

            # TODO: Remove the hardcoding, this is just for testing now
            self.telemetry.open('0.0.0.0', 14553)
            self.craft.openCommunications('127.0.0.1', 14550)
            settings.logger.info("---Re-establishing telemetry and craft communications")

            return "('ACK', 'RESET COMMS')"
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Reset Comms Error: %s - %s" % (e, str(v)))
            return "('ERR', 'RESET COMMS', '%s')" % str.upper(str(v))

    def set_multicast(self):
        try:
            value = self.message[2]
            if 'ON' in value:
                settings.multicast_sending = True
            elif 'OFF' in value:
                settings.multicast_sending = False
            else:
                raise Exception('Unknown setting value!')

            return "('ACK', 'SET_MULTICAST', %s)" % value
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Set Multicast Error: %s - %s" % (e, str(v)))
            return "('ERR', 'SET_MULTICAST', '%s')" % str.upper(str(v))

    def add_listener(self):
        try:
            ip = self.message[2]
            port = self.message[3]
            filter = self.message[4]

            if ip == 'docker.for.mac.localhost':
                ip = 'localhost'

            # look through list to see if already exists
            # if so, remove
            for client in settings.telemetry_direct_clients:
                if client.ip == ip and client.port == port:
                    settings.telemetry_direct_clients.remove(client)

            # create new object
            new_client = UDPClient(ip, port, filter)
            settings.telemetry_direct_clients.append(new_client)

            return "('ACK', 'ADD_LISTENER', '%s', %s)" % (ip, port)
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Add listener Error: %s - %s" % (e, str(v)))
            return "('ERR', 'ADD_LISTENER', '%s')" % str.upper(str(v))

    def remove_listener(self):
        try:
            ip = self.message[2]
            port = self.message[3]

            # look through list to see if already exists
            # if so, remove
            for client in settings.telemetry_direct_clients:
                if client.ip == ip and client.port == port:
                    settings.telemetry_direct_clients.remove(client)

            return "('ACK', 'REMOVE_LISTENER', %s, %s)" % (ip, port)
        except:
            e, v, t = sys.exc_info()
            settings.logger.error("Add listener Error: %s - %s" % (e, str(v)))
            return "('ERR', 'REMOVE_LISTENER', '%s')" % str.upper(str(v))


# fin
