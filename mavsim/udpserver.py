# -----------------------------------------------------------------------------
#  MAVSim::UDP Server
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from . import settings
import socket

class UDPServer:
    def __init__(self, ip, port):
        settings.logger.info("...udp server initialized")

        # Create a TCP/IP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # Bind the socket to the port
        server_address = (ip, port)
        settings.logger.info('......starting up udp server on %s port %s' % server_address)
        self.sock.bind(server_address)

    def receive(self):
        settings.logger.info('...udp server waiting to receive message')
        data, self.address = self.sock.recvfrom(4096)

        settings.logger.info('received %s bytes from %s' % (len(data), self.address))
        settings.logger.debug(data)

        return data.decode('UTF-8')

    def send(self, response):
            sent = self.sock.sendto(response.encode('UTF-8'), self.address)
            settings.logger.info('sent %s bytes back to %s' % (sent, self.address))

    def __del__(self):
        settings.logger.info("Closing udp socket")
        self.sock.close()

# fin
