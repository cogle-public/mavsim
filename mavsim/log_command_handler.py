# -----------------------------------------------------------------------------
#  MAVSim:: LOG Command Handler
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from . import settings

class LOGCommandHandler:
    def __init__(self, craft, mem_buffer):
        settings.logger.info("LOG command handler setup...")
        self.craft = craft
        self.mem_buffer = mem_buffer

        self.message = None
        self.switchDict = {'CUSTOM_LOG_ENTRY': self.custom_log_entry_command,
                           'LOG_MISSION_START': self.log_mission_start_command,
                           'LOG_MISSION_END': self.log_mission_end_command
                           }

    def process(self, message):
        self.message = message
        settings.logger.info("...processing command - %s" % message[1])

        return self.switchDict[message[1]]()

    def custom_log_entry_command(self):
        try:
            self.mem_buffer.store_custom_log(self.message[2],
                                             self.message[3],
                                             self.message[4])
            return "('ACK', 'CUSTOM_LOG_ENTRY')"
        except:
            return "('ERR', 'CUSTOM_LOG_ENTRY')"

    def log_mission_start_command(self):
        try:
            uuid = self.mem_buffer.store_mission_start(self.message[2],
                                                       self.message[3],
                                                       self.message[4])
            return "('ACK', 'LOG_MISSION_START', '%s')" % uuid
        except:
            return "('ERR', 'LOG_MISSION_START')"

    def log_mission_end_command(self):
        try:
            self.mem_buffer.store_mission_end(self.message[2],
                                              self.message[3])
            return "('ACK', 'LOG_MISSION_END')"
        except:
            return "('ERR', 'LOG_MISSION_END')"

# fin
