# -----------------------------------------------------------------------------
#  MAVSim::ArduPilotLight Telemetry Receiver
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from . import settings

class APLTelemetry:
    def __init__(self, buffer, craft):
        settings.logger.info("ArduPilot Light telemetry stream setup...")
        self.mem_buffer = buffer
        self.handle_messages = True
        self.craft = craft

    def __del__(self):
        pass

    def telereceiver(self, msg):
        if self.handle_messages:
            self.mem_buffer.update_telemetry(msg)
            settings.logger.debug('APL Telemetery Received -- "%s"' % msg)

            # if "_ACK" in msg.get_type():
            #    settings.logger.info("ArduPilot: %s" % msg)

            return 1
        else:
            return 0

    # Below operations not used, but included in case they are called

    def open(self, ip, port):
        pass

    def close(self):
        pass

    def setDataStreams(self, which=0, freq=4, enable=True):
        pass

# fin
