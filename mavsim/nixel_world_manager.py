import os
import sys
import tarfile
import json
import pathlib
from ast import literal_eval

from . import settings

import nixel
from COGLE.COGLE_scenario_generator import COGLEScenarioGenerator

script_dir = pathlib.Path(__file__).parent.resolve()
SCENARIO_DIR = os.path.join(str(script_dir), 'apl', 'nixel_scenarios')


class NixelWorldManager:
    def __init__(self):
        self.world_config = {}    # Handle to JSON file with world info
        nixel.settings.init()
        nixel.settings.logger_output('library')

        if not os.path.isdir(SCENARIO_DIR):
            settings.logger.info("Nixel scenario directory not found, creating it now at {}".format(SCENARIO_DIR))
            os.mkdir(SCENARIO_DIR)

    # utility function to get the name from a dna string
    def get_name_from_dna(self, dna_string):
        dna = dna_string
        world_name = 'nixel_scenario'
        if isinstance(dna_string, str):
            dna = literal_eval(dna_string)
        if '|' in dna[0]:
            # World name is attribute 0 in dna string, extract it
            world_name = next(x.split(':')[1] for x in dna[0].split('_') if '0:' in x)
        return world_name

    # Checks if the provided dna string exists within the directory
    def dna_in_library(self, dna_string):
        dna = dna_string
        if isinstance(dna_string, list):
            dna = str(dna_string)

        files = os.listdir(SCENARIO_DIR)
        for f in files:
            if '.tar' in f:
                fpath = os.path.join(SCENARIO_DIR, f)
                with tarfile.open(fpath) as package:
                    contents = package.getmembers()
                    for c in contents:
                        if '_parameters.json' in c.name:
                            with package.extractfile(c) as sf:
                                params = json.load(sf)
                                if params['dna_val'] == dna:
                                    return params['scenario_name']
        return ''

    # Cross check with world_config if worlds are in directory, if not then generate them
    def check_world_library(self):
        # Grab config file and update if possible
        settings.logger.info("Updating Nixel World Registry...")
        try:
            with open(os.path.join(script_dir, 'nixel_config.json')) as cf:
                self.world_config = json.load(cf)
                for name in self.world_config['scenarios'].keys():
                    settings.logger.info("Scenario found in nixel config file: {}".format(name))
        except IOError:
            settings.logger.info("Unable to load config file!")

        # Check through each tar file
        files = os.listdir(SCENARIO_DIR)
        # settings.logger.info(files)
        generated_scenarios = set()
        for f in files:
            if '.tar' in f:
                # settings.logger.info("Found {}".format(f))
                fpath = os.path.join(SCENARIO_DIR, f)
                with tarfile.open(fpath) as package:
                    contents = package.getmembers()
                    for c in contents:
                        if '_parameters.json' in c.name:
                            settings.logger.info("Registered {}".format(fpath))
                            with package.extractfile(c) as sf:
                                params = json.load(sf)
                                sname = params['scenario_name']
                                generated_scenarios.add((sname, params['dna_val']))
                            break

        # Find differences in list, use dna strings to generate missing worlds
        needed = set(self.world_config['scenarios'].items())
        settings.logger.debug("Scenarios present: {}".format(generated_scenarios))
        missing = list(needed - generated_scenarios)
        settings.logger.debug("Missing Nixel scenarios: {}".format(missing))

        if len(missing) > 0:
            settings.logger.info("Generating missing scenario packages {}, this may take a while...".format([name[0] for name in missing]))
            self.generate_set(missing)
            settings.logger.info("Finished generating missing scenarios")
        
    def single_generate(self, dna_string, name='', generators=None):
        dna = dna_string
        if isinstance(dna, str):
            dna = literal_eval(dna)

        name = self.dna_in_library(dna)

        if len(name) != 0: # Check if we have already generated this scenario
            settings.logger.info("Loading requested scenario from library")
            return name
        else:
            world_name = name
            if len(world_name) == 0:
                world_name = self.get_name_from_dna(dna)

            settings.logger.info('Generating "{}"'.format(world_name))

            nixel_generator = COGLEScenarioGenerator(world_name)
            if dna_string is not None:
                nixel_generator.world.set_dna(dna)
            if generators is not None and world_name != 'nixel_scenario':
                nixel_generator.set_layer_generators(literal_eval(generators))
            nixel_generator.generate_scenario()
            nixel_generator.export_scenario(directory=SCENARIO_DIR)
            nixel_generator.reset_scenario()
            return world_name

    # Worlds: list of (name, dna_string) tuples
    def generate_set(self, worlds):
        for world_name, dna_string in worlds:
            nixel_generator = COGLEScenarioGenerator(world_name)
            settings.logger.info('Generating "{}"'.format(world_name))
            nixel_generator.world.set_dna(literal_eval(dna_string))
            # if world_name == 'nixel_world':
            #     nixel_generator.set_layer_generators(self.world_config["generators"][world_name])
            # settings.logger.info(dna_string)

            if world_name in self.world_config["parameters"]:   # World has specified parameters
                nixel_generator.set_scenario_parameters(self.world_config["parameters"][world_name])
            nixel_generator.generate_scenario()     # Generate scenario
            
            nixel_generator.export_scenario(directory=SCENARIO_DIR)    # Export to new
            nixel_generator.reset_scenario()     # Clear world, prepare for new one
