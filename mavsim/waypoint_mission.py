# -----------------------------------------------------------------------------
#  MAVSim::Waypoint Mission
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from . import settings

class WaypointMission:
    def __init__(self, name, upper_bound_lat=0.0, upper_bound_lon=0.0,
                 lower_bound_lat=0.0, lower_bound_lon=0.0, note=''):
        settings.logger.info("Waypoint Mission setup...")
        self.name = name
        self.area.upper_lat = upper_bound_lat
        self.area.upper_lon = upper_bound_lon
        self.area.lower_lat = lower_bound_lat
        self.area.lower_lon = lower_bound_lon
        self.note = note

        self.waypoint_list = []

    class waypoint:
        def __init__(self, current, frame, command, p1, p2, p3, p4, lat, lon, alt, autocont):
            self.current = current
            self.frame = frame
            self.command = command
            self.p1 = p1
            self.p2 = p2
            self.p3 = p3
            self.p4 = p4
            self.lat = lat
            self.lon = lon
            self.alt = alt
            self.autocont = autocont

        def __repr__(self):
            return ("(current: %d frame: %d, command: %d, p1: %f, p2: %f, p3: %f, p4: %f, lat: %f, lon: %f, alt: %f, ac: %d)" %
                    (self.current, self.frame, self.command, self.p1, self.p2, self.p3,
                     self.p4, self.lat, self.lon, self.alt, self.autocont))

    def insert_waypoint(self, position, frame, command, p1, p2, p3, p4, lat,
                        lon, alt, autocont):
        self.waypoint_list.insert(position, waypoint(current, frame, command, p1, p2,
                                                     p3, p4,
                                                     lat, lon, alt, autocont))

    def append_waypoint(self, frame, command, p1, p2, p3, p4, lat,
                        lon, alt, autocont):
        self.waypoint_list.append(waypoint(current, frame, command, p1, p2,
                                           p3, p4,
                                           lat, lon, alt, autocont))

    def replace_waypoint(self, index, current, frame, command, p1, p2, p3, p4, lat,
                         lon, alt, autocont):
        self.waypoint_list[index].current = current
        self.waypoint_list[index].frame = frame
        self.waypoint_list[index].command = command
        self.waypoint_list[index].p1 = p1
        self.waypoint_list[index].p2 = p2
        self.waypoint_list[index].p3 = p3
        self.waypoint_list[index].p4 = p4
        self.waypoint_list[index].lat = lat
        self.waypoint_list[index].lon = lon
        self.waypoint_list[index].alt = alt
        self.waypoint_list[index].autocont = autocont

    def get_waypoint_list(self):
        output = "["
        count = 0
        length = len(self.waypoint_list)
        for item in self.waypoint_list:
            output = output + str(count) + ":" + item
            if count != (length - 1):
                output = output + ", "
            count = count + 1
        output = output + "]"

        return output

    def move_waypoint(self, source, sink):
        hold = self.waypoint_list[source]
        del self.waypoint_list[source]
        self.waypoint_list.insert(sink, hold)

    def __repr__(self):
        return self.name

# fin
