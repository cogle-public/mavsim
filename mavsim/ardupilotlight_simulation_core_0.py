# -----------------------------------------------------------------------------
#  MAVSim::ArduPilotLight Simulation Core
# -----------------------------------------------------------------------------
# Micro-Air Vehicle Simulator
# -----------------------------------------------------------------------------
__copyright__ = "Copyright (C) 2018, 2019, PARC, a Xerox company"
# -----------------------------------------------------------------------------

from . import settings
from .game_object import Obj_Layer_Object, Hidden_Layer_Object, Base_Layer_Object
from .nixel_loader import NixelLoader

import uuid
import tarfile
import sys
import tmx
import json
import parse
import xml.etree.ElementTree as ET
from random import randint
from ast import literal_eval
import pathlib

script_dir = pathlib.Path(__file__).parent.resolve()

# ArduPilot Simulation
#   1. The craft model exists and is put into an environment. All commands for the aircraft are sent to the aircraft,
#      which sets its state.
#   2. All reactions to  craft and world changes are broadcast through telemetry
#


class Game_Move:
    def __init__(self, x, y, altitude, yaw, pitch, roll, speed, cost, mode, armed):
        self.x = x
        self.y = y
        self.altitude = altitude
        self.yaw = yaw
        self.pitch = pitch
        self.roll = roll
        self.speed = speed
        self.cost = cost
        self.mode = mode
        self.armed = armed

    def __str__(self):
        return "(%d, %d, %d, %d, %d, %d, %d, %d, %d, %r)" % (self.x, self.y, self.altitude, self.yaw, self.pitch,
                                                             self.roll, self.speed, self.cost, self.mode, self.armed)


'''
    --------------------------------------------------------------------------------------------------------------------
                                                 APL Simulation Core
    --------------------------------------------------------------------------------------------------------------------
'''


class APL_Simulation_Core_0:
    def __init__(self, craft, telemetry, buffer, scenario, note, new_sim, flight_uuid):
        settings.logger.info("ArduPilot Light APL simulation core setup...")
        self.craft = craft
        self.telemetry = telemetry
        self.buffer = buffer
        self.scenario = scenario
        self.note = note
        self.flight_uuid = flight_uuid

        self.contents = None
        self.map = None
        self.scenario_info = None
        self.tmx = None
        self.obj_layer = None
        self.nixel_world = None

        self.object_layer_list = []
        self.goal_1 = ''
        self.goal_2 = ''
        self.goal_3 = ''
        self.mission = ''
        self.tmx_map = ''
        self.uuid_base = ''

        self.map_width = 0
        self.map_length = 0
        self.map_height = 4

        self.map_tiles = []
        self.hidden_tiles = []
        self.region_tiles = []
        self.hidden_layer_objects = []

        self.game_moves = []
        self.move_queue = []

        self.hiker_x = 0
        self.hiker_y = 0

        self.craft_fuel = 0
        self.craft_wind_direction = 1
        self.craft_x = 0
        self.craft_y = 0
        self.craft_home_x = 0
        self.craft_home_y = 0
        self.craft_home_z = 0
        self.craft_altitude = 0
        self.craft_yaw = 0


        # self.craft_trace_file = open("craft_trace.txt","w+")
        # self.craft_trace_file.write('X, Y, Z\n')

        # Setup new instance UUID for this simulation session
        self.session = settings.simulation_session_uuid = str(uuid.uuid4())

        # Log new simulation session in database
        self.db_record_uuid = self.buffer.mark_start_of_APL_simulation(self.scenario, self.session, self.note)

        if new_sim:
            settings.logger.info("Opening scenario file: %s" % self.scenario)

            msg = "COMMAND_ACK {command : 33330004, result : 0}"
            self.telemetry.telereceiver(msg)

            # TODO: This will eventually need to load from the repo and not from the local filesystem, but for now
            #       this will suffice

            '''
                Load important files for data extraction and for map use
            '''
            if settings.is_nixel_world:    # NIXEL CHANGE: Substitute in Nixel world loading
                file_string = str(script_dir) + "/apl/nixel_scenarios/" + settings.nwm.get_name_from_dna(self.scenario) + ".tar"
                self.nixel_world = NixelLoader()
                self.nixel_world.load_nixel_world(file_string)
                
                # Extract map data
                self.map_width = self.nixel_world.map_width
                settings.logger.info('Map has a width of %d tiles.' % self.map_width)
                self.map_length = self.nixel_world.map_length
                settings.logger.info('Map has a length of %d tiles.' % self.map_length)
                self.map_height = self.nixel_world.map_height
                settings.logger.info('Map has a height of %d voxels.' % self.map_height)

                mission_data = self.nixel_world.scenario_metadata['missions'][0]

                # Extract mission data
                settings.logger.info('~~~~~ Processing goals...')
                self.goal_1 = mission_data['goal_1']
                if len(self.goal_1) > 0:
                    self.telemetry.telereceiver("GOAL {order : 1, goal : '%s'}" % self.goal_1)

                self.goal_2 = mission_data['goal_2']
                if len(self.goal_2) > 0:
                    self.telemetry.telereceiver("GOAL {order : 2, goal : '%s'}" % self.goal_2)

                self.goal_3 = mission_data['goal_3']
                if len(self.goal_3) > 0:
                    self.telemetry.telereceiver("GOAL {order : 3, goal : '%s'}" % self.goal_3)

                settings.logger.info('~~~~~ Processing mission...')
                self.mission = mission_data['mission']
                self.tmx_map = mission_data['map']
                self.telemetry.telereceiver("SCENARIO {map : '%s', description : '%s', suuid : '%s'}" % (
                                            self.tmx_map, self.mission, str(self.db_record_uuid)))

                self.buffer.exigisi_add_sim_session_uuid_to_flight_apl(self.flight_uuid, self.session)

                self.wind_direction = mission_data['wind_direction']

                settings.logger.info('~~~~~ Processing fuel_start...')
                fuel_val = mission_data['fuel_start']
                if fuel_val > 0:
                    self.craft.fuel = int(fuel_val)
                    self.craft_fuel = self.craft.fuel

                # Extract layer objects
                self.hidden_layer_objects.extend(self.nixel_world.hidden_layer_objects)
                self.object_layer_list.extend(self.nixel_world.obj_layer_objects)

                start = Hidden_Layer_Object(mission_data['start']['name'],
                                            mission_data['start']['description'],
                                            mission_data['start']['type'],
                                            uuid.uuid4(),
                                            mission_data['start']['agent_visible'],
                                            mission_data['start']['screen_visible'],
                                            literal_eval(mission_data['start']['location']))

                self.hidden_layer_objects.append(start)
            else:
                file_string = str(script_dir) + "/apl/scenarios/" + self.scenario + ".tgz"
                tar = tarfile.open(file_string)
                for member in tar.getmembers():
                    # print(member)
                    if 'contents.json' in member.name:
                        settings.logger.info('Loading content data')
                        f = tar.extractfile(member)
                        self.contents = f.read()
                    elif 'map.tmx' in member.name:
                        settings.logger.info('Loading map data')
                        f = tar.extractfile(member)
                        self.map = f.read()
                    elif 'scenario.yml' in member.name:
                        settings.logger.info('Loading scenario data')
                        f = tar.extractfile(member)
                        self.scenario_info = f.read()
                    else:
                        pass
                        # print('Ignoring...')

                # TODO: For now, we are not going to process the contents OR the scenario files since they don't currently
                #       have any items of interest.

                '''
                    Goofy TMX library does not handle Tiled Object Layer items correctly, so we extract here to parse them 
                    sepearately. This is okay since we would have to do this anyway.
                '''
                root = ET.fromstring(self.map)
                for objs in root.findall('objectgroup'):
                    self.obj_layer = objs
                    root.remove(objs)

                '''
                    Process the objects into an object list for checking against
                '''
                for obj in self.obj_layer:
                    type = int(obj[0][6].attrib['value'])

                    # TODO: Improve this XML parsing within the tree to not hardcode positions
                    # TODO: Handle other types

                    if type == 1:
                        o = Obj_Layer_Object(obj.attrib['name'],
                                             float(obj.attrib['x']),        # x
                                             float(obj.attrib['y']),        # y
                                             float(obj.attrib['width']),    # width
                                             float(obj.attrib['height']),   # height
                                             obj[0][3].attrib['value'],     # name
                                             int(obj[0][6].attrib['value']),    # type
                                             obj[0][1].attrib['value'],     # description
                                             obj[0][0].attrib['value'],     # code
                                             int(obj[0][4].attrib['value']),
                                             literal_eval(obj[0][5].attrib['value']),
                                             literal_eval(obj[0][2].attrib['value']))
                        self.object_layer_list.append(o)
                    else:
                        raise ValueError('Unknown Type in Object Layer in APL map', self.scenario, type)

                    # print(o)

                # Write an object layered removed TMX file to drive for reading
                file = open('transient.tmx', 'w')
                # file = open(str(script_dir()/'transient.tmx'), 'w')
                file.write(ET.tostring(root, encoding='unicode', method='xml'))
                file.close()

                # TMX lib cannot read from string, so we have to get from an OS file :(
                self.tmx = tmx.TileMap.load('transient.tmx')
                # self.tmx = tmx.TileMap.load(str(script_dir()/'transient.tmx').read())

                '''
                    Pull out scenario information from the TMX file
                '''
                settings.logger.info('Map has a height of %d tiles.' % self.tmx.height)
                self.map_length = self.tmx.height
                settings.logger.info('Map has a width of %d tiles.' % self.tmx.width)
                self.map_width = self.tmx.width

                settings.logger.info('Map has a tileheight of %d pixels.' % self.tmx.tileheight)
                settings.logger.info('Map has a tilewidth of %d pixels.' % self.tmx.tilewidth)

                for prop in self.tmx.properties:
                    # settings.logger.info(prop.name)

                    if prop.name == 'goal_1':
                        self.goal_1 = str(prop.value)
                        if len(self.goal_1) > 0:
                            self.telemetry.telereceiver("GOAL {order : 1, goal : '%s'}" % self.goal_1)
                    elif prop.name == 'goal_2':
                        self.goal_2 = str(prop.value)
                        if len(self.goal_2) > 0:
                            self.telemetry.telereceiver("GOAL {order : 2, goal : '%s'}" % self.goal_2)
                    elif prop.name == 'goal_3':
                        self.goal_3 = str(prop.value)
                        if len(self.goal_3) > 0:
                            self.telemetry.telereceiver("GOAL {order : 3, goal : '%s'}" % self.goal_3)
                    elif prop.name == 'mission':
                        self.mission = prop.value
                    elif prop.name == 'map':
                        self.tmx_map = prop.value
                    elif prop.name == 'uuid_base':
                        self.uuid_base = prop.value
                    elif prop.name == 'fuel_start':
                        self.craft.fuel = int(prop.value)
                        self.craft_fuel = self.craft.fuel
                    elif prop.name == 'wind_direction':
                        self.craft.wind_direction = int(prop.value)
                        self.craft_wind_direction = self.craft.wind_direction
                    else:
                        settings.logger.error('Uknown scenario map property of %s' % prop.name)

                self.telemetry.telereceiver("SCENARIO {map : '%s', description : '%s', suuid : '%s'}" % (self.tmx_map, self.mission, str(self.db_record_uuid)))
                self.buffer.exigisi_add_sim_session_uuid_to_flight_apl(self.flight_uuid, self.session)
                self.telemetry.telereceiver("WIND {direction : %d, speed : %d, speed_z : %d}" % (self.craft.wind_direction, randint(1, 5), randint(0, 1)))

                for layer in self.tmx.layers:
                    settings.logger.info('Processing: %s' % layer.name)

                    if layer.name == 'Tile Layer 1':
                        # self.map_tiles = layer.tiles
                        pass
                    elif layer.name == 'Hidden Layer 1':
                        for index, tile in enumerate(layer.tiles):
                            if tile.gid > 0:

                                name = "Test"
                                description = ""
                                type = -1
                                gp_uuid = str(uuid.uuid4())
                                agent_visible = -1
                                screen_visible = -1

                                for set in self.tmx.tilesets:
                                    if set.name == 'tileset2':
                                        for t in set.tiles:
                                            if (t.id + set.firstgid) == tile.gid:
                                                for p in t.properties:
                                                    if p.name == 'name':
                                                        name = p.value
                                                    elif p.name == 'description':
                                                        description = p.value
                                                    elif p.name == 'type':
                                                        type = int(p.value)
                                                    elif p.name == 'gp_uuid':
                                                        gp_uuid = p.value
                                                    elif p.name == 'agent_visible':
                                                        agent_visible = int(p.value)
                                                    elif p.name == 'screen_visible':
                                                        screen_visible = int(p.value)
                                                    else:
                                                        pass

                                if name == 'Test':
                                    pass
                                else:
                                    settings.logger.info('Found %s (%d) at %s [Index: %d]' % (name, tile.gid,
                                                                                        self.get_coordinates(index),
                                                                                        index))

                                    if name == 'hiker':
                                        coord = self.get_coordinates(index)
                                        self.hiker_x = coord[0]
                                        self.hiker_y = coord[1]

                                    hlo = Hidden_Layer_Object(name, description, type, gp_uuid,
                                                              agent_visible, screen_visible, self.get_coordinates(index))

                                    self.hidden_layer_objects.append(hlo)
                                    settings.logger.info(hlo)
                    elif layer.name == 'Region Layer 1':
                        pass
                    else:
                        settings.logger.error('Unknown scenario map layer of %s' % layer.name)

                # settings.logger.info('Loaded %d map tiles' % len(self.map_tiles))
                # settings.logger.info('Loaded %d hidden tiles' % len(self.hidden_tiles))

                # Is this an Augmented file?
                self.augmented = False
                if '-A-' in self.scenario:
                    settings.logger.info('~~~~~ This is an augmented scenario!')
                    self.augmented = True

                    # Construct index name
                    name = parse.parse('{base}-A-{aug}', self.scenario)
                    conf = str(script_dir) + "/apl/scenarios/" + name['base'] + '-A-index.json'

                    settings.logger.info('~~~~~ Opening %s and %s' % (self.scenario, conf))

                    self.conf_data = []
                    with open(conf) as data_file:
                        self.conf_data = json.load(data_file)

                    settings.logger.info('~~~~~ Processing goals...')

                    focus = name['aug']

                    self.goal_1 = str(self.conf_data[focus]['goal_1'])
                    if len(self.goal_1) > 0:
                        self.telemetry.telereceiver("GOAL {order : 1, goal : '%s'}" % self.goal_1)

                    self.goal_2 = str(self.conf_data[focus]['goal_2'])
                    if len(self.goal_2) > 0:
                        self.telemetry.telereceiver("GOAL {order : 2, goal : '%s'}" % self.goal_2)

                    self.goal_3 = str(self.conf_data[focus]['goal_3'])
                    if len(self.goal_3) > 0:
                        self.telemetry.telereceiver("GOAL {order : 3, goal : '%s'}" % self.goal_3)

                    settings.logger.info('~~~~~ Processing mission...')

                    self.mission = str(self.conf_data[focus]['mission'])

                    settings.logger.info('~~~~~ Processing fuel_start...')

                    fuel_val = int(self.conf_data[focus]['fuel_start'])
                    if fuel_val > 0:
                        self.craft.fuel = int(fuel_val)
                        self.craft_fuel = self.craft.fuel

                    self.craft.wind_direction = int(self.conf_data[focus]['wind_direction'])
                    self.craft_wind_direction = self.craft.wind_direction

                    settings.logger.info('~~~~~ Processing start...')

                    start_obj = Hidden_Layer_Object(str(self.conf_data[focus]['start']['name']),
                                                    str(self.conf_data[focus]['start']['description']),
                                                    int(self.conf_data[focus]['start']['type']),
                                                    uuid.uuid4(),
                                                    int(self.conf_data[focus]['start']['agent_visible']),
                                                    int(self.conf_data[focus]['start']['screen_visible']),
                                                    literal_eval(self.conf_data[focus]['start']['location']))

                    self.hidden_layer_objects.append(start_obj)

                    settings.logger.info('~~~~~ Processing hiker...')

                    hiker_obj = Hidden_Layer_Object(str(self.conf_data[focus]['hiker']['name']),
                                                    str(self.conf_data[focus]['hiker']['description']),
                                                    int(self.conf_data[focus]['hiker']['type']),
                                                    uuid.uuid4(),
                                                    int(self.conf_data[focus]['hiker']['agent_visible']),
                                                    int(self.conf_data[focus]['hiker']['screen_visible']),
                                                    literal_eval(self.conf_data[focus]['hiker']['location']))

                    self.hidden_layer_objects.append(hiker_obj)

            settings.logger.info('Craft initialization...')

            '''
                Setup plane start
            '''
            settings.logger.info(settings.sim_op_state)
            settings.logger.info(self.hidden_layer_objects)
            for item in self.hidden_layer_objects:
                if item.name == 'start':
                    self.craft.x = item.x
                    self.craft.y = item.y

                    self.craft.home_x = item.x
                    self.craft.home_y = item.y

                    self.craft_x = self.craft.x
                    self.craft_y = self.craft.y
                    self.craft_home_x = self.craft.home_x
                    self.craft_home_y = self.craft.home_y

                    if hasattr(item, 'z'):  # Don't instantly die when loading into Nixel maps
                        self.craft.altitude = item.z
                        self.craft.home_z = item.z

                        self.craft_altitude = self.craft.altitude
                        self.craft_home_z = self.craft.home_z

                    # Establishing initial yaw
                    self.craft.yaw = -1
                    self.craft_yaw = self.craft.yaw
                    # Which airport am I located at ?
                    for o in self.object_layer_list:
                        if o.type == 1:
                            if o.takeoff[0] == self.craft.x and o.takeoff[1] == self.craft.y:
                                self.craft.yaw = o.orientation
                                self.craft_yaw = self.craft.yaw
                                settings.logger.info('Craft is located at %s airport.' % o.code)

                    if self.craft.yaw == -1:
                        self.craft.yaw = 3
                        self.craft_yaw = self.craft.yaw

                    settings.logger.info('Placing craft at (%d, %d) heading %d' % (self.craft.x, self.craft.y, self.craft.yaw))
                    # print(self.about_this_coordinate(item.x, item.y))

            self.telemetry.telereceiver("INITIAL_STATE {x : %d, y : %d, z : %d, h : %d, p : %d, r : %d, speed : %d, fuel : %d, armed : %d, mode : %d}" %
                                        (self.craft.x, self.craft.y, self.craft.altitude, self.craft.yaw, self.craft.pitch, self.craft.roll,
                                         self.craft.speed, self.craft.fuel, int(self.craft.armed), int(self.craft.mode)))
            # settings.logger.info(self.about_this_coordinate(self.craft.x, self.craft.y).altitude)

            settings.logger.info('Scenario initialization complete. Let\'s play a game.')
            msg = "MS_MISSION_START { name : '" + self.scenario + "', info : '" + self.note + "'}"
            self.telemetry.telereceiver(msg)

            self.craft.set_telemetry(self.telemetry)
            self.craft.sim = self

            self.telemetry.telereceiver(
                "GAME_SCORE {total_1 : %d, change_1 : %d, reason_1 : '%s', total_2 : %d, change_2 : %d, reason_2 : '%s', total_3 : %d, change_3 : %d, reason_3 : '%s'}" %
                (self.craft.total_1, 0, ' ', self.craft.total_2, 0, 'MT', self.craft.total_3, 0, 'MT',))

            self.telemetry.telereceiver("YOUR_TURN")

        ################################################################################################################
        #
        #
        #    E X T E N D E D Simulation with Craft State already restored
        #
        #
        ################################################################################################################
        else:
            settings.logger.info(" o()xxxx[{:::::::::::::::::::::::::::::> OPENING and setting up an extended scenario")
            settings.logger.info("Opening scenario file: %s" % self.scenario)

            msg = "COMMAND_ACK {command : 33330001, result : 0}"
            self.telemetry.telereceiver(msg)

            # TODO: This will eventually need to load from the repo and not from the local filesystem, but for now
            #       this will suffice

            '''
                Load important files for data extraction and for map use
            '''
            file_string = str(script_dir) + "/apl/scenarios/" + self.scenario + ".tgz"
            tar = tarfile.open(file_string)
            for member in tar.getmembers():
                # print(member)
                if 'contents.json' in member.name:
                    settings.logger.info('Loading content data')
                    f = tar.extractfile(member)
                    self.contents = f.read()
                elif 'map.tmx' in member.name:
                    settings.logger.info('Loading map data')
                    f = tar.extractfile(member)
                    self.map = f.read()
                elif 'scenario.yml' in member.name:
                    settings.logger.info('Loading scenario data')
                    f = tar.extractfile(member)
                    self.scenario_info = f.read()
                else:
                    pass
                    # print('Ignoring...')

            # TODO: For now, we are not going to process the contents OR the scenario files since they don't currently
            #       have any items of interest.

            '''
                Goofy TMX library does not handle Tiled Object Layer items correctly, so we extract here to parse them 
                sepearately. This is okay since we would have to do this anyway.
            '''
            root = ET.fromstring(self.map)
            for objs in root.findall('objectgroup'):
                self.obj_layer = objs
                root.remove(objs)

            '''
                Process the objects into an object list for checking against 
            '''
            for obj in self.obj_layer:
                type = int(obj[0][6].attrib['value'])

                # TODO: Improve this XML parsing within the tree to not hardcode positions
                # TODO: Handle other types

                if type == 1:
                    o = Obj_Layer_Object(obj.attrib['name'],
                                         float(obj.attrib['x']),
                                         float(obj.attrib['y']),
                                         float(obj.attrib['width']),
                                         float(obj.attrib['height']),
                                         obj[0][3].attrib['value'],
                                         int(obj[0][6].attrib['value']),
                                         obj[0][1].attrib['value'],
                                         obj[0][0].attrib['value'],
                                         int(obj[0][4].attrib['value']),
                                         literal_eval(obj[0][5].attrib['value']),
                                         literal_eval(obj[0][2].attrib['value']))

                    self.object_layer_list.append(o)
                else:
                    raise ValueError('Unknown Type in Object Layer in APL map', self.scenario, type)

                # print(o)

            # Write an object layered removed TMX file to drive for reading
            file = open('transient.tmx', 'w')
            file.write(ET.tostring(root, encoding='unicode', method='xml'))
            file.close()

            # TMX lib cannot read from string, so we have to get from an OS file :(
            self.tmx = tmx.TileMap.load('transient.tmx')

            '''
                Pull out scenario information from the TMX file
            '''
            settings.logger.info('Map has a height of %d tiles.' % self.tmx.height)
            self.map_length = self.tmx.height
            settings.logger.info('Map has a width of %d tiles.' % self.tmx.width)
            self.map_width = self.tmx.width

            settings.logger.info('Map has a tileheight of %d pixels.' % self.tmx.tileheight)
            settings.logger.info('Map has a tilewidth of %d pixels.' % self.tmx.tilewidth)

            for prop in self.tmx.properties:
                # settings.logger.info(prop.name)

                if prop.name == 'goal_1':
                    self.goal_1 = str(prop.value)
                    if len(self.goal_1) > 0:
                        self.telemetry.telereceiver("GOAL {order : 1, goal : '%s'}" % self.goal_1)
                elif prop.name == 'goal_2':
                    self.goal_2 = str(prop.value)
                    if len(self.goal_2) > 0:
                        self.telemetry.telereceiver("GOAL {order : 2, goal : '%s'}" % self.goal_2)
                elif prop.name == 'goal_3':
                    self.goal_3 = str(prop.value)
                    if len(self.goal_3) > 0:
                        self.telemetry.telereceiver("GOAL {order : 3, goal : '%s'}" % self.goal_3)
                elif prop.name == 'mission':
                    self.mission = prop.value
                elif prop.name == 'map':
                    self.tmx_map = prop.value
                elif prop.name == 'uuid_base':
                    self.uuid_base = prop.value
                elif prop.name == 'fuel_start':
                    self.craft.fuel = int(prop.value)
                    self.craft_fuel = self.craft.fuel
                elif prop.name == 'wind_direction':
                    self.craft.wind_direction = int(prop.value)
                    self.craft_wind_direction = self.craft.wind_direction
                else:
                    settings.logger.error('Uknown scenario map property of %s' % prop.name)

            self.telemetry.telereceiver("SCENARIO {map : '%s', description : '%s', suuid : '%s'}" % (
                                        self.tmx_map, self.mission, str(self.db_record_uuid)))
            self.buffer.exigisi_add_sim_session_uuid_to_flight_apl(self.flight_uuid, self.session)
            self.telemetry.telereceiver("WIND {direction : %d, speed : %d, speed_z : %d}" % (
                                        self.craft.wind_direction, randint(1, 5), randint(0, 1)))

            for layer in self.tmx.layers:
                settings.logger.info('Processing: %s' % layer.name)

                if layer.name == 'Tile Layer 1':
                    # self.map_tiles = layer.tiles
                    pass
                elif layer.name == 'Hidden Layer 1':
                    for index, tile in enumerate(layer.tiles):
                        if tile.gid > 0:

                            name = "Test"
                            description = ""
                            type = -1
                            gp_uuid = str(uuid.uuid4())
                            agent_visible = -1
                            screen_visible = -1

                            for set in self.tmx.tilesets:
                                if set.name == 'tileset2':
                                    for t in set.tiles:
                                        if (t.id + set.firstgid) == tile.gid:
                                            for p in t.properties:
                                                if p.name == 'name':
                                                    name = p.value
                                                elif p.name == 'description':
                                                    description = p.value
                                                elif p.name == 'type':
                                                    type = int(p.value)
                                                elif p.name == 'gp_uuid':
                                                    gp_uuid = p.value
                                                elif p.name == 'agent_visible':
                                                    agent_visible = int(p.value)
                                                elif p.name == 'screen_visible':
                                                    screen_visible = int(p.value)
                                                else:
                                                    pass

                            if name == 'Test':
                                pass
                            else:
                                settings.logger.info('Found %s (%d) at %s [Index: %d]' % (name, tile.gid,
                                                                                        self.get_coordinates(index),
                                                                                        index))

                                if name == 'hiker':
                                    coord = self.get_coordinates(index)
                                    self.hiker_x = coord[0]
                                    self.hiker_y = coord[1]

                                hlo = Hidden_Layer_Object(name, description, type, gp_uuid,
                                                        agent_visible, screen_visible, self.get_coordinates(index))

                                self.hidden_layer_objects.append(hlo)
                                settings.logger.info(hlo)
                elif layer.name == 'Region Layer 1':
                    pass
                else:
                    settings.logger.error('Unknown scenario map layer of %s' % layer.name)

            # settings.logger.info('Loaded %d map tiles' % len(self.map_tiles))
            # settings.logger.info('Loaded %d hidden tiles' % len(self.hidden_tiles))

            # Is this an Augmented file?
            self.augmented = False
            if '-A-' in self.scenario:
                settings.logger.info('~~~~~ This is an augmented scenario!')
                self.augmented = True

                # Construct index name
                name = parse.parse('{base}-A-{aug}', self.scenario)
                conf = str(script_dir) + "/apl/scenarios/" + name['base'] + '-A-index.json'

                settings.logger.info('~~~~~ Opening %s and %s' % (self.scenario, conf))

                self.conf_data = []
                with open(conf) as data_file:
                    self.conf_data = json.load(data_file)

                settings.logger.info('~~~~~ Processing goals...')

                focus = name['aug']

                self.goal_1 = str(self.conf_data[focus]['goal_1'])
                if len(self.goal_1) > 0:
                    self.telemetry.telereceiver("GOAL {order : 1, goal : '%s'}" % self.goal_1)

                self.goal_2 = str(self.conf_data[focus]['goal_2'])
                if len(self.goal_2) > 0:
                    self.telemetry.telereceiver("GOAL {order : 2, goal : '%s'}" % self.goal_2)

                self.goal_3 = str(self.conf_data[focus]['goal_3'])
                if len(self.goal_3) > 0:
                    self.telemetry.telereceiver("GOAL {order : 3, goal : '%s'}" % self.goal_3)

                settings.logger.info('~~~~~ Processing mission...')

                self.mission = str(self.conf_data[focus]['mission'])

                settings.logger.info('~~~~~ Processing fuel_start...')

                fuel_val = int(self.conf_data[focus]['fuel_start'])
                if fuel_val > 0:
                    self.craft.fuel = int(fuel_val)
                    self.craft_fuel = self.craft.fuel

                self.craft.wind_direction = int(self.conf_data[focus]['wind_direction'])
                self.craft_wind_direction = self.craft.wind_direction

                settings.logger.info('~~~~~ Processing start...')

                start_obj = Hidden_Layer_Object(str(self.conf_data[focus]['start']['name']),
                                                str(self.conf_data[focus]['start']['description']),
                                                int(self.conf_data[focus]['start']['type']),
                                                uuid.uuid4(),
                                                int(self.conf_data[focus]['start']['agent_visible']),
                                                int(self.conf_data[focus]['start']['screen_visible']),
                                                literal_eval(self.conf_data[focus]['start']['location']))

                self.hidden_layer_objects.append(start_obj)

                settings.logger.info('~~~~~ Processing hiker...')

                hiker_obj = Hidden_Layer_Object(str(self.conf_data[focus]['hiker']['name']),
                                                str(self.conf_data[focus]['hiker']['description']),
                                                int(self.conf_data[focus]['hiker']['type']),
                                                uuid.uuid4(),
                                                int(self.conf_data[focus]['hiker']['agent_visible']),
                                                int(self.conf_data[focus]['hiker']['screen_visible']),
                                                literal_eval(self.conf_data[focus]['hiker']['location']))

                self.hidden_layer_objects.append(hiker_obj)

            settings.logger.info('Craft initialization...')

            self.telemetry.telereceiver(
                "INITIAL_STATE {x : %d, y : %d, z : %d, h : %d, p : %d, r : %d, speed : %d, fuel : %d, armed : %d, mode : %d}" %
                (self.craft.x, self.craft.y, self.craft.altitude, self.craft.yaw, self.craft.pitch, self.craft.roll,
                self.craft.speed, self.craft.fuel, int(self.craft.armed), self.craft.mode))

            settings.logger.info('Scenario initialization complete. Let\'s play a game.')
            msg = "MS_MISSION_START { name : '" + self.scenario + "', info : '" + self.note + "'}"
            self.telemetry.telereceiver(msg)

            self.craft.set_telemetry(self.telemetry)
            self.craft.sim = self

            self.telemetry.telereceiver(
                "GAME_SCORE {total_1 : %d, change_1 : %d, reason_1 : '%s', total_2 : %d, change_2 : %d, reason_2 : '%s', total_3 : %d, change_3 : %d, reason_3 : '%s'}" %
                (self.craft.total_1, 0, ' ', self.craft.total_2, 0, 'MT', self.craft.total_3, 0, 'MT',))

            self.telemetry.telereceiver("YOUR_TURN")

    def reset(self, note, new_uuid):
        self.note = note
        self.flight_uuid = new_uuid

        # self.craft.reinit()

        self.craft.fuel = self.craft_fuel
        self.craft.wind_direction = self.craft_wind_direction
        self.craft.x = self.craft_x
        self.craft.y = self.craft_y
        self.craft.home_x = self.craft_home_x
        self.craft.home_y = self.craft_home_y
        self.craft.home_z = self.craft_home_z
        self.craft.altitude = self.craft_altitude
        self.craft.yaw = self.craft_yaw

        self.db_record_uuid = self.buffer.mark_start_of_APL_simulation(self.scenario, self.session, self.note)
        self.buffer.exigisi_add_sim_session_uuid_to_flight_apl(self.flight_uuid, self.session)

        msg = "COMMAND_ACK {command : 33330001, result : 0}"
        self.telemetry.telereceiver(msg)

        if len(self.goal_1) > 0:
            self.telemetry.telereceiver("GOAL {order : 1, goal : '%s'}" % self.goal_1)

        if len(self.goal_2) > 0:
            self.telemetry.telereceiver("GOAL {order : 2, goal : '%s'}" % self.goal_2)

        if len(self.goal_3) > 0:
            self.telemetry.telereceiver("GOAL {order : 3, goal : '%s'}" % self.goal_3)

        self.telemetry.telereceiver("SCENARIO {map : '%s', description : '%s', suuid : '%s'}" % (
            self.tmx_map, self.mission, str(self.db_record_uuid)))

        self.telemetry.telereceiver("WIND {direction : %d, speed : %d, speed_z : %d}" % (
            self.craft.wind_direction, randint(1, 5), randint(0, 1)))

        self.telemetry.telereceiver(
            "INITIAL_STATE {x : %d, y : %d, z : %d, h : %d, p : %d, r : %d, speed : %d, fuel : %d, armed : %d, mode : %d}" %
            (self.craft.x, self.craft.y, self.craft.altitude, self.craft.yaw, self.craft.pitch, self.craft.roll,
            self.craft.speed, self.craft.fuel, int(self.craft.armed), self.craft.mode))

        msg = "MS_MISSION_START { name : '" + self.scenario + "', info : '" + self.note + "'}"
        self.telemetry.telereceiver(msg)

        self.telemetry.telereceiver(
            "GAME_SCORE {total_1 : %d, change_1 : %d, reason_1 : '%s', total_2 : %d, change_2 : %d, reason_2 : '%s', total_3 : %d, change_3 : %d, reason_3 : '%s'}" %
            (self.craft.total_1, 0, ' ', self.craft.total_2, 0, 'MT', self.craft.total_3, 0, 'MT',))

        self.telemetry.telereceiver("YOUR_TURN")


    def release(self):
        try:
            self.buffer.mark_end_of_APL_simulation(self.db_record_uuid)

            settings.logger.info('Scenario complete. Thank you for playing.')

            msg = "COMMAND_ACK { command : 33330002, result : 0 }"
            self.telemetry.telereceiver(msg)

            msg = "MS_MISSION_END { }"
            self.telemetry.telereceiver(msg)

            # self.craft_trace_file.close()

        except:  # catch all exceptions
            e, v, t = sys.exc_info()
            settings.logger.error("APL Simulation Core deletion Error: %s - %s" % (e, str(v)))
            msg = "COMMAND_ACK {command : 33330002, result : 1}"
            self.telemetry.telereceiver(msg)

    def point_in_polygon(self, bound_x, bound_y, bound_height, bound_width, x, y, pad_x, pad_y):
        if bound_x < (x + pad_x) < (bound_x + bound_width):
            if bound_y < (y + pad_y) < (bound_y + bound_height):
                return True

        return False

    def at_airport(self):
        airport = ""

        for obj in self.object_layer_list:  # NIXEL CHANGE: use coordinates appropriate for Nixel worlds
            if self.nixel_world is not None and self.point_in_polygon(obj.x, obj.y, obj.height, obj.width, self.craft.x, self.craft.y, 0, 0):
                if obj.type == 1:
                    return obj.name
            elif self.point_in_polygon(obj.x, obj.y, obj.height, obj.width, self.craft.x * 32, self.craft.y * 32, 16, 16):
                if obj.type == 1:
                    return obj.name

        return airport

    def get_coordinates(self, index):
        row = index // (self.map_width)
        column = index - (row * self.map_width)
        return (column, row)

    def get_index(self, x, y):
        return ((y * self.map_width) + x)

    def about_this_coordinate(self, x, y):

        if x < 0 or x > (self.map_width - 1) or y < 0 or y > (self.map_length - 1):
            return None

        if self.nixel_world is not None:    # NIXEL CHANGE: Turned into a wrapper, uses about_this_coordinate from NixelLoader
            return self.nixel_world.about_this_coordinate(x, y)
        else:
            index = self.get_index(x, y)
            name = 'M-T'
            description = ''
            altitude = -1
            type = -1

            for layer in self.tmx.layers:
                if layer.name == 'Tile Layer 1':
                    gid = layer.tiles[index].gid

                    if gid > 0:

                        name = "Test"
                        description = ""
                        type = -1
                        altitude = -1

                        for set in self.tmx.tilesets:
                            if set.name == 'tileset1':
                                for t in set.tiles:
                                    if (t.id + set.firstgid) == gid:
                                        for p in t.properties:
                                            if p.name == 'name':
                                                name = p.value
                                            elif p.name == 'description':
                                                description = p.value
                                            elif p.name == 'tiletype':
                                                type = int(p.value)
                                            elif p.name == 'altitude':
                                                altitude = int(p.value)
                                            else:
                                                pass

                    return (Base_Layer_Object(name, description, altitude, type, x, y))

                elif layer.name == 'Hidden Layer 1':
                    pass
                else:
                    pass

    def region_at_this_coordinate(self, x, y):

        if x < 0 or x > (self.map_width - 1) or y < 0 or y > (self.map_length - 1):
            return None

        if self.nixel_world is not None:
            name = 'M-T'
            description = ''
            altitude = -1
            type = -1

            # NIXEL CHANGE: TODO: Nixel needs to provide this!

            return (Base_Layer_Object(name, description, altitude, type, x, y))
        else:
            index = self.get_index(x, y)

            name = 'M-T'
            description = ''
            altitude = -1
            type = -1

            for layer in self.tmx.layers:
                if layer.name == 'Region Layer 1':
                    gid = layer.tiles[index].gid

                    if gid > 0:

                        name = "Test"
                        description = ""
                        type = -1
                        altitude = -1

                        for set in self.tmx.tilesets:
                            if set.name == 'regions':
                                for t in set.tiles:
                                    if (t.id + set.firstgid) == gid:
                                        for p in t.properties:
                                            if p.name == 'name':
                                                name = p.value
                                            elif p.name == 'description':
                                                description = p.value
                                            else:
                                                pass

                    return (Base_Layer_Object(name, description, altitude, type, x, y))
                else:
                    pass

    def get_positions_around_me(self, position, range):
        positions = []

        # Outer ring
        if range > 2:
            positions.append(((position[0]-2), (position[1]+2)))
            positions.append(((position[0]-2), (position[1]+1)))
            positions.append(((position[0]-2), (position[1])))
            positions.append(((position[0]-2), (position[1]-1)))
            positions.append(((position[0]-2), (position[1]-2)))
            positions.append(((position[0]+2), (position[1]+2)))
            positions.append(((position[0]+2), (position[1]+1)))
            positions.append(((position[0]+2), (position[1])))
            positions.append(((position[0]+2), (position[1]-1)))
            positions.append(((position[0]+2), (position[1]-2)))
            positions.append(((position[0]+1), (position[1]+2)))
            positions.append(((position[0]), (position[1]+2)))
            positions.append(((position[0]-1), (position[1]+2)))
            positions.append(((position[0]+1), (position[1]-2)))
            positions.append(((position[0]), (position[1]-2)))
            positions.append(((position[0]-1), (position[1]-2)))

        # Inner ring
        if range > 1:
            positions.append(((position[0]-1), (position[1]-1)))
            positions.append(((position[0]-1), (position[1])))
            positions.append(((position[0]-1), (position[1]+1)))
            positions.append(((position[0]), (position[1]-1)))
            positions.append(((position[0]), (position[1]+1)))
            positions.append(((position[0]+1), (position[1]-1)))
            positions.append(((position[0]+1), (position[1])))
            positions.append(((position[0]+1), (position[1]+1)))

        # Over
        positions.append(position)

        # Filter out any negative positions

        final_positions = []

        for pos in positions:
            if pos[0] >= 0:
                if pos[1] >= 0:
                    final_positions.append(pos)

        return final_positions

    '''
        Sense object at a position
    '''
    def obj_at_position(self, pos):

        if pos[0] < 0 or pos[0] >= self.map_width:
            return None
        elif pos[1] < 0 or pos[1] >= self.map_length:
            return None

        for hlo in self.hidden_layer_objects:
            if hlo.x == pos[0] and hlo.y == pos[1]:
                return hlo

    '''
        Am I at an airport?
    '''
    def simple_at_airport(self):
        obj = self.about_this_coordinate(self.craft.x, self.craft.y)
        if obj.name == 'runway':
            return True
        else:
            return False

    '''
        Determine if craft has crashed into any object
    '''
    def craft_collision(self):
        # Given current position, what is the base altitude on the map?
        blc = self.about_this_coordinate(self.craft.x, self.craft.y)
        settings.logger.info("~~ Craft at (%d, %d, %d) with %d fuel, terrain is '%s' : '%s' of type %d, and altitude %d ~~" %
            (self.craft.x, self.craft.y, self.craft.altitude, self.craft.fuel, blc.name, blc.description, blc.tiletype, blc.altitude))

        if blc.altitude >= self.craft.altitude and blc.altitude > 0:
            return True
        else:
            return False

    '''
        Advance Game by a Turn
        
        Responsible for:
            MS_CRAFT_CRASHED
            MS_PAYLOAD_HIT
            MS_SENSED_OBJECT
            CURRENT_GAME_SCORE
    '''
    def take_turn(self):
        cost = 1

        # Decrement fuel
        if self.craft.armed:
            self.craft.fuel -= 1 + self.craft.speed

        if self.craft.fuel <= 0:
            self.craft.armed = False
            self.craft.fuel = 0
            settings.logger.warning("~~ Craft is out of fuel and will be falling out of the sky if airborne! ~~")

        blc = self.about_this_coordinate(self.craft.x, self.craft.y)

        # Not armed and sitting on the ground? ??
        if self.craft.armed is False and self.craft.altitude == blc.altitude and self.craft.pitch > -50:
            settings.logger.info('--- WAITING ON GROUND DISARMED...')

            if self.craft.game_mode == 1 and self.craft.found_hiker is True and self.craft.landed_after_hiker_found is False:
                settings.logger.info('--- 50 points for landing!')
                self.craft.landed_after_hiker_found = True
                self.craft.total_1 += 50

                self.telemetry.telereceiver(
                    "GAME_SCORE {total_1 : %d, change_1 : %d, reason_1 : '%s', total_2 : %d, change_2 : %d, reason_2 : '%s', total_3 : %d, change_3 : %d, reason_3 : '%s'}" %
                    (self.craft.total_1, 50, 'Landed after hiker found', self.craft.total_2, 0, 'MT', self.craft.total_3,
                     0, 'MT',))
            else:
                settings.logger.info('--- NO points for landing. :( Better luck next time. (%d, %r, %r)' % (self.craft.game_mode, self.craft.found_hiker, self.craft.landed_after_hiker_found))

        # Turn off engine in flight, then you are a lawn dart
        elif self.craft.armed is False and self.craft.altitude > 0:
            self.craft.altitude -= 1
            self.craft.pitch = -90
            self.craft.speed = 1
            self.craft.yaw = 1
            self.craft.roll = randint(1, 359)

        # LOITERING mode
        elif self.craft.mode == 4:
            # Calculate next move in circling behavior
            settings.logger.info('--- LOITERING ..--``--..--``--..--``--...')
            self.craft.speed = 1

            # Get away from world boundaries
            if self.craft.loiter_x <= 0:
                self.craft.loiter_x = 1

            if self.craft.loiter_x >= (self.map_width - 1):
                self.craft.loiter_x = self.map_width - 2

            if self.craft.loiter_y <= 0:
                self.craft.loiter_y = 1

            if self.craft.loiter_y >= (self.map_length - 1):
                self.craft.loiter_y = self.map_length - 2

            distance_calc = self.craft.euclidean_distance(self.craft.x, self.craft.y, self.craft.loiter_x, self.craft.loiter_y)

            if distance_calc > 1.5:
                settings.logger.info('Too far from loiter point, need to traverse - %f ' % distance_calc)
                self.craft.flyToPlan(self.craft.loiter_x, self.craft.loiter_y, self.craft.altitude, None)
                self.craft.mode = 2
                self.craft.speed = 1

                # Pop 1 item off top of the stack
                step = self.move_queue.pop(0)
                settings.logger.info('Step --- %s' % str(step))

                # Update the game world
                self.craft.x = step.x
                self.craft.y = step.y
                self.craft.altitude = step.altitude
                self.craft.yaw = step.yaw
                self.craft.pitch = step.pitch
                self.craft.roll = step.roll

            else:
                settings.logger.info('Close, let\'s figure out where to go next')
                settings.logger.info('LOITER Current (%d, %d, %d, %d) loiter point is (%d, %d)' %
                    (self.craft.x, self.craft.y, self.craft.altitude, self.craft.yaw, self.craft.loiter_x, self.craft.loiter_y))

                # If ON spot move off by 1
                if self.craft.x == self.craft.loiter_x and self.craft.y == self.craft.loiter_y:
                    settings.logger.info('On loiter spot, need to move off')
                    move_dict = {1: [0, -1, 1], 2: [0, -1, 1], 3: [1, 0, 3], 4: [1, 0, 3], 5: [0, 1, 5], 6: [0, 1, 5], 7: [-1, 0, 7], 8: [-1, 0, 7]}
                    move = move_dict[self.craft.yaw]
                    self.craft.x = self.craft.x + move[0]
                    self.craft.y = self.craft.y + move[1]
                    self.craft.yaw = move[2]
                    settings.logger.info('Moving to (%d, %d) heading %d' %
                                         (self.craft.x, self.craft.y, self.craft.yaw))
                else:
                    # Determine current position
                    pos_dict = {(0, -1): 1, (1, -1): 2, (1, 0): 3, (1, 1): 4, (0, 1): 5, (-1, 1): 6, (-1, 0): 7, (-1, -1): 8}
                    pos = pos_dict[(self.craft.x - self.craft.loiter_x, self.craft.y - self.craft.loiter_y)]

                    # Determine next position counter-clockwise
                    next_move_dict = {1: [1, 0, 3], 2: [0, 1, 5], 3: [0, 1, 5], 4: [-1, 0, 7], 5: [-1, 0, 7], 6: [0, -1, 1], 7: [0, -1, 1],
                                      8: [1, 0, 3]}

                    next_move = next_move_dict[pos]
                    # settings.logger.info(next_move)
                    self.craft.x = self.craft.x + next_move[0]
                    self.craft.y = self.craft.y + next_move[1]
                    self.craft.yaw = next_move[2]
                    settings.logger.info('Loiter circling from position %d to (%d, %d) heading %d ------------------o' %
                                        (pos, self.craft.x, self.craft.y, self.craft.yaw))

        # Process the move queue
        elif len(self.move_queue) > 0:

            # Pop 1 item off top of the stack
            step = self.move_queue.pop(0)
            settings.logger.info('Step --- %s' % str(step))

            # Update the game world
            self.craft.x = step.x
            self.craft.y = step.y
            self.craft.altitude = step.altitude
            self.craft.yaw = step.yaw
            self.craft.pitch = step.pitch
            self.craft.roll = step.roll

            if self.craft.mode == 2:
                self.craft.speed = step.speed
                self.craft.armed = step.armed
                self.craft.mode = step.mode

            # Clamp speed on ground to 1 at a maximum
            if self.craft.altitude == blc.altitude and self.craft.speed > 1:
                self.craft.speed = 1

            cost = step.cost

            # In AUTO mode and nothing left to do
            if self.craft.mode == 2 and len(self.move_queue) == 0:
                settings.logger.info('--------------------------- Switching to RTL mode')
                self.craft.mode = 3
                self.craft.flyToPlan(self.craft.home_x, self.craft.home_y, self.craft.altitude, None)
                cost = 10

            # In RTL mode and nothing left to do
            elif self.craft.mode == 3 and len(self.move_queue) == 0:
                settings.logger.info('-------------------------- Switching to LOITER mode after RTL')
                self.craft.loiter_x = self.craft.home_x
                self.craft.loiter_y = self.craft.home_y
                self.craft.mode = 4
                cost = 10

            # In GUIDED mode and nothing left to do plus in the air
            elif self.craft.mode == 1 and len(self.move_queue) == 0 and self.craft.altitude > 0:
                settings.logger.info('-------------------------- Switching to LOITER mode after GUIDED')
                self.craft.loiter_x = step.x
                self.craft.loiter_y = step.y
                self.craft.mode = 4
                cost = 10

            # In GUIDED mode and nothing to do plus on ground
            elif self.craft.mode == 1 and len(self.move_queue) == 0 and self.craft.altitude == 0 and self.craft.speed == 1 and self.simple_at_airport():
                settings.logger.info('-------------------------- Switching to MANUAL and DISARM mode after GUIDED and slow at airport')
                self.craft.speed = 0
                self.craft.mode = 0
                self.craft.armed = False
                cost = 10
                settings.logger.info('-------------------------- Landed at airport')

            else:
                pass

        # Process a move that is not planned
        else:
            settings.logger.info('Manual mode flying to horizon')

            # In MANUAL and GUIDED mode, if in air and have speed, then move to next square
            if self.craft.altitude > 0 and self.craft.speed > 0:

                next_move_dict = {1: [0, -1], 2: [1, -1], 3: [1, 0], 4: [1, 1], 5: [0, 1], 6: [-1, 1,], 7: [-1, 0],
                                  8: [-1, -1]}
                next_move = next_move_dict[self.craft.yaw]
                self.craft.x = self.craft.x + next_move[0]
                self.craft.y = self.craft.y + next_move[1]

        self.telemetry.telereceiver(self.craft.output_heartbeat())
        self.telemetry.telereceiver(self.craft.output_global_position_int())
        self.telemetry.telereceiver(self.craft.output_attitude())
        self.telemetry.telereceiver(self.craft.output_airspeed())
        self.telemetry.telereceiver(self.craft.output_fuel())

        # Check to see if we crashed and put out the sensor information
        #
        # -- Crash when leave area
        if (self.craft.x < 0 or self.craft.x >= self.map_width or self.craft.y < 0 or self.craft.y >= self.map_length):
            self.telemetry.telereceiver("MS_CRAFT_CRASH {cause : %s, lat : %f, lon : %f, alt : %f, speed : %f}" %
                                        ("Left operating area, craft exploded in air", self.craft.y, self.craft.x,
                                         self.craft.altitude, self.craft.speed))
            settings.logger.info("!!!!! CRASH -- Left operating area, craft exploded in air !!!!!")
            settings.craft_state = settings.CraftState.OFF
            cost = 10
        #
        # -- Crash into ground
        elif (self.craft.altitude <= 0 and self.craft.speed > 1) or (self.craft.altitude <= 0 and self.craft.pitch < -50):
            self.telemetry.telereceiver("MS_CRAFT_CRASH {cause : %s, lat : %f, lon : %f, alt : %f, speed : %f}" %
                                        ("Collided with ground", self.craft.y, self.craft.x, self.craft.altitude,
                                         self.craft.speed))
            settings.logger.info("!!!!! CRASH -- Collided with ground !!!!!")
            settings.craft_state = settings.CraftState.OFF
            cost = 10
        #
        # -- Crash into object
        elif self.craft_collision():
            obstacle = self.about_this_coordinate(self.craft.x, self.craft.y).name
            self.telemetry.telereceiver("MS_CRAFT_CRASH {cause : Collided with (%s), lat : %f, lon : %f, alt : %f, speed : %f}" %
                                        (obstacle, self.craft.y, self.craft.x, self.craft.altitude,
                                         self.craft.speed))
            settings.logger.info("!!!!! CRASH -- Collided with (%s) !!!!!" % (obstacle))
            settings.craft_state = settings.CraftState.OFF
            cost = 10
        #
        # -- Crash if on the ground and not at an airport
        elif not self.simple_at_airport() and self.craft.altitude == blc.altitude:
            self.telemetry.telereceiver("MS_CRAFT_CRASH {cause : %s, lat : %f, lon : %f, alt : %f, speed : %f}" %
                                        ("Landed off airport property", self.craft.y, self.craft.x, self.craft.altitude,
                                         self.craft.speed))
            settings.logger.info("!!!!! CRASH -- Landed off airport property !!!!!")
            settings.craft_state = settings.CraftState.OFF
            cost = 10
        else:
            pass

        # TODO: Determine if in canyon and set in_canyon variable appropriately
        #       This relies on an area to be marked as a canyon on the world map
        #
        self.craft.in_canyon = False

        # TODO: Move scoring message

        # Region
        #
        region_info = self.region_at_this_coordinate(self.craft.x, self.craft.y)
        rinfo = '%s ~ %s' % (region_info.name, region_info.description)
        # print(rinfo)
        self.telemetry.telereceiver("REGION {region : '%s', x : %d, y : %d}" % (rinfo, self.craft.x, self.craft.y))

        # Game Score
        #
        # c1 = randint(1, 100)
        # c2 = randint(1, 100)
        # c3 = randint(1, 100)
        #
        # self.total_1 += c1
        # self.total_2 += c2
        # self.total_3 += c3
        #
        # self.telemetry.telereceiver(
        #     "GAME_SCORE {total_1 : %d, change_1 : %d, reason_1 : '%s', total_2 : %d, change_2 : %d, reason_2 : '%s', total_3 : %d, change_3 : %d, reason_3 : '%s'}" %
        #     (self.total_1, c1, 'Just because...', self.total_2, c2, 'MT', self.total_3, c3, 'MT',))

        return cost

    '''
        Advance Game by a step (turn)
        
        Responsible for:
            YOUR_TURN
    '''
    def take_step(self, step_size):
        # Process the steps for this turn up to the turn cost
        steps = 0

        while steps < step_size:
            steps += self.take_turn()
            # self.craft_trace_file.write('%d, %d, %d\n' % (self.craft.x, self.craft.y, self.craft.altitude))

        # Only do sensor checks at the end of a step
        #
        # Send sensor readings
        #
        sensing = self.get_positions_around_me((self.craft.x, self.craft.y), self.craft.altitude)
        for sense in sensing:
            obj = self.obj_at_position(sense)
            if self.craft.x == self.hiker_x and self.craft.y == self.hiker_y:
                obj = Hidden_Layer_Object('hiker', 'Artificially placed hiker', 2, str(uuid.uuid4()), 2, 2, (self.hiker_x, self.hiker_y))
            if obj is not None:
                settings.logger.info("#### I see a %s" % obj.name)
                if obj.agent_visible:
                    detected = False
                    # Determine variability
                    val = randint(1, 100)
                    if self.craft.altitude == 3 and val <= 70:
                        detected = True
                    elif self.craft.altitude == 2 and val <= 84:
                        detected = True
                    elif self.craft.altitude == 1:
                        detected = True

                    if detected:
                        self.telemetry.telereceiver('MS_SENSED_OBJECT {name : %s, lat : %f, lon : %f, alt : %f, radius : %f, description : %s, uuid : %s}' %
                                            (obj.name, obj.y, obj.x, 0, 1, obj.description, obj.gp_uuid))
                        if self.craft.game_mode == 1 and self.craft.found_hiker is False:
                            if obj.name == 'hiker':
                                self.craft.found_hiker = True
                                self.craft.total_1 += 100

                                self.telemetry.telereceiver(
                                "GAME_SCORE {total_1 : %d, change_1 : %d, reason_1 : '%s', total_2 : %d, change_2 : %d, reason_2 : '%s', total_3 : %d, change_3 : %d, reason_3 : '%s'}" %
                                (self.craft.total_1, 100, 'Found lost hiker', self.craft.total_2, 0, 'MT', self.craft.total_3, 0, 'MT',))

        # State update
        move_plan = []
        for move in self.move_queue:
            move_plan.append(str(move))

        self.craft.plan = list(move_plan)

        self.buffer.record_state_change(self.craft)
        self.telemetry.telereceiver("YOUR_TURN")

    '''
        Route for craft to signal move forward
        
    '''
    def forward_simulation(self, step_size):
        settings.logger.info("Forwarding simulation %d steps" % step_size)
        self.take_step(step_size)

    def restore_move_queue(self, move_list):
        self.move_queue = []

        for item in move_list:
            move = eval(item)
            if len(item) > 3:
                self.move_queue.append(Game_Move(move[0], move[1], move[2], move[3], move[4], move[5], move[6], move[7], move[8], move[9]))


# fin
