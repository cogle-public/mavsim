#!/bin/bash
#
# Installs a local virtual environment and installs the MAVSim package
#
rm -rf VENV
virtualenv VENV
source ./VENV/bin/activate
pip3 install -r requirements.txt
pip3 install git+https://gitlab.com/COGLEProject/mavsim.git
