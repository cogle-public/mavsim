from mavsim import mavsim

def callback(message):
    print("CALLBACK MSG >> %s" % message)

ms = mavsim.MAVSim(verbose=False,
                   quiet=False,
                   nodb=False,
                   server_ip='0.0.0.0',
                   server_port=14555,
                   instance_name='MAVSim',
                   session_name='Jeff',
                   pilot_name='Sally',
                   #
                   # Use the environmental variable LOGGING_DATABASE to set this for consistency across multiple
                   # components
                   #
                   # database_url='postgresql://postgres:123456@localhost:32768/apm_missions',
                   #
                   telemetry_cb=callback,
                   nixel_gen=True,  # Set this to true if you want to generate a chosen set of scenarios (from nixel_config.json).  Will not generate scenarios already present.
                   sim_op_state = 0)

# Example command to start a scenario
# print(ms.command("('SIM','NEW','kingdom-base-A-7','1234')"))

while 1:
    pass
