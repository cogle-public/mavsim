from mavsim import mavsim

# Testing Interface


def callback(message):
    print("CALLBACK MSG >> %s" % message)

ms = mavsim.MAVSim(verbose=False, quiet=False, nodb=False, server_ip='0.0.0.0', server_port=14555, instance_name='MAVSim',
                   session_name='Jeff', pilot_name='Sally', database_url='postgresql://postgres:123456@localhost:32768/apm_missions', telemetry_cb=callback)

print(ms.command("('SIM','NEW','kingdom-base-A-7','1234')"))

while 1:
    pass
