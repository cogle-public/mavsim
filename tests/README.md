# mavsim: tests

Use `example_usage.py` to see how to use the MAVSim package

Use `cl_udp_client.py` to send commands to MAVSim on the default IP and ports

Look at `add_static_objects.sh` to see how to add objects to your environment for detection by the smart sensor.

 
