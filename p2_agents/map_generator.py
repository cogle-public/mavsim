import ast
from PIL import Image, ImageDraw

class MapGenerator():
    def __init__(self, map = [], width = 400, height = 400, side = 20, path=[]):
        self.CANVAS_WIDTH  = width
        self.CANVAS_HEIGHT = height
        self.SIDE_LENGTH = side

        self.world = map
        self.flight_path = path


    def hex_to_RGB(self, hex):
        ''' "#FFFFFF" -> [255,255,255] '''
        # Pass 16 to the integer function for change of base
        return [int(hex[i:i+2], 16) for i in range(1,6,2)]


    def linear_gradient(self, start_hex, finish_hex="#FFFFFF", n=307):
        ''' returns a gradient list of (n) colors between
            two hex colors. start_hex and finish_hex
            should be the full six-digit color string,
            inlcuding the number sign ("#FFFFFF") '''
        # Starting and ending colors in RGB form
        s = self.hex_to_RGB(start_hex)
        f = self.hex_to_RGB(finish_hex)
        # Initilize a list of the output colors with the starting color
        RGB_list = [s]
        # Calcuate a color at each evenly spaced value of t from 1 to n
        for t in range(1, n):
            # Interpolate RGB vector for color at the current value of t
            curr_vector = [int(s[j] + (float(t)/(n-1))*(f[j]-s[j])) for j in range(3)]
            # Add it to our list of output colors
            RGB_list.append(curr_vector)

        return RGB_list


    def _scale_coordinates(self, generator, image_width, image_height, side_length=0):
        scaled_width = int(image_width / side_length)
        scaled_height = int(image_height / side_length)

        for coords in generator(scaled_width, scaled_height):
            yield [(x * side_length, y * side_length) for (x, y) in coords]


    def generate_unit_squares(self, image_width, image_height):
        """Generate coordinates for a tiling of unit squares."""
        # Iterate over the required rows and cells.  The for loops (x, y)
        # give the coordinates of the top left-hand corner of each square:
        #
        #      (x, y) +-----+ (x + 1, y)
        #             |     |
        #             |     |
        #             |     |
        #  (x, y + 1) +-----+ (x + 1, y + 1)
        #
        for x in range(image_width):
            for y in range(image_height):
                yield [(x, y), (x + 1, y), (x + 1, y + 1), (x, y + 1)]


    def generate_squares(self, *args, **kwargs):
        """Generate coordinates for a tiling of squares."""
        return self._scale_coordinates(self.generate_unit_squares, *args, **kwargs)

    def is_odd(self, num):
        if (num % 2) == 0:
            return False
        else:
            return True

    def scale_path_points(self, path):

        x_unit = self.CANVAS_WIDTH // self.SIDE_LENGTH
        x_offset = self.SIDE_LENGTH // 2

        y_unit = self.CANVAS_HEIGHT // self.SIDE_LENGTH
        y_offset = self.SIDE_LENGTH // 2

        new_path = []

        for x,y,z,h in path:
            new_x = x * x_unit + x_offset
            new_y = y * y_unit + y_offset

            new_path.append((new_x, new_y, z, h))

        return new_path

    def draw_tiling(self, coord_generator, filename, world, path=None, output_file=False, return_bytes=False, special=None):
        """
        Given a coordinate generator and a filename, render those coordinates
        in a new image and save them to the file.

        Color each tile as appropriate with the world type for that tile.
        """
        # print("----- Found a special: " + str(special))

        im = Image.new('RGBA', size=(self.CANVAS_WIDTH, self.CANVAS_HEIGHT))
        column = 0
        row = 0

        for shape in coord_generator(self.CANVAS_WIDTH, self.CANVAS_HEIGHT, self.SIDE_LENGTH):
            #if (is_odd(row) and is_odd(column)) or (not is_odd(row) and not is_odd(column)):
            #    fill_color = (255,255,255,255)
            #else:
            #    fill_color = (255, 161, 0, 255)

            color_dict = {
                1 : (0, 100, 14, 255),        # pine tree - dark green
                2 : (121, 151, 0, 255),       # grass - light green
                3 : (0, 172, 23, 255),        # pine trees - bright green
                4 : (95, 98, 57, 255),        # bush - green grey
                5 : (145, 116, 0, 255),       # trail - olive greenish brown
                6 : (95, 98, 57, 255),        # shorebank - green grey
                7 : (95, 98, 57, 255),        # bushes - green grey
                8 : (95, 98, 57, 255),        # white jeep - green grey
                9 : (95, 98, 57, 255),        # unstriped road - green grey
                10 : (95, 98, 57, 255),       # striped road - green grey
                11 : (95, 98, 57, 255),       # blue jeep - green grey
                12 : (95, 98, 57, 255),       # runway - green grey
                13 : (160, 160, 160, 255),    # flight tower - grey
                14 : (95, 98, 57, 255),       # flight tower - green grey
                15 : (0, 34, 255, 255),       # water - blue
                16 : (95, 98, 57, 255),       # family tent - green grey
                17 : (160, 160, 160, 255),    # firewatch tower - grey
                18 : (95, 98, 57, 255),       # firewatch tower - green grey
                19 : (0, 100, 14, 255),       # large hill - dark green (pine tree)
                20 : (95, 98, 57, 255),       # large hill - green grey
                21 : (95, 98, 57, 255),       # solo tent - green grey
                22 : (95, 98, 57, 255),       # mountain ridge - green grey ??
                23 : (95, 98, 57, 255),       # inactive campfire ring - green grey
                24 : (0, 100, 14, 255),       # mountain ridge - dark green (pine tree) (1)
                25 : (160, 160, 160, 255),    # mountain ridge - grey (2)
                26 : (0, 0, 0, 255),          # mountain ridge - black (3)
                27 : (95, 98, 57, 255),       # box canyon - green grey
                28 : (160, 160, 160, 255),    # box canyon - grey
                29 : (0, 0, 0, 255),          # box canyon - black
                30 : (0, 100, 14, 255),       # box canyon - dark green (pine tree)
                31 : (0, 0, 0, 255),          # mountain ridge - black (4)
                32 : (0, 100, 14, 255),       # small hill - dark green (pine tree)
                33 : (255, 161, 0, 255),      # active campfire ring - orange
                34 : (95, 98, 57, 255)        # cabin - green grey
                }

            # print("Column:" + str(column) + ", Row: " + str(row) + " -- " + str(world[row][column]))
            fill_color = color_dict[world[row][column]]

            if special is not None and (row, column) in special:
                # print("----- Found a special: " + str(special))
                fill_color = special[(row, column)]

            ImageDraw.Draw(im).polygon(shape, fill=fill_color)

            # print("Column:" + str(column) + ", Row: " + str(row) + " -- " + str(world[row][column]))

            row += 1
            if row >= 20:
                column += 1
                row = 0

        # Draw a line for the path
        #path = [(0,0), (1,1), (2,2), (3,3), (4,4), (5,5), (5,6), (5,7), (5,8)]

        if path is not None and len(path) > 1:
            new_path = self.scale_path_points(path)

            #start = '#660000'
            #end = '#7F1AE5'

            #start = '#630000'
            #end = '#FFC6A5'

            start = '#FF0000'
            end = '#FFFFFF'

            #start = '#390031'
            #end = '#DEBDDE'
            color_cycle = self.linear_gradient(start, end, n=len(new_path))

            # ImageDraw.Draw(im).line(xy=new_path, fill=color_cycle, width=2)

            seq = 1
            path_length = len(new_path)

            for start in new_path:
                segment = []
                segment.append((start[0],start[1]))
                end = new_path[seq]
                segment.append((end[0], end[1]))
                color = (color_cycle[seq][0], color_cycle[seq][1], color_cycle[seq][2], 255)

                seg_alt = start[2]
                if seg_alt == 0:
                    height = 1
                elif seg_alt == 1:
                    height = 2
                elif seg_alt == 2:
                    height = 6
                elif seg_alt == 3:
                    height = 10
                else:
                    height = 0

                #print(str(segment) + " - " + str(color))
                ImageDraw.Draw(im).line(xy=segment, fill=color, width=height)

                seq += 1
                if seq == path_length:
                    break

        #print(len(im.tobytes()))

        if output_file:
            im.save(filename)

        if return_bytes:
            return im.tobytes()
        else:
            return None

    def create_file(self, filename='squares.png', path=None, special=None):
        self.draw_tiling(self.generate_squares, filename, world=self.world, path=path, output_file=True, special=special)

    def get_bytes(self, path=None, special=None):
        return self.draw_tiling(self.generate_squares, filename='no.png', world=self.world, path=path, output_file=False, return_bytes=True, special=special)

    def create_file_and_get_bytes(self, filename='squares.png', path=None, special=None):
        return self.draw_tiling(self.generate_squares, filename, world=self.world, path=path, output_file=True, return_bytes=True, special=special)



# if __name__ == '__main__':
#     #world = "[[ 1.  2.  2.  1.  1.  1.  2.  2.  2.  1. 15. 15. 15. 15. 15. 15. 15.  1. 1.  1.],[ 1.  1.  1.  1.  1.  1.  2.  2.  2.  2.  2. 15. 15. 15. 15. 15. 15.  1. 1.  1.],[ 1.  1.  1.  2.  1.  2.  2.  2.  2.  1. 15. 15. 15. 15. 15. 15. 15.  1. 1.  1.],[ 1.  1.  1.  1.  1.  2.  2.  2.  2.  2. 15. 15. 15. 15. 15. 15. 15.  1. 1.  1.],[ 1.  1.  2.  1.  1.  2.  2.  2.  2. 15. 15. 15. 15. 15. 15. 15. 15.  1. 1.  2.],[ 1.  1.  1.  1.  1.  2.  1.  1. 15. 15. 15. 15. 15. 15. 15.  2.  1.  2. 1.  2.],[ 1.  1.  1.  1.  1.  1.  2.  2. 15. 15. 15. 15. 15. 15. 15.  1.  1.  2. 2.  1.],[ 1.  1.  2.  2.  1.  1.  1.  1. 15. 15. 15. 15. 15. 15. 15.  1.  2.  1. 1.  1.],[ 1.  2.  1.  1.  1.  1.  2. 15. 15. 15. 15. 15. 15. 15.  1.  1.  1.  2. 1.  1.],[ 2.  2.  1.  1.  1.  1.  1.  1. 15. 15. 15. 15. 15.  2.  1.  1.  2.  2. 1.  2.],[ 2.  2.  1.  1.  1.  1.  2. 15. 15. 15. 15. 15. 15.  2.  1.  1.  1.  2. 1.  1.],[ 1.  2.  1.  1.  1.  1.  2. 15. 15. 15. 15. 15. 15.  1.  2.  2.  1.  2. 1.  1.],[ 2.  2.  1.  1.  2.  2.  2. 15. 15. 15. 15. 15.  1.  1.  1.  2.  2.  2. 1.  1.],[ 1.  1.  2.  1.  1.  1.  1. 15. 15. 15. 15.  1.  2.  1.  1.  2.  1.  1. 1.  2.],[ 2.  1.  2.  1.  1.  1. 15. 15. 15. 15. 15.  1.  2.  1.  1.  2.  1.  1. 1.  2.],[ 1.  1.  1.  2.  2.  1. 15. 15. 15. 15. 15.  1.  1.  2.  2.  2.  1.  2. 2.  2.],[ 1.  1.  1.  1.  2.  1.  1. 15. 15. 15. 15.  2.  2.  2.  1.  1.  1.  1. 2.  1.],[ 1.  1. 10. 10. 10. 10. 10. 10. 10. 15.  1. 10. 10. 10. 10.  2.  1. 10. 10. 10.],[ 1.  1. 10. 17.  1.  1.  1. 15. 15. 10. 10.  2.  2.  2.  2. 10. 10.  1. 2.  1.],[ 1.  2. 10.  2.  1.  1. 15. 15. 15.  2.  1.  2.  2.  2.  2.  1.  2.  2. 2.  2.]]"
#     world = '[[1.0, 2.0, 2.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 1.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 1.0, 1.0, 1.0], [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 1.0, 1.0, 1.0], [1.0, 1.0, 1.0, 2.0, 1.0, 2.0, 2.0, 2.0, 2.0, 1.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 1.0, 1.0, 1.0], [1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 1.0, 1.0, 1.0], [1.0, 1.0, 2.0, 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 1.0, 1.0, 2.0], [1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 2.0, 1.0, 2.0, 1.0, 2.0], [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 1.0, 1.0, 2.0, 2.0, 1.0], [1.0, 1.0, 2.0, 2.0, 1.0, 1.0, 1.0, 1.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 1.0, 2.0, 1.0, 1.0, 1.0], [1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 2.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0], [2.0, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 15.0, 15.0, 15.0, 15.0, 15.0, 2.0, 1.0, 1.0, 2.0, 2.0, 1.0, 2.0], [2.0, 2.0, 1.0, 1.0, 1.0, 1.0, 2.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 2.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0], [1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 2.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 1.0, 2.0, 2.0, 1.0, 2.0, 1.0, 1.0], [2.0, 2.0, 1.0, 1.0, 2.0, 2.0, 2.0, 15.0, 15.0, 15.0, 15.0, 15.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 1.0, 1.0], [1.0, 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 15.0, 15.0, 15.0, 15.0, 1.0, 2.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0, 2.0], [2.0, 1.0, 2.0, 1.0, 1.0, 1.0, 15.0, 15.0, 15.0, 15.0, 15.0, 1.0, 2.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0, 2.0], [1.0, 1.0, 1.0, 2.0, 2.0, 1.0, 15.0, 15.0, 15.0, 15.0, 15.0, 1.0, 1.0, 2.0, 2.0, 2.0, 1.0, 2.0, 2.0, 2.0], [1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 15.0, 15.0, 15.0, 15.0, 2.0, 2.0, 2.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0], [1.0, 1.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 15.0, 1.0, 10.0, 10.0, 10.0, 10.0, 2.0, 1.0, 10.0, 10.0, 10.0], [1.0, 1.0, 10.0, 17.0, 1.0, 1.0, 1.0, 15.0, 15.0, 10.0, 10.0, 2.0, 2.0, 2.0, 2.0, 10.0, 10.0, 1.0, 2.0, 1.0], [1.0, 2.0, 10.0, 2.0, 1.0, 1.0, 15.0, 15.0, 15.0, 2.0, 1.0, 2.0, 2.0, 2.0, 2.0, 1.0, 2.0, 2.0, 2.0, 2.0]]'
#     new_world = world.replace(".0", "")
#     woorld = ast.literal_eval(new_world)
#
#     m = MapGenerator(map=woorld)
#     m.create_file('squares.png')
#
#     #draw_tiling(generate_squares, filename='squares.png', world=woorld)

