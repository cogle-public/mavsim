import sys
import os
import ast
import logging
import random
import time
import math

from mavsim import mavsim
from map_generator import MapGenerator

# ------------------------------------------------------------------------------------------------------ Support Classes

class CraftState():
    def __init__(self, x, y, z, h, armed, speed):
        self.x = x
        self.y = y
        self.z = z
        self.h = h
        self.last_h = h
        self.armed = armed
        self.speed = speed
        self.alive = True
        self.mode = 0   # 0 = NONE, 1 = MANUAL, 2 = GUIDED, 3 = AUTO, 4 = LOITER, 5 = RTL
        self.drops = 0


# ------------------------------------------------------------------------------------------------- File Scope Variables
logger = logging.getLogger(__name__)
hdlr = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(asctime)s %(levelname)s AGENT %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

AOI = (325, 85, 20, 20)
hiker_pos = (335, 90)
start_pos = (326, 90, 2)

# AOI = (425, 185, 20, 20)
# hiker_pos = (435, 203)

craft_state = CraftState(0,0,0,3,0,0)
craft_path = []
turn = 0

NUM_OF_EPOCHS = 1
epoch = 0

val = ""
map = ""
map_gen = None
map_features = {}

start_time = time.process_time()

NUM_OF_TURNS_EACH_EPOCH = 100

os.system('mkdir mission ; rm mission/*-*.png')

elevation_map = [[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                 [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]]


# TODO: Specials handled as a filescope
# TODO: Add bombing release and explosion
# TODO: Add in valid world check

# --------------------------------------------------------------------------------------------------- Support Operations
def callback(message):
    '''
        Callbackto capture and process all MAVSim command messages
    :param message:
    :return:
    '''
    logger.debug("CALLBACK MSG << %s" % message)

    # INITIAL_STATE {x : 344, y : 267, z : 0, h : 3, p : 0, r : 0, speed : 0, fuel : 0, armed : 0, mode : 0}
    if 'INITIAL_STATE' in str(message):
        payload = str(message).replace("INITIAL_STATE ", "")
        quote_list = ['x', 'y', 'z', 'h', 'p', 'r', 'speed', 'armed', 'fuel', 'mode']
        for item in quote_list:
            source = item + ' :'
            sink = "'" + item + "':"
            payload = payload.replace(source, sink)

        payload_dict = ast.literal_eval(payload)
        craft_state.x = payload_dict['x']
        craft_state.y = payload_dict['y']
        craft_state.z = payload_dict['z']
        craft_state.h = payload_dict['h']
        craft_state.armed = payload_dict['armed']
        craft_state.speed = payload_dict['speed']

        add_to_flight_path(craft_state.x, craft_state.y, craft_state.z, craft_state.h)
    # GLOBAL_POSITION_INT {time_boot_ms: 695479, lat: 302330000, lon: -1101560000, alt: 0, relative_alt: 0, vx: 344, vy: 267, vz: 0, hdg: 3}
    elif 'GLOBAL_POSITION_INT' in str(message):
        payload = str(message).replace("GLOBAL_POSITION_INT ", "")
        quote_list = ['time_boot_ms', 'lat', 'lon', 'relative_alt', ' alt', 'vx', 'vy', 'vz', 'hdg']
        for item in quote_list:
            source = item + ' :'
            sink = "'" + item + "':"
            payload = payload.replace(source, sink)

        payload_dict = ast.literal_eval(payload)
        craft_state.x = payload_dict['vx']
        craft_state.y = payload_dict['vy']
        craft_state.z = payload_dict['vz']
        craft_state.last_h = craft_state.h
        craft_state.h = payload_dict['hdg']

        if angle_delta(craft_state.h, craft_state.last_h) > 1:
            logger.error('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! > 45 degree angle !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

        add_to_flight_path(craft_state.x, craft_state.y, craft_state.z, craft_state.h)
    # HEARTBEAT {type : 1, autopilot : 3333, base_mode : 217, custom_mode : 12, system_status : 4, mavlink_version : 3}
    elif 'HEARTBEAT' in str(message):
        payload = str(message).replace("HEARTBEAT ", "")
        quote_list = ['type', 'autopilot', 'base_mode', 'custom_mode', 'system_status', 'mavlink_version']
        for item in quote_list:
            source = item + ' :'
            sink = "'" + item + "':"
            payload = payload.replace(source, sink)

        payload_dict = ast.literal_eval(payload)

        base = int(payload_dict['base_mode'])
        if base > 128:
            craft_state.armed = 1
            base -= 128
        else:
            craft_state.armed = 0

        custom = int(payload_dict['custom_mode'])

        # 0 = NONE, 1 = MANUAL, 2 = GUIDED, 3 = AUTO, 4 = LOITER, 5 = RTL
        if base == 81 and custom == 0:
            craft_state.mode = 1
            #print("Changed MODE to MANUAL")
        elif base == 89 and custom == 15:
            craft_state.mode = 2
            #print("Changed MODE to GUIDED")
        elif base == 89 and custom == 10:
            craft_state.mode = 3
            #print("Changed MODE to AUTO")
        elif base == 89 and custom == 12:
            craft_state.mode = 4
            #print("Changed MODE to LOITER")
        elif base == 89 and custom == 11:
            craft_state.mode = 5
            #print("Changed MODE to RTL")
        else:
            pass

    # MS_CRAFT_CRASH {cause : %s, lat : %f, lon : %f, alt : %f, speed : %f}
    elif 'MS_CRAFT_CRASH' in str(message):
        #print(message)
        #print("Craft at (%d, %d, %d) - local (%d, %d, %d)" % (craft_state.x, craft_state.y, craft_state.z, craft_state.x - AOI[0], craft_state.y - AOI[1], craft_state.z))
        payload = str(message).replace("MS_CRAFT_CRASH ", "")
        quote_list = ['cause', 'lat', 'lon', 'alt', 'speed']
        for item in quote_list:
            source = item + ' :'
            sink = "'" + item + "':"
            payload = payload.replace(source, sink)

        # payload_dict = ast.literal_eval(payload)
        # add_to_flight_path(craft_state.x, craft_state.y, craft_state.z, craft_state.h)
    else:
        pass

def add_to_flight_path(x, y, z, h):
    mx = x - AOI[0]
    my = y - AOI[1]
    craft_path.append((mx, my, z, h))
    logger.debug(str(craft_path))
    #logger.warning("Craft at %d, %d, %d" % (x, y, z))

    if craft_state.armed == 1:
        global val, map, map_gen
        if map_gen is None:
            val = ast.literal_eval(command("('FLIGHT','MS_QUERY_SLICE', %d, %d, %d, %d)" % (AOI[0], AOI[1], AOI[2], AOI[3])))
            map = val[2].replace(".0", "")
            map_gen = MapGenerator(map=ast.literal_eval(map))
        filename = "mission/%d-%d.png" % (epoch, turn)
        map_gen.create_file(filename=filename, path=craft_path, special=map_features)
        #print("Turn -- %d -- (%d, %d, %d) ~ (%d, %d, %d) - %d" % (turn, craft_state.x, craft_state.y, craft_state.z, craft_state.x - AOI[0], craft_state.y - AOI[1], craft_state.z, craft_state.h))

def valid_config():

    # Start location good
    #if not safe_location(start_pos[0] - AOI[0], start_pos[1] - AOI[1], start_pos[2]):
    #    return False

    # Hiker location good
    #map_val = map[hiker_pos[0]][hiker_pos[1]]
    #if elevation_map[hiker_pos[0]][hiker_pos[1]] > 2 or map_val == 15:
    #    return False

    return True

def euclidean_distance(x1, y1, x2, y2):
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

def euclidean_distance_3(x1, y1, z1, x2, y2, z2):
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2 + (z1 - z2) ** 2)

def command(cmd):
    logger.debug("COMMAND SENT >> %s" % cmd)
    val = ms.command(cmd)
    logger.debug("COMMAND RECEIVED << %s" % val)

    if 'CRAFT IS OFF OR DEAD' in val:
        craft_state.alive = False
        logger.error("Craft is dead.")

    return val

def determine_surroundings(x, y, z):
    around = []

    for a in range(5):
        for b in range(5):
            around.append((x - (a - 2), y - (b - 2)))

    final = []
    for i, j in around:
        if i >= 0 or j >= 0 or i >= 511 or j >= 511:
            final.append((i, j, z))

    return final

def angle_delta(a, b):
    val = abs(a - b)
    c = val

    if val > 4:
        if val == 5:
            c = 3
        elif val == 6:
            c = 2
        else:
            c = 1

    #print("The angle between %d and %d is %d" % (a, b, c))
    return c

def safe_location(x, y, z):
    #elevation = elevation_map[x - AOI[0]][y - AOI[1]]
    unsafe = False
    candidates = determine_surroundings(x, y, z)
    for a, b, c in candidates:
        if elevation_map[a - AOI[0]][b - AOI[1]] >= c:
            unsafe = True
            break

    if unsafe:
        #print("Unsafe goal :( - (%d, %d, %d) - local (%d, %d, %d)" % (x, y, z, x - AOI[0], y - AOI[1], z))
        return False
    else:
        #print("Safe goal set for - (%d, %d, %d) - local (%d, %d, %d)" % (x, y, z, x - AOI[0], y - AOI[1], z))
        return True

# ----------------------------------------------------------------------------------------------------------------- CORE
try:
    ms = mavsim.MAVSim(verbose=False,
                       quiet=True,
                       nodb=False,
                       server_ip='0.0.0.0',
                       server_port=14555,
                       instance_name='MAVSim',
                       session_name='Jeff',
                       pilot_name='Sally',
                       #
                       # Use the environmental variable LOGGING_DATABASE to set this for consistency across multiple
                       # components
                       #
                       # database_url='postgresql://postgres:123456@localhost:32768/apm_missions',
                       telemetry_cb=callback,
                       sim_op_state = 1)

    # Scenario setup
    dna = "['COGLE_0:stubland_1:512_2:512_3:256_4:7_5:24|-0.1426885426044464/Terrain_0:0_1:100_2:0.05_3:0.5_4:0.05_5:0.5_6:0.05_7:0.5_8:0.5_9:0.5_10:0.7_11:0.3_12:0.5_13:0.5_14:True/', '0.36023542284965515/Ocean_0:60/', '-0.43587446212768555/River_0:0.01_1:100/', '-0.3501245081424713/Tree_0:500_1:20.0_2:4.0_3:0.01_4:2.0_5:0.1_6:1.9_7:3.0_8:2.2_9:3.5/', '0.6151155829429626/Airport_0:15.0_1:25_2:35_3:1000_4:[]/', '0.34627288579940796/Building_0:150_1:10.0_2:[]_3:1/', '0.31582069396972656/Road_0:3_1:500/', '-0.061891376972198486/DropPackageMission_0:1_3:Find the hiker last located at (88, 186, 41)_4:Provision the hiker with Food_5:Return and report to Southeast International Airport (SEI) airport_6:Southeast Regional Airport_7:Southeast International Airport_8:0_9:20.0_10:20.0_11:40.0/', '-0.25830233097076416/Stub_0:0.8_1:1.0_2:1.0_3:1.0_4:1.0_5:1.0/']"
    command("('SIM','NEW',\"{}\",'1234')".format(dna))
    command("('FLIGHT','MS_SET_AOI', %d, %d, %d, %d)" % (AOI[0], AOI[1], AOI[2], AOI[3]))
    #print(command("('SIM','VIZ_RECORD', 1, 0)"))

    scan_x = AOI[0]
    scan_y = AOI[1]
    walls = []

    # Capture the elevation grid from MAVSim --
    #   Used by other components to determine safe paths.
    #
    while scan_y < AOI[1] + AOI[3]:
        while scan_x < AOI[0] + AOI[2]:
            v = command("('FLIGHT', 'MS_QUERY_TERRAIN', %d, %d)" % (scan_y, scan_x))
            val = ast.literal_eval(v)
            elevation_map[scan_y - AOI[1]][scan_x - AOI[0]] = val[4]
            scan_x += 1
        scan_y += 1
        scan_x = AOI[0]

    #print(map)

    start_time = time.process_time()
    command(("('SIM', 'POSITION_HIKER', %d, %d)" % (hiker_pos[0], hiker_pos[1])))
    map_features[(hiker_pos[0] - AOI[0], hiker_pos[1] - AOI[1])] = (255, 191, 0, 255)
    command("('SIM','LOAD', 326, 90, 2, 1, 3, 999999, 'True', 1, ['Food', 'Radio', 'Food', 'Radio'], 1, 'True', 0, '[]', '[]')")

    # Path cleanup from setup
    for jj in range(3):
        craft_path.pop(0)

    plan = [(3,1,3),
            (3,1,3),
            (4,1,3),
            (5,1,3),
            (6,1,3),
            (7,1,3),
            (8,1,3),
            (1,1,3),
            (2,1,3),
            (2,1,3),
            (3,1,3),
            (8,1,3),
            (3,1,3),
            (3,1,3),
            (4,1,3),
            (4,1,3),
            (4,1,3)]

    # plan =[]
    # step_yaw = craft_state.h
    #
    # for step in range(100):
    #     step_yaw += random.randint(-1, 1)
    #
    #     if step_yaw == 0:
    #         step_yaw = 8
    #     elif step_yaw == 9:
    #         step_yaw = 1
    #
    #     plan.append((step_yaw, 1, 3))


    for dir,dist,alt in plan:
        cmd = "('FLIGHT','HEAD_TO', %d, %d, %d)" % (dir,dist,alt)
        ret = command(cmd)
        #print("Turn " + str(turn) + " -- " + ret)
        if 'ERR' not in ret:
            turn += 1

    logger.info("***********************  End of flight with %d turns. Elapsed time = %.4f" % (turn, time.process_time() - start_time))
    #print(craft_path)

except:
    e, v, t = sys.exc_info()
    logger.error("Error: %s - %s" % (e, str(v)))



