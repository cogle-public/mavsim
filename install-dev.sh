#!/bin/bash
#
# Installs a local virtual environment and installs the MAVSim package
#
activate () {
    source ./VENV/bin/activate
    pip3 install -r requirements.txt
    pip3 install git+https://gitlab.com/COGLEProject/parc-nixel

    cp -R mavsim ./VENV/lib/python3.*/site-packages/.
    # mkdir ./VENV/lib/python3.7/site-packages/mavsim/apl/nixel_scenarios
}

rm -rf VENV
virtualenv VENV
activate
